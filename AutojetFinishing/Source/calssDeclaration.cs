﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace AutojetFinishing
{
    public class poster
    {
        public bool initDone { get; set; }
        public string jdnr { get; set; }
        public string jdline { get; set; }
        public string machine { get; set; }
        public int scannedTilesCount { get; set; }  //ilość zeskanowanych poprawnie kafelków
        public int tilesCount { get; set; }         //ilość kafelków wchodzących w skład plakatu

        public int finishingId { get; set; }        //numer plakatu, jesli jest to kompletacja juz wczesniej rozpoczeta (np. B+A)
        public bool isTyped { get; set; }           //plakat "typowany", czyli taki który ma literke w opisie 
        public bool addSubcolection { get; set; }   //do skompletowania konieczne jest zeskanowanie brytu B,
        public int rowCount { get; set; }           //ilość rzędów
        public int colCount { get; set; }           //ilość kolumn
        public int jdrow { get; set; }              //w trybie workModeSingleRow: numer wiersza, 
                                                    //w trybie workModeAllPoster: 0
                                                    //w trybie workModeFinishing2: -1

        public string jobformat { get; set; }       //– format pracy z naszego systemu ERP
        public string qtyordered { get; set; }      //ilość zamówiona przez Klienta (wydrukowaną ma Pan w tabeli Printtemp)
        public string customer { get; set; }        // – nazwa Klienta
        public string campaignname { get; set; }    // – nazwa kampanii
        public string campaigndesc { get; set; }    // – opis kampanii
        public string linedesc { get; set; }        // – opis konkretnej pracy
        public int qtyPrinted { get; set; }         //ilość wydrukowana (pobierana z Printtemp)
        public int qtyClosed { get; set; }          //ilość plakatów zamkniętych (pobierana z poster_closeed_log)
        public int qtyClosedVirtual { get; set; }   //ilość plakatów zamkniętych virtualnie (pobierana z poster_closeed_log)
        public bool scanInWrongMode { get; set; }

        public List<tileType> tilesList;

        public poster()
        {
            this.initDone = false;
            this.jdnr = "";
            this.jdline = "";
            this.machine = "";
            this.scannedTilesCount = 0;
            this.tilesCount = 0;

            this.finishingId = 0;
            this.isTyped = false;
            this.addSubcolection = false;
            this.rowCount = 0;
            this.colCount = 0;
            this.jdrow = 0;

            this.jobformat = "";
            this.qtyordered = "";
            this.customer = "";
            this.campaignname = "";
            this.campaigndesc = "";
            this.linedesc = "";
            this.qtyPrinted = 0;
            this.qtyClosed = 0;
            this.qtyClosedVirtual = 0;
            this.scanInWrongMode = false;

            this.tilesList = new List<tileType>();
        }

        public poster(poster p)
        {
            this.initDone = p.initDone;
            this.jdnr = p.jdnr;
            this.jdline = p.jdline;
            this.machine = p.machine;
            this.scannedTilesCount = p.scannedTilesCount;
            this.tilesCount = p.tilesCount;

            this.finishingId = p.finishingId;
            this.isTyped = p.isTyped;
            this.addSubcolection = p.addSubcolection;
            this.rowCount = p.rowCount;
            this.colCount = p.colCount;
            this.jdrow = p.jdrow;

            this.jobformat = p.jobformat;
            this.qtyordered = p.qtyordered;
            this.customer = p.customer;
            this.campaignname = p.campaignname;
            this.campaigndesc = p.campaigndesc;
            this.linedesc = p.linedesc;
            this.qtyPrinted = p.qtyPrinted;
            this.qtyClosed = p.qtyClosed;
            this.qtyClosedVirtual = p.qtyClosedVirtual;
            this.scanInWrongMode = p.scanInWrongMode;

            this.tilesList = new List<tileType>(p.tilesList);
        }

        public class tileType
        {
            public string tiles;        //numer oznaczający pozycję brytu na plakacie (np. 0A01, 0101, 0103, itp.)
            public bool tileScanDone;   //bryt zeskanowany poprawnie
            public int rowNum;          //nr rzędu brytu na plakacie
            public int colNum;          //nr kolumny brytu na plakacie
            public bool isVirtual;      //bryt nie został zeskanowany, ale operator potwierdził, że go skompletował (np. brak kodu kreskowego na brycie)

            public tileType()
            {
                this.tiles = "";
                this.tileScanDone = false;
                this.rowNum = 0;
                this.colNum = 0;
                this.isVirtual = false;
            }

            public tileType(tileType l)
            {
                this.tiles = l.tiles;
                this.tileScanDone = l.tileScanDone;
                this.rowNum = l.rowNum;
                this.colNum = l.colNum;
                this.isVirtual = l.isVirtual;
            }
        }
    }

    /// <summary>
    /// represents data scaned from a tile, 
    /// </summary>
    public class scanItem
    {
        /*
        `id` INT NOT NULL,
        `jdnr` VARCHAR(6) NULL,
        `jdline` VARCHAR(3) NULL,
        `tiles` VARCHAR(15) NULL,
        `machine` VARCHAR(15) NULL,
        `operatorId` INT NULL,
        `operationTimeStamp` DATETIME NULL,
        `operationRollback` BIT NULL DEFAULT false COMMENT 'true =  operation was canceled',
        `posterDone` BIT NULL DEFAULT false COMMENT 'true = whole poster was scanned, operation complete',
        `stationId` INT NULL COMMENT 'id of scaning station',
        PRIMARY KEY (`id`))
        */
        public int      id                  { get; set; }
        public string   jdnr                { get; set; }
        public string   jdline              { get; set; }
        public string   tiles               { get; set; }
        public int      tilecounter         { get; set; }
        public string   machine             { get; set; }
        public int      operatorId          { get; set; }
        public bool     operationRollback   { get; set; }
        public bool     posterDone          { get; set; }
        public int      stationId           { get; set; }
        public DateTime operationTimeStamp  { get; set; }

        public int      finishingId         { get; set; }
        public bool     subcollectionTile   { get; set; }   //kafelek był już wcześniej zeskanowany, ale jest to potrzebny kafelek do kompletacji typu B+A

        public scanItem()
        {
            id = 0;
            jdnr = "";
            jdline = "";
            tiles = "";
            tilecounter = 0;
            machine = "";
            operatorId = 0;
            operationRollback = false;
            posterDone = false;
            stationId = 0;
            operationTimeStamp = new DateTime();
            subcollectionTile = false;
            finishingId = 0;
        }

        public scanItem(int _id, string _jdnr, string _jdline, string _tiles, int _tilecounter, string _machine, int _operatorId, bool _operationRollback, bool _posterDone, int _stationId, DateTime _operationTimeStamp, bool _subcollectionTile)
        {
            id = _id;
            jdnr = _jdnr;
            jdline = _jdline;
            tiles = _tiles;
            tilecounter = _tilecounter;
            machine = _machine;
            operatorId = _operatorId;
            operationRollback = _operationRollback;
            posterDone = _posterDone;
            stationId = _stationId;
            operationTimeStamp = _operationTimeStamp;
            subcollectionTile = _subcollectionTile;
            finishingId = 0;
        }

        public scanItem(string _jdnr, string _jdline, string _tiles, int _tilecounter, string _machine)
        {
            id = 0;
            jdnr = _jdnr;
            jdline = _jdline;
            tiles = _tiles;
            tilecounter = _tilecounter;
            machine = _machine;
            operatorId = 0;
            operationRollback = false;
            posterDone = false;
            stationId = 0;
            operationTimeStamp = DateTime.Now;
            subcollectionTile = false;
            finishingId = 0;
        }

    }

    public class dispatch_row
    {
        public int id { get; set; }  //ilość zeskanowanych poprawnie kafelków
        public string jdnr { get; set; }
        public string jdline { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public int qtyToPack { get; set; }         //ilość kafelków wchodzących w skład plakatu
        public int qtyPacked { get; set; }         //ilość kafelków wchodzących w skład plakatu
        public int boobooked_station_id { get; set; }

        /*  
        CREATE TABLE `tjdata`.`dispatch_table` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `jdnr` VARCHAR(6) NOT NULL,
        `jdline` VARCHAR(3) NOT NULL,
        `name` VARCHAR(35) NOT NULL,
        `address` VARCHAR(35) NOT NULL,
        `qty_to_pack` INT NOT NULL,
        `qty_packed` INT NOT NULL,
        PRIMARY KEY (`id`));
        */
    }
}
