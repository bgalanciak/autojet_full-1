﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
//using java.io;
//using TikaOnDotNet.TextExtraction;

namespace AutojetFinishing
{
    public static class dispachDataAcces
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<DispatchAddress> getAllEmptyDispatchAddresses()
        {
            List<DispatchAddress> retVal = new List<DispatchAddress>();

            string query = "Select * from dispatch_address_table where file_name is null or file_name = '' and courier_type != 'INNY' and courier_type != 'OSOBISTY'";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel != null)
            {
                foreach (DataRow dRow in dTabel.Rows)
                {
                    DispatchAddress item = new DispatchAddress();

                    /*
                    //public int id { get; set; }
                    public string reference_id { get; set; }
                    public string file_name { get; set; }
                    public DateTime created_date { get; set; }
                    public DateTime mod_date { get; set; }
                    public string courier_type { get; set; }
                    public string dispatch_name { get; set; }
                    public string dispatch_surname { get; set; }
                    public string dispatch_tel { get; set; }
                    public DateTime dispatch_date { get; set; }
                    public string street { get; set; }
                    public string city { get; set; }
                    public string post_code { get; set; }
                    public string dispatch_company { get; set; }
                    //public string description { get; set; }
                    public string list_description { get; set; }    //new
                    public string pack_type { get; set; }           //new
                    public int total_packs { get; set; }            //new
                    public int ref_from { get; set; }               //new
                    public int ref_to { get; set; }                 //new
                    public string deliv_list_no { get; set; }       //new
                     */

                    //item.id = int.Parse(dRow["id"].ToString());
                    item.reference_id = dRow["reference_id"].ToString();
                    item.file_name = dRow["file_name"].ToString();
                    item.ams_file_name = dRow["ams_file_name"].ToString();
                    item.created_date = DateTime.Parse(dRow["created_date"].ToString());
                    item.mod_date = DateTime.Parse(dRow["mod_date"].ToString());
                    item.courier_type = dRow["courier_type"].ToString();
                    item.dispatch_name = dRow["dispatch_name"].ToString();
                    item.dispatch_surname = dRow["dispatch_surname"].ToString();
                    item.dispatch_tel = dRow["dispatch_tel"].ToString();
                    item.dispatch_date = DateTime.Parse(dRow["dispatch_date"].ToString());
                    item.street = dRow["street"].ToString();
                    item.city = dRow["city"].ToString();
                    item.post_code = dRow["post_code"].ToString();
                    item.dispatch_company = dRow["dispatch_company"].ToString();
                    //item.description = dRow["description"].ToString();
                    item.list_description = dRow["list_description"].ToString();
                    item.pack_type = dRow["pack_type"].ToString();
                    item.total_packs = int.Parse(dRow["total_packs"].ToString());
                    item.ref_from = int.Parse(dRow["ref_from"].ToString());
                    item.ref_to = int.Parse(dRow["ref_to"].ToString());
                    item.deliv_list_no = dRow["deliv_list_no"].ToString();
                    item.comments = dRow["comments"].ToString();
                    retVal.Add(item);
                }
            }

            return retVal;
        }

        static public List<Files> GetFileList(string filePath)
        {
            List<Files> retVal = new List<Files>();

            DirectoryInfo d = new DirectoryInfo(filePath);//Assuming Test is your Folder
            FileInfo[] FilesArry = d.GetFiles("*.pdf"); //Getting Text files

            foreach (FileInfo file in FilesArry)
            {
                Files f = new Files();
                f.fileName = file.FullName;
                f.info = file;
                f.reference_id = "";

                retVal.Add(f);
            }

            return retVal;
        }

        //public static string openPdfFile(FileInfo file)
        //{
        //    var textExtractor = new TextExtractor();

        //    var wordDocContents = textExtractor.Extract(file.FullName);

        //    return wordDocContents.ToString();
        //}

        public static string findReferenceId(Files f)
        {
            //RID?:012345_001_100_1
            int start = f.text.IndexOf("ID?:");
            int len = 20;
            if (start > -1)
            {
                start += "ID?:".Length;
                return f.text.Substring(start, len);
            }
            else
                return null;
        }

        //====================================================================================================================================================
        public static int findRefFrom(Files f)
        {
            int retVal;
            
            if ((retVal = findRefFromDelta(f)) != 0)
            {
                return retVal;
            }
            else if ((retVal = findRefFromDPD(f)) != 0)
            {
                return retVal;
            }
            else if ((retVal = findRefFromFedEx(f)) != 0)
            {
                return retVal;
            }

            return 0;
        }

        public static int findRefFromFedEx(Files f)
        {
            if (f.text.IndexOf("FedEx Express Polska Sp. z o.o.") > -1) //jesli to list z FedEx
            {
                int start = f.text.IndexOf("Ilość paczek");
                int len = 7;
                if (start > -1)
                {
                    start += "Ilość paczek".Length + 4;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("z");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refFrom, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }

        public static int findRefFromDelta(Files f)
        {
            if (f.text.IndexOf("Delta Kurier Sp. z o. o.") > -1)    //jesli to list z Delta
            {
                int start = f.text.IndexOf("PACZKA:");
                int len = 6;
                if (start > -1)
                {
                    start += "PACZKA:".Length;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("/");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refFrom, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }

        public static int findRefFromDPD(Files f)
        {
            if (f.text.IndexOf("DPD Polska Sp. z o.o.") > -1)   //jeśli to list z DPD
            {
                int start = f.text.IndexOf("PACZKA:");
                int len = 6;
                if (start > -1)
                {
                    start += "PACZKA:".Length;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("/");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refFrom, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }
        //====================================================================================================================================================

        //====================================================================================================================================================
        public static int findRefTo(Files f)
        {
            int retVal;

            if ((retVal = findRefToDelta(f)) != 0)
            {
                return retVal;
            }
            else if ((retVal = findRefToDPD(f)) != 0)
            {
                return retVal;
            }
            else if ((retVal = findRefToFedEx(f)) != 0)
            {
                return retVal;
            }

            return 0;
        }

        public static int findRefToFedEx(Files f)
        {
            if (f.text.IndexOf("FedEx Express Polska Sp. z o.o.") > -1) //jesli to list z FedEx
            {
                int start = f.text.IndexOf("Ilość paczek");
                int len = 7;
                if (start > -1)
                {
                    start += "Ilość paczek".Length + 4;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("z");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 2, 2);

                        int res;
                        bool val = int.TryParse(refTo, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;

                }
                else return 0;
            }
            else return 0;
        }

        public static int findRefToDelta(Files f)
        {
            if (f.text.IndexOf("Delta Kurier Sp. z o. o.") > -1)    //jesli to list z Delta
            {
                int start = f.text.IndexOf("PACZKA:");
                int len = 6;
                if (start > -1)
                {
                    start += "PACZKA:".Length;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("/");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refTo, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }

        public static int findRefToDPD(Files f)
        {
            if (f.text.IndexOf("DPD Polska Sp. z o.o.") > -1)   //jeśli to list z DPD
            {
                int start = f.text.IndexOf("PACZKA:");
                int len = 6;
                if (start > -1)
                {
                    start += "PACZKA:".Length;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("/");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refTo, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }
        //====================================================================================================================================================

        //====================================================================================================================================================
        public static string findDeliveryNo(Files f)
        {
            string retVal;

            if ((retVal = findDeliveryNoDelta(f)) != null)
            {
                return retVal;
            }
            else if ((retVal = findDeliveryNoDPD(f)) != null)
            {
                return retVal;
            }
            else if ((retVal = findDeliveryNoFedEx(f)) != null)
            {
                return retVal;
            }

            return null;
        }

        public static string findDeliveryNoFedEx(Files f)
        {
            if (f.text.IndexOf("FedEx Express Polska Sp. z o.o.") > -1) //jesli to list z FedEx
            {
                int start = f.text.IndexOf("Nr listu:");
                int len = 13;
                if (start > -1)
                {
                    start += "Nr listu:".Length + 4;
                    string substr = f.text.Substring(start, len);
                    return substr;
                }
                else
                    return null;
            }
            else return null;
        }

        public static string findDeliveryNoDelta(Files f)
        {
            if (f.text.IndexOf("Delta Kurier Sp. z o. o.") > -1)    //jesli to list z Delta
            {
                int start = f.text.IndexOf("Nr list główny: ");
                int len = 13;
                if (start > -1)
                {
                    start += "Nr list główny: ".Length;
                    string substr = f.text.Substring(start, len);
                    return substr;
                }
                else
                    return null;
            }
            else return null;
        }

        public static string findDeliveryNoDPD(Files f)
        {
            if (f.text.IndexOf("DPD Polska Sp. z o.o.") > -1)   //jeśli to list z DPD
            {
                int start = f.text.IndexOf("Numer paczki");
                int len = 14;
                if (start > -1)
                {
                    start -= 20;
                    string substr = f.text.Substring(start, len);
                    return substr;                    
                }
                else
                    return null;
            }
            else return null;
        }
        //====================================================================================================================================================

        /// <summary>
        /// Return dispach address for specyfied reference_id ref_from and ref_to
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        static public DispatchAddress FindDispachAdress(Files f)
        {
            DispatchAddress address = new DispatchAddress();

            string query = $"Select * from dispatch_address_table where reference_id ='{f.reference_id}' and ref_from='{f.ref_from}' and ref_to='{f.ref_to}'";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                //address.id = int.Parse(dTabel.Rows[0]["id"].ToString());
                address.reference_id = dTabel.Rows[0]["reference_id"].ToString();
                address.file_name = dTabel.Rows[0]["file_name"].ToString();
                address.ams_file_name = dTabel.Rows[0]["ams_file_name"].ToString();
                address.created_date = DateTime.Parse(dTabel.Rows[0]["created_date"].ToString());
                address.mod_date = DateTime.Parse(dTabel.Rows[0]["mod_date"].ToString());
                address.courier_type = dTabel.Rows[0]["courier_type"].ToString();
                address.dispatch_name = dTabel.Rows[0]["dispatch_name"].ToString();
                address.dispatch_surname = dTabel.Rows[0]["dispatch_surname"].ToString();
                address.dispatch_tel = dTabel.Rows[0]["dispatch_tel"].ToString();
                address.dispatch_date = DateTime.Parse(dTabel.Rows[0]["dispatch_date"].ToString());
                address.street = dTabel.Rows[0]["street"].ToString();
                address.city = dTabel.Rows[0]["city"].ToString();
                address.post_code = dTabel.Rows[0]["post_code"].ToString();
                address.dispatch_company = dTabel.Rows[0]["dispatch_company"].ToString();
                //address.description = dTabel.Rows[0]["description"].ToString();
                address.list_description = dTabel.Rows[0]["list_description"].ToString();
                address.pack_type = dTabel.Rows[0]["pack_type"].ToString();
                address.total_packs = int.Parse(dTabel.Rows[0]["total_packs"].ToString());
                address.ref_from = int.Parse(dTabel.Rows[0]["ref_from"].ToString());
                address.ref_to = int.Parse(dTabel.Rows[0]["ref_to"].ToString());
                address.deliv_list_no = dTabel.Rows[0]["deliv_list_no"].ToString();
                address.comments = dTabel.Rows[0]["comments"].ToString();

                return address;
            }
            else
                return null;

        }

        /// <summary>
        /// Update dispach address with its filename
        /// </summary>
        /// <param name="addres"></param>
        static public void UpdateDispachAdress(DispatchAddress addres)
        {
            /*
            012345 – NR KP
            001 – LINIA KP
            100 – numer linii z rozdzielnika
            1 – unikalny numer seryjny jeżeli będą 2 rozdzielniki żeby zachować unikalność kodów

            012345_001_100_1_YYYY-MM-DD HH:MM:SS.pdf gdzie YYY-MM-DD HH:MM:SS to czas stworzenia pliku
             */
            
            string query = $"update dispatch_address_table set file_name = '{addres.file_name}' where reference_id = '{addres.reference_id}' and ref_from = {addres.ref_from} and ref_to = {addres.ref_to}";

            SQLConnection.UpdateData(query);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Returns all open dispach_item for selected jdline and jdnr 
        /// </summary>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        public static List<FinishingDispatchAddressAndItem> findAllDispachAddressAndItem(string jdline, string jdnr, int stationId)
        {
            List<FinishingDispatchAddressAndItem> retVal = new List<FinishingDispatchAddressAndItem>();

            //do 2017-08-02
            //string query = $"SELECT * FROM tjdata.dispatch_item_table left join tjdata.dispatch_address_table on dispatch_item_table.reference_id = dispatch_address_table.reference_id and dispatch_item_table.ref_from = dispatch_address_table.ref_from and dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            //query += $"where jdline = {jdline} and jdnr = {jdnr} and (booked_station_id=0 or booked_station_id={stationId}) order by priority desc, CASE LENGTH(motives) WHEN 1 THEN '0' WHEN 2 THEN '0' ELSE motives END, qty_to_pack desc, dispatch_item_table.reference_id";

            //string query = $"SELECT * FROM tjdata.dispatch_item_table left join tjdata.dispatch_address_table on dispatch_item_table.reference_id = dispatch_address_table.reference_id and dispatch_item_table.ref_from = dispatch_address_table.ref_from and dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            //query += $"where jdline = {jdline} and jdnr = {jdnr} and (booked_station_id=0 or (completation_done = 1 and stationId = {stationId})) ";
            //query += $"and CONCAT(dispatch_item_table.reference_id, dispatch_item_table.ref_from, dispatch_item_table.ref_to) not in (SELECT CONCAT(reference_id, ref_from, ref_to) FROM tjdata.dispatch_item_table  where partial_item=1 and (booked_station_id>0  or qty_packed>0 or completation_done=1) and booked_station_id!={stationId} group by reference_id, ref_from, ref_to) ";
            //query += $"order by priority desc, CASE LENGTH(motives) WHEN 1 THEN '0' WHEN 2 THEN '0' ELSE motives END, qty_to_pack desc, dispatch_item_table.reference_id";

            /*
                SELECT 
                    *
                FROM
                    tjdata.dispatch_item_table
                        LEFT JOIN
                    tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id
                        AND dispatch_item_table.ref_from = dispatch_address_table.ref_from
                        AND dispatch_item_table.ref_to = dispatch_address_table.ref_to
                WHERE
                    jdline = 2 AND jdnr = 12345
                        AND ((booked_station_id = 0 and completation_done = 0)
                        OR (booked_station_id = 1)
                        OR (completation_done = 1 AND stationId = 1))
        
                        AND CONCAT(dispatch_item_table.reference_id,
                            dispatch_item_table.ref_from,
                            dispatch_item_table.ref_to) NOT IN (SELECT 
                            CONCAT(reference_id, ref_from, ref_to)
                        FROM
                            tjdata.dispatch_item_table
                        WHERE
                            partial_item = 1
                            AND ((booked_station_id > 0) 
                                OR qty_packed > 0
                                OR completation_done = 1)
			                AND booked_station_id != 1
                            //AND stationId != 1
                        GROUP BY reference_id , ref_from , ref_to)
                ORDER BY priority DESC , CASE LENGTH(motives)
                    WHEN 1 THEN '0'
                    WHEN 2 THEN '0'
                    ELSE motives
                END , qty_to_pack DESC , dispatch_item_table.reference_id
             */

            /*
             CASE LENGTH(motives) ";
            query += $"WHEN 1 THEN '0' ";
            query += $"WHEN 2 THEN '0' ";
            query += $"ELSE motives ";
             */

            string query = $"SELECT *, ";
            query += $"CASE ";
            query += $"WHEN dispatch_item_table.tile=0 then 'PLAKAT' ";
            query += $"ELSE dispatch_item_table.tilename ";
            query += $"END as dispach_tiles ";
            query += $"FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"LEFT JOIN ";
            query += $"tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id ";
            query += $"AND dispatch_item_table.ref_from = dispatch_address_table.ref_from ";
            query += $"AND dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            query += $"WHERE ";
            query += $"jdline = {jdline} AND jdnr = {jdnr} ";
            query += $"AND ((booked_station_id = 0 and completation_done = 0) ";
            query += $"OR (booked_station_id = {stationId}) ";
            query += $"OR (completation_done = 1 AND stationId = {stationId})) ";
            query += $"AND (CONCAT(dispatch_item_table.reference_id, ";
            query += $"dispatch_item_table.ref_from, ";
            query += $"dispatch_item_table.ref_to) NOT IN (SELECT ";
            query += $"CONCAT(reference_id, ref_from, ref_to) ";

            query += $"FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"WHERE ";
            query += $"partial_item = 1 ";
            query += $"AND((booked_station_id > 0 AND booked_station_id != {stationId}) ";
            query += $"OR completation_done = 1 ) ";

            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                query += $"AND pack_type like 'ROLKA%' ";
            }
            else
            {
                query += $"AND pack_type not like 'ROLKA%' ";
            }
            if ((global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))    //mail p. Łukasza z 2018.03.13
            {
                query += $"AND pack_weight = 1 ";
            }
            else
            {
                query += $"AND pack_weight = 0 ";               //tel. p. Łukasza 2018-04-11
            }
            query += $"GROUP BY reference_id, ref_from, ref_to) or stationId = {stationId}) ";
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                query += $"AND pack_type like 'ROLKA%' ";
            }
            else
            {
                query += $"AND pack_type not like 'ROLKA%' ";
            }
            if ((global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))    //mail p. Łukasza z 2018.03.13
            {
                query += $"AND pack_weight = 1 ";
            }
            else
            {
                query += $"AND pack_weight = 0 ";               //tel. p. Łukasza 2018-04-11
            }
            query += $"ORDER BY ";
            query += $"CASE WHEN pack_date is not null THEN pack_date ELSE '2100-01-01' END, ";
            query += $"priority DESC, priority2 DESC, CASE LENGTH(motives) ";
            query += $"WHEN 1 THEN '0' ";
            query += $"WHEN 2 THEN '0' ";
            query += $"ELSE motives ";
            query += $"END , qty_to_pack DESC, dispatch_item_table.reference_id ";
            
            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                foreach (DataRow dRow in dTabel.Rows)
                {                    
                    FinishingDispatchAddressAndItem retitem = new FinishingDispatchAddressAndItem();
                    retitem = DataRowToFinishingDispatchAddressAndItem(dRow);
                    retVal.Add(retitem);
                }
            }

            Program.autojetFinishingMain.Hm.Info($"findAllDispachAddressAndItem(), retVal.Count() = {retVal.Count()}");
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        public static FinishingDispatchAddressAndItem findNextDispachAddressAndItem(string jdline, string jdnr, int stationId)
        {
            FinishingDispatchAddressAndItem retitem = new FinishingDispatchAddressAndItem();

            //do 2017-08-02
            //string query = $"SELECT * FROM tjdata.dispatch_item_table left join tjdata.dispatch_address_table on dispatch_item_table.reference_id = dispatch_address_table.reference_id and dispatch_item_table.ref_from = dispatch_address_table.ref_from and dispatch_item_table.ref_to = dispatch_address_table.ref_to where jdline = {jdline} and jdnr = {jdnr} and completation_done=false and (booked_station_id=0 or booked_station_id={stationId}) order by priority desc, CASE LENGTH(motives) WHEN 1 THEN '0' WHEN 2 THEN '0' ELSE motives END, qty_to_pack desc, dispatch_item_table.reference_id";

            //string query = $"SELECT * FROM tjdata.dispatch_item_table left join tjdata.dispatch_address_table on dispatch_item_table.reference_id = dispatch_address_table.reference_id and dispatch_item_table.ref_from = dispatch_address_table.ref_from and dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            //query += $"where jdline = {jdline} and jdnr = {jdnr} and completation_done=false and (booked_station_id=0 or (completation_done = 1 and stationId = {stationId})) ";
            //query += $"and CONCAT(dispatch_item_table.reference_id, dispatch_item_table.ref_from, dispatch_item_table.ref_to) not in (SELECT CONCAT(reference_id, ref_from, ref_to) FROM tjdata.dispatch_item_table  where partial_item=1 and (booked_station_id>0  or qty_packed>0 or completation_done=1) and booked_station_id!={stationId} group by reference_id, ref_from, ref_to) ";
            //query += $"order by priority desc, CASE LENGTH(motives) WHEN 1 THEN '0' WHEN 2 THEN '0' ELSE motives END, qty_to_pack desc, dispatch_item_table.reference_id";

            /*
            SELECT 
	            *
            FROM
	            tjdata.dispatch_item_table
		            LEFT JOIN
	            tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id
		            AND dispatch_item_table.ref_from = dispatch_address_table.ref_from
		            AND dispatch_item_table.ref_to = dispatch_address_table.ref_to
            WHERE
	            jdline = 2 AND jdnr = 12345
		            AND completation_done = 0
                    AND ((booked_station_id = 0)
			            OR (booked_station_id = 1))
		            AND CONCAT(dispatch_item_table.reference_id,
			            dispatch_item_table.ref_from,
			            dispatch_item_table.ref_to) NOT IN (SELECT 
			            CONCAT(reference_id, ref_from, ref_to)
		            FROM
			            tjdata.dispatch_item_table
		            WHERE
			            partial_item = 1
			            AND ((booked_station_id > 0) 
				            OR qty_packed > 0
				            OR completation_done = 1)
			            AND booked_station_id != 1
			            AND stationId != 1
		            GROUP BY reference_id , ref_from , ref_to)
            ORDER BY priority DESC , CASE LENGTH(motives)
	            WHEN 1 THEN '0'
	            WHEN 2 THEN '0'
	            ELSE motives
            END , qty_to_pack DESC , dispatch_item_table.reference_id
             */
            
            string query = $"SELECT *, ";   //TOP 1 
            query += $"CASE ";
            query += $"WHEN dispatch_item_table.tile=0 then 'PLAKAT' ";
            query += $"ELSE dispatch_item_table.tilename ";
            query += $"END as dispach_tiles ";
            query += $"FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"LEFT JOIN ";
            query += $"tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id ";
            query += $"AND dispatch_item_table.ref_from = dispatch_address_table.ref_from ";
            query += $"AND dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            query += $"WHERE ";
            query += $"jdline = {jdline} AND jdnr = {jdnr} ";
            query += $"AND (completation_done = 0) ";
            query += $"AND ((booked_station_id = 0) ";
            query += $"OR (booked_station_id = {stationId})) ";
            query += $"AND (CONCAT(dispatch_item_table.reference_id, ";
            query += $"dispatch_item_table.ref_from, ";
            query += $"dispatch_item_table.ref_to) NOT IN (SELECT ";
            query += $"CONCAT(reference_id, ref_from, ref_to) ";
            query += $"FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"WHERE ";
            query += $"partial_item = 1 ";
            query += $"AND ((booked_station_id > 0 AND booked_station_id != {stationId}) ";
            query += $"OR completation_done = 1 ) ";
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                query += $"AND pack_type like 'ROLKA%' ";
            }
            else
            {
                query += $"AND pack_type not like 'ROLKA%' ";
            }
            if ((global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))    //mail p. Łukasza z 2018.03.13
            {
                query += $"AND pack_weight = 1 ";
            }
            else
            {
                query += $"AND pack_weight = 0 ";               //tel. p. Łukasza 2018-04-11
            }
            query += $"GROUP BY reference_id, ref_from, ref_to) OR stationId = {stationId}) ";
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                query += $"AND pack_type like 'ROLKA%' ";
            }
            else
            {
                query += $"AND pack_type not like 'ROLKA%' ";
            }
            if (global.dispachMode == global.dispModeStroer)    //mail p. Łukasza z 2018.03.13
            {
                query += $"AND pack_weight = 1 ";
            }
            else
            {
                query += $"AND pack_weight = 0 ";               //tel. p. Łukasza 2018-04-11
            }
            query += $"ORDER BY ";
            query += $"CASE WHEN pack_date is not null THEN pack_date ELSE '2100-01-01' END, ";
            query += $"priority DESC, priority2 DESC, CASE LENGTH(motives) ";
            query += $"WHEN 1 THEN '0' ";
            query += $"WHEN 2 THEN '0' ";
            query += $"ELSE motives ";
            query += $"END , qty_to_pack DESC, dispatch_item_table.reference_id LIMIT 1";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                DataRow dRow = dTabel.Rows[0];

                retitem = DataRowToFinishingDispatchAddressAndItem(dRow);
            }

            Program.autojetFinishingMain.Hm.Info($"findNextDispachAddressAndItem(), retitem.item = {retitem.item}");
            return retitem;
        }

        /// <summary>
        /// Gets all subpackage for ZPL except those booked on other station 
        /// </summary>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        public static List<FinishingDispatchAddressAndItem> findAllDispachAddressAndItemForZPL(global.zplDataType zpl, int stationId)
        {
            List<FinishingDispatchAddressAndItem> retVal = new List<FinishingDispatchAddressAndItem>();

            string query = $"SELECT *, ";
            //if (Program.Settings.Data.stationType == stationTypeRolka)
            //{
                query += $"CASE ";
                query += $"WHEN dispatch_item_table.tile=0 then 'PLAKAT' ";
                query += $"ELSE dispatch_item_table.tilename ";
                query += $"END as dispach_tiles ";
            //}
            query += $"FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"LEFT JOIN ";
            query += $"tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id ";
            query += $"AND dispatch_item_table.ref_from = dispatch_address_table.ref_from ";
            query += $"AND dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            query += $"WHERE ";
            query += $"dispatch_item_table.reference_id = '{zpl.referenceID}' ";
            query += $"AND dispatch_item_table.ref_from = {zpl.ref_from} ";
            query += $"AND dispatch_item_table.ref_to = {zpl.ref_to} ";
            query += $"and(booked_station_id = 0 or booked_station_id ={stationId}) ";

            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                query += $"AND pack_type like 'ROLKA%' ";
            }
            else
            {
                query += $"AND pack_type not like 'ROLKA%' ";
            }
            if ((global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))    //mail p. Łukasza z 2018.03.13
            {
                query += $"AND pack_weight = 1 ";
            }
            else
            {
                query += $"AND pack_weight = 0 ";               //tel. p. Łukasza 2018-04-11
            }
            query += $"ORDER BY dispatch_item_table.subpack_from";

            /*
            SELECT 
                *
            FROM
                tjdata.dispatch_item_table
                    LEFT JOIN
                tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id
                    AND dispatch_item_table.ref_from = dispatch_address_table.ref_from
                    AND dispatch_item_table.ref_to = dispatch_address_table.ref_to
            WHERE
                dispatch_item_table.reference_id = '012345072616175730_34'
                    AND dispatch_item_table.ref_from = 1
                    AND dispatch_item_table.ref_to = 2
            ORDER BY dispatch_item_table.subpack_from
            */
            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                foreach (DataRow dRow in dTabel.Rows)
                {
                    FinishingDispatchAddressAndItem retitem = DataRowToFinishingDispatchAddressAndItem(dRow);
                    retVal.Add(retitem);
                }
            }

            Program.autojetFinishingMain.Hm.Info($"findAllDispachAddressAndItemForZPL(), retVal.Count() = {retVal.Count()}");
            return retVal;
        }

        /// <summary>
        /// Gets all subpackage for ZPL except those booked on other station 
        /// </summary>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        public static int countUncompletedSupackage(global.zplDataType zpl)
        {
            string query = $"SELECT  Count(*) FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"LEFT JOIN ";
            query += $"tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id ";
            query += $"AND dispatch_item_table.ref_from = dispatch_address_table.ref_from ";
            query += $"AND dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            query += $"WHERE ";
            query += $"dispatch_item_table.reference_id = '{zpl.referenceID}' ";
            query += $"AND dispatch_item_table.ref_from = {zpl.ref_from} ";
            query += $"AND dispatch_item_table.ref_to = {zpl.ref_to} ";
            query += $"AND completation_done = 0 ";
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                query += $"AND pack_type like 'ROLKA%' ";
            }
            else
            {
                query += $"AND pack_type not like 'ROLKA%' ";
            }
            if ((global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))    //mail p. Łukasza z 2018.03.13
            {
                query += $"AND pack_weight = 1 ";
            }
            else
            {
                query += $"AND pack_weight = 0 ";               //tel. p. Łukasza 2018-04-11
            }/*
            SELECT 
                Count(*)
            FROM
                tjdata.dispatch_item_table
                    LEFT JOIN
                tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id
                    AND dispatch_item_table.ref_from = dispatch_address_table.ref_from
                    AND dispatch_item_table.ref_to = dispatch_address_table.ref_to
            WHERE
                dispatch_item_table.reference_id = '012345072616175730_34'
                    AND dispatch_item_table.ref_from = 1
                    AND dispatch_item_table.ref_to = 2
		            and completation_done = 0
            */
            DataTable dTabel = SQLConnection.GetData(query);

            return int.Parse(dTabel.Rows[0][0].ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dRow"></param>
        /// <returns></returns>
        public static FinishingDispatchAddressAndItem DataRowToFinishingDispatchAddressAndItem(DataRow dRow)
        {
            FinishingDispatchAddressAndItem retVal = new FinishingDispatchAddressAndItem();

            /*
                //public int id { get; set; }
                //public int dispatch_id { get; set; }
                public int reference_id { get; set; }       //new
                public string jdnr { get; set; }
                public string jdline { get; set; }
                public int qty_to_pack { get; set; }
                public int qty_packed { get; set; }
                public bool partial_item { get; set; }
                public string barcode_data { get; set; }
                public int weight_act { get; set; }
                public int operatorId { get; set; }
                public DateTime created_date { get; set; }
                public DateTime mod_date { get; set; }
                //public string description { get; set; }
                public string pack_description { get; set; }    //new
                public int ref_from { get; set; }               //new
                public int ref_to { get; set; }                 //new
                public int subpack_from { get; set; }           //new
                public int subpack_to { get; set; }             //new
                public bool completation_done { get; set; }
                public int booked_station_id { get; set; }

                public int tile { get; set; }
                public int tileqty { get; set; }
                    */
            //FinishingDispatchAddressAndItem retitem = new FinishingDispatchAddressAndItem();
            //retitem.item.id = int.Parse(dRow["dispatch_item_table_id"].ToString());
            //retitem.item.dispatch_id = int.Parse(dRow["dispatch_id"].ToString());
            retVal.item.reference_id = dRow["reference_id"].ToString();                 //new
            retVal.item.jdnr = dRow["jdnr"].ToString();
            retVal.item.jdline = dRow["jdline"].ToString();
            retVal.item.qty_to_pack = int.Parse(dRow["qty_to_pack"].ToString());
            retVal.item.qty_packed = int.Parse(dRow["qty_packed"].ToString());
            retVal.item.partial_item = SQLConnection.dbBoolNullToBool(dRow["partial_item"]);
            retVal.item.barcode_data = dRow["barcode_data"].ToString();
            int weight_act = 0, operatorId = 0, tile = 0, tileqty = 0;
            int.TryParse(dRow["weight_act"].ToString(), out weight_act); //0
            int.TryParse(dRow["operatorId"].ToString(), out operatorId); //0
            int.TryParse(dRow["tile"].ToString(), out tile); //0
            int.TryParse(dRow["tileqty"].ToString(), out tileqty); //0

            retVal.item.weight_act = weight_act;
            retVal.item.operatorId = operatorId;
            retVal.item.tile = tile;
            retVal.item.tileqty = tileqty;

            retVal.item.created_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["created_date"]);
            retVal.item.mod_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["mod_date"]);
            //retitem.item.description = dRow["description"].ToString();
            retVal.item.pack_description = dRow["pack_description"].ToString();                //new
            retVal.item.ref_from = int.Parse(dRow["ref_from"].ToString());                     //new
            retVal.item.ref_to = int.Parse(dRow["ref_to"].ToString());                         //new
            retVal.item.subpack_from = int.Parse(dRow["subpack_from"].ToString());             //new
            retVal.item.subpack_to = int.Parse(dRow["subpack_to"].ToString());                 //new
            retVal.item.completation_done = SQLConnection.dbBoolNullToBool(dRow["completation_done"]);
            retVal.item.booked_station_id = int.Parse(dRow["booked_station_id"].ToString());
            retVal.item.motives = dRow["motives"].ToString();
            retVal.item.tileName = dRow["tilename"].ToString();

            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                retVal.item.dispach_tiles = dRow["dispach_tiles"].ToString();
            }
            retVal.item.pack_weight = SQLConnection.dbBoolNullToBool(dRow["pack_weight"]);

            //weight, można pobrać, ale nie jest to potrzebne
            //public int weight_model { get; set; }           //new, id from weight_model table
            //public DateTime pack_date { get; set; }         //new, 

            //retitem.address.id = int.Parse(dRow["dispatch_address_table_id"].ToString());
            retVal.address.reference_id = dRow["reference_id"].ToString();
            retVal.address.file_name = dRow["file_name"].ToString();
            retVal.address.ams_file_name = dRow["ams_file_name"].ToString();
            retVal.address.created_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["created_date"]);
            retVal.address.mod_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["mod_date"]);
            retVal.address.courier_type = dRow["courier_type"].ToString();
            retVal.address.dispatch_name = dRow["dispatch_name"].ToString();
            retVal.address.dispatch_surname = dRow["dispatch_surname"].ToString();
            retVal.address.dispatch_tel = dRow["dispatch_tel"].ToString();
            retVal.address.dispatch_date = DateTime.Parse(dRow["dispatch_date"].ToString());
            retVal.address.street = dRow["street"].ToString();
            retVal.address.city = dRow["city"].ToString();
            retVal.address.post_code = dRow["post_code"].ToString();
            retVal.address.dispatch_company = dRow["dispatch_company"].ToString();
            //retitem.address.description = dRow["description"].ToString();
            retVal.address.list_description = dRow["list_description"].ToString();
            retVal.address.pack_type = dRow["pack_type"].ToString();
            retVal.address.total_packs = int.Parse(dRow["total_packs"].ToString());
            retVal.address.ref_from = int.Parse(dRow["ref_from"].ToString());
            retVal.address.ref_to = int.Parse(dRow["ref_to"].ToString());
            retVal.address.deliv_list_no = dRow["deliv_list_no"].ToString();
            retVal.address.comments = dRow["comments"].ToString();
            

            return retVal;
        }

        /// <summary>
        /// Expired: Returns next open dispach_item for selected jdline and jdnr
        /// </summary>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        static public DispatchItem FindNextOpenDispach(int jdline, int jdnr, int stationId)
        {
            DispatchItem item = new DispatchItem();

            string query = $"SELECT * FROM tjdata.dispatch_item_table where jdline = '{jdline}' and jdnr = '{jdnr}' and completation_done=false and (booked_station_id=0 or booked_station_id={stationId}) order by dispatch_item_table.qty_to_pack desc";

            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count > 0)
            {
                //item.id = int.Parse(dTable.Rows[0]["dispatch_item_table_id"].ToString());
                //item.dispatch_id = int.Parse(dTable.Rows[0]["dispatch_id"].ToString());
                item.reference_id = dTable.Rows[0]["reference_id"].ToString();                 //new
                item.jdnr = dTable.Rows[0]["jdnr"].ToString();
                item.jdline = dTable.Rows[0]["jdline"].ToString();
                item.qty_to_pack = int.Parse(dTable.Rows[0]["qty_to_pack"].ToString());
                item.qty_packed = int.Parse(dTable.Rows[0]["qty_packed"].ToString());
                item.partial_item = SQLConnection.dbBoolNullToBool(dTable.Rows[0]["partial_item"]);
                item.barcode_data = dTable.Rows[0]["barcode_data"].ToString();
                item.weight_act = int.Parse(dTable.Rows[0]["weight_act"].ToString());
                item.operatorId = int.Parse(dTable.Rows[0]["operatorId"].ToString());
                item.created_date = SQLConnection.dbDateTimeNullToDateTime2(dTable.Rows[0]["created_date"]);
                item.mod_date = SQLConnection.dbDateTimeNullToDateTime2(dTable.Rows[0]["mod_date"]);
                //item.description = dTable.Rows[0]["description"].ToString();
                item.pack_description = dTable.Rows[0]["pack_description"].ToString();                //new
                item.ref_from = int.Parse(dTable.Rows[0]["ref_from"].ToString());                     //new
                item.ref_to = int.Parse(dTable.Rows[0]["ref_to"].ToString());                         //new
                item.subpack_from = int.Parse(dTable.Rows[0]["subpack_from"].ToString());             //new
                item.subpack_to = int.Parse(dTable.Rows[0]["subpack_to"].ToString());                 //new
                item.completation_done = SQLConnection.dbBoolNullToBool(dTable.Rows[0]["completation_done"]);
                item.booked_station_id = int.Parse(dTable.Rows[0]["booked_station_id"].ToString());
                item.motives = dTable.Rows[0]["motives"].ToString();
            }

            return item;
        }

        /// <summary>
        /// update itemId as booked for this station
        /// </summary>
        /// <param name="itemId"></param>
        static public void BookOpenDispach(DispatchItem item, int stationId)
        {
            string query = $"update dispatch_item_table set booked_station_id = {stationId} where reference_id = '{item.reference_id}' and ref_from = {item.ref_from} and ref_to = {item.ref_to} and subpack_from = '{item.subpack_from}' and subpack_to = '{item.subpack_to}'";
            SQLConnection.UpdateData(query);
        }

        /// <summary>
        /// Return id of booked dispach_item
        /// </summary>
        /// <returns></returns>
        static public int GetBookedItemId()
        {
            int retVal = -1;
            string query = $"select id from tjdata.dispatch_item_table where booked_station_id={Program.Settings.Data.stationId}";

            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count>0)
            {
                retVal = int.Parse(dTable.Rows[0][0].ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Count all dispach_item booked for this station
        /// </summary>
        /// <returns></returns>
        static public int CountBookedItems()
        {
            int retVal = -1;
            string query = $"select count(id) from tjdata.dispatch_item_table where booked_station_id={Program.Settings.Data.stationId}";

            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count > 0)
            {
                retVal = int.Parse(dTable.Rows[0][0].ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Set booked_id = 0 for all dispach_item booked for this station
        /// </summary>
        /// <returns></returns>
        static public void DisbookedAllItems()
        {
            string query = $"update dispatch_item_table set booked_station_id=0 where booked_station_id = {Program.Settings.Data.stationId}";
            SQLConnection.UpdateData(query);
        }

        /// <summary>
        /// Sets new vaule of qty_packed for selected id
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="packedQty"></param>
        static public void UpdateDispachItemPackedQty(DispatchItem item)
        {
            string query = $"update dispatch_item_table set qty_packed = {item.qty_packed}, pack_date = NOW() where reference_id = '{item.reference_id}' and ref_from = {item.ref_from} and ref_to = {item.ref_to} and subpack_from = '{item.subpack_from}' and subpack_to = '{item.subpack_to}'";
            SQLConnection.UpdateData(query);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="packedQty"></param>
        static public void UpdateDispachItemcompletationDone(DispatchItem item, int operatorId, int stationId)
        {
            string query = $"update dispatch_item_table set completation_done=1, booked_station_id=0, pack_date = NOW(), weight_act = {item.weight_act}, weight_model = {item.weight_model}, operatorId={operatorId}, stationId={stationId}, barcode_data='{item.reference_id}{item.ref_from.ToString("D2")}{item.ref_to.ToString("D2")}{item.subpack_from.ToString("D2")}{item.subpack_to.ToString("D2")}' where reference_id = '{item.reference_id}' and ref_from = {item.ref_from} and ref_to = {item.ref_to} and subpack_from = '{item.subpack_from}' and subpack_to = '{item.subpack_to}'";
            SQLConnection.UpdateData(query);
        }


        static public string GetFilenameFromReference(global.zplDataType data)
        {
            string retVsal = "";

            string query = $"SELECT file_name FROM tjdata.dispatch_address_table where reference_id='{data.referenceID}' and ref_from = {data.ref_from} and ref_to = {data.ref_to}";

            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count > 0)
            {
                retVsal = dTable.Rows[0]["file_name"].ToString();
            }

            return retVsal;
        }

        static public global.zplDataType GetReferenceFromFinishing(scanItem item)
        {
            global.zplDataType retVal = new global.zplDataType();

            string query = $"SELECT reference_id, ref_from, ref_to, subpack_from, subpack_to FROM tjdata.finishing_log where jdnr='{item.jdnr}' and jdline='{item.jdline}' and tilecounter={item.tilecounter} and tiles='{item.tiles}' and reference_id != ''";

            DataTable dTable = SQLConnection.GetData(query);

            Program.autojetFinishingMain.Hm.Info($"GetReferenceFromFinishing(), dTable.Rows.Count={dTable.Rows.Count.ToString()}");
            if (dTable.Rows.Count > 0)
            {
                retVal.referenceID = dTable.Rows[0]["reference_id"].ToString();
                retVal.ref_from = int.Parse(dTable.Rows[0]["ref_from"].ToString());
                retVal.ref_to = int.Parse(dTable.Rows[0]["ref_to"].ToString());
                retVal.subpack_from = int.Parse(dTable.Rows[0]["subpack_from"].ToString());
                retVal.subpack_to = int.Parse(dTable.Rows[0]["subpack_to"].ToString());
            }

            return retVal;
        }

        static public global.zplDataType GetReferenceFromFinishing2(scanItem item)
        {
            global.zplDataType retVal = new global.zplDataType();

            string query = $"SELECT reference_id, ref_from, ref_to, subpack_from, subpack_to FROM tjdata.finishing2_log where jdnr='{item.jdnr}' and jdline='{item.jdline}' and tilecounter={item.tilecounter} and tiles='{item.tiles}' and reference_id != ''";

            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count > 0)
            {
                retVal.referenceID = dTable.Rows[0]["reference_id"].ToString();
                retVal.ref_from = int.Parse(dTable.Rows[0]["ref_from"].ToString());
                retVal.ref_to = int.Parse(dTable.Rows[0]["ref_to"].ToString());
                retVal.subpack_from = int.Parse(dTable.Rows[0]["subpack_from"].ToString());
                retVal.subpack_to = int.Parse(dTable.Rows[0]["subpack_to"].ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Return all adresses for referenceID, reference_from and referenc_to
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        static public List<FinishingDispatchAddressAndItem> GetAllAdresAndItemFromReference(global.zplDataType data)
        {
            List<FinishingDispatchAddressAndItem> retVal = new List<FinishingDispatchAddressAndItem>();

            string query = $"SELECT *, ";
            //if (Program.Settings.Data.stationType == stationTypeRolka)
            //{
                query += $"CASE ";
                query += $"WHEN dispatch_item_table.tile=0 then 'PLAKAT' ";
                query += $"ELSE dispatch_item_table.tilename ";
                query += $"END as dispach_tiles ";
            //}
            query += $"FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"LEFT JOIN ";
            query += $"tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id ";
            query += $"AND dispatch_item_table.ref_from = dispatch_address_table.ref_from ";
            query += $"AND dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            query += $"WHERE ";
            query += $"dispatch_item_table.reference_id = '{data.referenceID}' ";
            query += $"AND dispatch_item_table.ref_from = {data.ref_from} ";
            query += $"AND dispatch_item_table.ref_to = {data.ref_to} ";
            if (Program.Settings.Data.stationType == global.stationTypeRolka)
            {
                query += $"AND pack_type like 'ROLKA%' ";
            }
            else
            {
                query += $"AND pack_type not like 'ROLKA%' ";
            }
            if ((global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))    //mail p. Łukasza z 2018.03.13
            {
                query += $"AND pack_weight = 1 ";
            }
            else
            {
                query += $"AND pack_weight = 0 ";               //tel. p. Łukasza 2018-04-11
            }

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                foreach (DataRow dRow in dTabel.Rows)
                {
                    FinishingDispatchAddressAndItem retitem = DataRowToFinishingDispatchAddressAndItem(dRow);
                    retVal.Add(retitem);
                }
            }

            Program.autojetFinishingMain.Hm.Info($"GetAllAdresAndItemFromReference(), retVal.Count() = {retVal.Count()}");
            return retVal;
        }

        /// <summary>
        /// Return adress for referenceID, reference_from, referenc_to, subpack_from, subpack_to
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        static public FinishingDispatchAddressAndItem GetAdresAndItemFromReference(global.zplDataType data)
        {
            FinishingDispatchAddressAndItem retVal = new FinishingDispatchAddressAndItem();

            string query = $"SELECT *, ";
            //if (Program.Settings.Data.stationType == stationTypeRolka)
            //{
                query += $"CASE ";
                query += $"WHEN dispatch_item_table.tile=0 then 'PLAKAT' ";
                query += $"ELSE dispatch_item_table.tilename ";
                query += $"END as dispach_tiles ";
            //}
            query += $"FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"LEFT JOIN ";
            query += $"tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id ";
            query += $"AND dispatch_item_table.ref_from = dispatch_address_table.ref_from ";
            query += $"AND dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            query += $"WHERE ";
            query += $"dispatch_item_table.reference_id = '{data.referenceID}' ";
            query += $"AND dispatch_item_table.ref_from = {data.ref_from} ";
            query += $"AND dispatch_item_table.ref_to = {data.ref_to} ";
            query += $"AND dispatch_item_table.subpack_from = {data.subpack_from} ";
            query += $"AND dispatch_item_table.subpack_to = {data.subpack_to} ";
            //if (Program.Settings.Data.stationType == global.stationTypeRolka)
            //{
            //    query += $"AND pack_type like 'ROLKA%' ";
            //}
            //else
            //{
            //    query += $"AND pack_type not like 'ROLKA%' ";
            //}
            //if ((global.dispachMode == global.dispModeStroer) || (global.dispachMode == global.dispModeStroerSubpack))    //mail p. Łukasza z 2018.03.13
            //{
            //    query += $"AND pack_weight = 1 ";
            //}
            //else
            //{
            //    query += $"AND pack_weight = 0 ";               //tel. p. Łukasza 2018-04-11
            //}

            string queryA = $"SELECT *, ";
            //if (Program.Settings.Data.stationType == stationTypeRolka)
            //{
            queryA += $"CASE ";
            queryA += $"WHEN dispatch_item_table_archive.tile=0 then 'PLAKAT' ";
            queryA += $"ELSE dispatch_item_table_archive.tilename ";
            queryA += $"END as dispach_tiles ";
            //}
            queryA += $"FROM ";
            queryA += $"tjdata.dispatch_item_table_archive ";
            queryA += $"LEFT JOIN ";
            queryA += $"tjdata.dispatch_address_table_archive ON dispatch_item_table_archive.reference_id = dispatch_address_table_archive.reference_id ";
            queryA += $"AND dispatch_item_table_archive.ref_from = dispatch_address_table_archive.ref_from ";
            queryA += $"AND dispatch_item_table_archive.ref_to = dispatch_address_table_archive.ref_to ";
            queryA += $"WHERE ";
            queryA += $"dispatch_item_table_archive.reference_id = '{data.referenceID}' ";
            queryA += $"AND dispatch_item_table_archive.ref_from = {data.ref_from} ";
            queryA += $"AND dispatch_item_table_archive.ref_to = {data.ref_to} ";
            queryA += $"AND dispatch_item_table_archive.subpack_from = {data.subpack_from} ";
            queryA += $"AND dispatch_item_table_archive.subpack_to = {data.subpack_to} ";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                retVal = DataRowToFinishingDispatchAddressAndItem(dTabel.Rows[0]);
            }
            else
            {
                dTabel = SQLConnection.GetData(queryA);
                if (dTabel.Rows.Count > 0)
                {
                    retVal = DataRowToFinishingDispatchAddressAndItem(dTabel.Rows[0]);
                }
            }

            Program.autojetFinishingMain.Hm.Info($"GetAdresAndItemFromReference(), retVal.item() = {retVal.item}");
            return retVal;
        }
        /**/

        //logika wyboru drukowanego ZPL
        //1.Jeśli partial:
        //1.1 Jeśli wszystkie subpack ukonczone to: etykieta końcowa (jesli istnieje) lub końcowa ZPL
        //1.2 Jeśli nie wszystkie ukończone to częściowa ZPL
        //2. Jeśli nie partial:
        //2.1 Etykieta końcowa (jesli istnieje) lub końcowa ZPL

        //=====================================================================================================================================


        //=====================================================================================================================================
        //waga
        /// <summary>
        /// Expired: Zwraca pack_type z tabeli dispatch_item_table na podstawie jdnr i jdline
        /// </summary>
        /// <param name="weightBarcode"></param>
        /// <returns></returns>
        static public string GetPackTypeFromDatabase(global.weightBarcodeScan weightBarcode)
        {
            string pack_type = "";

            string query = $"SELECT top 1 pack_type FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"WHERE ";
            query += $"jdnr = '{weightBarcode.jdnr}' ";
            query += $"AND jdline = {weightBarcode.jdline} ";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                pack_type = dTabel.Rows[0]["pack_type"].ToString();
            }

            Program.autojetFinishingMain.Hm.Info($"GetPackTypeFromDatabase: pack_type = {pack_type}");
            return pack_type;
        }

        /// <summary>
        /// zwraca ModeFrom (co pakuje się na wadze: cały plakat, jeden bryt czy kilka brytów) na podstawie tile i tileqty
        /// </summary>
        /// <param name="weightBarcode"></param>
        /// <returns></returns>
        static public int GetModelModeFromAdresAndItem(FinishingDispatchAddressAndItem adresAndItem)
        {
            int retVal;

            int tile = adresAndItem.item.tile; 
            int tileqty = adresAndItem.item.tileqty;
            
            if (tile == 0)
            {
                retVal = 0;
            }
            else if (tileqty > 1)
            {
                retVal = 1;
            }
            else
            {
                retVal = 2;
            }

            Program.autojetFinishingMain.Hm.Info($"GetModelModeFromAdresAndItem: retVal = {retVal.ToString()}");
            return retVal;
        }

        /// <summary>
        /// Pobranie danych z tabeli weight_model na podstawie weightBarcodeScan
        /// </summary>
        /// <param name="weightBarcode"></param>
        /// <returns></returns>
        static public global.weightModelDataType GetWeightModelFromDB(global.weightBarcodeScan weightBarcode)
        {
            global.weightModelDataType weightModel = new global.weightModelDataType();

            string query = $"SELECT * FROM ";
            query += $"weight_model ";
            query += $"WHERE ";
            query += $"jdnr = '{weightBarcode.jdnr}' ";
            query += $"AND jdline = '{weightBarcode.jdline}' ";
            query += $"AND pack_type = '{weightBarcode.pack_type}' ";
            query += $"order by created_date desc";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                weightModel.jdline = weightBarcode.jdline;
                weightModel.jdnr = weightBarcode.jdnr;
                weightModel.pack_type = weightBarcode.pack_type;
                int.TryParse(dTabel.Rows[0]["id"].ToString(), out weightModel.id);
                int.TryParse(dTabel.Rows[0]["weight_model"].ToString(), out weightModel.weight_model);
                int.TryParse(dTabel.Rows[0]["weight_tare"].ToString(), out weightModel.weight_tare);
            }
            else
            {
                weightModel.id = -1;
            }

            Program.autojetFinishingMain.Hm.Info($"GetWeightModelFromDB: weightModel.id = {weightModel.id}");
            return weightModel;
        }

        static public void insertWeightModel(global.weightModelDataType model)
        {
            /*
            CREATE TABLE `weight_model` (
              `id` int(11) NOT NULL,
              `jdnr` varchar(6) COLLATE utf8_polish_ci NOT NULL,
              `jdline` varchar(3) COLLATE utf8_polish_ci NOT NULL,
              `pack_type` varchar(45) COLLATE utf8_polish_ci NOT NULL,
              `created_date` datetime NOT NULL,
            `weight_model` int(11) NOT NULL,
            `weight_tare` int(11) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
             */
            //string insertQuery = string.Format($"INSERT INTO tjdata.weight_model(jdnr, jdline, pack_type, created_date, weight_model, weight_tare) VALUES ('{model.jdnr}', '{model.jdline}', '{model.pack_type}', NOW(), {model.weight_model}, {model.weight_tare})");

            string insertQuery;
            if (Program.Settings.Data.stationType == global.stationTypeNotRolka)
            {
                //insertQuery = string.Format($"INSERT INTO tjdata.weight_model(jdnr, jdline, pack_type, created_date, weight_model, weight_tare) VALUES ('{model.jdnr}', '{model.jdline}', 'PALETA', NOW(), {model.weight_model}, {model.weight_tare}), ('{model.jdnr}', '{model.jdline}', 'PACZKA', NOW(), {model.weight_model}, {model.weight_tare}), ('{model.jdnr}', '{model.jdline}', 'NAMAGAZYN', NOW(), {model.weight_model}, {model.weight_tare})");
                insertQuery = string.Format($"INSERT INTO tjdata.weight_model(jdnr, jdline, pack_type, created_date, weight_model, weight_tare) SELECT jdnr,jdline,'PALETA',NOW(),{model.weight_model},{model.weight_tare} FROM Jobsdata WHERE jdnr={model.jdnr} AND jobformat=(SELECT jobformat FROM Jobsdata jd2 WHERE jd2.jdnr = {model.jdnr} AND jd2.jdline = {model.jdline})");
                SQLConnection.InsertData(insertQuery);
                insertQuery = string.Format($"INSERT INTO tjdata.weight_model(jdnr, jdline, pack_type, created_date, weight_model, weight_tare) SELECT jdnr,jdline,'PACZKA',NOW(),{model.weight_model},{model.weight_tare} FROM Jobsdata WHERE jdnr={model.jdnr} AND jobformat=(SELECT jobformat FROM Jobsdata jd2 WHERE jd2.jdnr = {model.jdnr} AND jd2.jdline = {model.jdline})");
                SQLConnection.InsertData(insertQuery);
                insertQuery = string.Format($"INSERT INTO tjdata.weight_model(jdnr, jdline, pack_type, created_date, weight_model, weight_tare) SELECT jdnr,jdline,'NAMAGAZYN',NOW(),{model.weight_model},{model.weight_tare} FROM Jobsdata WHERE jdnr={model.jdnr} AND jobformat=(SELECT jobformat FROM Jobsdata jd2 WHERE jd2.jdnr = {model.jdnr} AND jd2.jdline = {model.jdline})");
                SQLConnection.InsertData(insertQuery);
            }
            else
            {
                //Zmiana przez ŁPA żeby dla wszytskich formatów robić INSERT w ramach danej KP
                //insertQuery = string.Format($"INSERT INTO tjdata.weight_model(jdnr, jdline, pack_type, created_date, weight_model, weight_tare) VALUES ('{model.jdnr}', '{model.jdline}', '{model.pack_type}', NOW(), {model.weight_model}, {model.weight_tare})");
                insertQuery = string.Format($"INSERT INTO tjdata.weight_model(jdnr, jdline, pack_type, created_date, weight_model, weight_tare) SELECT jdnr,jdline,'{model.pack_type}',NOW(),{model.weight_model},{model.weight_tare} FROM Jobsdata WHERE jdnr={model.jdnr}  AND jobformat=(SELECT jobformat FROM Jobsdata jd2 WHERE jd2.jdnr = {model.jdnr} AND jd2.jdline = {model.jdline})");
                //VALUES('{model.jdnr}', '{model.jdline}', '{model.pack_type}', NOW(), {model.weight_model}, {model.weight_tare})");
                SQLConnection.InsertData(insertQuery);
            }
            
        }


        static public bool isStroer(global.zplDataType zplData)
        {

            return false;
        }
    }
}
