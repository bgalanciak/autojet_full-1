﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace AutojetFinishing
{
    public static class global
    {
        public static user userAct = new user();
        public static List<user> userList = new List<user>();
        //public static List<user> userActiveList = new List<user>();

        public static int completionErrorShowTime;
        public static bool posterCompletionError = false;   //błąd kompletacji wysyłany do PLC w celu wyświetlenia błędu
        public static bool plcStartOn = false;              //informacja z PLC, że trawers jest w ruchu (w czasie skanowania),

        public static bool multiscanPlcEnd = false;         //zakończył się przejazd trawersu, jesli jest tez koniec obrabiania danyh to nalezy wyswietlic wynik skanowania
        public static bool multiscanDataEnd = false;        //zakończyła się analiza danych, jesli jest tez koniec przejazdu trawersu to nalezy wyswietlic wynik skanowania
        public static bool skipScanerData = false;

        public static List<scanError> scanErrorList = new List<scanError>();

        //public static List<dispatch_row> dispatchList = new List<dispatch_row>();

        public static FinishingDispatchAddressAndItem actAddressAndItem = new FinishingDispatchAddressAndItem();

        //for subpack
        public static zplDataType zplData;
        //public static bool subpackAdding = false;               //set after scanning proper ZPL label, 
        public static bool subpackAddingComplete = false;       //package, containing subpackage is complete, 
        public static List<FinishingDispatchAddressAndItem> subpackList;    //list of akk items in a package (based on ZPL data)
        public static List<FinishingDispatchAddressAndItem> FinishingDispatchAddressAndItemList;
            //

        public static int dispachMode = 0;
        public static bool dispachAllDone = false;    //tryb zdrukowania kopii etykiety
        public const int dispModeWarehouse = 0;
        public const int dispModeWaybill = 1;
        public const int dispModeWaybillSbpack = 2;
        public const int dispModeStroer = 3;            //tryb ponownego skanowania już skompletowanych paczek, bez kompletacji
        public const int dispModeStroerSubpack = 4;

        //
        public static float tableLayoutPanelWidth;

        //
        public static bool labelReprintMode = false;    //tryb zdrukowania kopii etykiety

        //
        public static bool multiscanAnalizeOn = false;


        //
        public static bool cognexEthConnectedLast = true;    //
        
        //const
        public const bool barcodeModeLader = false;         //0 - Lader, 1 - Fence
        public const bool barcodeModeFence = true;          //0 - Lader, 1 - Fence

        public const bool stationTypeRolka = true;
        public const bool stationTypeNotRolka = false;

        public static bool weightConnectionStatus = false;
        public static weightModelDataType weightModel = new weightModelDataType();

        //saved data for reprinting last label
        //global.actAddressAndItem, global.zplData, weight

        public static FinishingDispatchAddressAndItem lastLabelAdresAndItem = new FinishingDispatchAddressAndItem();
        public static global.zplDataType lastLabelZpl = new zplDataType();
        public static int lastLabelWeight;

        //maxymalna ilosc czesci plakatu
        public const int maxColCount = 10;
        public const int maxRowCount = 10;

        //
        public static bool skipIncomingScan = false;

        public static List<user> userActiveList
        {
            get
            {
                IEnumerable<user> ret = from u in userList where u.is_active select u;
                List<user> retVal = ret.ToList();
                return retVal;
            }
        }

        public static List<user> getAllUsers()
        {
            /*
            CREATE TABLE `tjdata`.`user_table` (
            `id` INT NOT NULL,
            `userFName` VARCHAR(45) NULL,
            `userLName` VARCHAR(45) NULL,
            `userPassword` VARCHAR(45) NULL,
            `qcNum` INT NULL,
            `isAdmin` BIT NULL,
            `isOperator` BIT NULL,
            `isActive` BIT NULL,
            PRIMARY KEY (`id`));
            */

            /*
            ALTER TABLE `tjdata`.`user_table` 
            CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
             */

            List<user> retVal = new List<user>();

            string query = string.Format("select * from tjdata.user_table order by userLName");
            DataTable dTable = SQLConnection.GetData(query);

            foreach (DataRow dRow in dTable.Rows)
            {
                user u = new user();
                u.id = int.Parse(dRow["id"].ToString());
                u.f_name = dRow["userFName"].ToString();
                u.l_name = dRow["userLName"].ToString();
                u.password = dRow["userPassword"].ToString();
                u.qc_num = int.Parse(dRow["qcNum"].ToString());
                u.is_admin = Convert.ToBoolean(int.Parse(dRow["isAdmin"].ToString()));
                u.is_operator = Convert.ToBoolean(int.Parse(dRow["isOperator"].ToString()));
                u.is_active = Convert.ToBoolean(int.Parse(dRow["isActive"].ToString()));
                u.userLogHash = dRow["userLogHash"].ToString();

                retVal.Add(u);
            }

            return retVal;
        }

        public static user findUserById(int id, List<user> list)
        {
            user retVal = new user();

            foreach (user u in list)
            {
                if (u.id == id)
                {
                    retVal = new user(u);
                }
            }

            return retVal;
        }

        public static user findUserByHash(string hash, List<user> list)
        {
            user retVal = new user();

            foreach (user u in list)
            {
                if (u.userLogHash == hash)
                {
                    retVal = new user(u);
                }
            }

            return retVal;
        }

        public static List<scanError> getAllErrors()
        {
            /*
            select * from tjdata.tile_scan_error_txt order by id
             */

            List<scanError> retVal = new List<scanError>();

            string query = string.Format("select * from tjdata.tile_scan_error_txt order by id");
            DataTable dTable = SQLConnection.GetData(query);

            Program.autojetFinishingMain.Hm.Info("getAllErrors(): dTable.Rows.Count={0}", dTable.Rows.Count.ToString());
            foreach (DataRow dRow in dTable.Rows)
            {
                scanError u = new scanError();
                u.id = int.Parse(dRow["id"].ToString());
                u.error_txt = dRow["error_txt"].ToString();
                u.closeVirtual = Convert.ToBoolean(int.Parse(dRow["closeVirtual"].ToString()));

                Program.autojetFinishingMain.Hm.Info("getAllErrors(): u.id={0}", u.id.ToString());

                retVal.Add(u);
            }

            return retVal;
        }

        public static scanError findErrorById(int id, List<scanError> list)
        {
            scanError retVal = new scanError();

            Program.autojetFinishingMain.Hm.Info("findErrorById(): id={0}", id.ToString());

            foreach (scanError u in list)
            {
                if (u.id == id)
                {
                    return new scanError(u);
                }
            }
            return null;
        }

        public class zplDataType
        {
            public string referenceID;      //always 22 characterscharacters
            public int ref_from;
            public int ref_to;
            public int subpack_from;
            public int subpack_to;
            public string zplType;          //nowa część kodu kreskowego, C = częściowa, K = końcowa
        }

        public class weightModelDataType
        {
            /*
            `id` int(11) NOT NULL,
            `jdnr` varchar(6) COLLATE utf8_polish_ci NOT NULL,
            `jdline` varchar(3) COLLATE utf8_polish_ci NOT NULL,
            `pack_type` varchar(45) COLLATE utf8_polish_ci NOT NULL,
            `created_date` datetime NOT NULL,
            `weight_model` int(11) NOT NULL,
            `weight_tare` int(11) NOT NULL,

             */
            public int id;
            public string jdnr;
            public string jdline;
            public string pack_type;
            public DateTime created_date;
            public int weight_model;
            public int weight_tare;
            public int weight_base; //masa poprzednich paczek dodawanych podczas ważenia
            public string tileName;
        }
        
        public class weightBarcodeScan
        {
            /*
            `id` int(11) NOT NULL,
            `jdnr` varchar(6) COLLATE utf8_polish_ci NOT NULL,
            `jdline` varchar(3) COLLATE utf8_polish_ci NOT NULL,
            `pack_type` varchar(45) COLLATE utf8_polish_ci NOT NULL,
            `created_date` datetime NOT NULL,
            `weight_model` int(11) NOT NULL,
            `weight_tare` int(11) NOT NULL,

             */
            public string scanData;
            public string jdnr;
            public string jdline;
            public string pack_type;
            public int modelType;       //0 = "PLAKATU", 1 = BRYTÓW, 2 = BRYTU 
            public string tileName;
            public int model_scan_count;    //ile detali ma zostać zważonych w celu pobrania modelu
        }
    }
}
