﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutojetFinishing
{
    public class user
    {
        /*
    CREATE TABLE `tjdata`.`user_table` (
        `id` INT NOT NULL,
        `userFName` VARCHAR(45) NULL,
        `userLName` VARCHAR(45) NULL,
        `userPassword` VARCHAR(45) NULL,
        `qcNum` INT NULL,
        `isAdmin` BIT NULL,
        `isOperator` BIT NULL,
        `isActive` BIT NULL,
        PRIMARY KEY (`id`));
         */
        
        public int      id          { get; set; }
        public string   f_name      { get; set; }
        public string   l_name      { get; set; }
        public string   password    { get; set; }
        public int      qc_num      { get; set; }
        public bool     is_operator { get; set; }
        public bool     is_admin    { get; set; }
        public bool     is_active   { get; set; }
        public string   login
        {
            get
            {
                return f_name + " " + l_name;
            }
        }
        public string   userLogHash { get; set; }


        public user()
        {
            id = -1;
            f_name = "";
            l_name = "";
            password = "";
            qc_num = 0;
            is_operator = false;
            is_admin = false;
            is_active = false;
            userLogHash = "";
        }

        public user(int _id, string _f_name, string _l_name, string _password, int _qc_num, bool _is_operator, bool _is_admin, bool _is_active, string _userLogHash)
        {
            id = _id;
            f_name = _f_name;
            l_name = _l_name;
            password = _password;
            qc_num = _qc_num;
            is_operator = _is_operator;
            is_admin = _is_admin;
            is_active = _is_active;
            userLogHash = _userLogHash;
        }

        public user(user _u)
        {
            id = _u.id;
            f_name = _u.f_name;
            l_name = _u.l_name;
            password = _u.password;
            qc_num = _u.qc_num;
            is_operator = _u.is_operator;
            is_admin = _u.is_admin;
            is_active = _u.is_active;
            userLogHash = _u.userLogHash;
        }

        public bool passwordOk(string _password)
        {
            return password.Equals(_password);
        }
    }
}
