﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Data;


namespace AutojetFinishing
{
    public partial class autojetFinishingMainForm
    {
        //List<FinishingDispatchAddressAndItem> FinishingDispatchAddressAndItemList;

        /// <summary>
        /// for tile scan
        /// </summary>
        /// <param name="posterAct"></param>
        public void findDispach(poster posterAct)
        {
            //
            global.actAddressAndItem = dispachDataAcces.findNextDispachAddressAndItem(posterAct.jdline, posterAct.jdnr, Program.Settings.Data.stationId);
            //if there is a dispach and it's not booked, book it
            if ((global.actAddressAndItem.item_reference_id != null) && (global.actAddressAndItem.item.booked_station_id == 0))
            {
                //book
                dispachDataAcces.BookOpenDispach(global.actAddressAndItem.item, Program.Settings.Data.stationId);
                global.actAddressAndItem.item.booked_station_id = Program.Settings.Data.stationId;
            }
        }

        public void findDispachForZPL(List<FinishingDispatchAddressAndItem> itemsList, string jdline, string jdnr)
        {
            //
            Program.autojetFinishingMain.Hm.Warning($"findDispachForZPL(): itemsList.Count={itemsList.Count.ToString()}, jdline={jdline}, jdnr={jdnr}");
            global.actAddressAndItem = findNextDispachAddressAndItemForZPL(itemsList, jdline, jdnr);    //dispachDataAcces.findNextDispachAddressAndItem(posterAct.jdline, posterAct.jdnr, Program.Settings.Data.stationId);
            //if there is a dispach and it's not booked, book it
            if (global.actAddressAndItem.item_reference_id != null)
            {
                if (global.actAddressAndItem.item.booked_station_id == 0)
                //book
                {
                    Program.autojetFinishingMain.Hm.Warning($"findDispachForZPL(): item found");
                    dispachDataAcces.BookOpenDispach(global.actAddressAndItem.item, Program.Settings.Data.stationId);
                    global.actAddressAndItem.item.booked_station_id = Program.Settings.Data.stationId;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemsList"></param>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        public FinishingDispatchAddressAndItem findNextDispachAddressAndItemForZPL(List<FinishingDispatchAddressAndItem> itemsList, string jdline, string jdnr)
        {
            FinishingDispatchAddressAndItem retVal = new FinishingDispatchAddressAndItem();

            foreach(FinishingDispatchAddressAndItem item in itemsList)
            {
                int itemJdline = int.Parse(item.item.jdline);
                int itemJdnr = int.Parse(item.item.jdnr);

                int cmpJdline = int.Parse(jdline);
                int cmpJdnr = int.Parse(jdnr);

                //if (item.item.jdline.Equals(jdline) && item.item.jdnr.Equals(jdnr))
                if (cmpJdline == itemJdline && cmpJdnr == itemJdnr)
                {
                    Program.autojetFinishingMain.Hm.Warning($"findNextDispachAddressAndItemForZPL(): Dispach found!, item.item_reference_id = {item.item_reference_id.ToString()}, item.item_ref_from = {item.item_ref_from.ToString()}, item.item_ref_to = {item.item_ref_to.ToString()} ");
                    return item;
                }
            }
            return null;
        }

        public void findAdresses(string jdline, string jdnr)
        {
            global.FinishingDispatchAddressAndItemList = new List<FinishingDispatchAddressAndItem>();

            global.FinishingDispatchAddressAndItemList = dispachDataAcces.findAllDispachAddressAndItem(jdline, jdnr, Program.Settings.Data.stationId);
            dgvDispatchTable.DataSource = global.FinishingDispatchAddressAndItemList;
            if (global.actAddressAndItem.item_reference_id != null)
            {
                //tylko ustawiam tryb, nie kasuje danych,
                //if (global.dispachMode != global.dispModeWaybill)
                //{//set new mode
                //    global.dispachMode = global.dispModeWaybill;

                //    //change colour of buttons
                //    bDispSubpackAddingOn.BackColor = default(Color);
                //    bDispWaybillOn.BackColor = Color.LightGreen;
                //    bDispWarehouseOn.BackColor = default(Color);

                //    Program.autojetFinishingMain.Hm.Warning("findAdresses(), new global.dispachMode = global.dispWaybill");
                //}//
                formatDispachDgv(global.actAddressAndItem.item);
            }
            //check if all dispach are done
            global.dispachAllDone = true;
            foreach (FinishingDispatchAddressAndItem i in global.FinishingDispatchAddressAndItemList)
            {
                if (!i.completation_done)
                    global.dispachAllDone = false;
            }
            Program.autojetFinishingMain.Hm.Warning($"findAdresses(): global.dispachAllDone = {global.dispachAllDone}");
        }

        public void findAdressesForZPL(global.zplDataType zpl, int stationId)
        {
            global.FinishingDispatchAddressAndItemList = new List<FinishingDispatchAddressAndItem>();

            global.FinishingDispatchAddressAndItemList = dispachDataAcces.findAllDispachAddressAndItemForZPL(zpl, stationId);
            dgvDispatchTable.DataSource = global.FinishingDispatchAddressAndItemList;
            if (global.actAddressAndItem.item_reference_id != null)
                formatDispachDgv(global.actAddressAndItem.item);

            //check if all dispach are done
            global.dispachAllDone = true;
            foreach (FinishingDispatchAddressAndItem i in global.FinishingDispatchAddressAndItemList)
            {
                if (!i.completation_done)
                    global.dispachAllDone = false;
            }
            Program.autojetFinishingMain.Hm.Warning($"findAdressesForZPL(): global.dispachAllDone = {global.dispachAllDone}");
            //
            if (global.dispachAllDone)
            {
                tableLayoutPanel7.RowStyles[2].Height = 0;
            }
        }

        private void formatDispachDgv(DispatchItem actItem)
        {
            //int actRowSel
            foreach (DataGridViewRow row in dgvDispatchTable.Rows)
            {
                string refId = row.Cells["item_reference_id"].Value.ToString();
                int refFrom = int.Parse(row.Cells["item_ref_from"].Value.ToString());
                int refTo = int.Parse(row.Cells["item_ref_to"].Value.ToString());
                bool completation_done = bool.Parse(row.Cells["completation_done"].Value.ToString());
                int booked_station_id = int.Parse(row.Cells["booked_station_id"].Value.ToString());

                if (completation_done)
                {
                    dgvDispatchTable.ClearSelection();
                    row.DefaultCellStyle.BackColor = Color.LightGreen;
                    row.Selected = true;
                }

            }
            UpdateDispachDGV();
            scrollGrid(dgvDispatchTable);
            dgvDispatchTable.ClearSelection();
            foreach (DataGridViewRow row in dgvDispatchTable.Rows)
            {
                string refId = row.Cells["item_reference_id"].Value.ToString();
                int refFrom = int.Parse(row.Cells["item_ref_from"].Value.ToString());
                int refTo = int.Parse(row.Cells["item_ref_to"].Value.ToString());
                bool completation_done = bool.Parse(row.Cells["completation_done"].Value.ToString());
                int booked_station_id = int.Parse(row.Cells["booked_station_id"].Value.ToString());
                if (actItem != null)
                {
                    if (actItem.reference_id.Equals(refId) && actItem.ref_from == refFrom && actItem.ref_to == refTo && booked_station_id == Program.Settings.Data.stationId)
                    {
                        row.Selected = true;
                    }
                }
            }
        }

        private void scrollGrid(DataGridView dgv)
        {
            if (dgv.SelectedRows.Count <= 0)
                return;
            int halfWay = (dgv.DisplayedRowCount(false) / 2);
            if (dgv.FirstDisplayedScrollingRowIndex + halfWay > dgv.SelectedRows[0].Index ||
                (dgv.FirstDisplayedScrollingRowIndex + dgv.DisplayedRowCount(false) - halfWay) <= dgv.SelectedRows[0].Index)
            {
                int targetRow = dgv.SelectedRows[0].Index;

                targetRow = Math.Max(targetRow - halfWay, 0);
                dgv.FirstDisplayedScrollingRowIndex = targetRow;
            }
        }

        public void UpdateDispachDGV()
        {
            dgvDispatchTable.Columns["address"].Visible = false;
            dgvDispatchTable.Columns["item"].Visible = false;
            dgvDispatchTable.Columns["item_reference_id"].Visible = false;
            dgvDispatchTable.Columns["item_ref_from"].Visible = false;
            dgvDispatchTable.Columns["item_ref_to"].Visible = false;
            dgvDispatchTable.Columns["completation_done"].Visible = false;
            dgvDispatchTable.Columns["booked_station_id"].Visible = false;
            dgvDispatchTable.Columns["partialItem"].Visible = false;
            dgvDispatchTable.Columns["partialItem"].Visible = Program.Settings.Data.stationType;    //false - nie rolka
            
            dgvDispatchTable.Columns["dispachName"].HeaderText = "Nazwa";
            dgvDispatchTable.Columns["dispachAddress"].HeaderText = "Adres";
            dgvDispatchTable.Columns["motives"].HeaderText = "M";
            dgvDispatchTable.Columns["dispachJdNrLine"].HeaderText = "Karta/Linia";
            dgvDispatchTable.Columns["partialItem"].HeaderText = "P.CZ.";
            dgvDispatchTable.Columns["qtyToPack"].HeaderText = "Ile";
            dgvDispatchTable.Columns["qtyPacked"].HeaderText = "Pak.";
            dgvDispatchTable.Columns["dispach_tiles"].HeaderText = "Bryty";
            //dgvDispatchTable.Columns["partialItem"].HeaderText = "P";
            dgvDispatchTable.Columns["motives"].HeaderText = "Mot.";


            dgvDispatchTable.Columns["dispachJdNrLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["qtyToPack"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["qtyPacked"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["dispach_tiles"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvDispatchTable.Columns["partialItem"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["motives"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvDispatchTable.Columns["dispachName"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["dispachAddress"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["dispachJdNrLine"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["qtyToPack"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["qtyPacked"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["partialItem"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["dispach_tiles"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvDispatchTable.Columns["motives"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvDispatchTable.Columns["dispachName"].Width = 250;
            dgvDispatchTable.Columns["dispachAddress"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvDispatchTable.Columns["dispachJdNrLine"].Width = 120;
            dgvDispatchTable.Columns["qtyToPack"].Width = 80;
            dgvDispatchTable.Columns["qtyPacked"].Width = 80;
            //dgvDispatchTable.Columns["partialItem"].Width = 80;
            if (Program.Settings.Data.stationType == global.stationTypeNotRolka) //nie rolka
            {
                dgvDispatchTable.Columns["motives"].Width = 80;
                dgvDispatchTable.Columns["dispach_tiles"].Width = 0;
            }
            else
            {
                dgvDispatchTable.Columns["dispach_tiles"].Width = 240;
                dgvDispatchTable.Columns["motives"].Width = 240;
            }

            dgvDispatchTable.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }

        /// <summary>
        /// Gets data of package after scanning ZPL code (used just once after scanning ZPL code)
        /// </summary>
        /// <param name="readZPL"></param>
        public void findSubpackToZPL(string readZPL)
        {
            Program.autojetFinishingMain.Hm.Info("findSubpackToZPL(), start");
            global.zplData = unpackZPL(readZPL);
            if (global.zplData == null)
            {
                Program.autojetFinishingMain.Hm.Info($"findSubpackToZPL(), zplData == null");
                return;
            }

            //sprawdzenie, czy wysyłka jest z magazynu:
            bool isStroer = dispachDataAcces.isStroer(global.zplData);

            global.subpackList = dispachDataAcces.findAllDispachAddressAndItemForZPL(global.zplData, Program.Settings.Data.stationId);
            Program.autojetFinishingMain.Hm.Info($"findSubpackToZPL(), findAllDispachAddressAndItemForZPL done, global.subpackList.count={global.subpackList.Count}");

            //czy jest to tryb na wysyłkę czy z magazynu (stroer)?
            if ((Program.Settings.Data.stationType == global.stationTypeNotRolka) && (global.subpackList.Count>0))
            {
                if (global.subpackList[0].item.pack_weight)
                    dispStroerSubpackOn();
                else
                    dispWaybillSubpackOn();
            }
            else
                dispWaybillSubpackOn();


            dgvDispatchTable.DataSource = global.subpackList;
            formatDispachDgv(null);
        }

        /// <summary>
        /// Turns read ZPL string to global.zplDataType
        /// </summary>
        /// <param name="readZPL"></param>
        /// <returns></returns>
        public global.zplDataType unpackZPL(string readZPL)
        {
            global.zplDataType retVal = new global.zplDataType();

            //
            //string referenceIDLong = readZPL.Substring(0, 22);
            int lastIndexOfK = readZPL.LastIndexOf("K");
            int lastIndexOfC = readZPL.LastIndexOf("C");
            //retVal.referenceID = referenceIDLong.Substring(lastIndexOfX + 1, referenceIDLong.Length - lastIndexOfX - 1);

            Program.autojetFinishingMain.Hm.Info($"unpackZPL(), readZPL = {readZPL}");
            //Zmiana 2018-12-06

            if (lastIndexOfC>0 || lastIndexOfK>0)
            {
                readZPL = readZPL.Substring(0, readZPL.Length - 1);
            }

            string query = string.Format("SELECT reference_id,ref_from,ref_to,subpack_from,subpack_to from dispatch_item_table where barcode_data = '{0}' ",readZPL);
            query = query;

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                retVal.referenceID = dTabel.Rows[0]["reference_id"].ToString();
                retVal.ref_from = int.Parse(dTabel.Rows[0]["ref_from"].ToString());
                retVal.ref_to = int.Parse(dTabel.Rows[0]["ref_to"].ToString());
                retVal.subpack_from = int.Parse(dTabel.Rows[0]["subpack_from"].ToString());
                retVal.subpack_to = int.Parse(dTabel.Rows[0]["subpack_to"].ToString());

                if (lastIndexOfC > 0)
                {
                    retVal.zplType = "C";
                }
                else if(lastIndexOfK > 0)
                {
                    retVal.zplType = "K";
                }

                Program.autojetFinishingMain.Hm.Info($"unpackZPL(), retVal.referenceID = {retVal.referenceID}");
            }
            else
            {
                query = string.Format("SELECT reference_id,ref_from,ref_to,subpack_from,subpack_to from dispatch_item_table_archive where barcode_data = '{0}' ", readZPL);
                dTabel = SQLConnection.GetData(query);

                if (dTabel.Rows.Count > 0)
                {
                    retVal.referenceID = dTabel.Rows[0]["reference_id"].ToString();
                    retVal.ref_from = int.Parse(dTabel.Rows[0]["ref_from"].ToString());
                    retVal.ref_to = int.Parse(dTabel.Rows[0]["ref_to"].ToString());
                    retVal.subpack_from = int.Parse(dTabel.Rows[0]["subpack_from"].ToString());
                    retVal.subpack_to = int.Parse(dTabel.Rows[0]["subpack_to"].ToString());

                    if (lastIndexOfC > 0)
                    {
                        retVal.zplType = "C";
                    }
                    else if (lastIndexOfK > 0)
                    {
                        retVal.zplType = "K";
                    }
                }
            }
            /*try
            {
                retVal.referenceID = readZPL.Substring(0, 22);  //constatn lenght of 22 characters
                retVal.ref_from = int.Parse(readZPL.Substring(22, 2));
                retVal.ref_to = int.Parse(readZPL.Substring(24, 2));
                retVal.subpack_from = int.Parse(readZPL.Substring(26, 2));
                retVal.subpack_to = int.Parse(readZPL.Substring(28, 2));
                retVal.zplType = readZPL.Substring(30, 1);

                Program.autojetFinishingMain.Hm.Info($"unpackZPL(), retVal.referenceID = {retVal.referenceID}");
            }
            catch (Exception e)
            {
                return new global.zplDataType();
            }*/

            return retVal;
        }

        public bool isPosterOnSubpackList(List<FinishingDispatchAddressAndItem> subpackList, string jdline, string jdnr)
        {
            Program.autojetFinishingMain.Hm.Info($"isPosterOnSubpackList(), input: jdline={jdline}, jdnr={jdnr}");

            foreach (FinishingDispatchAddressAndItem item in subpackList)
            {
                //Program.autojetFinishingMain.Hm.Info($"isPosterOnSubpackList(), cheking: jdline={item.item.jdline}, jdnr={item.item.jdnr}");
                int itemJdline = int.Parse(item.item.jdline);
                int itemJdnr = int.Parse(item.item.jdnr);

                int cmpJdline = int.Parse(jdline);
                int cmpJdnr = int.Parse(jdnr);

                //if (item.item.jdline.Equals(jdline) && item.item.jdnr.Equals(jdnr))
                if ((cmpJdline == itemJdline) && (cmpJdnr == itemJdnr) && (!item.item.completation_done))
                {
                    Program.autojetFinishingMain.Hm.Info($"isPosterOnSubpackList(), OK");
                    return true;
                }
            }
            return false;
        }

        public void findDispachForScan(string recStr)
        {
            global.zplDataType zpl = new global.zplDataType();

            //wyznaczenie referenceId, ref_form i ref_to do zmiennej global.zplDataType zpl;
            if (recStr.Length == 8)  //z kodu kreskoweg z brytu
            {
                int intOut;
                if (!int.TryParse(recStr, out intOut))
                {
                    Program.autojetFinishingMain.Hm.Info("findDispachForScan(), string is not proper reference ID");
                    return;
                }
                List<string> barcodes = new List<string>();
                barcodes.Add(recStr);
                List<string> barcode = getBarcodedataDB(barcodes);

                scanItem item = new scanItem();
                if (barcode.Count > 0)
                    item = scanDataToScanItem(barcode[0]);
                else
                    return;

                zpl = dispachDataAcces.GetReferenceFromFinishing(item);

                if (zpl.referenceID == null)
                {
                    Program.autojetFinishingMain.Hm.Info("findDispachForScan(), dispachDataAcces.GetReferenceFromFinishing==false");
                    zpl = dispachDataAcces.GetReferenceFromFinishing2(item);
                    if (zpl.referenceID == null)
                    {
                        Program.autojetFinishingMain.Hm.Info("findDispachForScan(), dispachDataAcces.GetReferenceFromFinishing2==false");
                    }
                    else
                    {
                        Program.autojetFinishingMain.Hm.Info("findDispachForScan(), dispachDataAcces.GetReferenceFromFinishing2==true");
                    }
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info("findDispachForScan(), dispachDataAcces.GetReferenceFromFinishing==true");
                }
            }
            else if (recStr.Length >= 30) 
                /*|| (recStr.Length == 31))  //kod ZPL z etykiety częściowej*/
            {
                //kod z etykiety ZPL, 
                zpl = unpackZPL(recStr);
                if (zpl.referenceID == null)
                {
                    Program.autojetFinishingMain.Hm.Info("findDispachForScan(), unpackZPL==false");
                }
                else
                {
                    Program.autojetFinishingMain.Hm.Info("findDispachForScan(), unpackZPL==true");
                }
            }

            //drukowanie etykiety jeśli mamy dane o wywsyłce
            if (zpl.referenceID == null)
            {
                Program.autojetFinishingMain.Hm.Info("findDispachForScan(), referenceID == null");
                return;
            }
            else
            {
                //wyswietlenie danych na formularzu
                lRepintRefId.Text = zpl.referenceID;
                lRepintRefFrom.Text = zpl.ref_from.ToString();
                lRepintRefTo.Text = zpl.ref_to.ToString();
                //string fileName = dispachDataAcces.GetFilenameFromReference(zpl);

                //pobieranie danych o wysyłce
                //List<FinishingDispatchAddressAndItem> allItems = dispachDataAcces.GetAllAdresAndItemFromReference(zpl);
                FinishingDispatchAddressAndItem item = dispachDataAcces.GetAdresAndItemFromReference(zpl);
                if (item.item.qty_packed >= item.item.qty_to_pack)
                {
                    Program.autojetFinishingMain.Hm.Info("findDispachForScan(), printing label...");
                    printDispachLabel(item, zpl, item.item.weight_act);
                }
                else
                { }
            }
        }

        /// <summary>
        /// same as printDispachLabel() but for parameters passing in reference
        /// </summary>
        public void printDispachLabel(FinishingDispatchAddressAndItem item, global.zplDataType zpl, int weight = 0)
        {
            if (item.item.partial_item)
            {
                //paczka składa się z części:
                Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=true");
                
                if (zpl == null)
                {
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), zpl == null");
                    zpl = new global.zplDataType();
                    //jesli brak danych w zpl to pobieram je z item,
                    zpl.referenceID = item.item.reference_id;
                    zpl.ref_from = item.item.ref_from;
                    zpl.ref_to = item.item.ref_to;
                    zpl.subpack_from = item.item.subpack_from;
                    zpl.subpack_to = item.item.subpack_to;
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), zpl updated");
                }

                if (zpl.referenceID == null)
                {
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), zpl == null");
                    zpl = new global.zplDataType();
                    //jesli brak danych w zpl to pobieram je z item,
                    zpl.referenceID = item.item.reference_id;
                    zpl.ref_from = item.item.ref_from;
                    zpl.ref_to = item.item.ref_to;
                    zpl.subpack_from = item.item.subpack_from;
                    zpl.subpack_to = item.item.subpack_to;
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), zpl updated");
                }

                //w gdy paczka jest partial_item=true ale jestem w trybie dispModeWaybill lub dispModeStroer to nie sprawdzam checkPartialPackageComplete bo to nigdy nie może mieć miejsca! (rozmowa z p. Łukaszem 19.04.2018)
                if (((global.labelReprintMode) && (!checkPartialPackageComplete(zpl))) ||
                    ((!global.labelReprintMode) && ((global.dispachMode == global.dispModeWaybill) || (global.dispachMode == global.dispModeStroer) || (!checkPartialPackageComplete(zpl)))))
                {
                    //nie wszystkie części spakowane
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), checkPartialPackageComplete=false");
                    //SendToPrinterZPLPartial(item);
                    SendToPrinterZPLPartialWeight(item, weight);
                }
                else
                {
                    //wszystkie części spakowane
                    //druk etykiety podstawowej:
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), checkPartialPackageComplete=true");
                    if ((item.address.file_name.Length == 0) || (item.address.file_name.Equals("NONE")))
                    {
                        //nie ma informacji o pliku w bazie danych:
                        Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=true, no file in db");
                        //if (Program.Settings.Data.stationType == false)
                        //{
                        //    SendToPrinterZPL(item);
                        //    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_itemSendToPrinterZPL");
                        //}
                        //else
                        //{
                        //    SendToPrinterZPLWeight(item, weight);
                        //    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), SendToPrinterZPLWeight");
                        //}
                        SendToPrinterZPLWeight(item, weight);
                        Program.autojetFinishingMain.Hm.Info("printDispachLabel(), SendToPrinterZPLWeight");
                    }
                    else
                    {
                        //jest informacja o pliku w bazie danych:
                        Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=true, filename in db");
                        if (System.IO.File.Exists(System.IO.Path.Combine(Program.Settings.Data.dispachFilePath, item.address.file_name)))
                        {
                            //plik istnieje
                            Program.autojetFinishingMain.Hm.Info($"printDispachLabel(), System.IO.File.Exists()=true");
                            SendToPrinter(System.IO.Path.Combine(Program.Settings.Data.dispachFilePath, item.address.file_name));
                        }
                        else
                        {
                            Program.autojetFinishingMain.Hm.Info($"printDispachLabel(), System.IO.File.Exists()=false");
                            //zgłosić błąd?
                        }
                    }

                    //druk etykiety ams:
                    try
                    {
                        Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=true, ams_file_name");
                        if (System.IO.File.Exists(System.IO.Path.Combine(Program.Settings.Data.dispachFilePath, item.address.ams_file_name)))
                        {
                            //plik istnieje
                            Program.autojetFinishingMain.Hm.Info($"printDispachLabel(), System.IO.File.Exists()=true");
                            SendToPrinter2(System.IO.Path.Combine(Program.Settings.Data.dispachFilePath, item.address.ams_file_name));
                        }
                        else
                        {
                            Program.autojetFinishingMain.Hm.Info($"printDispachLabel(), System.IO.File.Exists()=false");
                            //zgłosić błąd?
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                }
            }
            else
            {
                //paczka nie składa się z części:
                //druk etykiety podstawowej:
                Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=false, ");
                if ((item.address.file_name.Length == 0) || (item.address.file_name.Equals("NONE")))
                {
                    //nie ma informacji o pliku w bazie danych:
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=false, no file in db");
                    //if (Program.Settings.Data.stationType == false)
                    //{
                    //    SendToPrinterZPL(item);
                    //    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_itemSendToPrinterZPL");
                    //}
                    //else
                    //{
                    //    SendToPrinterZPLWeight(item, weight);
                    //    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), SendToPrinterZPLWeight");
                    //}
                    SendToPrinterZPLWeight(item, weight);
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), SendToPrinterZPLWeight");
                }
                else
                {
                    //jest informacja o pliku w bazie danych:
                    Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=false, filename in db");
                    if (System.IO.File.Exists(System.IO.Path.Combine(Program.Settings.Data.dispachFilePath, item.address.file_name)))
                    {
                        //plik istnieje
                        Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=false, System.IO.File.Exists()=true");
                        SendToPrinter(System.IO.Path.Combine(Program.Settings.Data.dispachFilePath, item.address.file_name));
                    }
                    else
                    {
                        Program.autojetFinishingMain.Hm.Info($"printDispachLabel(), System.IO.File.Exists()=false");
                        //zgłosić błąd?
                    }
                }

                //druk etykiety ams:
                try
                {
                    Program.autojetFinishingMain.Hm.Info($"printDispachLabel(), partial_item=false, ams_file_name={item.address.ams_file_name}");
                    if (System.IO.File.Exists(System.IO.Path.Combine(Program.Settings.Data.dispachFilePath, item.address.ams_file_name)))
                    {
                        //plik istnieje
                        Program.autojetFinishingMain.Hm.Info("printDispachLabel(), partial_item=false, System.IO.File.Exists()=true");
                        SendToPrinter2(System.IO.Path.Combine(Program.Settings.Data.dispachFilePath, item.address.ams_file_name));
                    }
                    else
                    {
                        Program.autojetFinishingMain.Hm.Info($"printDispachLabel(), System.IO.File.Exists()=false");
                        //zgłosić błąd?
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }
    }
}
