﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Threading;
using System.Diagnostics;
using System.Security.Cryptography;
//using Microsoft.ApplicationInsights;

namespace AutojetFinishing
{
    class utilities
    {
        /// <summary>
        /// Sets the window title.
        /// </summary>
        public static string setWindowTitle()
        {
            return ("AutojetFinishing " + GetProgramVersion());
        }

        public static void logAppStart()
        {
            //TelemetryClient tc = new TelemetryClient();

            //tc.InstrumentationKey = "9ef519a3-22c1-4837-bbf7-3fbf3a2d52f8";

            // Set session data:
            //tc.Context.User.Id = Environment.UserName;
            //tc.Context.Session.Id = Guid.NewGuid().ToString();
            //tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();

            string s = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
            //tc.TrackEvent("AutojetFinishing_" + GetProgramVersion() + "_" + s + "_" + Environment.MachineName);

            //tc.Flush(); // only for desktop apps
        }

        /// <summary>
        /// Gets the program version.
        /// </summary>
        /// <returns>The assembly version in form major.minor.build.revision.</returns>
        public static string GetProgramVersion()
        {
            //string version = Application.ProductVersion;
            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                version = ad.CurrentVersion.ToString();
            }

            return (version);
        }

        /// <summary>
        /// chceck if procss is alredy running. 
        /// </summary>
        /// <returns>tru if process is running</returns>
        public static bool chceckMultipleInstatnce()
        {
            String thisprocessname = Process.GetCurrentProcess().ProcessName;

            if (Process.GetProcesses().Count(p => p.ProcessName == thisprocessname) > 1)
                return true;
            else
                return
                    false;
        }

        /// <summary>
        /// http://stackoverflow.com/questions/11800958/using-ping-in-c-sharp
        /// </summary>
        /// <param name="nameOrAddress"></param>
        /// <returns></returns>
        public static bool PingHost(string nameOrAddress)
        {
            if (nameOrAddress == null)
                return false;

            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        
        /// <summary>
        /// main method for threatS
        /// http://stackoverflow.com/questions/661561/how-to-update-the-gui-from-another-thread-in-c
        /// </summary>
        public static void pingPlcMain()
        {
            bool plcPingOk = false;

            while (true)
            {
                Thread.Sleep(10);
                
                plcPingOk = PingHost(Program.Settings.Data.plcIp);
                Program.autojetFinishingMain.Invoke((MethodInvoker)delegate
                {
                    Program.autojetFinishingMain.plcPingOk = plcPingOk;    //;
                    //Program.autojetFinishingMain.ubdateUIFromThreat();
                    //Program.autojetSZKKMain.lPingRes.Text = plcPingOk.ToString(); // runs on UI thread
                });
            }
        }

        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }

    
}
