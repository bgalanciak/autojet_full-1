﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Globalization;
using System.ComponentModel;

namespace AutojetFinishing
{
    class windowsPrinter
    {

    }

    public partial class autojetFinishingMainForm
    {

        private void SendToPrinter(string path)
        {
            bwPrintPdf.RunWorkerAsync(path);
            Program.autojetFinishingMain.Hm.Info("SendToPrinter(): done");

            //System.ComponentModel.BackgroundWorker bwPrintObj = new BackgroundWorker();
            //bwPrintObj.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwPrintPdf_DoWork);
            //bwPrintObj.RunWorkerAsync(path);
            //Program.autojetFinishingMain.Hm.Info("SendToPrinter(): done");
        }

        private void SendToPrinter2(string path)
        {
            bwPrintPdf2.RunWorkerAsync(path);
            Program.autojetFinishingMain.Hm.Info("SendToPrinter2(): done");

            //System.ComponentModel.BackgroundWorker bwPrintObj = new BackgroundWorker();
            //bwPrintObj.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwPrintPdf_DoWork);
            //bwPrintObj.RunWorkerAsync(path);
            //Program.autojetFinishingMain.Hm.Info("SendToPrinter(): done");
        }

        private void bwPrintPdf_DoWork(object sender, DoWorkEventArgs e)
        {
            string path = e.Argument as string;

            ProcessStartInfo info = new ProcessStartInfo();
            info.Verb = "print";
            info.FileName = path; //@"e:\Install\PC_Soft\AutoJet\autojetSZKK\pdfFiles\FEDEX.PDF";
            info.CreateNoWindow = true;
            info.WindowStyle = ProcessWindowStyle.Hidden;

            Process p = new Process();
            p.StartInfo = info;
            p.Start();

            p.WaitForInputIdle();
            System.Threading.Thread.Sleep(3000);
            if (false == p.CloseMainWindow())
                p.Kill();
        }

        private void SendToPrinterZPL(FinishingDispatchAddressAndItem addres)
        {
            //http://labelary.com/viewer.html
            /*
            ^XA

            ^CF0,60
            ^FO50,50^FDPrint & Display (Polska) Sp. z o.o.^FS
            ^CF0,40
            ^FO50,100^FD_^FS
            ^FO50,150^GB700,1,3^FS
            
            ^FX Second section with recipient address and permit information.
            ^CFA,30
            ^FO50,200^FDKurier:^FS
            ^FO50,240^FDNazwa:^FS
            ^FO50,280^FD_^FS
            ^FO50,320^FD_^FS
            ^FO50,360^FDAdres:^FS
            ^FO50,400^FD_^FS
            ^FO50,440^FDZwartosc:^FS
            ^FO50,480^FDInfo: ^FS
            ^FO50,520^FDIlosc: ^FS
            ^FO50,560^FDNr listu:  ^FS
            ^FO50,600^FDData wys.: ^FS
            ^FO50,640^FDData pak.: ^FS
            
            ^FO250,200^FDKurier:^FS
            ^FO250,240^FDNazwa:^FS
            ^FO250,280^FD_^FS
            ^FO250,320^FD_^FS
            ^FO250,360^FDAdres:^FS
            ^FO250,400^FD_^FS
            ^FO250,440^FDZwartosc:^FS
            ^FO250,480^FDInfo: ^FS
            ^FO250,520^FDIlosc: ^FS
            ^FO250,560^FDNr listu:  ^FS
            ^FO250,600^FDData wys.: ^FS
            ^FO250,640^FDData pak.: ^FS
            
            ^CFA,15
            
            ^FO50,700^GB700,1,3^FS
            
            ^FX Third section with barcode.
            ^BY5,2,170
            ^FO175,750^BC^FD1234567890^FS
            
            ^XZ
             */
            /*
            Kurier: – courier_type
            Nazwa: - dispatch_company + ‘ ‘ + dispatch_name + ‘ ‘ + dispatch_surname
            Adres: - city + ‘ ‘ + post_code + ‘ ‘ +street
            ‘ ‘
            Zwartość: jdnr + ‘ ‘+ jdline +’ ‘+ pack_description
            Inf: pack_type + ‘ ‘ + subpack_from + ‘ z ‘ + subpack_to
            Ilość: qty_packed
            ‘ ‘
            Nr listu: Reference_id + ‘ ‘ ref_from + ‘ : ‘ + ref_to
            Data wys: dispatch_date
            Data pak: now() – tylko data bez godziny I minut
            ‘ ‘
             */
            string zawartoscP1, zawartoscP2, zawartoscP3, zawartoscP4;
            string zawartoscAll = $"{addres.item.jdnr} {addres.item.jdline} {addres.item.pack_description}";
            zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            zawartoscP2 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(30, zawartoscAll.Length - 30) : "";
            zawartoscP3 = zawartoscAll.Length > 60 ? zawartoscAll.Substring(60, zawartoscAll.Length - 60) : "";
            zawartoscP4 = zawartoscAll.Length > 90 ? zawartoscAll.Substring(90, zawartoscAll.Length - 90) : "";

            //zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            zawartoscP2 = zawartoscP2.Length > 30 ? zawartoscP2.Substring(0, 30) : zawartoscP2;
            zawartoscP3 = zawartoscP3.Length > 30 ? zawartoscP3.Substring(0, 30) : zawartoscP3;
            zawartoscP4 = zawartoscP4.Length > 30 ? zawartoscP4.Substring(0, 30) : zawartoscP4;

            string label = "^XA";
            label += "^CF0,60,50";
            label += "^FO50,50^FDPrint & Display (Polska) Sp. z o.o.^FS";
            label += "^CF0,40";
            label += "^FO50,100^FD^FS";
            label += "^FO50,100^GB700,1,3^FS";
            label += "^FX Second section with recipient address and permit information.";
            label += "^CF0,35";
            label += "^FO50,120^FDKurier:^FS";
            label += "^FO50,160^FDOdbiorca:^FS";
            label += "^FO50,200^FD_^FS";
            label += "^FO50,240^FD_^FS";
            label += "^FO50,280^FDAdres:^FS";
            label += "^FO50,320^FD_^FS";
            label += "^FO50,360^FDOpis:^FS";
            label += "^FO50,600^FDIlosc: ^FS";
            label += "^FO50,640^FDInfo: ^FS";
            label += "^FO50,680^FDNr listu:  ^FS";
            label += "^FO50,720^FDData wys.: ^FS";
            //label += "^FO50,640^FDData pak.: ^FS";
            label += $"^FO190,120^FD{addres.address.courier_type}^FS";
            label += $"^FO190,160^FD{addres.address.dispatch_company}^FS";
            label += $"^FO190,200^FD{addres.address.dispatch_name}^FS";
            label += $"^FO190,240^FD{addres.address.dispatch_surname}^FS";
            label += $"^FO190,280^FD{addres.address.city} {addres.address.post_code}^FS";
            label += $"^FO190,320^FD{addres.address.street}^FS";
            label += "^CF0,65,40";
            label += $"^FO190,360^FD{zawartoscP1}^FS";
            label += $"^FO190,420^FD{zawartoscP2}^FS";
            label += $"^FO190,480^FD{zawartoscP3}^FS";
            label += $"^FO190,540^FD{zawartoscP4}^FS";
            label += "^CF0,35";
            label += $"^FO190,600^FD{addres.item.qty_packed} ^FS";
            label += $"^FO190,640^FD{addres.address.pack_type} {addres.item.subpack_from} z {addres.item.subpack_to}^FS";
            label += $"^FO190,680^FD{addres.address.reference_id} {addres.address.ref_from} {addres.address.ref_to}^FS";
            label += $"^FO190,720^FD{addres.address.dispatch_date.ToString("dd.MM.yyyy")}^FS";
            //label += $"^FO250,640^FD{DateTime.Now.ToString("dd.MM.yyyy")}^FS";
            label += "^CFA,15";
            label += "^FO50,780^GB700,1,3^FS";

            label += "^FX Third section with barcode.";
            label += "^BY2,2,170";
            label += $"^FO55,830^BC,170,Y,N,N,A^FD{addres.address.reference_id}{addres.address.ref_from.ToString("D2")}{addres.address.ref_to.ToString("D2")}{addres.item.subpack_from.ToString("D2")}{addres.item.subpack_to.ToString("D2")}^FS";
            
            label += "^CF0,35";
            label += "^FO50,1100^FDUWAGI:^FS";
            label += $"^FO190,1100^FD{addres.address.comments}^FS";

            label += "^XZ";

            label = RemoveDiacritics(label);

            PrintDialog pd = new PrintDialog();
            pd.PrinterSettings = new PrinterSettings();

            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, label);
        }

        /// <summary>
        /// Etykieta końcowa na paczki które nie mają etykiety pdf
        /// </summary>
        /// <param name="addres"></param>
        /// <param name="weight"></param>
        private void SendToPrinterZPLWeight(FinishingDispatchAddressAndItem addres, int weight)
        {
            //http://labelary.com/viewer.html
            /*
            ^XA

            ^CF0,60
            ^FO50,50^FDPrint & Display (Polska) Sp. z o.o.^FS
            ^CF0,40
            ^FO50,100^FD_^FS
            ^FO50,150^GB700,1,3^FS
            
            ^FX Second section with recipient address and permit information.
            ^CFA,30
            ^FO50,200^FDKurier:^FS
            ^FO50,240^FDNazwa:^FS
            ^FO50,280^FD_^FS
            ^FO50,320^FD_^FS
            ^FO50,360^FDAdres:^FS
            ^FO50,400^FD_^FS
            ^FO50,440^FDZwartosc:^FS
            ^FO50,480^FDInfo: ^FS
            ^FO50,520^FDIlosc: ^FS
            ^FO50,560^FDNr listu:  ^FS
            ^FO50,600^FDData wys.: ^FS
            ^FO50,640^FDData pak.: ^FS
            
            ^FO250,200^FDKurier:^FS
            ^FO250,240^FDNazwa:^FS
            ^FO250,280^FD_^FS
            ^FO250,320^FD_^FS
            ^FO250,360^FDAdres:^FS
            ^FO250,400^FD_^FS
            ^FO250,440^FDZwartosc:^FS
            ^FO250,480^FDInfo: ^FS
            ^FO250,520^FDIlosc: ^FS
            ^FO250,560^FDNr listu:  ^FS
            ^FO250,600^FDData wys.: ^FS
            ^FO250,640^FDData pak.: ^FS
            
            ^CFA,15
            
            ^FO50,700^GB700,1,3^FS
            
            ^FX Third section with barcode.
            ^BY5,2,170
            ^FO175,750^BC^FD1234567890^FS
            
            ^XZ
             */
            /*
            Kurier: – courier_type
            Nazwa: - dispatch_company + ‘ ‘ + dispatch_name + ‘ ‘ + dispatch_surname
            Adres: - city + ‘ ‘ + post_code + ‘ ‘ +street
            ‘ ‘
            Zwartość: jdnr + ‘ ‘+ jdline +’ ‘+ pack_description
            Inf: pack_type + ‘ ‘ + subpack_from + ‘ z ‘ + subpack_to
            Ilość: qty_packed
            ‘ ‘
            Nr listu: Reference_id + ‘ ‘ ref_from + ‘ : ‘ + ref_to
            Data wys: dispatch_date
            Data pak: now() – tylko data bez godziny I minut
            ‘ ‘
             */
            string zawartoscP1, zawartoscP2, zawartoscP3, zawartoscP4;
            string zawartoscAll = $"{addres.item.jdnr} {addres.item.jdline} {addres.item.pack_description}";
            zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            zawartoscP2 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(30, zawartoscAll.Length - 30) : "";
            zawartoscP3 = zawartoscAll.Length > 60 ? zawartoscAll.Substring(60, zawartoscAll.Length - 60) : "";
            zawartoscP4 = zawartoscAll.Length > 90 ? zawartoscAll.Substring(90, zawartoscAll.Length - 90) : "";

            //zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            zawartoscP2 = zawartoscP2.Length > 30 ? zawartoscP2.Substring(0, 30) : zawartoscP2;
            zawartoscP3 = zawartoscP3.Length > 30 ? zawartoscP3.Substring(0, 30) : zawartoscP3;
            zawartoscP4 = zawartoscP4.Length > 30 ? zawartoscP4.Substring(0, 30) : zawartoscP4;

            string label = "^XA";
            label += "^CF0,60,50";
            label += "^FO50,50^FDPrint & Display (Polska) Sp. z o.o.^FS";
            label += "^CF0,40";
            label += "^FO50,100^FD^FS";
            label += "^FO50,100^GB700,1,3^FS";
            label += "^FX Second section with recipient address and permit information.";
            label += "^CF0,35";
            label += "^FO50,120^FDKurier:^FS";
            label += "^FO50,160^FDOdbiorca:^FS";
            label += "^FO50,200^FD_^FS";
            label += "^FO50,240^FD_^FS";
            label += "^FO50,280^FDAdres:^FS";
            label += "^FO50,320^FD_^FS";
            label += "^FO50,360^FDOpis:^FS";
            label += "^FO50,600^FDIlosc: ^FS";
            label += "^FO50,640^FDInfo: ^FS";
            label += "^FO450,720^FDWaga: ^FS";
            label += "^FO50,680^FDNr listu:  ^FS";
            label += "^FO50,720^FDData wys.: ^FS";
            //label += "^FO50,640^FDData pak.: ^FS";
            label += $"^FO190,120^FD{addres.address.courier_type}^FS";
            label += $"^FO190,160^FD{addres.address.dispatch_company}^FS";
            label += $"^FO190,200^FD{addres.address.dispatch_name}^FS";
            label += $"^FO190,240^FD{addres.address.dispatch_surname}^FS";
            label += $"^FO190,280^FD{addres.address.city} {addres.address.post_code}^FS";
            label += $"^FO190,320^FD{addres.address.street}^FS";
            label += "^CF0,65,40";
            label += $"^FO190,360^FD{zawartoscP1}^FS";
            label += $"^FO190,420^FD{zawartoscP2}^FS";
            label += $"^FO190,480^FD{zawartoscP3}^FS";
            label += $"^FO190,540^FD{zawartoscP4}^FS";
            label += "^CF0,35";
            label += $"^FO190,600^FD{addres.item.qty_packed} ^FS";
            //label += $"^FO190,640^FD{addres.address.pack_type} {addres.item.subpack_from} z {addres.item.subpack_to}^FS";
            label += $"^FO190,640^FD{addres.address.pack_type} {addres.item.ref_from} z {addres.item.ref_to}, czesc {addres.item.subpack_from} z {addres.item.subpack_to}^FS";
            //label += $"^FO600,640^FD{weight} [g]^FS";
            label += $"^FO190,680^FD{addres.address.reference_id} {addres.address.ref_from} {addres.address.ref_to}^FS";
            label += $"^FO190,720^FD{addres.address.dispatch_date.ToString("dd.MM.yyyy")}^FS";
            label += $"^FO570,720^FD{weight} [g]^FS";
            //label += $"^FO250,640^FD{DateTime.Now.ToString("dd.MM.yyyy")}^FS";
            label += "^CFA,15";
            label += "^FO50,780^GB700,1,3^FS";

            label += "^FX Third section with barcode.";
            label += "^BY2,2,170";
            label += $"^FO55,830^BC,170,Y,N,N,A^FD{addres.address.reference_id}{addres.address.ref_from.ToString("D2")}{addres.address.ref_to.ToString("D2")}{addres.item.subpack_from.ToString("D2")}{addres.item.subpack_to.ToString("D2")}K^FS";

            label += "^CF0,35";
            label += "^FO50,1100^FDUWAGI:^FS";
            label += $"^FO190,1100^FD{addres.address.comments}^FS";

            label += "^XZ";

            label = RemoveDiacritics(label);

            PrintDialog pd = new PrintDialog();
            pd.PrinterSettings = new PrinterSettings();

            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, label);

        }

        /// <summary>
        /// Etykieta częśćiowa (na części paczki składającej się z kilku podpaczek)
        /// </summary>
        /// <param name="addres"></param>
        private void SendToPrinterZPLPartial(FinishingDispatchAddressAndItem addres)
        {
            //http://labelary.com/viewer.html
            /*
            ^XA

            ^CF0,60
            ^FO50,50^FDPrint & Display (Polska) Sp. z o.o.^FS
            ^CF0,40
            ^FO50,100^FD_^FS
            ^FO50,150^GB700,1,3^FS
            
            ^FX Second section with recipient address and permit information.
            ^CFA,30
            ^FO50,200^FDKurier:^FS
            ^FO50,240^FDNazwa:^FS
            ^FO50,280^FD_^FS
            ^FO50,320^FD_^FS
            ^FO50,360^FDAdres:^FS
            ^FO50,400^FD_^FS
            ^FO50,440^FDZwartosc:^FS
            ^FO50,480^FDInfo: ^FS
            ^FO50,520^FDIlosc: ^FS
            ^FO50,560^FDNr listu:  ^FS
            ^FO50,600^FDData wys.: ^FS
            ^FO50,640^FDData pak.: ^FS
            
            ^FO250,200^FDKurier:^FS
            ^FO250,240^FDNazwa:^FS
            ^FO250,280^FD_^FS
            ^FO250,320^FD_^FS
            ^FO250,360^FDAdres:^FS
            ^FO250,400^FD_^FS
            ^FO250,440^FDZwartosc:^FS
            ^FO250,480^FDInfo: ^FS
            ^FO250,520^FDIlosc: ^FS
            ^FO250,560^FDNr listu:  ^FS
            ^FO250,600^FDData wys.: ^FS
            ^FO250,640^FDData pak.: ^FS
            
            ^CFA,15
            
            ^FO50,700^GB700,1,3^FS
            
            ^FX Third section with barcode.
            ^BY5,2,170
            ^FO175,750^BC^FD1234567890^FS
            
            ^XZ
             */
            /*
            Kurier: – courier_type
            Nazwa: - dispatch_company + ‘ ‘ + dispatch_name + ‘ ‘ + dispatch_surname
            Adres: - city + ‘ ‘ + post_code + ‘ ‘ +street
            ‘ ‘
            Zwartość: jdnr + ‘ ‘+ jdline +’ ‘+ pack_description
            Inf: pack_type + ‘ ‘ + subpack_from + ‘ z ‘ + subpack_to
            Ilość: qty_packed
            ‘ ‘
            Nr listu: Reference_id + ‘ ‘ ref_from + ‘ : ‘ + ref_to
            Data wys: dispatch_date
            Data pak: now() – tylko data bez godziny I minut
            ‘ ‘
             */

            //if (zawartoscAll.Length > 30)
            //{
            //    zawartoscP1 = zawartoscAll.Substring(0, 30);
            //}
            //else
            //{
            //    zawartoscP1 = zawartoscAll;
            //}

            //if (zawartoscAll.Length <= 30)
            //{
            //    zawartoscP2 = "";
            //}
            //else if ((zawartoscAll.Length > 30) && (zawartoscAll.Length <= 60))
            //{
            //    zawartoscP2 = zawartoscAll.Substring(30, zawartoscAll.Length - 30);
            //}
            //else
            //{
            //    zawartoscP2 = zawartoscAll.Substring(30, 30);
            //}

            //if (zawartoscAll.Length <= 60)
            //{
            //    zawartoscP3 = "";
            //}
            //else if ((zawartoscAll.Length > 60) && (zawartoscAll.Length <= 90))
            //{
            //    zawartoscP3 = zawartoscAll.Substring(60, zawartoscAll.Length - 60);
            //}
            //else
            //{
            //    zawartoscP3 = zawartoscAll.Substring(60, 30);
            //}

            //if (zawartoscAll.Length <= 90)
            //{
            //    zawartoscP4 = "";
            //}
            //else if ((zawartoscAll.Length > 90) && (zawartoscAll.Length <= 120))
            //{
            //    zawartoscP4 = zawartoscAll.Substring(90, zawartoscAll.Length - 90);
            //}
            //else
            //{
            //    zawartoscP4 = zawartoscAll.Substring(90, 30);
            //}
            //zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            //zawartoscP2 = zawartoscAll.Length > 60 ? zawartoscAll.Substring(30, 30) : zawartoscAll.Substring(30, zawartoscAll.Length - 30);
            //zawartoscP3 = zawartoscAll.Length > 90 ? zawartoscAll.Substring(60, 30) : zawartoscAll.Substring(60, zawartoscAll.Length - 30);
            //zawartoscP4 = zawartoscAll.Length > 120 ? zawartoscAll.Substring(90, 30) : zawartoscAll.Substring(90, zawartoscAll.Length - 30);

            string zawartoscP1, zawartoscP2, zawartoscP3, zawartoscP4;
            string zawartoscAll = $"{addres.item.jdnr} {addres.item.jdline} {addres.item.pack_description}";
            zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            zawartoscP2 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(30, zawartoscAll.Length - 30) : "";
            zawartoscP3 = zawartoscAll.Length > 60 ? zawartoscAll.Substring(60, zawartoscAll.Length - 60) : "";
            zawartoscP4 = zawartoscAll.Length > 90 ? zawartoscAll.Substring(90, zawartoscAll.Length - 90) : "";

            //zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            zawartoscP2 = zawartoscP2.Length > 30 ? zawartoscP2.Substring(0, 30) : zawartoscP2;
            zawartoscP3 = zawartoscP3.Length > 30 ? zawartoscP3.Substring(0, 30) : zawartoscP3;
            zawartoscP4 = zawartoscP4.Length > 30 ? zawartoscP4.Substring(0, 30) : zawartoscP4;

            string label = "^XA";
            label += "^CF0,60,50";
            label += "^FO50,50^FDCzesciowa^FS";
            label += "^CF0,40";
            label += "^FO50,100^FD^FS";
            label += "^FO50,100^GB700,1,3^FS";
            label += "^FX Second section with recipient address and permit information.";
            label += "^CF0,35";
            label += "^FO50,120^FDKurier:^FS";
            label += "^FO50,160^FDOdbiorca:^FS";
            label += "^FO50,200^FD_^FS";
            label += "^FO50,240^FD_^FS";
            label += "^FO50,280^FDAdres:^FS";
            label += "^FO50,320^FD_^FS";
            label += "^FO50,360^FDOpis:^FS";
            label += "^FO50,600^FDIlosc: ^FS";
            label += "^FO450,600^FDMotywy: ^FS";
            label += "^FO50,640^FDInfo: ^FS";
            label += "^FO50,680^FDNr listu:  ^FS";
            label += "^FO50,720^FDData wys.: ^FS";
            //label += "^FO50,640^FDData pak.: ^FS";
            label += $"^FO190,120^FD{addres.address.courier_type}^FS";
            label += $"^FO190,160^FD{addres.address.dispatch_company}^FS";
            label += $"^FO190,200^FD{addres.address.dispatch_name}^FS";
            label += $"^FO190,240^FD{addres.address.dispatch_surname}^FS";
            label += $"^FO190,280^FD{addres.address.city} {addres.address.post_code}^FS";
            label += $"^FO190,320^FD{addres.address.street}^FS";
            label += "^CF0,65,40";
            label += $"^FO190,360^FD{zawartoscP1}^FS";
            label += $"^FO190,420^FD{zawartoscP2}^FS";
            label += $"^FO190,480^FD{zawartoscP3}^FS";
            label += $"^FO190,540^FD{zawartoscP4}^FS";
            label += "^CF0,35";
            label += $"^FO190,600^FD{addres.item.qty_packed} ^FS";
            label += $"^FO570,600^FD{addres.item.motives} ^FS";
            label += $"^FO190,640^FD{addres.address.pack_type} {addres.item.ref_from} z {addres.item.ref_to}, czesc {addres.item.subpack_from} z {addres.item.subpack_to}^FS";
            label += $"^FO190,680^FD{addres.address.reference_id} {addres.address.ref_from} {addres.address.ref_to}^FS";
            label += $"^FO190,720^FD{addres.address.dispatch_date.ToString("dd.MM.yyyy")}^FS";
            //label += $"^FO250,640^FD{DateTime.Now.ToString("dd.MM.yyyy")}^FS";
            label += "^CFA,15";
            label += "^FO50,780^GB700,1,3^FS";

            label += "^FX Third section with barcode.";
            label += "^BY2,2,170";
            label += $"^FO55,830^BC,170,Y,N,N,A^FD{addres.address.reference_id.PadLeft(22, 'X')}{addres.address.ref_from.ToString("D2")}{addres.address.ref_to.ToString("D2")}{addres.item.subpack_from.ToString("D2")}{addres.item.subpack_to.ToString("D2")}^FS";

            label += "^CF0,35";
            label += "^FO50,1050^FDUWAGI:^FS";
            label += $"^FO190,1050^FD{addres.address.comments}^FS";
            label += "^FO30,20^GB760,1150,3^FS";
            label += "^CF0,60,50";
            label += "^FO50,1100^FDPrint & Display (Polska) Sp. z o.o.^FS";

            label += "^XZ";

            label = RemoveDiacritics(label);

            PrintDialog pd = new PrintDialog();
            pd.PrinterSettings = new PrinterSettings();

            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, label);
        }

        /// <summary>
        /// Etykieta częśćiowa (na części paczki składającej się z kilku podpaczek)
        /// </summary>
        /// <param name="addres"></param>
        /// <param name="weight"></param>
        private void SendToPrinterZPLPartialWeight(FinishingDispatchAddressAndItem addres, int weight)
        {
            //http://labelary.com/viewer.html
            /*
            ^XA

            ^CF0,60
            ^FO50,50^FDPrint & Display (Polska) Sp. z o.o.^FS
            ^CF0,40
            ^FO50,100^FD_^FS
            ^FO50,150^GB700,1,3^FS
            
            ^FX Second section with recipient address and permit information.
            ^CFA,30
            ^FO50,200^FDKurier:^FS
            ^FO50,240^FDNazwa:^FS
            ^FO50,280^FD_^FS
            ^FO50,320^FD_^FS
            ^FO50,360^FDAdres:^FS
            ^FO50,400^FD_^FS
            ^FO50,440^FDZwartosc:^FS
            ^FO50,480^FDInfo: ^FS
            ^FO50,520^FDIlosc: ^FS
            ^FO50,560^FDNr listu:  ^FS
            ^FO50,600^FDData wys.: ^FS
            ^FO50,640^FDData pak.: ^FS
            
            ^FO250,200^FDKurier:^FS
            ^FO250,240^FDNazwa:^FS
            ^FO250,280^FD_^FS
            ^FO250,320^FD_^FS
            ^FO250,360^FDAdres:^FS
            ^FO250,400^FD_^FS
            ^FO250,440^FDZwartosc:^FS
            ^FO250,480^FDInfo: ^FS
            ^FO250,520^FDIlosc: ^FS
            ^FO250,560^FDNr listu:  ^FS
            ^FO250,600^FDData wys.: ^FS
            ^FO250,640^FDData pak.: ^FS
            
            ^CFA,15
            
            ^FO50,700^GB700,1,3^FS
            
            ^FX Third section with barcode.
            ^BY5,2,170
            ^FO175,750^BC^FD1234567890^FS
            
            ^XZ
             */
            /*
            Kurier: – courier_type
            Nazwa: - dispatch_company + ‘ ‘ + dispatch_name + ‘ ‘ + dispatch_surname
            Adres: - city + ‘ ‘ + post_code + ‘ ‘ +street
            ‘ ‘
            Zwartość: jdnr + ‘ ‘+ jdline +’ ‘+ pack_description
            Inf: pack_type + ‘ ‘ + subpack_from + ‘ z ‘ + subpack_to
            Ilość: qty_packed
            ‘ ‘
            Nr listu: Reference_id + ‘ ‘ ref_from + ‘ : ‘ + ref_to
            Data wys: dispatch_date
            Data pak: now() – tylko data bez godziny I minut
            ‘ ‘
             */

            //if (zawartoscAll.Length > 30)
            //{
            //    zawartoscP1 = zawartoscAll.Substring(0, 30);
            //}
            //else
            //{
            //    zawartoscP1 = zawartoscAll;
            //}

            //if (zawartoscAll.Length <= 30)
            //{
            //    zawartoscP2 = "";
            //}
            //else if ((zawartoscAll.Length > 30) && (zawartoscAll.Length <= 60))
            //{
            //    zawartoscP2 = zawartoscAll.Substring(30, zawartoscAll.Length - 30);
            //}
            //else
            //{
            //    zawartoscP2 = zawartoscAll.Substring(30, 30);
            //}

            //if (zawartoscAll.Length <= 60)
            //{
            //    zawartoscP3 = "";
            //}
            //else if ((zawartoscAll.Length > 60) && (zawartoscAll.Length <= 90))
            //{
            //    zawartoscP3 = zawartoscAll.Substring(60, zawartoscAll.Length - 60);
            //}
            //else
            //{
            //    zawartoscP3 = zawartoscAll.Substring(60, 30);
            //}

            //if (zawartoscAll.Length <= 90)
            //{
            //    zawartoscP4 = "";
            //}
            //else if ((zawartoscAll.Length > 90) && (zawartoscAll.Length <= 120))
            //{
            //    zawartoscP4 = zawartoscAll.Substring(90, zawartoscAll.Length - 90);
            //}
            //else
            //{
            //    zawartoscP4 = zawartoscAll.Substring(90, 30);
            //}
            //zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            //zawartoscP2 = zawartoscAll.Length > 60 ? zawartoscAll.Substring(30, 30) : zawartoscAll.Substring(30, zawartoscAll.Length - 30);
            //zawartoscP3 = zawartoscAll.Length > 90 ? zawartoscAll.Substring(60, 30) : zawartoscAll.Substring(60, zawartoscAll.Length - 30);
            //zawartoscP4 = zawartoscAll.Length > 120 ? zawartoscAll.Substring(90, 30) : zawartoscAll.Substring(90, zawartoscAll.Length - 30);

            string zawartoscP1, zawartoscP2, zawartoscP3, zawartoscP4;
            string zawartoscAll = $"{addres.item.jdnr} {addres.item.jdline} {addres.item.pack_description}";
            zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            zawartoscP2 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(30, zawartoscAll.Length - 30) : "";
            zawartoscP3 = zawartoscAll.Length > 60 ? zawartoscAll.Substring(60, zawartoscAll.Length - 60) : "";
            zawartoscP4 = zawartoscAll.Length > 90 ? zawartoscAll.Substring(90, zawartoscAll.Length - 90) : "";

            //zawartoscP1 = zawartoscAll.Length > 30 ? zawartoscAll.Substring(0, 30) : zawartoscAll;
            zawartoscP2 = zawartoscP2.Length > 30 ? zawartoscP2.Substring(0, 30) : zawartoscP2;
            zawartoscP3 = zawartoscP3.Length > 30 ? zawartoscP3.Substring(0, 30) : zawartoscP3;
            zawartoscP4 = zawartoscP4.Length > 30 ? zawartoscP4.Substring(0, 30) : zawartoscP4;

            string label = "^XA";
            label += "^CF0,60,50";
            label += "^FO50,50^FDCzesciowa^FS";
            label += "^CF0,40";
            label += "^FO50,100^FD^FS";
            label += "^FO50,100^GB700,1,3^FS";
            label += "^FX Second section with recipient address and permit information.";
            label += "^CF0,35";
            label += "^FO50,120^FDKurier:^FS";
            label += "^FO50,160^FDOdbiorca:^FS";
            label += "^FO50,200^FD_^FS";
            label += "^FO50,240^FD_^FS";
            label += "^FO50,280^FDAdres:^FS";
            label += "^FO50,320^FD_^FS";
            label += "^FO50,360^FDOpis:^FS";
            label += "^FO50,600^FDIlosc: ^FS";
            label += "^FO450,600^FDMotywy: ^FS";
            label += "^FO50,640^FDInfo: ^FS";
            label += "^FO450,720^FDWaga: ^FS";
            label += "^FO50,680^FDNr listu:  ^FS";
            label += "^FO50,720^FDData wys.: ^FS";
            //label += "^FO50,640^FDData pak.: ^FS";
            label += $"^FO190,120^FD{addres.address.courier_type}^FS";
            label += $"^FO190,160^FD{addres.address.dispatch_company}^FS";
            label += $"^FO190,200^FD{addres.address.dispatch_name}^FS";
            label += $"^FO190,240^FD{addres.address.dispatch_surname}^FS";
            label += $"^FO190,280^FD{addres.address.city} {addres.address.post_code}^FS";
            label += $"^FO190,320^FD{addres.address.street}^FS";
            label += "^CF0,65,40";
            label += $"^FO190,360^FD{zawartoscP1}^FS";
            label += $"^FO190,420^FD{zawartoscP2}^FS";
            label += $"^FO190,480^FD{zawartoscP3}^FS";
            label += $"^FO190,540^FD{zawartoscP4}^FS";
            label += "^CF0,35";
            label += $"^FO190,600^FD{addres.item.qty_packed} ^FS";
            label += $"^FO570,600^FD{addres.item.motives} ^FS";
            label += $"^FO190,640^FD{addres.address.pack_type} {addres.item.ref_from} z {addres.item.ref_to}, czesc {addres.item.subpack_from} z {addres.item.subpack_to}^FS";
            label += $"^FO570,720^FD{weight} [g]^FS";
            label += $"^FO190,680^FD{addres.address.reference_id} {addres.address.ref_from} {addres.address.ref_to}^FS";
            label += $"^FO190,720^FD{addres.address.dispatch_date.ToString("dd.MM.yyyy")}^FS";
            //label += $"^FO250,640^FD{DateTime.Now.ToString("dd.MM.yyyy")}^FS";
            label += "^CFA,15";
            label += "^FO50,780^GB700,1,3^FS";

            label += "^FX Third section with barcode.";
            label += "^BY2,2,170";
            label += $"^FO55,830^BC,170,Y,N,N,A^FD{addres.address.reference_id.PadLeft(22, 'X')}{addres.address.ref_from.ToString("D2")}{addres.address.ref_to.ToString("D2")}{addres.item.subpack_from.ToString("D2")}{addres.item.subpack_to.ToString("D2")}C^FS";    //C na końcu dodane 2018-06-01 na prośbę p. Łukasza

            label += "^CF0,35";
            label += "^FO50,1050^FDUWAGI:^FS";
            label += $"^FO190,1050^FD{addres.address.comments}^FS";
            label += "^FO30,20^GB760,1150,3^FS";
            label += "^CF0,60,50";
            label += "^FO50,1100^FDPrint & Display (Polska) Sp. z o.o.^FS";

            label += "^XZ";

            label = RemoveDiacritics(label);

            PrintDialog pd = new PrintDialog();
            pd.PrinterSettings = new PrinterSettings();

            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, label);
        }

        static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            stringBuilder = stringBuilder.Replace("Ł", "L");
            stringBuilder = stringBuilder.Replace("ł", "l");

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }


        public class RawPrinterHelper
        {
            // Structure and API declarions:
            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
            public class DOCINFOA
            {
                [MarshalAs(UnmanagedType.LPStr)]
                public string pDocName;
                [MarshalAs(UnmanagedType.LPStr)]
                public string pOutputFile;
                [MarshalAs(UnmanagedType.LPStr)]
                public string pDataType;
            }
            [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

            [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool ClosePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

            [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool EndDocPrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool StartPagePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool EndPagePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

            // SendBytesToPrinter()
            // When the function is given a printer name and an unmanaged array
            // of bytes, the function sends those bytes to the print queue.
            // Returns true on success, false on failure.
            public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
            {
                Int32 dwError = 0, dwWritten = 0;
                IntPtr hPrinter = new IntPtr(0);
                DOCINFOA di = new DOCINFOA();
                bool bSuccess = false; // Assume failure unless you specifically succeed.

                di.pDocName = "My C#.NET RAW Document";
                di.pDataType = "RAW";

                // Open the printer.
                if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
                {
                    // Start a document.
                    if (StartDocPrinter(hPrinter, 1, di))
                    {
                        // Start a page.
                        if (StartPagePrinter(hPrinter))
                        {
                            // Write your bytes.
                            bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                            EndPagePrinter(hPrinter);
                        }
                        EndDocPrinter(hPrinter);
                    }
                    ClosePrinter(hPrinter);
                }
                // If you did not succeed, GetLastError may give more information
                // about why not.
                if (bSuccess == false)
                {
                    dwError = Marshal.GetLastWin32Error();
                }
                return bSuccess;
            }

            public static bool SendFileToPrinter(string szPrinterName, string szFileName)
            {
                // Open the file.
                FileStream fs = new FileStream(szFileName, FileMode.Open);
                // Create a BinaryReader on the file.
                BinaryReader br = new BinaryReader(fs);
                // Dim an array of bytes big enough to hold the file's contents.
                Byte[] bytes = new Byte[fs.Length];
                bool bSuccess = false;
                // Your unmanaged pointer.
                IntPtr pUnmanagedBytes = new IntPtr(0);
                int nLength;

                nLength = Convert.ToInt32(fs.Length);
                // Read the contents of the file into the array.
                bytes = br.ReadBytes(nLength);
                // Allocate some unmanaged memory for those bytes.
                pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
                // Copy the managed byte array into the unmanaged array.
                Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
                // Send the unmanaged bytes to the printer.
                bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, nLength);
                // Free the unmanaged memory that you allocated earlier.
                Marshal.FreeCoTaskMem(pUnmanagedBytes);
                return bSuccess;
            }
            public static bool SendStringToPrinter(string szPrinterName, string szString)
            {
                IntPtr pBytes;
                Int32 dwCount;
                // How many characters are in the string?
                dwCount = szString.Length;
                // Assume that the printer is expecting ANSI text, and then convert
                // the string to ANSI text.
                pBytes = Marshal.StringToCoTaskMemAnsi(szString);
                // Send the converted ANSI string to the printer.
                SendBytesToPrinter(szPrinterName, pBytes, dwCount);
                Marshal.FreeCoTaskMem(pBytes);
                return true;
            }
        }
    }
}

