﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

using MySql.Data.MySqlClient;

namespace AutojetFinishing
{
    public static class SQLConnection
    {
        //private static SqlConnection connection;

        private static MySqlConnection connection;

        static SQLConnection()
        {
            //connection = new SqlConnection();
            connection = new MySqlConnection();
        }

        public static bool Connect(StateChangeEventHandler stateChangeHandler)
        {
            if (connection.State != System.Data.ConnectionState.Open)
            {
                // Register the event change handler, make sure that only once.
                connection.StateChange -= new StateChangeEventHandler(stateChangeHandler);
                connection.StateChange += new StateChangeEventHandler(stateChangeHandler);

                // Build a string to connect to database.
                //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();

                builder.Server = Program.Settings.Data.DbServer;
                builder.UserID = Program.Settings.Data.DbUserID;
                builder.Password = Program.Settings.Data.DbPassword;
                builder.Database = Program.Settings.Data.DbDatabase;

                string connString = builder.ToString();
                connection.ConnectionString = connString;

                try
                {
                    //connection.com
                    connection.Open();
                }
                catch (InvalidOperationException)
                {

                    //HealthMonitor.ErrorDlg("dlg_db_connection_failed_invalid_op_msg");

                    return (false);
                }
                catch (SqlException)
                {
                    //HealthMonitor.ErrorDlg("dlg_db_connection_failed_sql_error_msg");

                    return (false);
                }
                catch
                {
                    //HealthMonitor.ErrorDlg("dlg_db_connection_failed_msg");

                    return (false);
                }
            }

            return (true);
        }

        public static void Disconnect()
        {
            connection.Close();
            connection.Dispose();
        }

        public static bool IsActive()
        {
            try
            {
                MySqlCommand SQLCmd = new MySqlCommand("SELECT NOW();", connection);                
                DateTime date = (DateTime)SQLCmd.ExecuteScalar();
            }
            catch
            { }

            return (connection.State == System.Data.ConnectionState.Open);
        }

        public static void CheckReconnect()
        {
            try
            {
                switch (connection.State)
                {
                    case ConnectionState.Broken:
                        connection.Close();
                        connection.Open();
                        break;
                    case ConnectionState.Closed:
                        connection.Open();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Program.autojetFinishingMain.Hm.Warning("Failed with {0}", ex.Message);
            }
        }

        /// <summary>
        /// Get database data.
        /// </summary>
        /// <param name="query">SQL query to use.</param>
        /// <returns>Data table holding the results or null on error.</returns>
        public static DataTable GetData(string query)
        {
            DataTable retval = null;

            if (IsActive())
            {
                Program.autojetFinishingMain.Hm.Info("Getting data using '{0}'", query);

                MySqlDataAdapter SQLDataAdapter = new MySqlDataAdapter(query, connection);
                SQLDataAdapter.SelectCommand.CommandTimeout = 300;
                
                retval = new DataTable();
                SQLDataAdapter.Fill(retval);

                Program.autojetFinishingMain.Hm.Info("Done: getting data using '{0}'", query);
            }
            else
            {
                retval = new DataTable();
            }

            return (retval);
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="query">SQL query to use.</param>
        /// <returns></returns>
        public static bool InsertData(string query)
        {
            if (IsActive())
            {
                Program.autojetFinishingMain.Hm.Info("Inserting data using '{0}'", query);

                //insert data
                //SqlCommand SQLCmd = new SqlCommand(query, connection);

                MySqlCommand SQLCmd = new MySqlCommand(query, connection);
                SQLCmd.ExecuteScalar();

                Program.autojetFinishingMain.Hm.Info("Done: inserting data using '{0}'", query);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update data to database.
        /// </summary>
        /// <param name="query">SQL query to use.</param>
        /// <returns></returns>
        public static bool UpdateData(string query)
        {
            if (IsActive())
            {
                Program.autojetFinishingMain.Hm.Info("Updating data using '{0}'", query);

                //update data
                //SqlCommand SQLCmd = new SqlCommand(query, connection);

                MySqlCommand SQLCmd = new MySqlCommand(query, connection);
                SQLCmd.ExecuteScalar();

                Program.autojetFinishingMain.Hm.Info("Done: updating data using '{0}'", query);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get count from database.
        /// </summary>
        /// <param name="query">The query to use.</param>
        /// <param name="value">Where to store the result.</param>
        /// <returns>True when successful, otherwise false.</returns>
        public static bool GetCount(string query, out int value)
        {
            if (IsActive())
            {
                Program.autojetFinishingMain.Hm.Info("Getting count data using '{0}'", query);

                //SqlCommand SQLCmd = new SqlCommand(query, connection);
                
                MySqlCommand SQLCmd = new MySqlCommand(query, connection);
                value = (int)SQLCmd.ExecuteScalar();

                Program.autojetFinishingMain.Hm.Info("Done: getting count data using '{0}'", query);
                return (true);
            }

            value = 0;
            return (false);
        }

        public static DateTime GetDateTime()
        {
            // Provide some reasonable value by default.
            DateTime current = DateTime.Now;

            if (IsActive())
            {
                string query = "SELECT GETDATE();";
                Program.autojetFinishingMain.Hm.Info("Getting current time data using '{0}'", query);

                MySqlCommand SQLCmd = new MySqlCommand(query, connection);
                current = (DateTime)SQLCmd.ExecuteScalar();

                Program.autojetFinishingMain.Hm.Info("Done: getting current time data using '{0}'", query);
                return (current);
            }

            return (current);
        }

        /// <summary>
        /// Convert int data (NULL is possible) from database to bool.
        /// </summary>
        /// <param name="dr">data from datatbase</param>
        /// <returns>Convertion result</returns>
        public static bool dbIntNullToBool(object dr)
        {
            if (dr != DBNull.Value)
            {
                int val = Int32.Parse(dr.ToString());
                if (val == 0)
                    return false;
                else
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Convert bool data (NULL is possible) from database to bool.
        /// </summary>
        /// <param name="dr">data from datatbase</param>
        /// <returns>Convertion result</returns>
        public static bool dbBoolNullToBool(object dr)
        {
            if (dr != DBNull.Value)
            {
                bool val = Convert.ToBoolean(dr);
                //int val = Int32.Parse(dr.ToString());
                if (val == false)
                    return false;
                else
                    return true;
            }
            return false;
        }

        public static DateTime dbDateTimeNullToDateTime2(object dr)
        {
            string input = dr.ToString();
            DateTime date;
            DateTime.TryParse(input, out date);
            return date;
        }
    }
}
