﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Threading;
using System.Runtime.InteropServices;
using AutojetFinishing;
using System.Globalization;

namespace FormUtils
{
    /// <summary>
    /// Provide simple text logger which text is destined to a TextBox control.
    /// </summary>
    public class SimpleTextLogger
    {
        public bool SaveToFile = true;

        public bool DisplayTime = true;
        public int MaxVisibleChars = 200000;
        public int MaxCachedChars = 1000000;


        /// <summary>
        /// The constructor.
        /// </summary>
        /// <param name="destination">The rich text box where the text will be output.</param>
        public SimpleTextLogger(RichTextBox destination, string fileName, bool saveToFile)
        {
            this.SaveToFile = saveToFile;

            if (destination == null)
            {
                throw new ArgumentException("The TextBox specified is invalid");
            }

            myRichTextBox = destination;
            myRichTextBox.ReadOnly = true;
            myRichTextBox.Font = new System.Drawing.Font("Lucida Console", 9.163636F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));

            try
            {
                logBuilder = new StringBuilder();
                logger = new StringWriter(logBuilder);
            }
            catch
            {
                throw new Exception("Cannot create StringWriter");
            }

            if (fileName != null)
            {
                this.logFileName = fileName;
            }
        }

        private delegate void setTextCallback(string line);
        private delegate void setTextCallbackWithColor(string line, Color color);


        public static void appendText(RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }


        private void logAppendLine(string line, System.Drawing.Color color)
        {
            if (this.myRichTextBox != null)
            {
                if (this.myRichTextBox.InvokeRequired)
                {
                    setTextCallbackWithColor d = new setTextCallbackWithColor(logAppendLine);
                    Program.autojetFinishingMain.BeginInvoke(d, line, color);
                }
                else
                {
                    if (myRichTextBox.Text.Length > this.MaxVisibleChars)
                    {
                        int len = 0;
                        StringReader sr = new StringReader(myRichTextBox.Text);
                        while (len < (this.MaxVisibleChars / 2))
                        {
                            len += sr.ReadLine().Length;
                        }

                        myRichTextBox.Text = myRichTextBox.Text.Remove(0, len);
                    }

                    appendText(this.myRichTextBox, line + System.Environment.NewLine, color);
                    this.myRichTextBox.SelectionStart = this.myRichTextBox.Text.Length;
                    this.myRichTextBox.ScrollToCaret();
                }
            }
        }


        public void LogLine(string line, System.Drawing.Color color)
        {
            string str;

            //
            /*
            if (Program.CmdArgs.Debug == false)
            {
                return;
            }
            */

            string timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);

            if (this.DisplayTime == true)
            {
                //str = DateTime.Now.ToLongTimeString() + " " + line;
                str = timestamp + " " + line;
            }
            else
            {
                str = line;
            }


            lock (this.loggerLock)
            {
                if (this.logBuilder.Length > this.MaxCachedChars)
                {
                    this.logBuilder.Clear();
                }

                logAppendLine(str, color);
                
                if (SaveToFile)
                {
                    if (logFileName != null)
                    {
                        using (StreamWriter f = File.AppendText(logFileName))
                        {
                            //str = DateTime.Now.GetDateTimeFormats()[0] +
                            //      " " +
                            //      DateTime.Now.ToLongTimeString() +
                            //      " " +
                            //      line +
                            //      System.Environment.NewLine;
                            str = timestamp +
                            " " +
                            line +
                            System.Environment.NewLine;

                            f.Write(str);
                        }
                    }
                }
            }
        }

        private object loggerLock = new object();
        private RichTextBox myRichTextBox;
        private string logFileName;
        private StringBuilder logBuilder;
        private StringWriter logger;

    }
}
