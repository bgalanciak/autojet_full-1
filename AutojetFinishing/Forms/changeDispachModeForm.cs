﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class changeDispachModeForm : Form
    {
        public int mode = 0;    //1 - dispWaybill, 2 - dispSubpackAdding
        public string recStr;    //for passing incoming data to back main form


        public changeDispachModeForm()
        {
            InitializeComponent();
        }

        //disable form from resize and move
        //protected override void WndProc(ref Message message)
        //{
        //    const int WM_SYSCOMMAND = 0x0112;
        //    const int SC_MOVE = 0xF010;

        //    switch (message.Msg)
        //    {
        //        case WM_SYSCOMMAND:
        //            int command = message.WParam.ToInt32() & 0xfff0;
        //            if (command == SC_MOVE)
        //                return;
        //            break;
        //    }
        //    base.WndProc(ref message);
        //}

        //odczyt danych ze skanera kodów kreskowych
        public void scannerSerialRecieved(string recStr)
        {
            //label1.Text = recStr;

            if (recStr.Length == 20)
            {
                if (recStr.Equals("XM000000000000000010"))
                {
                    //przełącz w tryb: bez wysyłek
                    this.recStr = recStr;
                    this.Close();
                }
            }
            else if ((recStr.Length == 30) || (recStr.Length == 31))
            {
                //zpl
                this.recStr = recStr;
                this.Close();
            }
            else
            {
                //label1.Text = recStr;
            }
        }

        //https://stackoverflow.com/questions/7301825/windows-forms-how-to-hide-close-x-button
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void changeDispachModeForm_Load(object sender, EventArgs e)
        {
            
        }
    }
}
