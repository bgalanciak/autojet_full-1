﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class formatListForm : Form
    {
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            switch (m.Msg)
            {
                case 0x84: //WM_NCHITTEST
                    var result = (HitTest)m.Result.ToInt32();
                    if (result == HitTest.Left || result == HitTest.Right)
                        m.Result = new IntPtr((int)HitTest.Caption);
                    if (result == HitTest.TopLeft || result == HitTest.TopRight)
                        m.Result = new IntPtr((int)HitTest.Top);
                    if (result == HitTest.BottomLeft || result == HitTest.BottomRight)
                        m.Result = new IntPtr((int)HitTest.Bottom);

                    break;
            }
        }
        enum HitTest
        {
            Caption = 2,
            Transparent = -1,
            Nowhere = 0,
            Client = 1,
            Left = 10,
            Right = 11,
            Top = 12,
            TopLeft = 13,
            TopRight = 14,
            Bottom = 15,
            BottomLeft = 16,
            BottomRight = 17,
            Border = 18
        }

        public List<StringValue> formatList = new List<StringValue>();
        public List<StringValue> formatListNew = new List<StringValue>();

        public class StringValue
        {
            public StringValue(string s)
            {
                _value = s;
            }
            public string Value { get { return _value; } set { _value = value; } }
            string _value;
        }

        public formatListForm()
        {
            InitializeComponent();
        }

        private void bFormatCancel_Click(object sender, EventArgs e)
        {
            tbNewFormat.Text = "";
        }

        private void formatListForm_Load(object sender, EventArgs e)
        {
            dgvFormats.DataSource = formatList;
        }

        private void bFormatSave_Click(object sender, EventArgs e)
        {
            if (tbNewFormat.Text.Length > 0)
            {
                StringValue s = new StringValue(tbNewFormat.Text);
                formatListNew.Add(s);
                formatList.Add(s);
                dgvFormats.DataSource = null;
                dgvFormats.DataSource = formatList;
                dgvFormats.Rows[dgvFormats.Rows.Count - 1].Selected = true;
                tbNewFormat.Text = "";
            }
        }

        private void bCloseForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
