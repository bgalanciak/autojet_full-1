﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class usersManagement : Form
    {
        public usersManagement()
        {
            InitializeComponent();
        }

        private void usersManagement_Load(object sender, EventArgs e)
        {
            int boolWidth = 60;
            int numWidth = 60;

            dgvUsers.DataSource = global.userList;
            
            this.dgvUsers.Columns["password"].Visible = false;
            this.dgvUsers.Columns["login"].Visible = false;
            this.dgvUsers.Columns["userLogHash"].Visible = false;
            
            this.dgvUsers.Columns["id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.dgvUsers.Columns["id"].Width = numWidth;
            this.dgvUsers.Columns["id"].HeaderText = "Id";

            this.dgvUsers.Columns["f_name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgvUsers.Columns["f_name"].HeaderText = "Imię";

            this.dgvUsers.Columns["l_name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgvUsers.Columns["l_name"].HeaderText = "Nazwisko";
            
            this.dgvUsers.Columns["qc_num"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.dgvUsers.Columns["qc_num"].Width = numWidth;
            this.dgvUsers.Columns["qc_num"].HeaderText = "Numer QC";

            this.dgvUsers.Columns["is_admin"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.dgvUsers.Columns["is_admin"].Width = boolWidth;
            this.dgvUsers.Columns["is_admin"].HeaderText = "Administrator";

            this.dgvUsers.Columns["is_operator"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.dgvUsers.Columns["is_operator"].Width = boolWidth;
            this.dgvUsers.Columns["is_operator"].HeaderText = "Operator";

            this.dgvUsers.Columns["is_active"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.dgvUsers.Columns["is_active"].Width = boolWidth;
            this.dgvUsers.Columns["is_active"].HeaderText = "Aktywny";

        }

        private void bEdit_Click(object sender, EventArgs e)
        {
            int selectedrowindex = dgvUsers.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dgvUsers.Rows[selectedrowindex];  
            int id = int.Parse(selectedRow.Cells["id"].Value.ToString());

            user editUser = global.findUserById(id, global.userList);

            userEdit userEditForm = new userEdit();
            userEditForm.mode = true;
            userEditForm.actUserData = editUser;

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

     
            if (userEditForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                /*
                `id` INT NOT NULL,
                `userFName` VARCHAR(45) NULL,
                `userLName` VARCHAR(45) NULL,
                `userPassword` VARCHAR(45) NULL,
                `qcNum` INT NULL,
                `isAdmin` BIT NULL,
                `isOperator` BIT NULL,
                `isActive` BIT NULL,
                 * 
                UPDATE tutorials_tbl
                SET tutorial_title="Learning JAVA"
                WHERE tutorial_id=3
                 */

                /*update tjdata.user_table SET userFName='us', userLName='l', userPassword='1', qcNum=1, isAdmin=True, isOperator=False, isActive=True where id='1';*/

                string query = string.Format("update tjdata.user_table SET userFName='{0}', userLName='{1}', qcNum={2}, userPassword='{3}', isAdmin={4}, isOperator={5}, isActive={6}, userLogHash='{7}' where id='{8}'", userEditForm.actUserData.f_name, userEditForm.actUserData.l_name, userEditForm.actUserData.qc_num.ToString(), userEditForm.actUserData.password, userEditForm.actUserData.is_admin, userEditForm.actUserData.is_operator, userEditForm.actUserData.is_active, userEditForm.actUserData.userLogHash, userEditForm.actUserData.id);

                SQLConnection.UpdateData(query);

                global.userList = global.getAllUsers();
                dgvUsers.DataSource = global.userList;
            }

        }

        private void bUserNew_Click(object sender, EventArgs e)
        {
            userEdit userEditForm = new userEdit();
            userEditForm.mode = false;
            userEditForm.cbIsActive.Checked = true;

            //DOPISANE ŁP 2018-09-05
            Program.autojetFinishingMain.Activate();

            if (userEditForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                /*
                `id` INT NOT NULL,
                `userFName` VARCHAR(45) NULL,
                `userLName` VARCHAR(45) NULL,
                `userPassword` VARCHAR(45) NULL,
                `qcNum` INT NULL,
                `isAdmin` BIT NULL,
                `isOperator` BIT NULL,
                `isActive` BIT NULL,
                 * 
                UPDATE tutorials_tbl
                SET tutorial_title="Learning JAVA"
                WHERE tutorial_id=3
                 */

                /*update tjdata.user_table SET userFName='us', userLName='l', userPassword='1', qcNum=1, isAdmin=True, isOperator=False, isActive=True where id='1';*/

                //string query = string.Format("update tjdata.user_table SET userFName='{0}', userLName='{1}', qcNum={2}, userPassword='{3}', isAdmin={4}, isOperator={5}, isActive={6} where id='{7}'", userEditForm.actUserData.f_name, userEditForm.actUserData.l_name, userEditForm.actUserData.qc_num.ToString(), userEditForm.actUserData.password, userEditForm.actUserData.is_admin, userEditForm.actUserData.is_operator, userEditForm.actUserData.is_active, userEditForm.actUserData.id);
                //INSERT INTO `tjdata`.`user_table` (`userFName`, `userLName`, `userPassword`, `qcNum`, `isAdmin`, `isOperator`, `isActive`) VALUES ('3', 'd', 'a', '3', 0, 1, 1);

                string query = string.Format("Insert into tjdata.user_table (userFName, userLName, qcNum, userPassword, isAdmin, isOperator, isActive, userLogHash) values('{0}','{1}',{2},'{3}', {4}, {5}, {6}, '{7}')", userEditForm.actUserData.f_name, userEditForm.actUserData.l_name, userEditForm.actUserData.qc_num.ToString(), userEditForm.actUserData.password, userEditForm.actUserData.is_admin, userEditForm.actUserData.is_operator, userEditForm.actUserData.is_active, userEditForm.actUserData.userLogHash);

                SQLConnection.InsertData(query);

                global.userList = global.getAllUsers();
                dgvUsers.DataSource = global.userList;
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
