﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class noDispachErrorForm : Form
    {
        //disable form from resize and move
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        //odczyt danych ze skanera kodów kreskowych
        public void scannerSerialRecieved(string recStr)
        {

            if (recStr.Length == 20)
            {
                errorSourceAnalyze(recStr);
            }
            else
            {
            }
        }

        private void errorSourceAnalyze(string recStr)
        {
            if (recStr.Equals("XC000000000000000001"))
            {
                this.Close();
            }
            else
            {
                //lErrorTxt.Text = recStr;
            }
        }

        public noDispachErrorForm()
        {
            InitializeComponent();
        }

        private void noDispachErrorForm_Load(object sender, EventArgs e)
        {

        }
    }
}
