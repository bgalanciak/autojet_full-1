﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class userLogin : Form
    {
        public userLogin()
        {
            InitializeComponent();
        }

        private void userLogin_Load(object sender, EventArgs e)
        {
            this.Text = "Logowanie";

            cbUserList.DataSource = global.userActiveList;
            cbUserList.DisplayMember = "login";
            cbUserList.ValueMember = "id";

            if (Program.Settings.Data.lastLogUserId > 0)
            {
                user u = global.findUserById(Program.Settings.Data.lastLogUserId, global.userActiveList);

                if (u.id == Program.Settings.Data.lastLogUserId)
                {
                    cbUserList.SelectedValue = Program.Settings.Data.lastLogUserId;
                }
            }
        }

        public void scannerSerialRecieved(string recStr)
        {
            Program.autojetFinishingMain.Hm.Warning(string.Format("scannerSerialRecieved: payload = {0}", recStr));
            
            if (recStr.Length == 20)
            {
                lUserLogHash.Text = recStr;
            }
            logByHash(recStr);
        }

        public bool logByHash(string hash)
        { 
            user logUser = global.findUserByHash(hash, global.userActiveList);

            if (logUser.id != -1)
            {
                global.userAct = new user(logUser);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            return false;
        }

        private void bLogin_Click(object sender, EventArgs e)
        {
            int loginUserId = int.Parse(cbUserList.SelectedValue.ToString());
            user logUser = global.findUserById(loginUserId, global.userActiveList);

            user nu = new user();
            if (nu == null)
                MessageBox.Show("new = null");

            if (logUser.passwordOk(tbPassword.Text))
            {
                global.userAct = new user(logUser);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Błąd logowania. Niepoprawne hasło", "Błąd logowania", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbPassword.Text = "";
                this.ActiveControl = tbPassword;
            }
        }
    }
}
