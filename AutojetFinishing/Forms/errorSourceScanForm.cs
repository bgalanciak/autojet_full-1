﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class errorSourceScanForm : Form
    {
        public int errorSourceCount = 0;    //ile kodów kreskowych z przyczyną błędu musi zostać zekanowanych aby zamknąć okno
        public int errorSourceRead = 0;    //ileość zeskanowanych kodów kreskowych z przyczyną błędu 

        //public string errorText;

        public errorSourceScanForm()
        {
            InitializeComponent();
        }

        private void errorForm_Load(object sender, EventArgs e)
        {
            errorTextUpdate();
            //tPlcRead.Enabled = true; 
        }

        public void errorTextUpdate()
        {
            lErrorText.Text = "Błąd kompletacji" + Environment.NewLine + string.Format("Zeskanowano {0} z {1} kodów z przyczyną wystąpienia błędu", errorSourceRead, errorSourceCount);
        }

        //disable form from resize and move
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void bAccept_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        //private void tPlcRead_Tick(object sender, EventArgs e)
        //{
        //    if (Program.autojetFinishingMain.plcRead() == true)
        //    {
        //        label1.Text = "end";
        //        this.Close();
        //    }
        //}

        //odczyt danych ze skanera kodów kreskowych
        public void scannerSerialRecieved(string recStr)
        {
            if (recStr.Length == 20)
            {
                errorSourceAnalyze(recStr);
                lScanError.Text = "";
            }
            else
            {
                lScanError.Text = "Zeskanowano błędny kod kreskowy";
            }
        }

        public List<int> errorList = new List<int>();

        private void errorSourceAnalyze(string recStr)
        {
            if (recStr.Substring(0, 2).Equals("XE"))
            {
                int errorId = 0;
                bool res = int.TryParse(recStr.Substring(2), out errorId);
                if (res)
                {
                    errorList.Add(errorId);
                    errorSourceRead++;
                    errorTextUpdate();
                    if (errorSourceRead == errorSourceCount)
                    {
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        this.Close();
                    }
                }
                else
                {
                    lScanError.Text = "Zeskanowano niepoprawny kod błędu";
                }
            }
            else
            {
                lScanError.Text = "Zeskanowano kod kreskowy nie jest kodem błędu";
            }
        }
    }
}
