﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetFinishing
{
    public partial class weightModelScan : Form
    {
        //disable form from resize and move
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        public weightModelScan()
        {
            InitializeComponent();
        }

        public int modelMode;   //0 = model weight + tare weight, 1 = model, 2 = tare
        public int modelType;   //0 = "PLAKATU", 1 = BRYTÓW, 2 = BRYTU 

        public global.weightModelDataType weightModel = new global.weightModelDataType();   

        public const int modelModeAll = 0;
        public const int modelModeModel = 1;
        public const int modelModeTare = 2;
        public int model_scan_count;

        private void weightModelScan_Load(object sender, EventArgs e)
        {
            commandMain(0);
        }

        public void weightSerialRecieved(string recStr)
        {
            //Program.autojetFinishingMain.Hm.Info("weightSerialRecieved: recStr={0} ", recStr);
            commandMain(weightAnalyze(recStr));
        }

        public int weightAnalyze(string scanStr)
        {
            int weightRead = -1;
            try
            {
                //string gram = scanStr.Substring(13, 7);
                //rozwiązanie p. Łukasza 2018-04-13
                string gram = scanStr.Substring(Program.Settings.Data.scaleFrom, Program.Settings.Data.scaleHowMany);
                gram = gram.Trim();

                if (Program.Settings.Data.scaleMass.Equals("kg"))
                {
                    double weightTemp = 0;
                    gram = gram.Replace(".", ",");
                    weightTemp = (double.Parse(gram) * 1000);
                    gram = weightTemp.ToString();
                }

                int weightGr = int.Parse(gram);
                weightRead = weightGr;
                //Program.autojetFinishingMain.Hm.Info($"weightAnalyze: weightRead = {weightGr} ");
            }
            catch (Exception e)
            {
                //Program.autojetFinishingMain.Hm.Info($"weightAnalyze: recStr = {scanStr}, error: {e.Message} ");
            }

            return weightRead;
        }

        public int stepAct = 0;

        public void commandMain(int weight)
        {
            //sprawdzenie czy nie wystąpił błąd w weightAnalyze
            if (weight == -1)
                return;

            if (modelMode == modelModeAll)
            {
                if (stepAct == 0)
                {
                    lCommand.Text = $"Połóż i zważ opakowanie dla KP {weightModel.jdnr} LINII {weightModel.jdline}";
                }
                else if (stepAct == 1)
                {
                    weightModel.weight_tare = weight;
                    //weightModel.weight_tare = 0;

                    if (modelType==0)   //0 = "PLAKATU", 1 = BRYTÓW, 2 = BRYTU 
                        lCommand.Text = $"Połóż i zważ {model_scan_count} szt. PLAKATU z KP {weightModel.jdnr} LINII {weightModel.jdline}";
                    else if (modelType == 1)
                        lCommand.Text = $"Połóż i zważ {model_scan_count} szt. BRYTÓW {weightModel.tileName} z KP {weightModel.jdnr} LINII {weightModel.jdline}";
                    else
                        lCommand.Text = $"Połóż i zważ {model_scan_count} szt. BRYTU {weightModel.tileName} z KP {weightModel.jdnr} LINII {weightModel.jdline}";
                    //lCommand.Text += " wraz z opakowaniem";
                }
                else if (stepAct == 2)
                {
                    //weightModel.weight_model = (weight - weightModel.weight_tare) / model_scan_count;
                    weightModel.weight_model = (weight) / model_scan_count;
                    this.Close();
                }    
            }
            else if (modelMode == modelModeModel)
            {
                if (stepAct == 0)
                {
                    if (modelType == 0)   //0 = "PLAKATU", 1 = BRYTÓW, 2 = BRYTU 
                        lCommand.Text = $"Połóż i zważ {model_scan_count} szt. PLAKATU z KP {weightModel.jdnr} LINII {weightModel.jdline}";
                    else if (modelType == 1)
                        lCommand.Text = $"Połóż i zważ {model_scan_count} szt. BRYTÓW {weightModel.tileName} z KP {weightModel.jdnr} LINII {weightModel.jdline}";
                    else
                        lCommand.Text = $"Połóż i zważ {model_scan_count} szt. BRYTU {weightModel.tileName} z KP {weightModel.jdnr} LINII {weightModel.jdline}";
                    
                }
                else if (stepAct == 1)
                {
                    weightModel.weight_model = (weight - weightModel.weight_tare) / model_scan_count;
                    this.Close();
                }
            }
            else if (modelMode == modelModeTare)
            {
                if (stepAct == 0)
                {
                    lCommand.Text = $"Połóż i zważ opakowanie dla KP {weightModel.jdnr} LINII {weightModel.jdline}";
                }
                else if (stepAct == 1)
                {
                    weightModel.weight_tare = weight;
                    this.Close();
                }               
            }
            stepAct++;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int weightAct;
            if(int.TryParse(tbWeight.Text, out weightAct))
            {
                commandMain(weightAct);
            }

        }
    }
}
