﻿namespace AutojetFinishing
{
    partial class formatListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dgvFormats = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.bFormatSave = new System.Windows.Forms.Button();
            this.bFormatCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbNewFormat = new System.Windows.Forms.TextBox();
            this.bCloseForm = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFormats)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Formaty do kompletacji typu A+B";
            // 
            // dgvFormats
            // 
            this.dgvFormats.AllowUserToAddRows = false;
            this.dgvFormats.AllowUserToDeleteRows = false;
            this.dgvFormats.AllowUserToResizeRows = false;
            this.dgvFormats.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvFormats.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFormats.Location = new System.Drawing.Point(12, 34);
            this.dgvFormats.MultiSelect = false;
            this.dgvFormats.Name = "dgvFormats";
            this.dgvFormats.ReadOnly = true;
            this.dgvFormats.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFormats.Size = new System.Drawing.Size(230, 176);
            this.dgvFormats.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Format:";
            // 
            // bFormatSave
            // 
            this.bFormatSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bFormatSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bFormatSave.Location = new System.Drawing.Point(6, 62);
            this.bFormatSave.Name = "bFormatSave";
            this.bFormatSave.Size = new System.Drawing.Size(103, 32);
            this.bFormatSave.TabIndex = 5;
            this.bFormatSave.Text = "Zapisz";
            this.bFormatSave.UseVisualStyleBackColor = true;
            this.bFormatSave.Click += new System.EventHandler(this.bFormatSave_Click);
            // 
            // bFormatCancel
            // 
            this.bFormatCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bFormatCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bFormatCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bFormatCancel.Location = new System.Drawing.Point(119, 62);
            this.bFormatCancel.Name = "bFormatCancel";
            this.bFormatCancel.Size = new System.Drawing.Size(103, 32);
            this.bFormatCancel.TabIndex = 6;
            this.bFormatCancel.Text = "Anuluj";
            this.bFormatCancel.UseVisualStyleBackColor = true;
            this.bFormatCancel.Click += new System.EventHandler(this.bFormatCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.tbNewFormat);
            this.groupBox1.Controls.Add(this.bFormatCancel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.bFormatSave);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 254);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodaj nowy format ";
            // 
            // tbNewFormat
            // 
            this.tbNewFormat.Location = new System.Drawing.Point(72, 29);
            this.tbNewFormat.Name = "tbNewFormat";
            this.tbNewFormat.Size = new System.Drawing.Size(150, 24);
            this.tbNewFormat.TabIndex = 8;
            // 
            // bCloseForm
            // 
            this.bCloseForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bCloseForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bCloseForm.Location = new System.Drawing.Point(12, 216);
            this.bCloseForm.Name = "bCloseForm";
            this.bCloseForm.Size = new System.Drawing.Size(230, 32);
            this.bCloseForm.TabIndex = 9;
            this.bCloseForm.Text = "Zamknij";
            this.bCloseForm.UseVisualStyleBackColor = true;
            this.bCloseForm.Click += new System.EventHandler(this.bCloseForm_Click);
            // 
            // formatListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(254, 366);
            this.Controls.Add(this.bCloseForm);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvFormats);
            this.Controls.Add(this.label1);
            this.Name = "formatListForm";
            this.Text = "formatListForm";
            this.Load += new System.EventHandler(this.formatListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFormats)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvFormats;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bFormatSave;
        private System.Windows.Forms.Button bFormatCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbNewFormat;
        private System.Windows.Forms.Button bCloseForm;

    }
}