﻿namespace AutojetFinishing
{
    partial class autojetFinishingMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(autojetFinishingMainForm));
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formatyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDebugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.initDgvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testPrinterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.initToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dispToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findDispachToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findAddressesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateDispachCountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findSubpackToZPLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tstToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkmodelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newmodelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getnumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.statusStripeDb = new System.Windows.Forms.ToolStripStatusLabel();
            this.scannersConnectionStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStripePLC = new System.Windows.Forms.ToolStripStatusLabel();
            this.ststusStripCognex = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslWeight = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageControl = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.bDispStroerSubpackOn = new System.Windows.Forms.Button();
            this.bDispStroerOn = new System.Windows.Forms.Button();
            this.bDispSubpackAddingOn = new System.Windows.Forms.Button();
            this.bDispWaybillOn = new System.Windows.Forms.Button();
            this.bOperationAddManual = new System.Windows.Forms.Button();
            this.bOperationRollback = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bInfoOkErr = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lJobformat = new System.Windows.Forms.Label();
            this.lUserName = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lWeightNet = new System.Windows.Forms.Label();
            this.lJdnrJdline = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lLinedesc = new System.Windows.Forms.Label();
            this.lCustomer = new System.Windows.Forms.Label();
            this.lCampaigndesc = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lCampaignname = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lQtyordered = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lWeightModel = new System.Windows.Forms.Label();
            this.lWeightTare = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lQtyClosed = new System.Windows.Forms.Label();
            this.bWorkModeFinishing2 = new System.Windows.Forms.Button();
            this.bWorkModeSingleRow = new System.Windows.Forms.Button();
            this.bWorkModeAllPoster = new System.Windows.Forms.Button();
            this.pBarcodesFence = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pBarcodesLadder = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.dgvDispatchTable = new System.Windows.Forms.DataGridView();
            this.lDispSubpackAddInfo = new System.Windows.Forms.Label();
            this.tpReports = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.gbReportType = new System.Windows.Forms.GroupBox();
            this.bReportKpLkp = new System.Windows.Forms.Button();
            this.bReportKp = new System.Windows.Forms.Button();
            this.bReportDate = new System.Windows.Forms.Button();
            this.tcReports = new System.Windows.Forms.TabControl();
            this.tpDate = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.dtpReportParam = new System.Windows.Forms.DateTimePicker();
            this.dtpReportNP = new System.Windows.Forms.CheckBox();
            this.bReportMake11 = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.dtpReportNS = new System.Windows.Forms.CheckBox();
            this.Courier = new System.Windows.Forms.ComboBox();
            this.tpKP = new System.Windows.Forms.TabPage();
            this.lReportKpError = new System.Windows.Forms.Label();
            this.bReportMake21 = new System.Windows.Forms.Button();
            this.tbReportKp = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tpKPLKP = new System.Windows.Forms.TabPage();
            this.lReportKpLkpError = new System.Windows.Forms.Label();
            this.bReportMake31 = new System.Windows.Forms.Button();
            this.tbReportKpLkp = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tpInfo = new System.Windows.Forms.TabPage();
            this.tabPageDebug = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.debugTestBox = new System.Windows.Forms.RichTextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cbPCErrorSymulaton = new System.Windows.Forms.CheckBox();
            this.tpTCPServerDebug = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.listMessages = new System.Windows.Forms.ListBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.listLog = new System.Windows.Forms.ListBox();
            this.tpUnused = new System.Windows.Forms.TabPage();
            this.bTestInput = new System.Windows.Forms.Button();
            this.tbInputStr = new System.Windows.Forms.TextBox();
            this.rtbErrorList = new System.Windows.Forms.RichTextBox();
            this.dgvMain = new System.Windows.Forms.DataGridView();
            this.tpLabelReprint = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.lRepintRefTo = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lRepintRefFrom = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lRepintRefId = new System.Windows.Forms.Label();
            this.lLabelReprintTitle = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tConnections = new System.Windows.Forms.Timer(this.components);
            this.tMsgOk = new System.Windows.Forms.Timer(this.components);
            this.tErrorReset = new System.Windows.Forms.Timer(this.components);
            this.tDispatchDone = new System.Windows.Forms.Timer(this.components);
            this.bwPrintPdf = new System.ComponentModel.BackgroundWorker();
            this.bwPrintPdf2 = new System.ComponentModel.BackgroundWorker();
            this.tcpServerCgnex = new tcpServer.TcpServer(this.components);
            this.mainMenuStrip.SuspendLayout();
            this.mainStatusStrip.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabPageControl.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.pBarcodesFence.SuspendLayout();
            this.pBarcodesLadder.SuspendLayout();
            this.panel14.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDispatchTable)).BeginInit();
            this.tpReports.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.gbReportType.SuspendLayout();
            this.tcReports.SuspendLayout();
            this.tpDate.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tpKP.SuspendLayout();
            this.tpKPLKP.SuspendLayout();
            this.tabPageDebug.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tpTCPServerDebug.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tpUnused.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
            this.tpLabelReprint.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.pomocToolStripMenuItem,
            this.dispToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.weightToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.mainMenuStrip.Size = new System.Drawing.Size(1664, 28);
            this.mainMenuStrip.TabIndex = 0;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formatyToolStripMenuItem,
            this.tsmiUsers});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(93, 24);
            this.fileToolStripMenuItem.Text = "Ustawienia";
            // 
            // formatyToolStripMenuItem
            // 
            this.formatyToolStripMenuItem.Name = "formatyToolStripMenuItem";
            this.formatyToolStripMenuItem.Size = new System.Drawing.Size(166, 26);
            this.formatyToolStripMenuItem.Text = "Formaty";
            this.formatyToolStripMenuItem.Click += new System.EventHandler(this.formatyToolStripMenuItem_Click);
            // 
            // tsmiUsers
            // 
            this.tsmiUsers.Name = "tsmiUsers";
            this.tsmiUsers.Size = new System.Drawing.Size(166, 26);
            this.tsmiUsers.Text = "Użytkownicy";
            this.tsmiUsers.Click += new System.EventHandler(this.tsmiUsers_Click);
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oProgramieToolStripMenuItem,
            this.debugToolStripMenuItem,
            this.createDatabaseToolStripMenuItem,
            this.showDebugToolStripMenuItem,
            this.initDgvToolStripMenuItem,
            this.testPrinterToolStripMenuItem,
            this.initToolStripMenuItem});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(66, 24);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(230, 26);
            this.oProgramieToolStripMenuItem.Text = "O programie...";
            this.oProgramieToolStripMenuItem.Click += new System.EventHandler(this.oProgramieToolStripMenuItem_Click);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(230, 26);
            this.debugToolStripMenuItem.Text = "Debug";
            this.debugToolStripMenuItem.Click += new System.EventHandler(this.debugToolStripMenuItem_Click);
            // 
            // createDatabaseToolStripMenuItem
            // 
            this.createDatabaseToolStripMenuItem.Name = "createDatabaseToolStripMenuItem";
            this.createDatabaseToolStripMenuItem.Size = new System.Drawing.Size(230, 26);
            this.createDatabaseToolStripMenuItem.Text = "Database column add";
            this.createDatabaseToolStripMenuItem.Visible = false;
            this.createDatabaseToolStripMenuItem.Click += new System.EventHandler(this.createDatabaseToolStripMenuItem_Click);
            // 
            // showDebugToolStripMenuItem
            // 
            this.showDebugToolStripMenuItem.Name = "showDebugToolStripMenuItem";
            this.showDebugToolStripMenuItem.Size = new System.Drawing.Size(230, 26);
            this.showDebugToolStripMenuItem.Text = "Pokaż Debug";
            this.showDebugToolStripMenuItem.Click += new System.EventHandler(this.showDebugToolStripMenuItem_Click);
            // 
            // initDgvToolStripMenuItem
            // 
            this.initDgvToolStripMenuItem.Name = "initDgvToolStripMenuItem";
            this.initDgvToolStripMenuItem.Size = new System.Drawing.Size(230, 26);
            this.initDgvToolStripMenuItem.Text = "InitDgv";
            this.initDgvToolStripMenuItem.Visible = false;
            this.initDgvToolStripMenuItem.Click += new System.EventHandler(this.initDgvToolStripMenuItem_Click);
            // 
            // testPrinterToolStripMenuItem
            // 
            this.testPrinterToolStripMenuItem.Name = "testPrinterToolStripMenuItem";
            this.testPrinterToolStripMenuItem.Size = new System.Drawing.Size(230, 26);
            this.testPrinterToolStripMenuItem.Text = "TestPrinter";
            this.testPrinterToolStripMenuItem.Visible = false;
            this.testPrinterToolStripMenuItem.Click += new System.EventHandler(this.testPrinterToolStripMenuItem_Click);
            // 
            // initToolStripMenuItem
            // 
            this.initToolStripMenuItem.Name = "initToolStripMenuItem";
            this.initToolStripMenuItem.Size = new System.Drawing.Size(230, 26);
            this.initToolStripMenuItem.Text = "Init";
            this.initToolStripMenuItem.Visible = false;
            this.initToolStripMenuItem.Click += new System.EventHandler(this.initToolStripMenuItem_Click);
            // 
            // dispToolStripMenuItem
            // 
            this.dispToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findDispachToolStripMenuItem,
            this.findAddressesToolStripMenuItem,
            this.updateDispachCountToolStripMenuItem,
            this.findSubpackToZPLToolStripMenuItem,
            this.tstToolStripMenuItem});
            this.dispToolStripMenuItem.Name = "dispToolStripMenuItem";
            this.dispToolStripMenuItem.Size = new System.Drawing.Size(51, 24);
            this.dispToolStripMenuItem.Text = "Disp";
            this.dispToolStripMenuItem.Visible = false;
            // 
            // findDispachToolStripMenuItem
            // 
            this.findDispachToolStripMenuItem.Name = "findDispachToolStripMenuItem";
            this.findDispachToolStripMenuItem.Size = new System.Drawing.Size(223, 26);
            this.findDispachToolStripMenuItem.Text = "findDispach";
            this.findDispachToolStripMenuItem.Click += new System.EventHandler(this.findDispachToolStripMenuItem_Click);
            // 
            // findAddressesToolStripMenuItem
            // 
            this.findAddressesToolStripMenuItem.Name = "findAddressesToolStripMenuItem";
            this.findAddressesToolStripMenuItem.Size = new System.Drawing.Size(223, 26);
            this.findAddressesToolStripMenuItem.Text = "findAddresses";
            this.findAddressesToolStripMenuItem.Click += new System.EventHandler(this.findAddressesToolStripMenuItem_Click);
            // 
            // updateDispachCountToolStripMenuItem
            // 
            this.updateDispachCountToolStripMenuItem.Name = "updateDispachCountToolStripMenuItem";
            this.updateDispachCountToolStripMenuItem.Size = new System.Drawing.Size(223, 26);
            this.updateDispachCountToolStripMenuItem.Text = "updateDispachCount";
            this.updateDispachCountToolStripMenuItem.Click += new System.EventHandler(this.updateDispachCountToolStripMenuItem_Click);
            // 
            // findSubpackToZPLToolStripMenuItem
            // 
            this.findSubpackToZPLToolStripMenuItem.Name = "findSubpackToZPLToolStripMenuItem";
            this.findSubpackToZPLToolStripMenuItem.Size = new System.Drawing.Size(223, 26);
            this.findSubpackToZPLToolStripMenuItem.Text = "findSubpackToZPL";
            this.findSubpackToZPLToolStripMenuItem.Click += new System.EventHandler(this.findSubpackToZPLToolStripMenuItem_Click);
            // 
            // tstToolStripMenuItem
            // 
            this.tstToolStripMenuItem.Name = "tstToolStripMenuItem";
            this.tstToolStripMenuItem.Size = new System.Drawing.Size(223, 26);
            this.tstToolStripMenuItem.Text = "tst";
            this.tstToolStripMenuItem.Click += new System.EventHandler(this.tstToolStripMenuItem_Click);
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(68, 24);
            this.updateToolStripMenuItem.Text = "update";
            this.updateToolStripMenuItem.Visible = false;
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // weightToolStripMenuItem
            // 
            this.weightToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkmodelToolStripMenuItem,
            this.newmodelToolStripMenuItem,
            this.plusToolStripMenuItem,
            this.getnumToolStripMenuItem});
            this.weightToolStripMenuItem.Name = "weightToolStripMenuItem";
            this.weightToolStripMenuItem.Size = new System.Drawing.Size(68, 24);
            this.weightToolStripMenuItem.Text = "Weight";
            // 
            // checkmodelToolStripMenuItem
            // 
            this.checkmodelToolStripMenuItem.Name = "checkmodelToolStripMenuItem";
            this.checkmodelToolStripMenuItem.Size = new System.Drawing.Size(141, 26);
            this.checkmodelToolStripMenuItem.Text = "check";
            this.checkmodelToolStripMenuItem.Click += new System.EventHandler(this.checkmodelToolStripMenuItem_Click);
            // 
            // newmodelToolStripMenuItem
            // 
            this.newmodelToolStripMenuItem.Name = "newmodelToolStripMenuItem";
            this.newmodelToolStripMenuItem.Size = new System.Drawing.Size(141, 26);
            this.newmodelToolStripMenuItem.Text = "model";
            this.newmodelToolStripMenuItem.Click += new System.EventHandler(this.newmodelToolStripMenuItem_Click);
            // 
            // plusToolStripMenuItem
            // 
            this.plusToolStripMenuItem.Name = "plusToolStripMenuItem";
            this.plusToolStripMenuItem.Size = new System.Drawing.Size(141, 26);
            this.plusToolStripMenuItem.Text = "plus";
            this.plusToolStripMenuItem.Click += new System.EventHandler(this.plusToolStripMenuItem_Click);
            // 
            // getnumToolStripMenuItem
            // 
            this.getnumToolStripMenuItem.Name = "getnumToolStripMenuItem";
            this.getnumToolStripMenuItem.Size = new System.Drawing.Size(141, 26);
            this.getnumToolStripMenuItem.Text = "get_num";
            this.getnumToolStripMenuItem.Click += new System.EventHandler(this.getnumToolStripMenuItem_Click);
            // 
            // mainStatusStrip
            // 
            this.mainStatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusStripeDb,
            this.scannersConnectionStatus,
            this.statusStripePLC,
            this.ststusStripCognex,
            this.tsslUser,
            this.tsslWeight});
            this.mainStatusStrip.Location = new System.Drawing.Point(0, 1018);
            this.mainStatusStrip.Name = "mainStatusStrip";
            this.mainStatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.mainStatusStrip.Size = new System.Drawing.Size(1664, 22);
            this.mainStatusStrip.TabIndex = 1;
            this.mainStatusStrip.Text = "statusStrip1";
            // 
            // statusStripeDb
            // 
            this.statusStripeDb.AutoSize = false;
            this.statusStripeDb.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.statusStripeDb.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.statusStripeDb.Name = "statusStripeDb";
            this.statusStripeDb.Size = new System.Drawing.Size(200, 17);
            this.statusStripeDb.Text = "statusStripeDb";
            this.statusStripeDb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // scannersConnectionStatus
            // 
            this.scannersConnectionStatus.AutoSize = false;
            this.scannersConnectionStatus.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.scannersConnectionStatus.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.scannersConnectionStatus.Name = "scannersConnectionStatus";
            this.scannersConnectionStatus.Size = new System.Drawing.Size(200, 17);
            this.scannersConnectionStatus.Text = "serialConnectionStatus";
            this.scannersConnectionStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusStripePLC
            // 
            this.statusStripePLC.AutoSize = false;
            this.statusStripePLC.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.statusStripePLC.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.statusStripePLC.Name = "statusStripePLC";
            this.statusStripePLC.Size = new System.Drawing.Size(200, 17);
            this.statusStripePLC.Text = "statusStripePLC";
            this.statusStripePLC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ststusStripCognex
            // 
            this.ststusStripCognex.AutoSize = false;
            this.ststusStripCognex.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.ststusStripCognex.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.ststusStripCognex.Name = "ststusStripCognex";
            this.ststusStripCognex.Size = new System.Drawing.Size(200, 17);
            this.ststusStripCognex.Text = "ststusStripCognex";
            this.ststusStripCognex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslUser
            // 
            this.tsslUser.AutoSize = false;
            this.tsslUser.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslUser.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslUser.Name = "tsslUser";
            this.tsslUser.Size = new System.Drawing.Size(200, 17);
            this.tsslUser.Text = "tsslUser";
            this.tsslUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslWeight
            // 
            this.tsslWeight.AutoSize = false;
            this.tsslWeight.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsslWeight.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.tsslWeight.Name = "tsslWeight";
            this.tsslWeight.Size = new System.Drawing.Size(200, 17);
            this.tsslWeight.Text = ".";
            this.tsslWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageControl);
            this.tabControlMain.Controls.Add(this.tpReports);
            this.tabControlMain.Controls.Add(this.tabPageDebug);
            this.tabControlMain.Controls.Add(this.tpTCPServerDebug);
            this.tabControlMain.Controls.Add(this.tpUnused);
            this.tabControlMain.Controls.Add(this.tpLabelReprint);
            this.tabControlMain.Controls.Add(this.tabPage1);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 28);
            this.tabControlMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1664, 990);
            this.tabControlMain.TabIndex = 2;
            this.tabControlMain.SelectedIndexChanged += new System.EventHandler(this.tabControlMain_SelectedIndexChanged);
            // 
            // tabPageControl
            // 
            this.tabPageControl.Controls.Add(this.tableLayoutPanel1);
            this.tabPageControl.Location = new System.Drawing.Point(4, 25);
            this.tabPageControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageControl.Name = "tabPageControl";
            this.tabPageControl.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageControl.Size = new System.Drawing.Size(1656, 961);
            this.tabPageControl.TabIndex = 0;
            this.tabPageControl.Text = "Control";
            this.tabPageControl.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1467F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel14, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 677F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1648, 953);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.tableLayoutPanel3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1471, 274);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.tableLayoutPanel1.SetRowSpan(this.panel3, 3);
            this.panel3.Size = new System.Drawing.Size(173, 675);
            this.panel3.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.Controls.Add(this.tlpMain, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(171, 673);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 10;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(31, 29);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 10;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMain.Size = new System.Drawing.Size(101, 615);
            this.tlpMain.TabIndex = 0;
            this.tlpMain.Paint += new System.Windows.Forms.PaintEventHandler(this.tlpMain_Paint);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tableLayoutPanel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1459, 84);
            this.panel2.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.Controls.Add(this.bDispStroerSubpackOn, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.bDispStroerOn, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.bDispSubpackAddingOn, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.bDispWaybillOn, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.bOperationAddManual, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.bOperationRollback, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1457, 82);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // bDispStroerSubpackOn
            // 
            this.bDispStroerSubpackOn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bDispStroerSubpackOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDispStroerSubpackOn.Location = new System.Drawing.Point(1214, 4);
            this.bDispStroerSubpackOn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDispStroerSubpackOn.Name = "bDispStroerSubpackOn";
            this.bDispStroerSubpackOn.Size = new System.Drawing.Size(239, 74);
            this.bDispStroerSubpackOn.TabIndex = 22;
            this.bDispStroerSubpackOn.Text = "Z mag. część.";
            this.bDispStroerSubpackOn.UseVisualStyleBackColor = true;
            // 
            // bDispStroerOn
            // 
            this.bDispStroerOn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bDispStroerOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDispStroerOn.Location = new System.Drawing.Point(972, 4);
            this.bDispStroerOn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDispStroerOn.Name = "bDispStroerOn";
            this.bDispStroerOn.Size = new System.Drawing.Size(234, 74);
            this.bDispStroerOn.TabIndex = 21;
            this.bDispStroerOn.Text = "Z magazynu";
            this.bDispStroerOn.UseVisualStyleBackColor = true;
            // 
            // bDispSubpackAddingOn
            // 
            this.bDispSubpackAddingOn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bDispSubpackAddingOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDispSubpackAddingOn.Location = new System.Drawing.Point(730, 4);
            this.bDispSubpackAddingOn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDispSubpackAddingOn.Name = "bDispSubpackAddingOn";
            this.bDispSubpackAddingOn.Size = new System.Drawing.Size(234, 74);
            this.bDispSubpackAddingOn.TabIndex = 19;
            this.bDispSubpackAddingOn.Text = "Wysyłka częściowa";
            this.bDispSubpackAddingOn.UseVisualStyleBackColor = true;
            // 
            // bDispWaybillOn
            // 
            this.bDispWaybillOn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bDispWaybillOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDispWaybillOn.Location = new System.Drawing.Point(488, 4);
            this.bDispWaybillOn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDispWaybillOn.Name = "bDispWaybillOn";
            this.bDispWaybillOn.Size = new System.Drawing.Size(234, 74);
            this.bDispWaybillOn.TabIndex = 18;
            this.bDispWaybillOn.Text = "Wysyłki";
            this.bDispWaybillOn.UseVisualStyleBackColor = true;
            // 
            // bOperationAddManual
            // 
            this.bOperationAddManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bOperationAddManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bOperationAddManual.Image = global::AutojetFinishing.Properties.Resources._1444836762_f_check_256;
            this.bOperationAddManual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bOperationAddManual.Location = new System.Drawing.Point(246, 4);
            this.bOperationAddManual.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bOperationAddManual.Name = "bOperationAddManual";
            this.bOperationAddManual.Size = new System.Drawing.Size(234, 74);
            this.bOperationAddManual.TabIndex = 3;
            this.bOperationAddManual.Text = "Zakończ kompletację bez brakujących brytów";
            this.bOperationAddManual.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.bOperationAddManual.UseVisualStyleBackColor = true;
            this.bOperationAddManual.Click += new System.EventHandler(this.bOperationAddManual_Click);
            // 
            // bOperationRollback
            // 
            this.bOperationRollback.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bOperationRollback.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bOperationRollback.Image = global::AutojetFinishing.Properties.Resources._1444836784_f_cross_256;
            this.bOperationRollback.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bOperationRollback.Location = new System.Drawing.Point(4, 4);
            this.bOperationRollback.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bOperationRollback.Name = "bOperationRollback";
            this.bOperationRollback.Size = new System.Drawing.Size(234, 74);
            this.bOperationRollback.TabIndex = 2;
            this.bOperationRollback.Text = "Anuluj kompletację";
            this.bOperationRollback.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.bOperationRollback.UseVisualStyleBackColor = true;
            this.bOperationRollback.Click += new System.EventHandler(this.bOperationRollback_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.bInfoOkErr);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(4, 1049);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1459, 1);
            this.panel4.TabIndex = 3;
            // 
            // bInfoOkErr
            // 
            this.bInfoOkErr.BackColor = System.Drawing.Color.Transparent;
            this.bInfoOkErr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bInfoOkErr.Location = new System.Drawing.Point(0, 0);
            this.bInfoOkErr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bInfoOkErr.Name = "bInfoOkErr";
            this.bInfoOkErr.Size = new System.Drawing.Size(1457, 0);
            this.bInfoOkErr.TabIndex = 1;
            this.bInfoOkErr.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.tableLayoutPanel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(4, 96);
            this.panel7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel7.Name = "panel7";
            this.tableLayoutPanel1.SetRowSpan(this.panel7, 2);
            this.panel7.Size = new System.Drawing.Size(1459, 268);
            this.panel7.TabIndex = 4;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel8.Controls.Add(this.lJobformat, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.lUserName, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.label12, 0, 7);
            this.tableLayoutPanel8.Controls.Add(this.lWeightNet, 1, 7);
            this.tableLayoutPanel8.Controls.Add(this.lJdnrJdline, 1, 6);
            this.tableLayoutPanel8.Controls.Add(this.label2, 0, 6);
            this.tableLayoutPanel8.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.lLinedesc, 1, 5);
            this.tableLayoutPanel8.Controls.Add(this.lCustomer, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.lCampaigndesc, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.label15, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.lCampaignname, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.label20, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label14, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.lQtyordered, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.label13, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.lWeightModel, 2, 7);
            this.tableLayoutPanel8.Controls.Add(this.lWeightTare, 3, 7);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 8;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1457, 266);
            this.tableLayoutPanel8.TabIndex = 14;
            // 
            // lJobformat
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lJobformat, 2);
            this.lJobformat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lJobformat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lJobformat.Location = new System.Drawing.Point(204, 0);
            this.lJobformat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lJobformat.Name = "lJobformat";
            this.lJobformat.Size = new System.Drawing.Size(746, 33);
            this.lJobformat.TabIndex = 20;
            this.lJobformat.Text = ".";
            this.lJobformat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lUserName
            // 
            this.lUserName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lUserName.Location = new System.Drawing.Point(958, 0);
            this.lUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lUserName.Name = "lUserName";
            this.lUserName.Size = new System.Drawing.Size(495, 33);
            this.lUserName.TabIndex = 19;
            this.lUserName.Text = ".";
            this.lUserName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(4, 231);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(192, 35);
            this.label12.TabIndex = 18;
            this.label12.Text = "Waga:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lWeightNet
            // 
            this.lWeightNet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lWeightNet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lWeightNet.Location = new System.Drawing.Point(204, 231);
            this.lWeightNet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lWeightNet.Name = "lWeightNet";
            this.lWeightNet.Size = new System.Drawing.Size(369, 35);
            this.lWeightNet.TabIndex = 17;
            this.lWeightNet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lJdnrJdline
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lJdnrJdline, 3);
            this.lJdnrJdline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lJdnrJdline.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lJdnrJdline.Location = new System.Drawing.Point(204, 198);
            this.lJdnrJdline.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lJdnrJdline.Name = "lJdnrJdline";
            this.lJdnrJdline.Size = new System.Drawing.Size(1249, 33);
            this.lJdnrJdline.TabIndex = 15;
            this.lJdnrJdline.Text = ".";
            this.lJdnrJdline.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(4, 198);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 33);
            this.label2.TabIndex = 14;
            this.label2.Text = "Karta/linia";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(4, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(192, 33);
            this.label10.TabIndex = 0;
            this.label10.Text = "Format pracy";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lLinedesc
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lLinedesc, 3);
            this.lLinedesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lLinedesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lLinedesc.Location = new System.Drawing.Point(204, 165);
            this.lLinedesc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lLinedesc.Name = "lLinedesc";
            this.lLinedesc.Size = new System.Drawing.Size(1249, 33);
            this.lLinedesc.TabIndex = 11;
            this.lLinedesc.Text = ".";
            this.lLinedesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lCustomer
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lCustomer, 3);
            this.lCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lCustomer.Location = new System.Drawing.Point(204, 66);
            this.lCustomer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lCustomer.Name = "lCustomer";
            this.lCustomer.Size = new System.Drawing.Size(1249, 33);
            this.lCustomer.TabIndex = 13;
            this.lCustomer.Text = ".";
            this.lCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lCampaigndesc
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lCampaigndesc, 3);
            this.lCampaigndesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lCampaigndesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lCampaigndesc.Location = new System.Drawing.Point(204, 132);
            this.lCampaigndesc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lCampaigndesc.Name = "lCampaigndesc";
            this.lCampaigndesc.Size = new System.Drawing.Size(1249, 33);
            this.lCampaigndesc.TabIndex = 10;
            this.lCampaigndesc.Text = ".";
            this.lCampaigndesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(4, 165);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(192, 33);
            this.label15.TabIndex = 5;
            this.label15.Text = "Opis pracy";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(4, 33);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(192, 33);
            this.label11.TabIndex = 1;
            this.label11.Text = "Il. zam. / wydr.";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lCampaignname
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lCampaignname, 3);
            this.lCampaignname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lCampaignname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lCampaignname.Location = new System.Drawing.Point(204, 99);
            this.lCampaignname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lCampaignname.Name = "lCampaignname";
            this.lCampaignname.Size = new System.Drawing.Size(1249, 33);
            this.lCampaignname.TabIndex = 9;
            this.lCampaignname.Text = ".";
            this.lCampaignname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(4, 66);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(192, 33);
            this.label20.TabIndex = 12;
            this.label20.Text = "Klient";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(4, 132);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(192, 33);
            this.label14.TabIndex = 4;
            this.label14.Text = "Opis kampanii";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lQtyordered
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lQtyordered, 3);
            this.lQtyordered.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lQtyordered.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lQtyordered.Location = new System.Drawing.Point(204, 33);
            this.lQtyordered.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lQtyordered.Name = "lQtyordered";
            this.lQtyordered.Size = new System.Drawing.Size(1249, 33);
            this.lQtyordered.TabIndex = 7;
            this.lQtyordered.Text = ".";
            this.lQtyordered.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(4, 99);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(192, 33);
            this.label13.TabIndex = 3;
            this.label13.Text = "Nazwa kampanii";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lWeightModel
            // 
            this.lWeightModel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lWeightModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lWeightModel.Location = new System.Drawing.Point(581, 231);
            this.lWeightModel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lWeightModel.Name = "lWeightModel";
            this.lWeightModel.Size = new System.Drawing.Size(369, 35);
            this.lWeightModel.TabIndex = 21;
            this.lWeightModel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lWeightTare
            // 
            this.lWeightTare.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lWeightTare.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lWeightTare.Location = new System.Drawing.Point(958, 231);
            this.lWeightTare.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lWeightTare.Name = "lWeightTare";
            this.lWeightTare.Size = new System.Drawing.Size(495, 35);
            this.lWeightTare.TabIndex = 22;
            this.lWeightTare.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(1471, 4);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(173, 262);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 12;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel2.Controls.Add(this.lQtyClosed, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.bWorkModeFinishing2, 8, 1);
            this.tableLayoutPanel2.Controls.Add(this.bWorkModeSingleRow, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.bWorkModeAllPoster, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.pBarcodesFence, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pBarcodesLadder, 6, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(171, 260);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // lQtyClosed
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.lQtyClosed, 12);
            this.lQtyClosed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lQtyClosed.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lQtyClosed.Location = new System.Drawing.Point(4, 98);
            this.lQtyClosed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lQtyClosed.Name = "lQtyClosed";
            this.lQtyClosed.Size = new System.Drawing.Size(163, 162);
            this.lQtyClosed.TabIndex = 0;
            this.lQtyClosed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lQtyClosed.Click += new System.EventHandler(this.lQtyClosed_Click);
            // 
            // bWorkModeFinishing2
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.bWorkModeFinishing2, 4);
            this.bWorkModeFinishing2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bWorkModeFinishing2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bWorkModeFinishing2.Location = new System.Drawing.Point(116, 53);
            this.bWorkModeFinishing2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bWorkModeFinishing2.Name = "bWorkModeFinishing2";
            this.bWorkModeFinishing2.Size = new System.Drawing.Size(51, 41);
            this.bWorkModeFinishing2.TabIndex = 14;
            this.bWorkModeFinishing2.Text = "Składanie rzędów";
            this.bWorkModeFinishing2.UseVisualStyleBackColor = true;
            this.bWorkModeFinishing2.Click += new System.EventHandler(this.bWorkModeFinishing2_Click);
            // 
            // bWorkModeSingleRow
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.bWorkModeSingleRow, 4);
            this.bWorkModeSingleRow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bWorkModeSingleRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bWorkModeSingleRow.Location = new System.Drawing.Point(4, 53);
            this.bWorkModeSingleRow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bWorkModeSingleRow.Name = "bWorkModeSingleRow";
            this.bWorkModeSingleRow.Size = new System.Drawing.Size(48, 41);
            this.bWorkModeSingleRow.TabIndex = 12;
            this.bWorkModeSingleRow.Text = "Pojedyncze rzędy";
            this.bWorkModeSingleRow.UseVisualStyleBackColor = true;
            this.bWorkModeSingleRow.Click += new System.EventHandler(this.bWorkModeSingleRow_Click);
            // 
            // bWorkModeAllPoster
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.bWorkModeAllPoster, 4);
            this.bWorkModeAllPoster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bWorkModeAllPoster.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bWorkModeAllPoster.Location = new System.Drawing.Point(60, 53);
            this.bWorkModeAllPoster.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bWorkModeAllPoster.Name = "bWorkModeAllPoster";
            this.bWorkModeAllPoster.Size = new System.Drawing.Size(48, 41);
            this.bWorkModeAllPoster.TabIndex = 13;
            this.bWorkModeAllPoster.Text = "Cały plakat";
            this.bWorkModeAllPoster.UseVisualStyleBackColor = true;
            this.bWorkModeAllPoster.Click += new System.EventHandler(this.bWorkModeAllPoster_Click);
            // 
            // pBarcodesFence
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.pBarcodesFence, 6);
            this.pBarcodesFence.Controls.Add(this.label5);
            this.pBarcodesFence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pBarcodesFence.Location = new System.Drawing.Point(4, 4);
            this.pBarcodesFence.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pBarcodesFence.Name = "pBarcodesFence";
            this.pBarcodesFence.Size = new System.Drawing.Size(76, 41);
            this.pBarcodesFence.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 41);
            this.label5.TabIndex = 0;
            this.label5.Text = "PIONOWE";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pBarcodesLadder
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.pBarcodesLadder, 6);
            this.pBarcodesLadder.Controls.Add(this.label7);
            this.pBarcodesLadder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pBarcodesLadder.Location = new System.Drawing.Point(88, 4);
            this.pBarcodesLadder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pBarcodesLadder.Name = "pBarcodesLadder";
            this.pBarcodesLadder.Size = new System.Drawing.Size(79, 41);
            this.pBarcodesLadder.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 41);
            this.label7.TabIndex = 1;
            this.label7.Text = "POZIOME";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.tableLayoutPanel7);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(4, 372);
            this.panel14.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1459, 669);
            this.panel14.TabIndex = 5;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.panel15, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.panel16, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.lDispSubpackAddInfo, 0, 2);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1457, 667);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label1);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(4, 4);
            this.panel15.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1449, 41);
            this.panel15.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1449, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wysyłka";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.dgvDispatchTable);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(4, 53);
            this.panel16.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1449, 609);
            this.panel16.TabIndex = 1;
            // 
            // dgvDispatchTable
            // 
            this.dgvDispatchTable.AllowUserToAddRows = false;
            this.dgvDispatchTable.AllowUserToDeleteRows = false;
            this.dgvDispatchTable.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDispatchTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDispatchTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDispatchTable.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDispatchTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDispatchTable.Location = new System.Drawing.Point(0, 0);
            this.dgvDispatchTable.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvDispatchTable.Name = "dgvDispatchTable";
            this.dgvDispatchTable.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDispatchTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDispatchTable.RowHeadersVisible = false;
            this.dgvDispatchTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDispatchTable.Size = new System.Drawing.Size(1449, 609);
            this.dgvDispatchTable.TabIndex = 0;
            // 
            // lDispSubpackAddInfo
            // 
            this.lDispSubpackAddInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lDispSubpackAddInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lDispSubpackAddInfo.ForeColor = System.Drawing.Color.Red;
            this.lDispSubpackAddInfo.Location = new System.Drawing.Point(4, 666);
            this.lDispSubpackAddInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lDispSubpackAddInfo.Name = "lDispSubpackAddInfo";
            this.lDispSubpackAddInfo.Size = new System.Drawing.Size(1449, 1);
            this.lDispSubpackAddInfo.TabIndex = 2;
            this.lDispSubpackAddInfo.Text = "Wskaż co dołączasz skanując NRKP.NRLINIIKP lub kod z brytu. Jeżeli chcesz przerwa" +
    "ć dopakowywanie OPUŚĆ TRYB ŁĄCZENIA";
            this.lDispSubpackAddInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpReports
            // 
            this.tpReports.Controls.Add(this.tableLayoutPanel10);
            this.tpReports.Location = new System.Drawing.Point(4, 25);
            this.tpReports.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpReports.Name = "tpReports";
            this.tpReports.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpReports.Size = new System.Drawing.Size(1656, 954);
            this.tpReports.TabIndex = 6;
            this.tpReports.Text = "Raporty";
            this.tpReports.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.dgvReport, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.gbReportType, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.tcReports, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 320F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1648, 946);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // dgvReport
            // 
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel10.SetColumnSpan(this.dgvReport, 2);
            this.dgvReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReport.Location = new System.Drawing.Point(4, 324);
            this.dgvReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.Size = new System.Drawing.Size(1640, 618);
            this.dgvReport.TabIndex = 2;
            // 
            // gbReportType
            // 
            this.gbReportType.Controls.Add(this.bReportKpLkp);
            this.gbReportType.Controls.Add(this.bReportKp);
            this.gbReportType.Controls.Add(this.bReportDate);
            this.gbReportType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbReportType.Location = new System.Drawing.Point(4, 4);
            this.gbReportType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbReportType.Name = "gbReportType";
            this.gbReportType.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbReportType.Size = new System.Drawing.Size(816, 312);
            this.gbReportType.TabIndex = 0;
            this.gbReportType.TabStop = false;
            this.gbReportType.Text = "Raport";
            // 
            // bReportKpLkp
            // 
            this.bReportKpLkp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportKpLkp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bReportKpLkp.Location = new System.Drawing.Point(8, 160);
            this.bReportKpLkp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bReportKpLkp.Name = "bReportKpLkp";
            this.bReportKpLkp.Size = new System.Drawing.Size(800, 62);
            this.bReportKpLkp.TabIndex = 6;
            this.bReportKpLkp.Text = "Raport po numerze KP i LKP";
            this.bReportKpLkp.UseVisualStyleBackColor = true;
            this.bReportKpLkp.Click += new System.EventHandler(this.bReportKpLkp_Click);
            // 
            // bReportKp
            // 
            this.bReportKp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportKp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bReportKp.Location = new System.Drawing.Point(8, 91);
            this.bReportKp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bReportKp.Name = "bReportKp";
            this.bReportKp.Size = new System.Drawing.Size(800, 62);
            this.bReportKp.TabIndex = 5;
            this.bReportKp.Text = "Raport po numerze KP";
            this.bReportKp.UseVisualStyleBackColor = true;
            this.bReportKp.Click += new System.EventHandler(this.bReportKp_Click);
            // 
            // bReportDate
            // 
            this.bReportDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bReportDate.Location = new System.Drawing.Point(8, 22);
            this.bReportDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bReportDate.Name = "bReportDate";
            this.bReportDate.Size = new System.Drawing.Size(800, 62);
            this.bReportDate.TabIndex = 4;
            this.bReportDate.Text = "Raport po dacie wysyłki";
            this.bReportDate.UseVisualStyleBackColor = true;
            this.bReportDate.Click += new System.EventHandler(this.bReportDate_Click);
            // 
            // tcReports
            // 
            this.tcReports.Controls.Add(this.tpDate);
            this.tcReports.Controls.Add(this.tpKP);
            this.tcReports.Controls.Add(this.tpKPLKP);
            this.tcReports.Controls.Add(this.tpInfo);
            this.tcReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcReports.Location = new System.Drawing.Point(828, 4);
            this.tcReports.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tcReports.Name = "tcReports";
            this.tcReports.SelectedIndex = 0;
            this.tcReports.Size = new System.Drawing.Size(816, 312);
            this.tcReports.TabIndex = 1;
            // 
            // tpDate
            // 
            this.tpDate.Controls.Add(this.tableLayoutPanel11);
            this.tpDate.Location = new System.Drawing.Point(4, 25);
            this.tpDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpDate.Name = "tpDate";
            this.tpDate.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpDate.Size = new System.Drawing.Size(808, 283);
            this.tpDate.TabIndex = 0;
            this.tpDate.Text = "Parametry";
            this.tpDate.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.bReportMake11, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.panel13, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 3;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(800, 275);
            this.tableLayoutPanel11.TabIndex = 16;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label31);
            this.panel8.Controls.Add(this.dtpReportParam);
            this.panel8.Controls.Add(this.dtpReportNP);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(1, 1);
            this.panel8.Margin = new System.Windows.Forms.Padding(1);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(398, 149);
            this.panel8.TabIndex = 0;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label31.Location = new System.Drawing.Point(4, 7);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(72, 31);
            this.label31.TabIndex = 9;
            this.label31.Text = "Data";
            // 
            // dtpReportParam
            // 
            this.dtpReportParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpReportParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpReportParam.Location = new System.Drawing.Point(11, 42);
            this.dtpReportParam.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpReportParam.MinDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.dtpReportParam.Name = "dtpReportParam";
            this.dtpReportParam.Size = new System.Drawing.Size(367, 34);
            this.dtpReportParam.TabIndex = 8;
            // 
            // dtpReportNP
            // 
            this.dtpReportNP.AutoSize = true;
            this.dtpReportNP.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpReportNP.Location = new System.Drawing.Point(11, 86);
            this.dtpReportNP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpReportNP.Name = "dtpReportNP";
            this.dtpReportNP.Size = new System.Drawing.Size(357, 35);
            this.dtpReportNP.TabIndex = 12;
            this.dtpReportNP.Text = "Pokaż tylko niespakowane";
            this.dtpReportNP.UseVisualStyleBackColor = true;
            // 
            // bReportMake11
            // 
            this.bReportMake11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel11.SetColumnSpan(this.bReportMake11, 2);
            this.bReportMake11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bReportMake11.Location = new System.Drawing.Point(4, 217);
            this.bReportMake11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bReportMake11.Name = "bReportMake11";
            this.bReportMake11.Size = new System.Drawing.Size(792, 54);
            this.bReportMake11.TabIndex = 10;
            this.bReportMake11.Text = "GENERUJ RAPORT";
            this.bReportMake11.UseVisualStyleBackColor = true;
            this.bReportMake11.Click += new System.EventHandler(this.bReportMake11_Click);
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label39);
            this.panel13.Controls.Add(this.dtpReportNS);
            this.panel13.Controls.Add(this.Courier);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(401, 1);
            this.panel13.Margin = new System.Windows.Forms.Padding(1);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(398, 149);
            this.panel13.TabIndex = 1;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label39.Location = new System.Drawing.Point(4, 7);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(86, 31);
            this.label39.TabIndex = 15;
            this.label39.Text = "Kurier";
            // 
            // dtpReportNS
            // 
            this.dtpReportNS.AutoSize = true;
            this.dtpReportNS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpReportNS.Location = new System.Drawing.Point(11, 86);
            this.dtpReportNS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpReportNS.Name = "dtpReportNS";
            this.dtpReportNS.Size = new System.Drawing.Size(318, 35);
            this.dtpReportNS.TabIndex = 13;
            this.dtpReportNS.Text = "Pokaż tylko niewysłane";
            this.dtpReportNS.UseVisualStyleBackColor = true;
            // 
            // Courier
            // 
            this.Courier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Courier.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Courier.FormattingEnabled = true;
            this.Courier.Location = new System.Drawing.Point(11, 39);
            this.Courier.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Courier.Name = "Courier";
            this.Courier.Size = new System.Drawing.Size(367, 38);
            this.Courier.TabIndex = 14;
            this.Courier.Text = "WSZYSCY";
            // 
            // tpKP
            // 
            this.tpKP.Controls.Add(this.lReportKpError);
            this.tpKP.Controls.Add(this.bReportMake21);
            this.tpKP.Controls.Add(this.tbReportKp);
            this.tpKP.Controls.Add(this.label9);
            this.tpKP.Location = new System.Drawing.Point(4, 25);
            this.tpKP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpKP.Name = "tpKP";
            this.tpKP.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpKP.Size = new System.Drawing.Size(807, 284);
            this.tpKP.TabIndex = 1;
            this.tpKP.Text = "Parametry";
            this.tpKP.UseVisualStyleBackColor = true;
            // 
            // lReportKpError
            // 
            this.lReportKpError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportKpError.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportKpError.ForeColor = System.Drawing.Color.Red;
            this.lReportKpError.Location = new System.Drawing.Point(15, 89);
            this.lReportKpError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lReportKpError.Name = "lReportKpError";
            this.lReportKpError.Size = new System.Drawing.Size(769, 47);
            this.lReportKpError.TabIndex = 9;
            this.lReportKpError.Text = "Błędny numer KP";
            this.lReportKpError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bReportMake21
            // 
            this.bReportMake21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bReportMake21.Location = new System.Drawing.Point(15, 206);
            this.bReportMake21.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bReportMake21.Name = "bReportMake21";
            this.bReportMake21.Size = new System.Drawing.Size(769, 59);
            this.bReportMake21.TabIndex = 8;
            this.bReportMake21.Text = "GENERUJ RAPORT";
            this.bReportMake21.UseVisualStyleBackColor = true;
            this.bReportMake21.Click += new System.EventHandler(this.bReportMake21_Click);
            // 
            // tbReportKp
            // 
            this.tbReportKp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReportKp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportKp.Location = new System.Drawing.Point(15, 47);
            this.tbReportKp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbReportKp.Name = "tbReportKp";
            this.tbReportKp.Size = new System.Drawing.Size(768, 37);
            this.tbReportKp.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(8, 12);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(153, 31);
            this.label9.TabIndex = 6;
            this.label9.Text = "Numeru KP";
            // 
            // tpKPLKP
            // 
            this.tpKPLKP.Controls.Add(this.lReportKpLkpError);
            this.tpKPLKP.Controls.Add(this.bReportMake31);
            this.tpKPLKP.Controls.Add(this.tbReportKpLkp);
            this.tpKPLKP.Controls.Add(this.label32);
            this.tpKPLKP.Location = new System.Drawing.Point(4, 25);
            this.tpKPLKP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpKPLKP.Name = "tpKPLKP";
            this.tpKPLKP.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpKPLKP.Size = new System.Drawing.Size(807, 284);
            this.tpKPLKP.TabIndex = 2;
            this.tpKPLKP.Text = "Parametry";
            this.tpKPLKP.UseVisualStyleBackColor = true;
            // 
            // lReportKpLkpError
            // 
            this.lReportKpLkpError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lReportKpLkpError.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lReportKpLkpError.ForeColor = System.Drawing.Color.Red;
            this.lReportKpLkpError.Location = new System.Drawing.Point(15, 85);
            this.lReportKpLkpError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lReportKpLkpError.Name = "lReportKpLkpError";
            this.lReportKpLkpError.Size = new System.Drawing.Size(769, 47);
            this.lReportKpLkpError.TabIndex = 13;
            this.lReportKpLkpError.Text = "Błędny numer KP.LKP";
            this.lReportKpLkpError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bReportMake31
            // 
            this.bReportMake31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bReportMake31.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bReportMake31.Location = new System.Drawing.Point(15, 203);
            this.bReportMake31.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bReportMake31.Name = "bReportMake31";
            this.bReportMake31.Size = new System.Drawing.Size(769, 59);
            this.bReportMake31.TabIndex = 12;
            this.bReportMake31.Text = "GENERUJ RAPORT";
            this.bReportMake31.UseVisualStyleBackColor = true;
            this.bReportMake31.Click += new System.EventHandler(this.bReportMake31_Click);
            // 
            // tbReportKpLkp
            // 
            this.tbReportKpLkp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbReportKpLkp.Location = new System.Drawing.Point(15, 47);
            this.tbReportKpLkp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbReportKpLkp.Name = "tbReportKpLkp";
            this.tbReportKpLkp.Size = new System.Drawing.Size(768, 37);
            this.tbReportKpLkp.TabIndex = 11;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label32.Location = new System.Drawing.Point(8, 12);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(212, 31);
            this.label32.TabIndex = 10;
            this.label32.Text = "Numeru KP.LKP";
            // 
            // tpInfo
            // 
            this.tpInfo.Location = new System.Drawing.Point(4, 25);
            this.tpInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpInfo.Name = "tpInfo";
            this.tpInfo.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpInfo.Size = new System.Drawing.Size(807, 284);
            this.tpInfo.TabIndex = 3;
            this.tpInfo.Text = "Info";
            this.tpInfo.UseVisualStyleBackColor = true;
            // 
            // tabPageDebug
            // 
            this.tabPageDebug.Controls.Add(this.tableLayoutPanel6);
            this.tabPageDebug.Location = new System.Drawing.Point(4, 25);
            this.tabPageDebug.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageDebug.Name = "tabPageDebug";
            this.tabPageDebug.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageDebug.Size = new System.Drawing.Size(1656, 954);
            this.tabPageDebug.TabIndex = 1;
            this.tabPageDebug.Text = "Debug";
            this.tabPageDebug.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel6.Controls.Add(this.debugTestBox, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1648, 946);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // debugTestBox
            // 
            this.debugTestBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.debugTestBox.Location = new System.Drawing.Point(4, 53);
            this.debugTestBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.debugTestBox.Name = "debugTestBox";
            this.debugTestBox.Size = new System.Drawing.Size(1640, 889);
            this.debugTestBox.TabIndex = 0;
            this.debugTestBox.Text = "";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.cbPCErrorSymulaton);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(4, 4);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1640, 41);
            this.panel5.TabIndex = 1;
            // 
            // cbPCErrorSymulaton
            // 
            this.cbPCErrorSymulaton.AutoSize = true;
            this.cbPCErrorSymulaton.Location = new System.Drawing.Point(1339, 11);
            this.cbPCErrorSymulaton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPCErrorSymulaton.Name = "cbPCErrorSymulaton";
            this.cbPCErrorSymulaton.Size = new System.Drawing.Size(173, 21);
            this.cbPCErrorSymulaton.TabIndex = 0;
            this.cbPCErrorSymulaton.Text = "Test sygnalizacji błędu";
            this.cbPCErrorSymulaton.UseVisualStyleBackColor = true;
            this.cbPCErrorSymulaton.CheckedChanged += new System.EventHandler(this.cbPCErrorSymulaton_CheckedChanged);
            // 
            // tpTCPServerDebug
            // 
            this.tpTCPServerDebug.Controls.Add(this.tableLayoutPanel4);
            this.tpTCPServerDebug.Location = new System.Drawing.Point(4, 25);
            this.tpTCPServerDebug.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpTCPServerDebug.Name = "tpTCPServerDebug";
            this.tpTCPServerDebug.Size = new System.Drawing.Size(1656, 954);
            this.tpTCPServerDebug.TabIndex = 2;
            this.tpTCPServerDebug.Text = "tpTCPServerDebug";
            this.tpTCPServerDebug.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.panel9, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel10, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel11, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.panel12, 0, 3);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1656, 954);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.listMessages);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(4, 53);
            this.panel9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1648, 420);
            this.panel9.TabIndex = 0;
            // 
            // listMessages
            // 
            this.listMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listMessages.FormattingEnabled = true;
            this.listMessages.ItemHeight = 16;
            this.listMessages.Location = new System.Drawing.Point(0, 0);
            this.listMessages.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listMessages.Name = "listMessages";
            this.listMessages.Size = new System.Drawing.Size(1648, 420);
            this.listMessages.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.button1);
            this.panel10.Controls.Add(this.label17);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(4, 4);
            this.panel10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1648, 41);
            this.panel10.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(248, 6);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 12);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 17);
            this.label17.TabIndex = 0;
            this.label17.Text = "Messages";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label18);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(4, 481);
            this.panel11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1648, 41);
            this.panel11.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 14);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 17);
            this.label18.TabIndex = 1;
            this.label18.Text = "Log";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.listLog);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(4, 530);
            this.panel12.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1648, 420);
            this.panel12.TabIndex = 3;
            // 
            // listLog
            // 
            this.listLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listLog.FormattingEnabled = true;
            this.listLog.ItemHeight = 16;
            this.listLog.Location = new System.Drawing.Point(0, 0);
            this.listLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listLog.Name = "listLog";
            this.listLog.Size = new System.Drawing.Size(1648, 420);
            this.listLog.TabIndex = 0;
            // 
            // tpUnused
            // 
            this.tpUnused.Controls.Add(this.bTestInput);
            this.tpUnused.Controls.Add(this.tbInputStr);
            this.tpUnused.Controls.Add(this.rtbErrorList);
            this.tpUnused.Controls.Add(this.dgvMain);
            this.tpUnused.Location = new System.Drawing.Point(4, 25);
            this.tpUnused.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpUnused.Name = "tpUnused";
            this.tpUnused.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpUnused.Size = new System.Drawing.Size(1656, 954);
            this.tpUnused.TabIndex = 3;
            this.tpUnused.Text = "Unused";
            this.tpUnused.UseVisualStyleBackColor = true;
            // 
            // bTestInput
            // 
            this.bTestInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bTestInput.Location = new System.Drawing.Point(1221, 50);
            this.bTestInput.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bTestInput.Name = "bTestInput";
            this.bTestInput.Size = new System.Drawing.Size(100, 28);
            this.bTestInput.TabIndex = 4;
            this.bTestInput.Text = "Test";
            this.bTestInput.UseVisualStyleBackColor = true;
            this.bTestInput.Visible = false;
            // 
            // tbInputStr
            // 
            this.tbInputStr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbInputStr.Location = new System.Drawing.Point(1329, 52);
            this.tbInputStr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbInputStr.Name = "tbInputStr";
            this.tbInputStr.Size = new System.Drawing.Size(228, 22);
            this.tbInputStr.TabIndex = 3;
            this.tbInputStr.Text = "0697490020A03041";
            this.tbInputStr.Visible = false;
            // 
            // rtbErrorList
            // 
            this.rtbErrorList.Location = new System.Drawing.Point(32, 240);
            this.rtbErrorList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rtbErrorList.Name = "rtbErrorList";
            this.rtbErrorList.Size = new System.Drawing.Size(455, 47);
            this.rtbErrorList.TabIndex = 2;
            this.rtbErrorList.Text = "";
            // 
            // dgvMain
            // 
            this.dgvMain.AllowUserToAddRows = false;
            this.dgvMain.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMain.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMain.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvMain.Location = new System.Drawing.Point(32, 74);
            this.dgvMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvMain.MultiSelect = false;
            this.dgvMain.Name = "dgvMain";
            this.dgvMain.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMain.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMain.Size = new System.Drawing.Size(1115, 121);
            this.dgvMain.TabIndex = 1;
            // 
            // tpLabelReprint
            // 
            this.tpLabelReprint.Controls.Add(this.tableLayoutPanel9);
            this.tpLabelReprint.Location = new System.Drawing.Point(4, 25);
            this.tpLabelReprint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpLabelReprint.Name = "tpLabelReprint";
            this.tpLabelReprint.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpLabelReprint.Size = new System.Drawing.Size(1656, 961);
            this.tpLabelReprint.TabIndex = 4;
            this.tpLabelReprint.Text = "Etykiety";
            this.tpLabelReprint.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.lRepintRefTo, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.label8, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.lRepintRefFrom, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.lRepintRefId, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.lLabelReprintTitle, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.panel6, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 6;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.63636F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.63636F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.63636F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.63636F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1648, 953);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // lRepintRefTo
            // 
            this.lRepintRefTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lRepintRefTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lRepintRefTo.Location = new System.Drawing.Point(828, 709);
            this.lRepintRefTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lRepintRefTo.Name = "lRepintRefTo";
            this.lRepintRefTo.Size = new System.Drawing.Size(816, 121);
            this.lRepintRefTo.TabIndex = 7;
            this.lRepintRefTo.Text = ".";
            this.lRepintRefTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(4, 709);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(816, 121);
            this.label8.TabIndex = 6;
            this.label8.Text = "Ref to";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lRepintRefFrom
            // 
            this.lRepintRefFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lRepintRefFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lRepintRefFrom.Location = new System.Drawing.Point(828, 588);
            this.lRepintRefFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lRepintRefFrom.Name = "lRepintRefFrom";
            this.lRepintRefFrom.Size = new System.Drawing.Size(816, 121);
            this.lRepintRefFrom.TabIndex = 5;
            this.lRepintRefFrom.Text = ".";
            this.lRepintRefFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(4, 588);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(816, 121);
            this.label6.TabIndex = 4;
            this.label6.Text = "Ref from";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lRepintRefId
            // 
            this.lRepintRefId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lRepintRefId.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lRepintRefId.Location = new System.Drawing.Point(828, 467);
            this.lRepintRefId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lRepintRefId.Name = "lRepintRefId";
            this.lRepintRefId.Size = new System.Drawing.Size(816, 121);
            this.lRepintRefId.TabIndex = 3;
            this.lRepintRefId.Text = ".";
            this.lRepintRefId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lLabelReprintTitle
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.lLabelReprintTitle, 2);
            this.lLabelReprintTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lLabelReprintTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lLabelReprintTitle.Location = new System.Drawing.Point(4, 0);
            this.lLabelReprintTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lLabelReprintTitle.Name = "lLabelReprintTitle";
            this.lLabelReprintTitle.Size = new System.Drawing.Size(1640, 62);
            this.lLabelReprintTitle.TabIndex = 0;
            this.lLabelReprintTitle.Text = "Drukowanie kopii etykiety";
            this.lLabelReprintTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.panel6, 2);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(4, 66);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1640, 397);
            this.panel6.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1640, 397);
            this.label3.TabIndex = 0;
            this.label3.Text = "Zeskanuj kod z brytu aby wydrukować etykietę";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(4, 467);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(816, 121);
            this.label4.TabIndex = 2;
            this.label4.Text = "Ref ID";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(1656, 954);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tConnections
            // 
            this.tConnections.Interval = 1000;
            this.tConnections.Tick += new System.EventHandler(this.tConnections_Tick);
            // 
            // tMsgOk
            // 
            this.tMsgOk.Tick += new System.EventHandler(this.tMsgOk_Tick);
            // 
            // tErrorReset
            // 
            this.tErrorReset.Interval = 300;
            this.tErrorReset.Tick += new System.EventHandler(this.tErrorReset_Tick);
            // 
            // tDispatchDone
            // 
            this.tDispatchDone.Interval = 500;
            this.tDispatchDone.Tick += new System.EventHandler(this.tDispatchDone_Tick);
            // 
            // bwPrintPdf
            // 
            this.bwPrintPdf.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwPrintPdf_DoWork);
            // 
            // bwPrintPdf2
            // 
            this.bwPrintPdf2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwPrintPdf_DoWork);
            // 
            // tcpServerCgnex
            // 
            this.tcpServerCgnex.Encoding = ((System.Text.Encoding)(resources.GetObject("tcpServerCgnex.Encoding")));
            this.tcpServerCgnex.IdleTime = 50;
            this.tcpServerCgnex.IsOpen = false;
            this.tcpServerCgnex.MaxCallbackThreads = 100;
            this.tcpServerCgnex.MaxSendAttempts = 3;
            this.tcpServerCgnex.Port = -1;
            this.tcpServerCgnex.VerifyConnectionInterval = 100;
            this.tcpServerCgnex.OnConnect += new tcpServer.tcpServerConnectionChanged(this.tcpServerCgnex_OnConnect);
            this.tcpServerCgnex.OnDataAvailable += new tcpServer.tcpServerConnectionChanged(this.tcpServerCgnex_OnDataAvailable);
            this.tcpServerCgnex.OnError += new tcpServer.tcpServerError(this.tcpServerCgnex_OnError);
            // 
            // autojetFinishingMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1664, 1040);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.mainStatusStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "autojetFinishingMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.autojetFinishingMainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.autojetFinishingMainForm_FormClosed);
            this.Load += new System.EventHandler(this.autojetFinishingMainForm_Load);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.mainStatusStrip.ResumeLayout(false);
            this.mainStatusStrip.PerformLayout();
            this.tabControlMain.ResumeLayout(false);
            this.tabPageControl.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.pBarcodesFence.ResumeLayout(false);
            this.pBarcodesLadder.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDispatchTable)).EndInit();
            this.tpReports.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.gbReportType.ResumeLayout(false);
            this.tcReports.ResumeLayout(false);
            this.tpDate.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.tpKP.ResumeLayout(false);
            this.tpKP.PerformLayout();
            this.tpKPLKP.ResumeLayout(false);
            this.tpKPLKP.PerformLayout();
            this.tabPageDebug.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tpTCPServerDebug.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.tpUnused.ResumeLayout(false);
            this.tpUnused.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
            this.tpLabelReprint.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.StatusStrip mainStatusStrip;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageControl;
        private System.Windows.Forms.TabPage tabPageDebug;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.RichTextBox debugTestBox;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Timer tConnections;
        private System.Windows.Forms.ToolStripStatusLabel statusStripeDb;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.ToolStripStatusLabel scannersConnectionStatus;
        private System.Windows.Forms.Button bOperationRollback;
        private System.Windows.Forms.Button bOperationAddManual;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lLinedesc;
        private System.Windows.Forms.Label lCampaigndesc;
        private System.Windows.Forms.Label lCampaignname;
        private System.Windows.Forms.Label lQtyordered;
        private System.Windows.Forms.Button bWorkModeAllPoster;
        private System.Windows.Forms.Button bWorkModeSingleRow;
        private System.Windows.Forms.ToolStripMenuItem formatyToolStripMenuItem;
        private System.Windows.Forms.Button bWorkModeFinishing2;
        private System.Windows.Forms.ToolStripMenuItem createDatabaseToolStripMenuItem;
        private System.Windows.Forms.TabPage tpTCPServerDebug;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ListBox listMessages;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListBox listLog;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripStatusLabel statusStripePLC;
        private System.Windows.Forms.ToolStripStatusLabel ststusStripCognex;
        private tcpServer.TcpServer tcpServerCgnex;
        private System.Windows.Forms.Label lCustomer;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Timer tMsgOk;
        private System.Windows.Forms.ToolStripMenuItem tsmiUsers;
        private System.Windows.Forms.ToolStripStatusLabel tsslUser;
        private System.Windows.Forms.TabPage tpUnused;
        private System.Windows.Forms.DataGridView dgvMain;
        private System.Windows.Forms.RichTextBox rtbErrorList;
        private System.Windows.Forms.CheckBox cbPCErrorSymulaton;
        private System.Windows.Forms.Timer tErrorReset;
        private System.Windows.Forms.ToolStripMenuItem showDebugToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvDispatchTable;
        private System.Windows.Forms.ToolStripMenuItem initDgvToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label lJdnrJdline;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bTestInput;
        private System.Windows.Forms.TextBox tbInputStr;
        private System.Windows.Forms.ToolStripMenuItem testPrinterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem initToolStripMenuItem;
        private System.Windows.Forms.Timer tDispatchDone;
        private System.Windows.Forms.ToolStripMenuItem dispToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findDispachToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findAddressesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateDispachCountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.Label lQtyClosed;
        private System.Windows.Forms.TabPage tpLabelReprint;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label lLabelReprintTitle;
        private System.Windows.Forms.ToolStripMenuItem findSubpackToZPLToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker bwPrintPdf;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lRepintRefTo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lRepintRefFrom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lRepintRefId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem tstToolStripMenuItem;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button bInfoOkErr;
        private System.Windows.Forms.Panel pBarcodesFence;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pBarcodesLadder;
        private System.Windows.Forms.Label label7;
        private System.ComponentModel.BackgroundWorker bwPrintPdf2;
        private System.Windows.Forms.Label lJobformat;
        private System.Windows.Forms.Label lUserName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lWeightNet;
        private System.Windows.Forms.Label lWeightModel;
        private System.Windows.Forms.Label lWeightTare;
        private System.Windows.Forms.ToolStripStatusLabel tsslWeight;
        private System.Windows.Forms.ToolStripMenuItem weightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkmodelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newmodelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getnumToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lDispSubpackAddInfo;
        private System.Windows.Forms.Button bDispStroerSubpackOn;
        private System.Windows.Forms.Button bDispStroerOn;
        private System.Windows.Forms.Button bDispSubpackAddingOn;
        private System.Windows.Forms.Button bDispWaybillOn;
        private System.Windows.Forms.TabPage tpReports;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.GroupBox gbReportType;
        private System.Windows.Forms.Button bReportKpLkp;
        private System.Windows.Forms.Button bReportKp;
        private System.Windows.Forms.Button bReportDate;
        private System.Windows.Forms.TabControl tcReports;
        private System.Windows.Forms.TabPage tpDate;
        private System.Windows.Forms.TabPage tpKP;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox Courier;
        private System.Windows.Forms.CheckBox dtpReportNS;
        private System.Windows.Forms.CheckBox dtpReportNP;
        private System.Windows.Forms.Button bReportMake11;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DateTimePicker dtpReportParam;
        private System.Windows.Forms.DataGridView dgvReport;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label lReportKpError;
        private System.Windows.Forms.Button bReportMake21;
        private System.Windows.Forms.TextBox tbReportKp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tpKPLKP;
        private System.Windows.Forms.Label lReportKpLkpError;
        private System.Windows.Forms.Button bReportMake31;
        private System.Windows.Forms.TextBox tbReportKpLkp;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tpInfo;
    }
}

