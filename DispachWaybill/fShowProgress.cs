﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DispachWaybill
{
    public partial class fShowProgress : Form
    {
        public fShowProgress()
        {
            InitializeComponent();
        }

        public class progressStatus
        {
            public int all;
            public int done;
        }

        public progressStatus progress = new progressStatus();

        //private const int CP_NOCLOSE_BUTTON = 0x200;
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams myCp = base.CreateParams;
        //        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        //        return myCp;
        //    }
        //}

        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }

        private void fShowProgress_Load(object sender, EventArgs e)
        {
            pbProgress.Maximum = progress.all;
            updateUI();
        }

        public void updateUI()
        {
            pbProgress.Value = progress.done;
            lProgress.Text = $"Wczytano {progress.done} z {progress.all}...";
        }

    }
}
