﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace DispachWaybill
{
   public class serialPortConnection
   {
		public delegate void onRs232Recieved(string recStr);
		public event onRs232Recieved dataRecieved;
    
		private SerialPort port;
		private bool disposed = false;

	    public serialPortConnection(SerialPort port)
		{
			this.port = port;
		}

		delegate void scanEventCallback(string scan);

		public void send(byte[] toSend)
	    {
			if (this.Connected())
				this.port.Write(toSend, 0, toSend.Length);
	    }

		private void scanEvent(string scan)
		{
            if (Program.DispachWaybillForm.InvokeRequired)
			{
				scanEventCallback d = new scanEventCallback(scanEvent);
                Program.DispachWaybillForm.BeginInvoke(d, scan);
			}
			else
			{
				dataRecieved(scan);
			}
		}

		private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
		{
			SerialPort sp = (SerialPort)sender;
			string indata = sp.ReadExisting();
			//indata = indata.Trim('\n', '\r');

			//Program.proPlatePrintMainForm.Hm.Info("RS-232 on port {0}: '{1}'", this.port.PortName, indata);
			scanEvent(indata);
		}

		public bool ReConnect(string comName, string comSpeed)
		{
			if (this.Connected())				
				return true;
			else
			{
				return Connect(comName, comSpeed);
			}
		}

		public bool Connect(string comName, string comSpeed)
		{
			if (!this.Connected())
			{
				this.port.BaudRate = int.Parse(comSpeed);
				this.port.Parity = Parity.None;
				this.port.DataBits = 8;
				this.port.PortName = comName;

				try
				{
					this.port.Open();
					this.port.DataReceived += this.DataReceivedHandler;
					//Program.proPlatePrintMainForm.Hm.Warning("RS 232C port on {0} connected OK", comName);
					return (true);
				}
				catch (Exception)
				{
					//Program.proPlatePrintMainForm.Hm.Warning("RS 232C port on {0} not connected", comName);
					return (false);
				}
			}
			else
			{
				//Program.proPlatePrintMainForm.Hm.Warning("RS 232C port on {0} alerady connected", comName);
				return (true);
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					// Free other state (managed objects).
				}

				// Free unmanaged objects.
				if (this.Connected())
				{
					this.port.Close();
				}

				disposed = true;
			}																											
		}

		~serialPortConnection()
		{
			Dispose(false);
		}

		public bool Connected()
		{
			return (this.port.IsOpen);
		}
   }
}
