﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using java.io;
using TikaOnDotNet.TextExtraction;

namespace DispachWaybill
{
    public static class dataAcces
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<DispatchAddress> getAllEmptyDispatchAddresses()
        {
            List<DispatchAddress> retVal = new List<DispatchAddress>();

            string query = "Select * from dispatch_address_table where file_name is null or file_name = '' and courier_type != 'INNY' and courier_type != 'OSOBISTY'";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel != null)
            {
                foreach (DataRow dRow in dTabel.Rows)
                {
                    DispatchAddress item = new DispatchAddress();

                    /*
                    //public int id { get; set; }
                    public string reference_id { get; set; }
                    public string file_name { get; set; }
                    public DateTime created_date { get; set; }
                    public DateTime mod_date { get; set; }
                    public string courier_type { get; set; }
                    public string dispatch_name { get; set; }
                    public string dispatch_surname { get; set; }
                    public string dispatch_tel { get; set; }
                    public DateTime dispatch_date { get; set; }
                    public string street { get; set; }
                    public string city { get; set; }
                    public string post_code { get; set; }
                    public string dispatch_company { get; set; }
                    //public string description { get; set; }
                    public string list_description { get; set; }    //new
                    public string pack_type { get; set; }           //new
                    public int total_packs { get; set; }            //new
                    public int ref_from { get; set; }               //new
                    public int ref_to { get; set; }                 //new
                    public string deliv_list_no { get; set; }       //new
                     */

                    //item.id = int.Parse(dRow["id"].ToString());
                    item.reference_id = dRow["reference_id"].ToString();
                    item.file_name = dRow["file_name"].ToString();
                    item.created_date = DateTime.Parse(dRow["created_date"].ToString());
                    item.mod_date = DateTime.Parse(dRow["mod_date"].ToString());
                    item.courier_type = dRow["courier_type"].ToString();
                    item.dispatch_name = dRow["dispatch_name"].ToString();
                    item.dispatch_surname = dRow["dispatch_surname"].ToString();
                    item.dispatch_tel = dRow["dispatch_tel"].ToString();
                    item.dispatch_date = DateTime.Parse(dRow["dispatch_date"].ToString());
                    item.street = dRow["street"].ToString();
                    item.city = dRow["city"].ToString();
                    item.post_code = dRow["post_code"].ToString();
                    item.dispatch_company = dRow["dispatch_company"].ToString();
                    //item.description = dRow["description"].ToString();
                    item.list_description = dRow["list_description"].ToString();
                    item.pack_type = dRow["pack_type"].ToString();
                    item.total_packs = int.Parse(dRow["total_packs"].ToString());
                    item.ref_from = int.Parse(dRow["ref_from"].ToString());
                    item.ref_to = int.Parse(dRow["ref_to"].ToString());
                    item.deliv_list_no = dRow["deliv_list_no"].ToString();
                    item.comments = dRow["comments"].ToString();
                    retVal.Add(item);
                }
            }

            return retVal;
        }

        static public List<Files> GetFileList(string filePath)
        {
            List<Files> retVal = new List<Files>();

            DirectoryInfo d = new DirectoryInfo(filePath);//Assuming Test is your Folder
            FileInfo[] FilesArry = d.GetFiles("*.pdf"); //Getting Text files

            foreach (FileInfo file in FilesArry)
            {
                Files f = new Files();
                f.fileName = file.FullName;
                f.info = file;
                f.reference_id = "";

                retVal.Add(f);
            }

            return retVal;
        }

        public static string openPdfFile(FileInfo file)
        {
            var textExtractor = new TextExtractor();

            var wordDocContents = textExtractor.Extract(file.FullName);

            return wordDocContents.ToString();
        }

        public static string findReferenceId(Files f)
        {
            //ID?:084992080113111853_001
            int start = f.text.IndexOf("ID?:");
            int len = 22;
            if (start > -1)
            {
                start += "ID?:".Length;
                return f.text.Substring(start, len);
            }
            else
                return null;
        }

        //====================================================================================================================================================
        public static int findRefFrom(Files f)
        {
            int retVal;
            
            if ((retVal = findRefFromDelta(f)) != 0)
            {
                return retVal;
            }
            else if ((retVal = findRefFromDPD(f)) != 0)
            {
                return retVal;
            }
            else if ((retVal = findRefFromFedEx(f)) != 0)
            {
                return retVal;
            }

            return 0;
        }

        public static int findRefFromFedEx(Files f)
        {
            if (f.text.IndexOf("FedEx Express Polska Sp. z o.o.") > -1) //jesli to list z FedEx
            {
                int start = f.text.IndexOf("Ilość paczek");
                int len = 7;
                if (start > -1)
                {
                    start += "Ilość paczek".Length + 4;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("z");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refFrom, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }

        public static int findRefFromDelta(Files f)
        {
            if (f.text.IndexOf("Delta Kurier Sp. z o. o.") > -1)    //jesli to list z Delta
            {
                int start = f.text.IndexOf("PACZKA:");
                int len = 6;
                if (start > -1)
                {
                    start += "PACZKA:".Length;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("/");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refFrom, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }

        public static int findRefFromDPD(Files f)
        {
            if (f.text.IndexOf("DPD Polska Sp. z o.o.") > -1)   //jeśli to list z DPD
            {
                int start = f.text.IndexOf("PACZKA:");
                int len = 6;
                if (start > -1)
                {
                    start += "PACZKA:".Length;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("/");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refFrom, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }
        //====================================================================================================================================================

        //====================================================================================================================================================
        public static int findRefTo(Files f)
        {
            int retVal;

            if ((retVal = findRefToDelta(f)) != 0)
            {
                return retVal;
            }
            else if ((retVal = findRefToDPD(f)) != 0)
            {
                return retVal;
            }
            else if ((retVal = findRefToFedEx(f)) != 0)
            {
                return retVal;
            }

            return 0;
        }

        public static int findRefToFedEx(Files f)
        {
            if (f.text.IndexOf("FedEx Express Polska Sp. z o.o.") > -1) //jesli to list z FedEx
            {
                int start = f.text.IndexOf("Ilość paczek");
                int len = 7;
                if (start > -1)
                {
                    start += "Ilość paczek".Length + 4;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("z");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 2, 2);

                        int res;
                        bool val = int.TryParse(refTo, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;

                }
                else return 0;
            }
            else return 0;
        }

        public static int findRefToDelta(Files f)
        {
            if (f.text.IndexOf("Delta Kurier Sp. z o. o.") > -1)    //jesli to list z Delta
            {
                int start = f.text.IndexOf("PACZKA:");
                int len = 6;
                if (start > -1)
                {
                    start += "PACZKA:".Length;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("/");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refTo, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }

        public static int findRefToDPD(Files f)
        {
            if (f.text.IndexOf("DPD Polska Sp. z o.o.") > -1)   //jeśli to list z DPD
            {
                int start = f.text.IndexOf("PACZKA:");
                int len = 6;
                if (start > -1)
                {
                    start += "PACZKA:".Length;
                    string substr = f.text.Substring(start, len);
                    int start2 = substr.IndexOf("/");

                    if (start2 > -1)
                    {
                        string refFrom = substr.Substring(0, start2);
                        string refTo = substr.Substring(start2 + 1, 2);

                        int res;
                        bool val = int.TryParse(refTo, out res);

                        if (val)
                            return res;
                        else
                            return 0;
                    }
                    else return 0;
                }
                else return 0;
            }
            else return 0;
        }
        //====================================================================================================================================================

        //====================================================================================================================================================
        public static string findDeliveryNo(Files f)
        {
            string retVal;

            if ((retVal = findDeliveryNoDelta(f)) != null)
            {
                return retVal;
            }
            else if ((retVal = findDeliveryNoDPD(f)) != null)
            {
                return retVal;
            }
            else if ((retVal = findDeliveryNoFedEx(f)) != null)
            {
                return retVal;
            }

            return null;
        }

        public static string findDeliveryNoFedEx(Files f)
        {
            if (f.text.IndexOf("FedEx Express Polska Sp. z o.o.") > -1) //jesli to list z FedEx
            {
                int start = f.text.IndexOf("Nr listu:");
                int len = 13;
                if (start > -1)
                {
                    start += "Nr listu:".Length + 4;
                    string substr = f.text.Substring(start, len);
                    return substr;
                }
                else
                    return null;
            }
            else return null;
        }

        public static string findDeliveryNoDelta(Files f)
        {
            if (f.text.IndexOf("Delta Kurier Sp. z o. o.") > -1)    //jesli to list z Delta
            {
                int start = f.text.IndexOf("Nr list główny: ");
                int len = 13;
                if (start > -1)
                {
                    start += "Nr list główny: ".Length;
                    string substr = f.text.Substring(start, len);
                    return substr;
                }
                else
                    return null;
            }
            else return null;
        }

        public static string findDeliveryNoDPD(Files f)
        {
            if (f.text.IndexOf("DPD Polska Sp. z o.o.") > -1)   //jeśli to list z DPD
            {
                int start = f.text.IndexOf("Numer paczki");
                int len = 16;   //14
                if (start > -1)
                {
                    start -= 21;    //20
                    string substr = f.text.Substring(start, len);

                    if (!substr.Trim().EndsWith("W"))
                    {
                        substr = f.text.Substring(start, len-2);
                    }

                    if (!substr.Trim().EndsWith("W"))
                    {
                        substr = f.text.Substring(start, len);
                    }

                    return substr.Trim();     
                }
                else
                    return null;
            }
            else return null;
        }
        //====================================================================================================================================================

        /// <summary>
        /// Return dispach address for specyfied reference_id ref_from and ref_to
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        static public DispatchAddress FIndDispachAdress(Files f)
        {
            DispatchAddress address = new DispatchAddress();

            //Zmiana ŁP 2018-11-29
            //Dla PALET UPDATUJ NIE PATRZĄC NA REF_FROM I REF_TO
            string query = "";

            //if (f.reference_id.StartsWith("PALETA"))
            //{
             //  query = $"Select * from dispatch_address_table where reference_id ='{f.reference_id}'";
            //}
            //else
            //{
               query = $"Select * from dispatch_address_table where reference_id ='{f.reference_id}' and ref_from='{f.ref_from}' and ref_to='{f.ref_to}'";
            //}

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                //address.id = int.Parse(dTabel.Rows[0]["id"].ToString());
                address.reference_id = dTabel.Rows[0]["reference_id"].ToString();
                address.file_name = dTabel.Rows[0]["file_name"].ToString();
                address.created_date = DateTime.Parse(dTabel.Rows[0]["created_date"].ToString());
                address.mod_date = DateTime.Parse(dTabel.Rows[0]["mod_date"].ToString());
                address.courier_type = dTabel.Rows[0]["courier_type"].ToString();
                address.dispatch_name = dTabel.Rows[0]["dispatch_name"].ToString();
                address.dispatch_surname = dTabel.Rows[0]["dispatch_surname"].ToString();
                address.dispatch_tel = dTabel.Rows[0]["dispatch_tel"].ToString();
                address.dispatch_date = DateTime.Parse(dTabel.Rows[0]["dispatch_date"].ToString());
                address.street = dTabel.Rows[0]["street"].ToString();
                address.city = dTabel.Rows[0]["city"].ToString();
                address.post_code = dTabel.Rows[0]["post_code"].ToString();
                address.dispatch_company = dTabel.Rows[0]["dispatch_company"].ToString();
                //address.description = dTabel.Rows[0]["description"].ToString();
                address.list_description = dTabel.Rows[0]["list_description"].ToString();
                address.pack_type = dTabel.Rows[0]["pack_type"].ToString();
                address.total_packs = int.Parse(dTabel.Rows[0]["total_packs"].ToString());
                address.ref_from = int.Parse(dTabel.Rows[0]["ref_from"].ToString());
                address.ref_to = int.Parse(dTabel.Rows[0]["ref_to"].ToString());
                address.deliv_list_no = dTabel.Rows[0]["deliv_list_no"].ToString();
                address.comments = dTabel.Rows[0]["comments"].ToString();

                return address;
            }
            else
                return null;

        }

        /// <summary>
        /// Update dispach address with its filename
        /// </summary>
        /// <param name="addres"></param>
        static public void UpdateDispachAdress(DispatchAddress addres)
        {
            /*
            012345 – NR KP
            001 – LINIA KP
            100 – numer linii z rozdzielnika
            1 – unikalny numer seryjny jeżeli będą 2 rozdzielniki żeby zachować unikalność kodów

            012345_001_100_1_YYYY-MM-DD HH:MM:SS.pdf gdzie YYY-MM-DD HH:MM:SS to czas stworzenia pliku
             */
            
            string query = $"update dispatch_address_table set file_name = '{addres.file_name}', deliv_list_no = '{addres.deliv_list_no}' where reference_id = '{addres.reference_id}' and ref_from = {addres.ref_from} and ref_to = {addres.ref_to}";

            SQLConnection.UpdateData(query);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Returns all open dispach_item for selected jdline and jdnr 
        /// </summary>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        public static List<FinishingDispatchAddressAndItem> findAllDispach(int jdline, int jdnr)
        {
            List<FinishingDispatchAddressAndItem> retVal = new List<FinishingDispatchAddressAndItem>();

            string query = $"SELECT * FROM tjdata.dispatch_item_table left join tjdata.dispatch_address_table on dispatch_item_table.reference_id = dispatch_address_table.reference_id and dispatch_item_table.ref_from = dispatch_address_table.ref_from and dispatch_item_table.ref_to = dispatch_address_table.ref_to where jdline = {jdline} and jdnr = {jdnr}";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                foreach (DataRow dRow in dTabel.Rows)
                {
                    /*
                    //public int id { get; set; }
                    //public int dispatch_id { get; set; }
                    public int reference_id { get; set; }       //new
                    public string jdnr { get; set; }
                    public string jdline { get; set; }
                    public int qty_to_pack { get; set; }
                    public int qty_packed { get; set; }
                    public bool partial_item { get; set; }
                    public string barcode_data { get; set; }
                    public int weight_act { get; set; }
                    public int operatorId { get; set; }
                    public DateTime created_date { get; set; }
                    public DateTime mod_date { get; set; }
                    //public string description { get; set; }
                    public string pack_description { get; set; }    //new
                    public int ref_from { get; set; }               //new
                    public int ref_to { get; set; }                 //new
                    public int subpack_from { get; set; }           //new
                    public int subpack_to { get; set; }             //new
                    public bool completation_done { get; set; }
                    public int booked_station_id { get; set; }
                     */
                    FinishingDispatchAddressAndItem retitem = new FinishingDispatchAddressAndItem();
                    //retitem.item.id = int.Parse(dRow["dispatch_item_table_id"].ToString());
                    //retitem.item.dispatch_id = int.Parse(dRow["dispatch_id"].ToString());
                    retitem.item.reference_id = dRow["reference_id"].ToString();                 //new
                    retitem.item.jdnr = dRow["jdnr"].ToString();
                    retitem.item.jdline = dRow["jdline"].ToString();
                    retitem.item.qty_to_pack = int.Parse(dRow["qty_to_pack"].ToString());
                    retitem.item.qty_packed = int.Parse(dRow["qty_packed"].ToString());
                    retitem.item.partial_item = SQLConnection.dbBoolNullToBool(dRow["partial_item"]);
                    retitem.item.barcode_data = dRow["barcode_data"].ToString();
                    int weight_act = 0, operatorId = 0;
                    int.TryParse(dRow["weight_act"].ToString(), out weight_act); //0
                    int.TryParse(dRow["operatorId"].ToString(), out operatorId); //0

                    retitem.item.weight_act = weight_act;
                    retitem.item.operatorId = operatorId;

                    retitem.item.created_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["created_date"]);
                    retitem.item.mod_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["mod_date"]);
                    //retitem.item.description = dRow["description"].ToString();
                    retitem.item.pack_description = dRow["pack_description"].ToString();                //new
                    retitem.item.ref_from = int.Parse(dRow["ref_from"].ToString());                     //new
                    retitem.item.ref_to = int.Parse(dRow["ref_to"].ToString());                         //new
                    retitem.item.subpack_from = int.Parse(dRow["subpack_from"].ToString());             //new
                    retitem.item.subpack_to = int.Parse(dRow["subpack_to"].ToString());                 //new
                    retitem.item.completation_done = SQLConnection.dbBoolNullToBool(dRow["completation_done"]);
                    retitem.item.booked_station_id = int.Parse(dRow["booked_station_id"].ToString());

                    //retitem.address.id = int.Parse(dRow["dispatch_address_table_id"].ToString());
                    retitem.address.reference_id = dRow["reference_id"].ToString();
                    retitem.address.file_name = dRow["file_name"].ToString();
                    retitem.address.created_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["created_date"]);
                    retitem.address.mod_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["mod_date"]);
                    retitem.address.courier_type = dRow["courier_type"].ToString();
                    retitem.address.dispatch_name = dRow["dispatch_name"].ToString();
                    retitem.address.dispatch_surname = dRow["dispatch_surname"].ToString();
                    retitem.address.dispatch_tel = dRow["dispatch_tel"].ToString();
                    retitem.address.dispatch_date = DateTime.Parse(dRow["dispatch_date"].ToString());
                    retitem.address.street = dRow["street"].ToString();
                    retitem.address.city = dRow["city"].ToString();
                    retitem.address.post_code = dRow["post_code"].ToString();
                    retitem.address.dispatch_company = dRow["dispatch_company"].ToString();
                    //retitem.address.description = dRow["description"].ToString();
                    retitem.address.list_description = dRow["list_description"].ToString();
                    retitem.address.pack_type = dRow["pack_type"].ToString();
                    retitem.address.total_packs = int.Parse(dRow["total_packs"].ToString());
                    retitem.address.ref_from = int.Parse(dRow["ref_from"].ToString());
                    retitem.address.ref_to = int.Parse(dRow["ref_to"].ToString());
                    retitem.address.deliv_list_no = dRow["deliv_list_no"].ToString();
                    retitem.address.comments = dRow["comments"].ToString();
                    retVal.Add(retitem);
                }
            }
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        public static FinishingDispatchAddressAndItem findNextDispachAddressAndItem(int jdline, int jdnr, int stationId)
        {
            FinishingDispatchAddressAndItem retitem = new FinishingDispatchAddressAndItem();

            string query = $"SELECT * FROM tjdata.dispatch_item_table left join tjdata.dispatch_address_table on dispatch_item_table.reference_id = dispatch_address_table.reference_id and dispatch_item_table.ref_from = dispatch_address_table.ref_from and dispatch_item_table.ref_to = dispatch_address_table.ref_to where jdline = {jdline} and jdnr = {jdnr} and completation_done=false and (booked_station_id=0 or booked_station_id={stationId})";

            DataTable dTabel = SQLConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                DataRow dRow = dTabel.Rows[0];
                /*
                //public int id { get; set; }
                //public int dispatch_id { get; set; }
                public int reference_id { get; set; }       //new
                public string jdnr { get; set; }
                public string jdline { get; set; }
                public int qty_to_pack { get; set; }
                public int qty_packed { get; set; }
                public bool partial_item { get; set; }
                public string barcode_data { get; set; }
                public int weight_act { get; set; }
                public int operatorId { get; set; }
                public DateTime created_date { get; set; }
                public DateTime mod_date { get; set; }
                //public string description { get; set; }
                public string pack_description { get; set; }    //new
                public int ref_from { get; set; }               //new
                public int ref_to { get; set; }                 //new
                public int subpack_from { get; set; }           //new
                public int subpack_to { get; set; }             //new
                public bool completation_done { get; set; }
                public int booked_station_id { get; set; }
                    */
                //FinishingDispatchAddressAndItem retitem = new FinishingDispatchAddressAndItem();
                //retitem.item.id = int.Parse(dRow["dispatch_item_table_id"].ToString());
                //retitem.item.dispatch_id = int.Parse(dRow["dispatch_id"].ToString());
                retitem.item.reference_id = dRow["reference_id"].ToString();                 //new
                retitem.item.jdnr = dRow["jdnr"].ToString();
                retitem.item.jdline = dRow["jdline"].ToString();
                retitem.item.qty_to_pack = int.Parse(dRow["qty_to_pack"].ToString());
                retitem.item.qty_packed = int.Parse(dRow["qty_packed"].ToString());
                retitem.item.partial_item = SQLConnection.dbBoolNullToBool(dRow["partial_item"]);
                retitem.item.barcode_data = dRow["barcode_data"].ToString();
                int weight_act = 0, operatorId = 0;
                int.TryParse(dRow["weight_act"].ToString(), out weight_act); //0
                int.TryParse(dRow["operatorId"].ToString(), out operatorId); //0

                retitem.item.weight_act = weight_act;
                retitem.item.operatorId = operatorId;

                retitem.item.created_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["created_date"]);
                retitem.item.mod_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["mod_date"]);
                //retitem.item.description = dRow["description"].ToString();
                retitem.item.pack_description = dRow["pack_description"].ToString();                //new
                retitem.item.ref_from = int.Parse(dRow["ref_from"].ToString());                     //new
                retitem.item.ref_to = int.Parse(dRow["ref_to"].ToString());                         //new
                retitem.item.subpack_from = int.Parse(dRow["subpack_from"].ToString());             //new
                retitem.item.subpack_to = int.Parse(dRow["subpack_to"].ToString());                 //new
                retitem.item.completation_done = SQLConnection.dbBoolNullToBool(dRow["completation_done"]);
                retitem.item.booked_station_id = int.Parse(dRow["booked_station_id"].ToString());

                //retitem.address.id = int.Parse(dRow["dispatch_address_table_id"].ToString());
                retitem.address.reference_id = dRow["reference_id"].ToString();
                retitem.address.file_name = dRow["file_name"].ToString();
                retitem.address.created_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["created_date"]);
                retitem.address.mod_date = SQLConnection.dbDateTimeNullToDateTime2(dRow["mod_date"]);
                retitem.address.courier_type = dRow["courier_type"].ToString();
                retitem.address.dispatch_name = dRow["dispatch_name"].ToString();
                retitem.address.dispatch_surname = dRow["dispatch_surname"].ToString();
                retitem.address.dispatch_tel = dRow["dispatch_tel"].ToString();
                retitem.address.dispatch_date = DateTime.Parse(dRow["dispatch_date"].ToString());
                retitem.address.street = dRow["street"].ToString();
                retitem.address.city = dRow["city"].ToString();
                retitem.address.post_code = dRow["post_code"].ToString();
                retitem.address.dispatch_company = dRow["dispatch_company"].ToString();
                //retitem.address.description = dRow["description"].ToString();
                retitem.address.list_description = dRow["list_description"].ToString();
                retitem.address.pack_type = dRow["pack_type"].ToString();
                retitem.address.total_packs = int.Parse(dRow["total_packs"].ToString());
                retitem.address.ref_from = int.Parse(dRow["ref_from"].ToString());
                retitem.address.ref_to = int.Parse(dRow["ref_to"].ToString());
                retitem.address.deliv_list_no = dRow["deliv_list_no"].ToString();
                retitem.address.comments = dRow["comments"].ToString();
            }

            return retitem;
        }

        /// <summary>
        /// Returns next open dispach_item for selected jdline and jdnr
        /// </summary>
        /// <param name="jdline"></param>
        /// <param name="jdnr"></param>
        /// <returns></returns>
        static public DispatchItem FindNextOpenDispach(int jdline, int jdnr, int stationId)
        {
            DispatchItem item = new DispatchItem();

            string query = $"SELECT * FROM tjdata.dispatch_item_table where jdline = '{jdline}' and jdnr = '{jdnr}' and completation_done=false and (booked_station_id=0 or booked_station_id={stationId})";

            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count > 0)
            {
                //item.id = int.Parse(dTable.Rows[0]["dispatch_item_table_id"].ToString());
                //item.dispatch_id = int.Parse(dTable.Rows[0]["dispatch_id"].ToString());
                item.reference_id = dTable.Rows[0]["reference_id"].ToString();                 //new
                item.jdnr = dTable.Rows[0]["jdnr"].ToString();
                item.jdline = dTable.Rows[0]["jdline"].ToString();
                item.qty_to_pack = int.Parse(dTable.Rows[0]["qty_to_pack"].ToString());
                item.qty_packed = int.Parse(dTable.Rows[0]["qty_packed"].ToString());
                item.partial_item = SQLConnection.dbBoolNullToBool(dTable.Rows[0]["partial_item"]);
                item.barcode_data = dTable.Rows[0]["barcode_data"].ToString();
                item.weight_act = int.Parse(dTable.Rows[0]["weight_act"].ToString());
                item.operatorId = int.Parse(dTable.Rows[0]["operatorId"].ToString());
                item.created_date = SQLConnection.dbDateTimeNullToDateTime2(dTable.Rows[0]["created_date"]);
                item.mod_date = SQLConnection.dbDateTimeNullToDateTime2(dTable.Rows[0]["mod_date"]);
                //item.description = dTable.Rows[0]["description"].ToString();
                item.pack_description = dTable.Rows[0]["pack_description"].ToString();                //new
                item.ref_from = int.Parse(dTable.Rows[0]["ref_from"].ToString());                     //new
                item.ref_to = int.Parse(dTable.Rows[0]["ref_to"].ToString());                         //new
                item.subpack_from = int.Parse(dTable.Rows[0]["subpack_from"].ToString());             //new
                item.subpack_to = int.Parse(dTable.Rows[0]["subpack_to"].ToString());                 //new
                item.completation_done = SQLConnection.dbBoolNullToBool(dTable.Rows[0]["completation_done"]);
                item.booked_station_id = int.Parse(dTable.Rows[0]["booked_station_id"].ToString());
            }

            return item;
        }

        /// <summary>
        /// update itemId as booked for this station
        /// </summary>
        /// <param name="itemId"></param>
        static public void BookOpenDispach(DispatchItem item, int stationId)
        {
            string query = $"update dispatch_item_table set booked_station_id = {stationId} where reference_id = '{item.reference_id}' and ref_from = {item.ref_from} and ref_to = {item.ref_to}";
            SQLConnection.UpdateData(query);
        }

        /// <summary>
        /// Return id of booked dispach_item
        /// </summary>
        /// <returns></returns>
        static public int GetBookedItemId()
        {
            int retVal = -1;
            string query = $"select id from tjdata.dispatch_item_table where booked_station_id={global.stationId}";

            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count>0)
            {
                retVal = int.Parse(dTable.Rows[0][0].ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Count all dispach_item booked for this station
        /// </summary>
        /// <returns></returns>
        static public int CountBookedItems()
        {
            int retVal = -1;
            string query = $"sselect count(id) from tjdata.dispatch_item_table where booked_station_id={global.stationId}";

            DataTable dTable = SQLConnection.GetData(query);

            if (dTable.Rows.Count > 0)
            {
                retVal = int.Parse(dTable.Rows[0][0].ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Set booked_id = 0 for all dispach_item booked for this station
        /// </summary>
        /// <returns></returns>
        static public void DisbookedAllItems()
        {
            string query = $"update dispatch_item_table set booked_station_id=0 where booked_station_id = {global.stationId}";
            SQLConnection.UpdateData(query);
        }

        /// <summary>
        /// Sets new vaule of qty_packed for selected id
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="packedQty"></param>
        static public void UpdateDispachItemPackedQty(DispatchItem item)
        {
            string query = $"update dispatch_item_table set qty_packed={item.qty_packed} where reference_id = '{item.reference_id}' and ref_from = {item.ref_from} and ref_to = {item.ref_to}";
            SQLConnection.UpdateData(query);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="packedQty"></param>
        static public void UpdateDispachItemcompletationDone(DispatchItem item)
        {
            string query = $"update dispatch_item_table set completation_done=1, booked_station_id=0 where reference_id = '{item.reference_id}' and ref_from = {item.ref_from} and ref_to = {item.ref_to}";
            SQLConnection.UpdateData(query);
        }
    }
}
