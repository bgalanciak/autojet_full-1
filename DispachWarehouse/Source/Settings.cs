﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Data;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.ComponentModel;
using System.Net;

namespace DispachWarehouse
{
   /// <summary>
   /// Simple setting store. I decided not to use interfaces but define all data right here.
   /// </summary>
	public class Settings
	{
		public string settingsFilePath;
		public SettingsData Data;

		/// <summary>
		/// The actual settings placeholder. Put anything you need here.
		/// </summary>
		public class SettingsData
		{
            public string DbServer;
            public string DbUserID; 
            public string DbPassword; 
            public string DbDatabase;

            public string barcodeScannerSerial1;
            public string barcodeCradleSerial1;
            public string barcodeScannerSerial2;
            public string barcodeCradleSerial2;

            public int dpdBarcodePrefix;
            public int dpdBarcodeLen;

            public string reportPrinterName;

            public SettingsData()
            {
                DbServer = "";
                DbUserID = "";
                DbPassword = "";
                DbDatabase = "";

                barcodeScannerSerial1 = "";
                barcodeCradleSerial1 = "";
                barcodeScannerSerial2 = "";
                barcodeCradleSerial2 = "";
            }
        }

        public Settings()
		{
		}

		public Settings(string fileName)
		{
			this.settingsFilePath = fileName;
			this.Data = new SettingsData();            
            //this.Save();
			this.Load();
		}

		public void Save()
		{
			Type type = this.Data.GetType();
			XmlDocument retval;

			XmlWriterSettings xmlWriterSettings = new XmlWriterSettings
			{
			Indent = true,
			OmitXmlDeclaration = false,
			Encoding = Encoding.UTF8
			};

			// Create a serializer for the acknowledgement. The namespace stuff below causes that no
			// namespace definitions are emitted.
			System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(type);
			System.Xml.Serialization.XmlSerializerNamespaces ns = new System.Xml.Serialization.XmlSerializerNamespaces();
			ns.Add("", "");

			MemoryStream memoryStream = new MemoryStream();
			XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
			x.Serialize(memoryStream, this.Data, ns);

			memoryStream.Position = 0;
			retval = new XmlDocument();
			retval.Load(memoryStream);

			xmlWriter.Close();
			memoryStream.Close();
            try
            {
                retval.Save(this.settingsFilePath);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu pliku konfiguracyjnego.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
		}

		/// <summary>
		/// Load the setting from file.
		/// </summary>
		public void Load()
		{
			XmlRootAttribute itemRoot = new XmlRootAttribute();
			itemRoot.ElementName = "pro_plate_settings";
			itemRoot.IsNullable = true;
			itemRoot.Namespace = null;

			XmlSerializer serializer = new XmlSerializer(typeof(Settings.SettingsData));

			// Use the following logic: if settings cannot be loaded then raise an exception
			// and create an empty; schema - compliant file (but only if we are in debug mode).
			try
			{
				using (StreamReader reader = new StreamReader(this.settingsFilePath))
				{
					this.Data = serializer.Deserialize(reader) as Settings.SettingsData;
				}
			}
			catch (Exception e)
			{
				/*
				if (Program.CmdArgs.Debug)
				{
				// Recreate the file.
				this.Save();
				}
				*/
                MessageBox.Show(
                   string.Format(
                   "Fatal error occurred: '{0}'\n\n" +
                   "Program will be closed.\n" +
                   "Please contact to aplication producer to solve it.",
                   String.Format("\nCan't load settings file in set path: '{0}'", this.settingsFilePath)
                   ),
                "Health monitor message",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
                );
                
                throw new Exception(e.ToString());
			}
		}
	}
}
