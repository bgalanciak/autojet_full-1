﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispachWarehouse
{
    public partial class DispachWarehouseMain
    {
        private void startLoginUser(int id)
        {
            if (modeSimulation)
                return;

            station[id].DebugMsg.Add($"loginUser: init");

            global.userList = global.getAllUsers();   //

            station[id].TcMain.SelectedTab = station[id].TpLogin;
            station[id].ModeAct = MODE_LOGIN;
            station[id].CbLoginUsers.DataSource = global.userActiveList;
            station[id].CbLoginUsers.DisplayMember = "login";
            station[id].CbLoginUsers.ValueMember = "id";
        }

        public void logUserByBarcode(string recStr, int stationId)
        {
            station[stationId].DebugMsg.Add($"logUserByBarcode: payload = {recStr}, id = {stationId.ToString()}");

            if (recStr.Length == 20)
            {
                //lUserLogHash.Text = recStr;
            }

            user logUser = global.findUserByHash(recStr, global.userActiveList);

            if (logUser.id != -1)
            {
                logUserOk(logUser, stationId);
            }
            else
            {
                logUserErr(stationId);
            }
        }

        /// <summary>
        /// do dodania: logowanie po kliknięciu przycisku
        /// </summary>
        private void logByPass(int stationId)
        {
            int loginUserId = int.Parse(station[stationId].CbLoginUsers.SelectedValue.ToString());
            user logUser = global.findUserById(loginUserId, global.userActiveList);

            user nu = new user();

            if (logUser.passwordOk(station[stationId].TbLoginPass.Text))
            {
                logUserOk(logUser, stationId);
            }
            else
            {
                logUserErr(stationId);
            }
        }

        public void logUserOk(user logUser, int stationId)
        {
            station[stationId].ActUser = new user(logUser);
            station[stationId].TcMain.SelectedTab = station[stationId].TpMain;
            //station[stationId].LSatationName.Text = $"Stacja {stationId} - {logUser.f_name} {logUser.l_name}";
            station[stationId].LOperatorName.Text = $"{logUser.f_name} {logUser.l_name}";
            showStationInfo($"Poprawnie zalogowano: {logUser.f_name} {logUser.l_name}", stationId);
            station[stationId].TsslUser.Text = $"Operator {logUser.f_name} {logUser.l_name}";
        }

        public void logUserErr(int stationId)
        {
            showStationInfo($"Błędne hasło lub kod logowania!", stationId);
        }

        public void logOutUser(int stationId)
        {
            station[stationId].ModeAct = MODE_LOGIN;

            station[stationId].BModeAct.Visible = false;
            station[stationId].BModeAct.Text = "";

            station[stationId].ActUser = new user();
            station[stationId].TcMain.SelectedTab = station[stationId].TpLogin;
            station[stationId].LOperatorName.Text = $"";
            showStationInfo($"Wylogowano operatora", stationId);
            station[stationId].TsslUser.Text = $"";
        }
    }
}
