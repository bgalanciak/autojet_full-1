﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DispachWarehouse
{
    public partial class DispachWarehouseMain
    {
        public const int MODE_LOGIN = 1;
        public const int MODE_STORAGE_UNIT = 2;
        public const int MODE_STORAGE_PRINT_LABEL = 3;
        public const int MODE_ALLOCATION = 4;
        public const int MODE_ALLOCATION_CHANGE = 5;
        public const int MODE_ISSUING = 6;
        public const int MODE_REPORT = 7;

        public const int BARCODE_UNKNOWN = 0;       //
        public const int BARCODE_STORAGE_UNIT = 1;  //
        public const int BARCODE_ZPL = 2;           //
        public const int BARCODE_FEDEX = 3;         //
        public const int BARCODE_DPD = 4;           //
        public const int BARCODE_VIVA = 5;           //
        public const int BARCODE_SHELL = 6;           //
        public const int BARCODE_DROZDZ = 7;           //

        public class barcodeAnalize
        {
            private stationClass station;
            private int stationId;

            private DispachWarehouseMain refForm;

            public stationClass Station { get => station; set => station = value; }
            public int StationId { get => stationId; set => stationId = value; }

            private delegate void showStationInfoDelegate(string msg, int id);
            private showStationInfoDelegate showStationInfoDel;
            private showStationInfoDelegate updateTbReportKp;
            private showStationInfoDelegate updateTbReportKpLkp;
            private showStationInfoDelegate updateTbReportZpl;
            private showStationInfoDelegate updateTbReportUnit;

            private delegate void singleIntDelegate(int id);
            private singleIntDelegate TpStorageUnitActiveDel;
            private singleIntDelegate TpStoragePrintLabelActiveDel;
            private singleIntDelegate TpAllocationActiveDel;
            private singleIntDelegate TpAllocationChangeActiveDel;
            private singleIntDelegate TpIssuingActiveDel;
            private singleIntDelegate TpReportDel;
            private singleIntDelegate reportShow;

            private singleIntDelegate StorageUnitPageUpdate;
            private singleIntDelegate AllocationPageUpdate;
            private singleIntDelegate AllocationChangePageUpdate;
            private singleIntDelegate IssuingPageUpdate;
            private singleIntDelegate ReportPageUpdate;

            private singleIntDelegate errorOnOff;

            private delegate void logUserByBarcodeDelegate(string msg, int id);
            private logUserByBarcodeDelegate logUserByBarcodeDel;
            private singleIntDelegate logOutUserDel;

            public barcodeAnalize(DispachWarehouseMain form, stationClass station, int stationId)
            {
                this.refForm = form;
                this.Station = station;
                this.StationId = stationId;
                this.showStationInfoDel = refForm.showStationInfo;
                this.TpStorageUnitActiveDel = refForm.TpStorageUnitActive;
                this.TpStoragePrintLabelActiveDel = refForm.TpStorageLabelPrintActive;
                this.TpAllocationActiveDel = refForm.TpAllocationActive;
                this.TpAllocationChangeActiveDel = refForm.TpAllocationChangeActive;
                this.TpIssuingActiveDel = refForm.TpIssuingActive;          
                this.TpReportDel = refForm.TpReportActive;
                this.StorageUnitPageUpdate = refForm.storageUnitPageUpdate;
                this.AllocationPageUpdate = refForm.allocationPageUpdate;
                this.AllocationChangePageUpdate = refForm.allocationChangePageUpdate;
                this.IssuingPageUpdate = refForm.issuingPageUpdate;
                this.ReportPageUpdate = refForm.reportPageUpdate;
                this.logUserByBarcodeDel = refForm.logUserByBarcode;
                this.logOutUserDel = refForm.logOutUser;

                this.errorOnOff = refForm.errorShowOnOff;
                this.updateTbReportKp = refForm.updateTbReportKp;
                this.updateTbReportKpLkp = refForm.updateTbReportKpLkp;
                this.updateTbReportZpl = refForm.updateTbReportZpl;
                this.updateTbReportUnit = refForm.updateTbReportUnit;

                this.reportShow = refForm.reportShow;
            }

            /// <summary>
            /// wywołuje delegata metode BeginInvoke z delegatem showStationInfoDel
            /// </summary>
            /// <param name="msg"></param>
            /// <param name="stationId"></param>
            private void showStationInfo(string msg, int stationId)
            {
                object[] args = new object[] { msg, stationId };
                refForm.BeginInvoke(showStationInfoDel, args);
            }

            public void Run(string barcode)
            {
                station.BarcodeAnalizeOff.WaitOne();
                station.DebugMsg.Add($"Start: {Thread.CurrentThread.ManagedThreadId}");
                //Thread.Sleep(5000);

                if (((station.ActUser == null) || (station.ActUser.id == -1)) && (barcode.Length != 20))     //długość kodu logowania
                {
                    //użytkownik nie zalogowany i nie jest to kod logowania to przerywam analizę
                    showStationInfo("Operator nie zalogowany!", stationId);
                    //return;
                }
                else if ((barcode.Length == 10) && (barcode.Substring(0,1).Equals("X")))   //kod sterujący, 
                {
                    if (barcode.Equals("XM10000010"))   //kod sterujący: nowe miejsce odkładcze
                    {
                        station.ModeAct = MODE_STORAGE_UNIT;
                        changeModeStorage(stationId);
                    }
                    else if (barcode.Equals("XM10000020"))
                    {
                        station.ModeAct = MODE_ALLOCATION;
                        changeModeAllocation(stationId);
                    }
                    else if (barcode.Equals("XM10000030"))
                    {
                        station.ModeAct = MODE_ALLOCATION_CHANGE;
                        changeModeAllocationChange(stationId);
                    }
                    else if (barcode.Equals("XM10000040"))
                    {
                        station.ModeAct = MODE_ISSUING;
                        changeModeIssuing(stationId);
                    }
                    else if (barcode.Equals("XM10000050"))
                    {
                        station.ModeAct = MODE_REPORT;
                        changeModeReport(stationId);
                    }
                    else if (barcode.Equals("XC10000001"))  //Zamknij komunikat o błędzie
                    {
                        stationErrorOff(stationId);
                    }
                    else if (barcode.Equals("XM10000011"))  //dodruk ostatniej karty
                    {
                        //station.ShowError = false;
                        //PostDelegate(errorOnOff, stationId);
                        printLastStorageLabel(StationId);
                    }
                    else if (barcode.Equals("XM10000060"))  //Zmień tryb: dodruk etykiet
                    {
                        station.ModeAct = MODE_STORAGE_PRINT_LABEL;
                        changeModeStoragePrintLabel(stationId);
                    }
                    else if (barcode.Equals("XM10000070"))  //wyloguj
                    {
                        station.ModeAct = MODE_LOGIN;
                        changeModeLogout(stationId);
                    }
                    else if (barcode.Equals("XM10000080"))  //drukuj raport
                    {
                        station.raportSet = station.Data.datasetReport(station.ReportSelect);
                        PostDelegate(reportShow, stationId);
                    }
                }
                else if (station.ModeAct == MODE_LOGIN)
                {
                    PostDelegate(logUserByBarcodeDel, barcode, stationId);
                }
                else if (station.ModeAct == MODE_STORAGE_UNIT)
                {
                    analizeStorageUnit(barcode, stationId);
                }
                else if (station.ModeAct == MODE_STORAGE_PRINT_LABEL)
                {
                    analizeStoragePrintLabel(barcode, stationId);
                }
                else if (station.ModeAct == MODE_ALLOCATION)
                {
                    analizeAllocation(barcode, stationId);
                }
                else if (station.ModeAct == MODE_ALLOCATION_CHANGE)
                {
                    analizeAllocationChange(barcode, stationId);
                }
                else if (station.ModeAct == MODE_ISSUING)
                {
                    analizeIssuing(barcode, stationId);
                }
                else if (station.ModeAct == MODE_REPORT)
                {
                    analizeReport(barcode, stationId);
                }
                station.BarcodeAnalizeOff.Set();
                station.DebugMsg.Add($"End: {Thread.CurrentThread.ManagedThreadId}");

            }

            private void printLastStorageLabel(int stationId)
            {
                if (station.LastMadeStorage.Unit_simple)
                    station.ZebraPrinter.SendToPrinterUnitSimple(station.LastMadeStorage);
                else
                    station.ZebraPrinter.SendToPrinterUnitNotSimple(station.LastMadeStorage);

                station.DebugMsg.Add($"printLastStorageLabel {Thread.CurrentThread.ManagedThreadId}: Storage label prointed");
            }

            public void PostDelegate(Delegate deleg, params object[] objects)
            {
                refForm.BeginInvoke(deleg, objects);
            }

            public void changeModeStorage(int stattionId)
            {
                PostDelegate(TpStorageUnitActiveDel, stattionId);
            }

            public void changeModeAllocation(int stattionId)
            {
                station.Allocation = new stationClass.allocationStepClass();
                PostDelegate(TpAllocationActiveDel, stattionId);
            }

            public void changeModeAllocationChange(int stattionId)
            {
                station.ReAllocationSource = new stationClass.allocationStepClass();
                station.ReAllocationDestination = new stationClass.allocationStepClass();
                PostDelegate(AllocationChangePageUpdate, stattionId);
                PostDelegate(TpAllocationChangeActiveDel, stattionId);
            }

            public void changeModeStoragePrintLabel(int stattionId)
            {
                PostDelegate(TpStoragePrintLabelActiveDel, stattionId);
            }

            public void changeModeLogout(int stattionId)
            {
                PostDelegate(logOutUserDel, stattionId);
            }

            public void changeModeIssuing(int stattionId)
            {
                station.Issuing = new stationClass.allocationStepClass();
                PostDelegate(IssuingPageUpdate, stattionId);
                PostDelegate(TpIssuingActiveDel, stattionId);
            }

            public void changeModeReport(int stattionId)
            {
                PostDelegate(TpReportDel, stattionId);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="barcode"></param>
            /// <param name="stationId"></param>
            public void analizeStoragePrintLabel(string barcode, int stationId)
            {
                try
                {
                    //decode barcode
                    storageUnit outStorage;
                    global.zplShortDataType outZpl;
                    string outFedex;
                    int barcodeType = decodeBarcode(barcode, out outStorage, out outZpl, out outFedex);

                    //kod z etykiety miejsca odkładczego, należy ponownie wydrukować etykietę
                    if (barcodeType == BARCODE_STORAGE_UNIT)
                    {
                        //jesli sygnalizowany był błąd to go kasuję
                        stationErrorOff(stationId);
                        station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: kod z etykiety miejsca odkładczego, należy ponownie wydrukować etykietę");

                        if (outStorage.Unit_simple)
                        {
                            station.ZebraPrinter.SendToPrinterUnitSimple(outStorage);
                            station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: Unit_simple = true");
                        }
                        else
                        {
                            station.ZebraPrinter.SendToPrinterUnitNotSimple(outStorage);
                            station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: Unit_simple = false");
                        }
                        showStationInfo("Wydrukowano dodatkową etykietę dla miejsca odkładczego", stationId);
                    }
                    else
                    {
                        stationErrorOn(stationId, 2);
                    }
                }
                catch (Exception e)
                {
                    stationErrorOn(stationId, 1);
                    station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: ERROR! exception message: {e.Message}");
                }
            }


            /// <summary>
            /// operacje w trybie MODE_STORAGE_UNIT
            /// </summary>
            /// <param name="barcode"></param>
            /// <param name="stationId"></param>
            public void analizeStorageUnit(string barcode, int stationId)
            {
                try
                {
                    //decode barcode
                    storageUnit outStorage;
                    global.zplShortDataType outZpl;
                    string outFedex;
                    int barcodeType = decodeBarcode(barcode, out outStorage, out outZpl, out outFedex);

                    //tworzenie miejsca okdłaczego
                    if ((barcodeType == BARCODE_ZPL) || (barcodeType == BARCODE_DPD))
                    {
                        station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: tworzenie miejsca okdłaczego");

                        //pobieram dane o wysyłce
                        List<FinishingDispatchAddressAndItem> finishingDispatchAddressAndItems = station.Data.findAllDispachAddressAndItemForZPL(outZpl);
                        station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: finishingDispatchAddressAndItems.Count = {finishingDispatchAddressAndItems.Count.ToString()}");

                        if (finishingDispatchAddressAndItems.Count > 0)
                        {
                            stationErrorOff(stationId);

                            station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: address found");

                            //dopisałem kurierów DROZDZ VIVA I SHELL
                            if (finishingDispatchAddressAndItems[0].courier_type.Equals("DPD") || finishingDispatchAddressAndItems[0].courier_type.Equals("FEDEX") || finishingDispatchAddressAndItems[0].courier_type.Equals("FEDEX-S") || finishingDispatchAddressAndItems[0].courier_type.Equals("DROZDZ") || finishingDispatchAddressAndItems[0].courier_type.Equals("VIVA") || finishingDispatchAddressAndItems[0].courier_type.Equals("SHELL") || finishingDispatchAddressAndItems[0].courier_type.Equals("UPS") || finishingDispatchAddressAndItems[0].courier_type.Equals("MURAWSKI"))
                            {
                                //tworze nowe miejsce odkładcze bez adresu
                                storageUnit storageNew = new storageUnit();
                                storageNew.Courier_type = finishingDispatchAddressAndItems[0].courier_type;
                                storageNew.Dispatch_date_CM = finishingDispatchAddressAndItems[0].address.dispatch_date;
                                storageNew.Unit_simple = true;
                                storageNew.Operator_id = station.ActUser.id;
                                storageNew.ReferenceID = outZpl.referenceID;
                                storageNew.Ref_from = outZpl.ref_from;
                                storageNew.Ref_to = outZpl.ref_to;

                                storageUnit storage = station.Data.insertStorageUnit(storageNew);
                                station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: createStorageUnit for unitSimpe = true done");
                                //drukuje etykietę
                                station.ZebraPrinter.SendToPrinterUnitSimple(storage);
                                station.LastMadeStorage = storage;
                                station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: SendToPrinterUnitSimple done");
                                station.StorageUnitNew = storage;
                                station.StorageUnits = station.Data.getStorageUnitAllActive();
                            }
                            else
                            {
                                //tworze nowe miejsce odkładcze z adresem
                                storageUnit storageNew = new storageUnit();
                                storageNew.Courier_type = finishingDispatchAddressAndItems[0].courier_type;
                                storageNew.Dispatch_date_CM = finishingDispatchAddressAndItems[0].address.dispatch_date; // Maks. data_CM
                                storageNew.Dispatch_company = finishingDispatchAddressAndItems[0].address.dispatch_company;
                                storageNew.Dispatch_name = finishingDispatchAddressAndItems[0].address.dispatch_name;
                                storageNew.Dispatch_surname = finishingDispatchAddressAndItems[0].address.dispatch_surname;
                                storageNew.Street = finishingDispatchAddressAndItems[0].dispachAdressStreet;
                                storageNew.City = finishingDispatchAddressAndItems[0].dispachAdressCity;
                                storageNew.Post_code = finishingDispatchAddressAndItems[0].dispachAdressPost;
                                storageNew.ReferenceID = outZpl.referenceID;
                                storageNew.Ref_from = outZpl.ref_from;
                                storageNew.Ref_to = outZpl.ref_to;

                                storageNew.Unit_simple = false;
                                storageNew.Operator_id = station.ActUser.id;

                                storageUnit storage = station.Data.insertStorageUnit(storageNew);
                                station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: createStorageUnit for unitSimpe = false done");
                                //drukuje etykietę
                                station.ZebraPrinter.SendToPrinterUnitNotSimple(storage);
                                station.LastMadeStorage = storage;
                                station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: SendToPrinterUnitNotSimple done");
                                station.StorageUnitNew = storage;
                                station.StorageUnits = station.Data.getStorageUnitAllActive();
                            }
                        }
                        else
                        {
                            station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: address not found");
                            stationErrorOn(stationId, 4);
                        }
                    }
                    else
                    {
                            stationErrorOn(stationId, 5);           
                    }
                    //wyświetlenie danych
                    PostDelegate(StorageUnitPageUpdate, stationId);
                }
                catch (Exception e)
                {
                    stationErrorOn(stationId, 1);
                    station.DebugMsg.Add($"analizeStorageUnit {Thread.CurrentThread.ManagedThreadId}: ERROR! exception message: {e.Message}");
                }
            }

            /// <summary>
            /// operacje w trybie MODE_ALLOCATION
            /// </summary>
            /// <param name="barcode"></param>
            /// <param name="stationId"></param>
            public void analizeAllocation(string barcode, int stationId)
            {
                try
                {
                    //decode barcode
                    storageUnit outStorage;
                    global.zplShortDataType outZpl;
                    string outFedex;
                    int barcodeType = decodeBarcode(barcode, out outStorage, out outZpl, out outFedex);

                    if (!station.Allocation.StorageScanDone)
                    {
                        station.DebugMsg.Add($"analizeAllocation {Thread.CurrentThread.ManagedThreadId}: station.Allocation.StorageScanDone");
                        //pierwszy etap - skanowanie miejsca odkłądczego
                        if (barcodeType == BARCODE_STORAGE_UNIT)
                        {
                            station.Allocation.Storage = outStorage;

                            //pobieram allokacje dla tego miejsca
                            station.Allocation.DeliveryDispachShows = station.Data.getAllDispatchDeliveryShowForUnitId(outStorage.Id);
                            station.Allocation.PackCounter = station.Data.countPackagesForUnitId(outStorage.Id);
                            station.Allocation.StorageScanDone = true;

                            station.DebugMsg.Add($"analizeAllocation {Thread.CurrentThread.ManagedThreadId}: barcodeType == BARCODE_STORAGE_UNIT");
                            //jesli sygnalizowany był błąd to go kasuję
                            stationErrorOff(StationId);
                        }
                        else
                        {
                            stationErrorOn(stationId, 2);
                        }
                    }
                    else if (station.Allocation.ZplScanDone)
                    {
                        if (barcodeType == BARCODE_FEDEX)
                        {
                            //trzeci etap - skanowanie etykiety FEDEX
                            
                            //jesli sygnalizowany był błąd to go kasuję
                            stationErrorOff(stationId);

                            station.Allocation.ZplScanDone = false;

                            //allokacja
                            dispatchDelivery delivery = createDispatchDelivery(station.Allocation.ZplShort, station.Allocation.Storage.Id, outFedex);
                            dispatchDelivery newDelivery = station.Data.insertDispatchDelivery(delivery);
                            station.Allocation.DeliveryDispachShows = station.Data.getAllDispatchDeliveryShowForUnitId(station.Allocation.Storage.Id);

                            station.DebugMsg.Add($"analizeAllocation {Thread.CurrentThread.ManagedThreadId}: barcodeType == BARCODE_FEDEX");

                            //
                            station.Data.updateDeliveryListNoForFedex(station.Allocation.ZplShort, outFedex);
                        }
                        else
                        {
                            stationErrorOn(stationId, 3);
                        }
                    }
                    else
                    {
                        if ((barcodeType == BARCODE_ZPL) || (barcodeType == BARCODE_DPD))
                        {
                            if (station.Data.checkDeliveryAllocated(outZpl))
                            {
                                stationErrorOn(stationId, 6);
                            }
                            else
                            {
                                bool allocationDone = allocateDispach(station.Allocation, outZpl, false,barcodeType);

                                if (allocationDone)
                                {
                                    if ((station.Allocation.Storage.Unit_simple) && ((station.Allocation.Storage.Courier_type.Equals("FEDEX")) || (station.Allocation.Storage.Courier_type.Equals("FEDEX-S"))))
                                    {
                                        station.Allocation.ZplScanDone = true;
                                        station.DebugMsg.Add($"analizeAllocation {Thread.CurrentThread.ManagedThreadId}: ZplScanDone = true");
                                    }
                                    else
                                    {
                                        station.Allocation.StorageScanDone = false;
                                        station.DebugMsg.Add($"analizeAllocation {Thread.CurrentThread.ManagedThreadId}: StorageScanDone = false");
                                    }
                                }
                            }
                        }
                        else
                        {
                            stationErrorOn(stationId, 5);
                        }
                    }
                    //wyświetlenie danych
                    PostDelegate(AllocationPageUpdate, stationId);
                }
                catch (Exception e)
                {
                    stationErrorOn(stationId, 1);
                    station.DebugMsg.Add($"analizeAllocation {Thread.CurrentThread.ManagedThreadId}: ERROR! exception message: {e.Message}");
                }
            }

            public bool allocateDispach(stationClass.allocationStepClass al, global.zplShortDataType outZpl, bool allocationChange,int barcodeType)
            {
                //pobieram dane o wysyłce
                List<FinishingDispatchAddressAndItem> finishingDispatchAddressAndItems = station.Data.findAllDispachAddressAndItemForZPL(outZpl);
                station.DebugMsg.Add($"allocateDispach {Thread.CurrentThread.ManagedThreadId}: finishingDispatchAddressAndItems.Count = {finishingDispatchAddressAndItems.Count.ToString()}");

                if (finishingDispatchAddressAndItems.Count > 0)
                {
                    //jesli sygnalizowany był błąd to go kasuję
                    stationErrorOff(stationId);

                    station.DebugMsg.Add($"allocateDispach {Thread.CurrentThread.ManagedThreadId}: address found");

                    if (al.Storage.Unit_simple)
                    {
                        station.DebugMsg.Add($"allocateDispach {Thread.CurrentThread.ManagedThreadId}: Unit_simple = true");
                        //sprawdzam czy zgadza to paczka tego samego typu co miejsce odkładcze
                        if ((finishingDispatchAddressAndItems[0].courier_type.Equals("DPD")) && (al.Storage.Courier_type.Equals("DPD")) || (finishingDispatchAddressAndItems[0].courier_type.Equals("DROZDZ")) && (al.Storage.Courier_type.Equals("DROZDZ")) || (finishingDispatchAddressAndItems[0].courier_type.Equals("SHELL")) && (al.Storage.Courier_type.Equals("SHELL")) || (finishingDispatchAddressAndItems[0].courier_type.Equals("VIVA")) && (al.Storage.Courier_type.Equals("VIVA")) || (finishingDispatchAddressAndItems[0].courier_type.Equals("UPS")) && (al.Storage.Courier_type.Equals("UPS")) || (finishingDispatchAddressAndItems[0].courier_type.Equals("MURAWSKI")) && (al.Storage.Courier_type.Equals("MURAWSKI")))
                        {
                            //allokacja
                            if (allocationChange)
                            {
                                station.Data.updateDispatchDeliveryObsolet(outZpl);
                            }
                            dispatchDelivery delivery = createDispatchDelivery(outZpl, al.Storage.Id, null);
                            dispatchDelivery newDelivery = station.Data.insertDispatchDelivery(delivery);
                            al.DeliveryDispachShows = station.Data.getAllDispatchDeliveryShowForUnitId(al.Storage.Id);
                            al.PackCounter = station.Data.countPackagesForUnitId(al.Storage.Id);
                            station.DebugMsg.Add($"allocateDispach {Thread.CurrentThread.ManagedThreadId}: al.Storage.Unit_simple DONE");
                        }
                        else if (((finishingDispatchAddressAndItems[0].courier_type.Equals("FEDEX")) || (finishingDispatchAddressAndItems[0].courier_type.Equals("FEDEX-S"))) && ((al.Storage.Courier_type.Equals("FEDEX")) || (al.Storage.Courier_type.Equals("FEDEX-S"))))
                        {
                            if (allocationChange)
                            {
                                station.Data.updateDispatchDeliveryObsolet(outZpl);
                                dispatchDelivery delivery = createDispatchDelivery(outZpl, al.Storage.Id, null);   //dodać numer listu przewozowego
                                dispatchDelivery newDelivery = station.Data.insertDispatchDelivery(delivery);
                                al.DeliveryDispachShows = station.Data.getAllDispatchDeliveryShowForUnitId(al.Storage.Id);
                                al.PackCounter = station.Data.countPackagesForUnitId(al.Storage.Id);
                            }
                            else
                            {
                                //czekam na list kurierski fedex
                                //al.ZplScanDone = true;
                                al.ZplShort = outZpl;

                                station.DebugMsg.Add($"allocateDispach {Thread.CurrentThread.ManagedThreadId}: waiting for FEDEX scan");
                            }
                        }
                        else
                        {
                            stationErrorOn(stationId, 8);
                            return false;
                        }
                    }
                    else
                    {
                        station.DebugMsg.Add($"allocateDispach {Thread.CurrentThread.ManagedThreadId}: Unit_simple = false");
                        //sprawdzam czy zgadza się adres /ALE POMIJAM KURIERÓW VIVA SHELL I DROZDZ
                        //DOPSIANE ŁPA
                        if ((finishingDispatchAddressAndItems[0].courier_type.Equals("DROZDZ") || finishingDispatchAddressAndItems[0].courier_type.Equals("VIVA") || finishingDispatchAddressAndItems[0].courier_type.Equals("SHELL") || finishingDispatchAddressAndItems[0].courier_type.Equals("UPS") || finishingDispatchAddressAndItems[0].courier_type.Equals("MURAWSKI")) && finishingDispatchAddressAndItems[0].courier_type.Equals(al.Storage.Courier_type))
                        {
                            //allokacja
                            if (allocationChange)
                            {
                                station.Data.updateDispatchDeliveryObsolet(outZpl);
                            }
                            dispatchDelivery delivery = createDispatchDelivery(outZpl, al.Storage.Id, null);
                            dispatchDelivery newDelivery = station.Data.insertDispatchDelivery(delivery);
                            al.DeliveryDispachShows = station.Data.getAllDispatchDeliveryShowForUnitId(al.Storage.Id);
                            al.PackCounter = station.Data.countPackagesForUnitId(al.Storage.Id);
                            station.DebugMsg.Add($"allocateDispach {Thread.CurrentThread.ManagedThreadId}: NOT al.Storage.Unit_simple DONE");

                        }
                        else if ((finishingDispatchAddressAndItems[0].dispachName.Equals(al.Storage.dispachShowName)) && (finishingDispatchAddressAndItems[0].courier_type.Equals(al.Storage.Courier_type)))
                        {
                            //allokacja
                            if (allocationChange)
                            {
                                station.Data.updateDispatchDeliveryObsolet(outZpl);
                            }
                            dispatchDelivery delivery = createDispatchDelivery(outZpl, al.Storage.Id, null);
                            dispatchDelivery newDelivery = station.Data.insertDispatchDelivery(delivery);
                            al.DeliveryDispachShows = station.Data.getAllDispatchDeliveryShowForUnitId(al.Storage.Id);
                            al.PackCounter = station.Data.countPackagesForUnitId(al.Storage.Id);
                            station.DebugMsg.Add($"allocateDispach {Thread.CurrentThread.ManagedThreadId}: NOT al.Storage.Unit_simple DONE");
                        }
                        else if (!finishingDispatchAddressAndItems[0].courier_type.Equals(al.Storage.Courier_type))
                        {
                            stationErrorOn(stationId, 8);
                            return false;
                        }
                        else
                        {
                            stationErrorOn(stationId, 9);
                            return false;
                        }
                    }
                }
                else
                {
                    if (barcodeType.Equals(BARCODE_DPD))
                    {
                        //Dodane ŁPA
                        //Dodałem warunek bo jak ktoś wprowadził popranwy kod DPD ale poza systemem barkodów 
                        //to system pokazywał kod jako niepoprawny a to nie jest prawda. Poprostu paczki nie ma w systemie
                        stationErrorOn(stationId, 13);
                    }
                    else
                    { 
                        stationErrorOn(stationId, 5);
                    }
                    return false;
                }

                return true;
            }

            public void analizeAllocationChange(string barcode, int stationId)
            {
                try
                {
                    //decode barcode
                    storageUnit outStorage;
                    global.zplShortDataType outZpl;
                    string outFedex;
                    int barcodeType = decodeBarcode(barcode, out outStorage, out outZpl, out outFedex);

                    if (!station.ReAllocationSource.ZplScanDone)
                    {
                        if ((barcodeType == BARCODE_ZPL) || (barcodeType == BARCODE_DPD))
                        {
                            //czy paczka jest zaalokowana, nie ważne czy jest released
                            (bool isAllocated, int storageUnitId) = station.Data.checkDispachAllocatedOrReleased(outZpl);
                            if (isAllocated)
                            {
                                station.ReAllocationSource.Storage = station.Data.getStorageUnitById(storageUnitId);
                                station.ReAllocationSource.DeliveryDispachShows = new List<dispatchDeliveryShow> { station.Data.getDispatchDeliveryShowForZpl(outZpl) };
                                station.ReAllocationSource.PackCounter = station.Data.countPackagesForZPL(outZpl);
                                station.ReAllocationSource.ZplShort = outZpl;
                                station.ReAllocationSource.ZplScanDone = true;
                                station.ReAllocationDestination = new stationClass.allocationStepClass();
                            }
                            else
                            {
                                stationErrorOn(stationId, 11);  //Paczka nie jest zaalokowana
                            }
                        }
                        else
                        {
                            stationErrorOn(stationId, 0);  //Nie rozpoznano kodu kreskowego
                        }
                    }
                    else
                    {
                        if (barcodeType == BARCODE_STORAGE_UNIT)
                        {
                            //alokacja paczki na nowy storage unit
                            //bool allocationDone = allocateDispach(station.ReAllocationSource, station.ReAllocationSource.ZplShort, true, outStorage);
                            station.ReAllocationDestination.Storage = outStorage;

                            //Dodane ŁPA
                            //Dodałem barcodeType do funkcji
                            bool allocationDone = allocateDispach(station.ReAllocationDestination, station.ReAllocationSource.ZplShort, true,barcodeType);
                            if (allocationDone)
                            {
                                station.ReAllocationSource.ZplScanDone = false;
                                station.ReAllocationDestination.Storage = outStorage;
                            }
                            else
                            {
                                station.ReAllocationDestination.Storage = null;
                            }
                        }
                        else
                        {
                            stationErrorOn(stationId, 0);  //Nie rozpoznano kodu kreskowego
                        }
                    }
                    PostDelegate(AllocationChangePageUpdate, stationId);
                }
                catch (Exception e)
                {
                    stationErrorOn(stationId, 1);
                    station.DebugMsg.Add($"analizeAllocationChange {Thread.CurrentThread.ManagedThreadId}: ERROR! exception message: {e.Message}");
                }
            }

            /// <summary>
            /// operacje w trybie MODE_ALLOCATION
            /// </summary>
            /// <param name="barcode"></param>
            /// <param name="stationId"></param>
            public void analizeIssuing(string barcode, int stationId)
            {
                try
                {
                    //decode barcode
                    storageUnit outStorage;
                    global.zplShortDataType outZpl;
                    string outFedex;
                    int barcodeType = decodeBarcode(barcode, out outStorage, out outZpl, out outFedex);

                    if (barcodeType == BARCODE_STORAGE_UNIT)
                    {
                        //czy miejsce nie jest zamknięte

                        bool isOpen = station.Data.checkStorageClosed(outStorage);
                        bool isEmpty = station.Data.checkStorageEmpty(outStorage);
                        //sprawdzamy jakie paczki nie są jeszcze wydane

                        if ((isOpen) && (!isEmpty))
                        {
                            station.Issuing.Storage = outStorage;

                            station.Issuing.DeliveryDispachShows = station.Data.getAllDispatchDeliveryShowForUnitId(outStorage.Id, true);
                            station.Issuing.PackCounter = station.Data.countPackagesForUnitId(outStorage.Id);
                            //Zamieniłem kolejność żeby pokazywać paczki do wysłania bez tych, które były już wysłane z tej etykiety
                            station.Data.releaseDispachForUnitId(outStorage.Id);
                        }
                        else
                        {
                            stationErrorOn(stationId, 10);  //Miejsce odbiorcze jest puste lub zamknięte
                        }
                    }
                    else if ((barcodeType == BARCODE_ZPL) || (barcodeType == BARCODE_DPD))
                    {
                        (bool isAllocated, int storageUnitId) = station.Data.checkDispachAllocated(outZpl);
                        if (isAllocated)
                        {
                            station.Issuing.Storage = station.Data.getStorageUnitById(storageUnitId);

                            station.Issuing.DeliveryDispachShows = new List<dispatchDeliveryShow> { station.Data.getDispatchDeliveryShowForZpl(outZpl) };
                            station.Issuing.PackCounter = station.Data.countPackagesForZPL(outZpl);
                            //Zamieniłem kolejność żeby pokazywać paczki do wysłania bez tych, które były już wysłane z tej etykiety
                            station.Data.releaseDispachForZpl(outZpl);
                        }
                        else
                        {
                            stationErrorOn(stationId, 11);  //Paczka nie jest zaalokowana
                        }
                    }
                    else
                    {
                        stationErrorOn(stationId, 0);  //Nie rozpoznano kodu kreskowego
                    }
                    PostDelegate(IssuingPageUpdate, stationId);
                }
                catch (Exception e)
                {
                    stationErrorOn(stationId, 1);
                    station.DebugMsg.Add($"analizeIssuing {Thread.CurrentThread.ManagedThreadId}: ERROR! exception message: {e.Message}");
                }

            }

            public void analizeReport(string barcode, int stationId)
            {
                station.DebugMsg.Add($"analizeReport {Thread.CurrentThread.ManagedThreadId}: Start...");
                try
                {
                    if (barcode.Equals("RS00000001"))
                    {
                        if (station.ReportType != REPORT_ZPL)
                        {
                            (station.DispachReports, station.ReportSelect) = station.Data.dispachReport(station.ReportWhere);

                            PostDelegate(ReportPageUpdate, stationId);
                            station.DebugMsg.Add($"analizeReport {Thread.CurrentThread.ManagedThreadId}: station.ReportType != REPORT_ZPL");
                        }
                        else
                        {
                            string kp = station.Data.getKPFromId(station.ReportWhere);

                            if (!string.IsNullOrEmpty(kp))
                            {
                                //WHERE(di.jdnr =[JDNR Z ZAPYTANIA POWYŻEJ] OR jd.jdnr =[JDNR Z ZAPYTANIA POWYŻEJ])
                                //AND(dd.allocation_obsolete = 0 OR dd.allocation_obsolete IS NULL)
                                //AND(da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL)

                                string where = "";

                                where += $" WHERE(di.jdnr = {kp} OR jd.jdnr = {kp})";
                                where += $" AND(dd.allocation_obsolete = 0 OR dd.allocation_obsolete IS NULL)";
                                where += $" AND(da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL)";

                                (station.DispachReports, station.ReportSelect) = station.Data.dispachReport(where);

                                PostDelegate(ReportPageUpdate, stationId);
                            }
                            else
                            {
                                stationErrorOn(stationId, 3);
                            }
                        }
                    }
                    else
                    {
                        //this.updateTbReportKp = refForm.updateTbReportKp;
                        //this.updateTbReportKpLkp = refForm.updateTbReportKpLkp;
                        //this.updateTbReportZpl = refForm.updateTbReportZpl;
                        if (station.ReportType == REPORT_KP)
                        {
                            PostDelegate(updateTbReportKp, barcode, stationId);
                        }
                        else if (station.ReportType == REPORT_KPLKP)
                        {
                            PostDelegate(updateTbReportKpLkp, barcode, stationId);
                        }
                        else if (station.ReportType == REPORT_ZPL)
                        {
                            PostDelegate(updateTbReportZpl, barcode, stationId);
                        }
                        else if (station.ReportType == REPORT_UNIT)
                        {
                            storageUnit outStorage;
                            global.zplShortDataType outZpl;
                            string outFedex;
                            int barcodeType = decodeBarcode(barcode, out outStorage, out outZpl, out outFedex);
                            if (barcodeType == BARCODE_STORAGE_UNIT)
                                PostDelegate(updateTbReportUnit, outStorage.Id.ToString(), stationId);
                        }
                    }
                }
                catch (Exception e)
                {
                    stationErrorOn(stationId, 1);
                    station.DebugMsg.Add($"analizeReport {Thread.CurrentThread.ManagedThreadId}: ERROR! exception message: {e.Message}");
                }
            }

            public dispatchDelivery createDispatchDelivery(global.zplShortDataType zplShort, int storageId, string dispatch_waybill)
            {
                dispatchDelivery dispatchDelivery = new dispatchDelivery();

                dispatchDelivery.Storage_unit_id = storageId;   // storage.Id;
                dispatchDelivery.Reference_id = zplShort.referenceID;
                dispatchDelivery.Ref_from = zplShort.ref_from;
                dispatchDelivery.Ref_to = zplShort.ref_to;
                dispatchDelivery.Allocation_operator_id = Station.ActUser.id;
                dispatchDelivery.Dispatch_waybill = dispatch_waybill;

                return dispatchDelivery;
            }

            /// <summary>
            /// analiza kodu kreskowego i pobieranie możliwie jak najwięcej danych z niego, 
            /// </summary>
            /// <param name="barcode"></param>
            /// <param name="outStorage"></param>
            /// <param name="outZpl"></param>
            /// <param name="outFedex"></param>
            /// <returns>typ zwracanego kodu</returns>
            public int decodeBarcode(string barcode, out storageUnit outStorage, out global.zplShortDataType outZpl, out string outFedex)
            {
                int retVal = 0;
                outStorage = new storageUnit();
                outZpl = new global.zplShortDataType();
                outFedex = string.Empty;


                if ((barcode.Length == 11) && (barcode.Substring(0, 1).Equals("U")))
                {
                    station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: Barcode lenght == 11 and U");
                    int storageId;

                    bool isInt = int.TryParse(barcode.Substring(1, barcode.Length - 1), out storageId);
                    if (isInt)
                    {
                        station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: barcode is storage");
                        outStorage.Id = storageId;
                        outStorage = station.Data.getStorageUnitById(outStorage.Id);
                        if (outStorage != null)
                        {
                            retVal = BARCODE_STORAGE_UNIT;
                            station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: storage found, barcode = BARCODE_STORAGE_UNIT");
                        }
                        else
                        {
                            retVal = BARCODE_UNKNOWN;
                            station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: no storage found, retVal = BARCODE_UNKNOWN");
                        }
                    }
                    else
                    {
                        retVal = BARCODE_UNKNOWN;
                        station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: is NOT storage, barcode = BARCODE_UNKNOWN");
                    }
                }
                else if (barcode.Length == 28)  //%0047400 0000169245928W 101616 => 0000169245928W
                {
                    station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: barcode.Length == 28");
                    string dpdCode = barcode.Substring(Program.Settings.Data.dpdBarcodePrefix, Program.Settings.Data.dpdBarcodeLen);
                    outZpl = station.Data.unpackDPD(dpdCode);

                    if (outZpl == null)
                    {
                        retVal = BARCODE_UNKNOWN;
                        station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: no zpl found, barcode = BARCODE_UNKNOWN");
                    }
                    else
                    {
                        retVal = BARCODE_DPD;
                        station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: zpl found, barcode = BARCODE_DPD");
                    }
                }
                else if (barcode.Length == 13)
                {
                    outFedex = barcode;
                    retVal = BARCODE_FEDEX;
                    station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: barcode.Length == 13, barcode = BARCODE_FEDEX");
                }
                else
                {
                    station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: is barcode zpl?");
                    //sprawdzam czy nie jest to stary kod kreskowy bez litery na końcu
                    bool isOldBarcode = int.TryParse(barcode.Substring(barcode.Length - 1, 1), out _);

                    if (!isOldBarcode)
                    {
                        outZpl = station.Data.unpackZPLShort(barcode.Substring(0, barcode.Length - 1));
                        outZpl.zplType = barcode.Substring(barcode.Length - 1, 1);
                        station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: is barcode new zpl?");
                    }
                    else
                    {
                        outZpl = station.Data.unpackZPLShort(barcode);
                        outZpl.zplType = "K";
                        station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: is barcode old zpl?");
                    }

                    if ((outZpl.referenceID == null) || (!outZpl.zplType.Equals("K")))
                    {
                        retVal = BARCODE_UNKNOWN;
                        station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: no zpl found, retVal = BARCODE_UNKNOWN");
                    }
                    else
                    {
                        retVal = BARCODE_ZPL;
                        station.DebugMsg.Add($"decodeBarcode {Thread.CurrentThread.ManagedThreadId}: zpl found, retVal = BARCODE_ZPL");
                    }
                }
                return retVal;
            }

            public void stationErrorOff(int stationId)
            {
                if (station.ShowError == true)
                {
                    //jesli sygnalizowany był błąd to go kasuję
                    station.ShowError = false;
                    PostDelegate(errorOnOff, stationId);
                }
            }

            //Dopisane ŁPA
            public void stationErrorOn(int stationId, int errorId = 0)
            {
                if (errorId == 0)
                {
                    showStationInfo("Nie rozpoznano kodu kreskowego", stationId);
                }
                else if (errorId == 1)
                {
                    showStationInfo("Błąd przetwarzania danych", stationId);
                }
                else if (errorId == 2)
                {
                    showStationInfo("Zeskanuj kod z etykiety miejsca odkładczego", stationId);
                }
                else if (errorId == 3)
                {
                    showStationInfo("Zeskanuj kod z etykiety FEDEX", stationId);
                }
                else if (errorId == 4)
                {
                    showStationInfo("Nie znaleziono adresu", stationId);
                }
                else if (errorId == 5)
                {
                    showStationInfo("Kod nie został odnaleziony w bazie danych", stationId);
                }
                else if (errorId == 6)
                {
                    showStationInfo("Paczka jest już zaalokowana", stationId);
                }
                else if (errorId == 7)
                {
                    showStationInfo("Nie znaleziono KP", stationId);
                }
                else if (errorId == 8)
                {
                    showStationInfo("Nie można zaalokować - różni kurierzy", stationId);
                }
                else if (errorId == 9)
                {
                    showStationInfo("Nie można zaalokować - różni odbiorcy", stationId);
                }
                else if (errorId == 10)
                {
                    showStationInfo("Miejsce odbiorcze jest puste lub zamknięte", stationId);
                }
                else if (errorId == 11)
                {
                    showStationInfo("Paczka nie jest zaalokowana lub została wysłana", stationId);
                }
                else if (errorId == 12)
                {
                    showStationInfo("Paczka nie jest zaalokowana lub została wysłana", stationId);
                }
                else if (errorId == 13)
                {
                    showStationInfo("List DPD wprowadzony poza systemem. Nie mogę zarejestrować paczki", stationId);
                }
                else
                    showStationInfo("Nie rozpoznano kodu kreskowego", stationId);

                station.ShowError = true;
                PostDelegate(errorOnOff, stationId);
            }

        }
        
        #region showStationInfo
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="stationId"></param>
        public void showStationInfo(string info, int stationId)
        {
            station[stationId].TLogInfoOff.Stop();
            station[stationId].LInfo.Text = info;
            station[stationId].TLogInfoOff.Start();
            station[stationId].DebugMsg.Add($"showStationInfo: {info}");
        }

        public void TLogInfoOff1_Tick(object sender, EventArgs e)
        {
            station[1].LInfo.Text = string.Empty;
            station[1].TLogInfoOff.Stop();
        }

        public void TLogInfoOff2_Tick(object sender, EventArgs e)
        {
            station[2].LInfo.Text = string.Empty;
            station[2].TLogInfoOff.Stop();
        }

        public void TErrorShow1_Tick(object sender, EventArgs e)
        {
            if (station[1].ShowError)
            {
                zebraScanner.scannerBeep(station[1].ScannerID);
            }
        }

        public void errorShowOnOff(int stationId)
        {
            if (station[stationId].ShowError)
            {
                station[stationId].TErrorShow.Start();
                zebraScanner.scannerLEDRedOn(station[stationId].ScannerID);
                zebraScanner.scannerBeep(station[stationId].ScannerID);
            }
            else
            {
                station[stationId].TErrorShow.Stop();
                zebraScanner.scannerLEDRedOff(station[stationId].ScannerID);
            }
        }

        #endregion
    }

}
