﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispachWarehouse
{
    public class dataAcces
    {
        private int stationId;
        private SQLConnection SqlConnection;

        public int StationId { get => stationId;}

        public dataAcces(int stationId, SQLConnection sql)
        {
            this.stationId = stationId;
            this.SqlConnection = sql;
        }

        public List<FinishingDispatchAddressAndItem> findAllDispachAddressAndItemForZPL(global.zplShortDataType zpl)
        {
            List<FinishingDispatchAddressAndItem> retVal = new List<FinishingDispatchAddressAndItem>();

            string query = $"SELECT *, ";
            query += $"CASE ";
            query += $"WHEN dispatch_item_table.tile=0 then 'PLAKAT' ";
            query += $"ELSE dispatch_item_table.tilename ";
            query += $"END as dispach_tiles ";
            query += $"FROM ";
            query += $"tjdata.dispatch_item_table ";
            query += $"LEFT JOIN ";
            query += $"tjdata.dispatch_address_table ON dispatch_item_table.reference_id = dispatch_address_table.reference_id ";
            query += $"AND dispatch_item_table.ref_from = dispatch_address_table.ref_from ";
            query += $"AND dispatch_item_table.ref_to = dispatch_address_table.ref_to ";
            //query += $"AND dispatch_item_table.subpack_from = dispatch_address_table.subpack_from ";
            //query += $"AND dispatch_item_table.subpack_to = dispatch_address_table.subpack_to ";
            query += $"where ";
            query += $"dispatch_item_table.reference_id = '{zpl.referenceID}' ";
            query += $"and dispatch_item_table.ref_from = '{zpl.ref_from}' ";
            query += $"AND dispatch_item_table.ref_to = '{zpl.ref_to}' ";

            string queryArhive = $"SELECT *, ";
            queryArhive += $"CASE ";
            queryArhive += $"WHEN dispatch_item_table_archive.tile=0 then 'PLAKAT' ";
            queryArhive += $"ELSE dispatch_item_table_archive.tilename ";
            queryArhive += $"END as dispach_tiles ";
            queryArhive += $"FROM ";
            queryArhive += $"tjdata.dispatch_item_table_archive ";
            queryArhive += $"LEFT JOIN ";
            queryArhive += $"tjdata.dispatch_address_table_archive ON dispatch_item_table_archive.reference_id = dispatch_address_table_archive.reference_id ";
            queryArhive += $"AND dispatch_item_table_archive.ref_from = dispatch_address_table_archive.ref_from ";
            queryArhive += $"AND dispatch_item_table_archive.ref_to = dispatch_address_table_archive.ref_to ";
            //queryArhive += $"AND dispatch_item_table.subpack_from = dispatch_address_table.subpack_from ";
            //queryArhive += $"AND dispatch_item_table.subpack_to = dispatch_address_table.subpack_to ";
            queryArhive += $"where ";
            queryArhive += $"dispatch_item_table_archive.reference_id = '{zpl.referenceID}' ";
            queryArhive += $"and dispatch_item_table_archive.ref_from = '{zpl.ref_from}' ";
            queryArhive += $"AND dispatch_item_table_archive.ref_to = '{zpl.ref_to}' ";

            DataTable dTabel = this.SqlConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                foreach (DataRow dRow in dTabel.Rows)
                {
                    FinishingDispatchAddressAndItem retitem = DataRowToFinishingDispatchAddressAndItem(dRow);
                    retVal.Add(retitem);
                }
            }
            else
            {
                DataTable dTabelArhive = this.SqlConnection.GetData(queryArhive);
                if (dTabelArhive.Rows.Count > 0)
                {
                    foreach (DataRow dRow in dTabelArhive.Rows)
                    {
                        FinishingDispatchAddressAndItem retitem = DataRowToFinishingDispatchAddressAndItem(dRow);
                        retVal.Add(retitem);
                    }
                }
            }

            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dRow"></param>
        /// <returns></returns>
        public FinishingDispatchAddressAndItem DataRowToFinishingDispatchAddressAndItem(DataRow dRow)
        {
            FinishingDispatchAddressAndItem retVal = new FinishingDispatchAddressAndItem();

            retVal.item.reference_id = dRow["reference_id"].ToString();                 //new
            retVal.item.jdnr = dRow["jdnr"].ToString();
            retVal.item.jdline = dRow["jdline"].ToString();
            retVal.item.qty_to_pack = int.Parse(dRow["qty_to_pack"].ToString());
            retVal.item.qty_packed = int.Parse(dRow["qty_packed"].ToString());
            retVal.item.partial_item = this.SqlConnection.dbBoolNullToBool(dRow["partial_item"]);
            retVal.item.barcode_data = dRow["barcode_data"].ToString();
            int weight_act = 0, operatorId = 0, tile = 0, tileqty = 0;
            int.TryParse(dRow["weight_act"].ToString(), out weight_act); //0
            int.TryParse(dRow["operatorId"].ToString(), out operatorId); //0
            int.TryParse(dRow["tile"].ToString(), out tile); //0
            int.TryParse(dRow["tileqty"].ToString(), out tileqty); //0

            retVal.item.weight_act = weight_act;
            retVal.item.operatorId = operatorId;
            retVal.item.tile = tile;
            retVal.item.tileqty = tileqty;

            retVal.item.created_date = SqlConnection.dbDateTimeNullToDateTime2(dRow["created_date"]);
            retVal.item.mod_date = SqlConnection.dbDateTimeNullToDateTime2(dRow["mod_date"]);
            retVal.item.pack_description = dRow["pack_description"].ToString();                //new
            retVal.item.ref_from = int.Parse(dRow["ref_from"].ToString());                     //new
            retVal.item.ref_to = int.Parse(dRow["ref_to"].ToString());                         //new
            retVal.item.subpack_from = int.Parse(dRow["subpack_from"].ToString());             //new
            retVal.item.subpack_to = int.Parse(dRow["subpack_to"].ToString());                 //new
            retVal.item.completation_done = SqlConnection.dbBoolNullToBool(dRow["completation_done"]);
            retVal.item.booked_station_id = int.Parse(dRow["booked_station_id"].ToString());
            retVal.item.motives = dRow["motives"].ToString();
            retVal.item.tileName = dRow["tilename"].ToString();

            retVal.item.dispach_tiles = dRow["dispach_tiles"].ToString();
            retVal.item.pack_weight = SqlConnection.dbBoolNullToBool(dRow["pack_weight"]);

            
            retVal.address.reference_id = dRow["reference_id"].ToString();
            retVal.address.file_name = dRow["file_name"].ToString();
            retVal.address.ams_file_name = dRow["ams_file_name"].ToString();
            retVal.address.created_date = SqlConnection.dbDateTimeNullToDateTime2(dRow["created_date"]);
            retVal.address.mod_date = SqlConnection.dbDateTimeNullToDateTime2(dRow["mod_date"]);
            retVal.address.courier_type = dRow["courier_type"].ToString();
            retVal.address.dispatch_name = dRow["dispatch_name"].ToString();
            retVal.address.dispatch_surname = dRow["dispatch_surname"].ToString();
            retVal.address.dispatch_tel = dRow["dispatch_tel"].ToString();
            retVal.address.dispatch_date = DateTime.Parse(dRow["dispatch_date"].ToString());
            retVal.address.street = dRow["street"].ToString();
            retVal.address.city = dRow["city"].ToString();
            retVal.address.post_code = dRow["post_code"].ToString();
            retVal.address.dispatch_company = dRow["dispatch_company"].ToString();
            retVal.address.list_description = dRow["list_description"].ToString();
            retVal.address.pack_type = dRow["pack_type"].ToString();
            retVal.address.total_packs = int.Parse(dRow["total_packs"].ToString());
            retVal.address.ref_from = int.Parse(dRow["ref_from"].ToString());
            retVal.address.ref_to = int.Parse(dRow["ref_to"].ToString());
            retVal.address.deliv_list_no = dRow["deliv_list_no"].ToString();
            retVal.address.comments = dRow["comments"].ToString();

            return retVal;
        }

        public global.zplShortDataType unpackDPD(string dpdDeliveryCode)
        {
            global.zplShortDataType retVal = new global.zplShortDataType();

            string query = $"SELECT * from dispatch_address_table where deliv_list_no = '{dpdDeliveryCode}' ";
            DataTable dTabel = this.SqlConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                retVal = zplShortDataRowToZPL(dTabel.Rows[0]); 
            }
            else
            {
                string queryArhive = $"SELECT * from dispatch_address_table_archive where deliv_list_no = '{dpdDeliveryCode}' ";
                DataTable dTabelArchive = this.SqlConnection.GetData(queryArhive);
                if (dTabelArchive.Rows.Count > 0)
                {
                    retVal = zplShortDataRowToZPL(dTabelArchive.Rows[0]);
                }
            }

            return retVal;
        }

        public global.zplDataType unpackZPL(string readZPL)
        {
            global.zplDataType retVal = new global.zplDataType();

            //try
            //{
            //    retVal.referenceID = readZPL.Substring(0, 22);          //constatn lenght of 22 characters
            //    retVal.ref_from = int.Parse(readZPL.Substring(22, 2));
            //    retVal.ref_to = int.Parse(readZPL.Substring(24, 2));
            //    retVal.subpack_from = int.Parse(readZPL.Substring(26, 2));
            //    retVal.subpack_to = int.Parse(readZPL.Substring(28, 2));
            //    retVal.zplType = readZPL.Substring(30, 1);
            //}
            //catch (Exception e)
            //{
            //    return new global.zplDataType();
            //}

            string query = $"SELECT * from dispatch_item_table where barcode_data = '{readZPL}' ";
            DataTable dTabel = this.SqlConnection.GetData(query);

            if (dTabel.Rows.Count>0)
            {
                retVal = zplDataRowToZPL(dTabel.Rows[0]);
            }
            else
            {
                string queryArhive = $"SELECT * from dispatch_item_table_archive where barcode_data = '{readZPL}' ";
                DataTable dTabelArchive = this.SqlConnection.GetData(queryArhive);
                if (dTabelArchive.Rows.Count > 0)
                {
                    retVal = zplDataRowToZPL(dTabelArchive.Rows[0]);
                }
            }

            return retVal;
        }

        public global.zplShortDataType unpackZPLShort(string readZPL)
        {
            global.zplShortDataType retVal = new global.zplShortDataType();

            //try
            //{
            //    retVal.referenceID = readZPL.Substring(0, 22);          //constatn lenght of 22 characters
            //    retVal.ref_from = int.Parse(readZPL.Substring(22, 2));
            //    retVal.ref_to = int.Parse(readZPL.Substring(24, 2));
            //    retVal.subpack_from = int.Parse(readZPL.Substring(26, 2));
            //    retVal.subpack_to = int.Parse(readZPL.Substring(28, 2));
            //    retVal.zplType = readZPL.Substring(30, 1);
            //}
            //catch (Exception e)
            //{
            //    return new global.zplDataType();
            //}

            string query = $"SELECT * from dispatch_item_table where barcode_data = '{readZPL}' ";
            DataTable dTabel = this.SqlConnection.GetData(query);

            if (dTabel.Rows.Count > 0)
            {
                retVal = zplShortDataRowToZPL(dTabel.Rows[0]);
            }
            else
            {
                string queryArhive = $"SELECT * from dispatch_item_table_archive where barcode_data = '{readZPL}' ";
                DataTable dTabelArchive = this.SqlConnection.GetData(queryArhive);
                if (dTabelArchive.Rows.Count > 0)
                {
                    retVal = zplShortDataRowToZPL(dTabelArchive.Rows[0]);
                }
            }

            return retVal;
        }

        public global.zplDataType zplDataRowToZPL(DataRow dRow)
        {
            global.zplDataType retVal = new global.zplDataType();

            //tilemax, ref_from, priority2, pack_weight, priority, qty_to_pack, pack_description, motives, tile, tileqty, /
            //weight_act, operatorId, tilename, update_batch_no, stationId, completation_done, reference_id, jdnr, mod_date, 
            //partial_item, ref_to, subpack_from, barcode_data, jdline, subpack_to, pack_date, qty_packed, created_date, weight_model, booked_station_id
            retVal.referenceID = dRow["reference_id"].ToString();        //constatn lenght of 22 characters
            retVal.ref_from = int.Parse(dRow["ref_from"].ToString());
            retVal.ref_to = int.Parse(dRow["ref_to"].ToString());
            retVal.subpack_from = int.Parse(dRow["subpack_from"].ToString());
            retVal.subpack_to = int.Parse(dRow["subpack_to"].ToString());
            //retVal.zplType = dRow[""].ToString();
            return retVal;
        }

        public global.zplShortDataType zplShortDataRowToZPL(DataRow dRow)
        {
            global.zplShortDataType retVal = new global.zplShortDataType();

            retVal.referenceID = dRow["reference_id"].ToString();        //constatn lenght of 22 characters
            retVal.ref_from = int.Parse(dRow["ref_from"].ToString());
            retVal.ref_to = int.Parse(dRow["ref_to"].ToString());
            return retVal;
        }

        public storageUnit createStorageUnit(FinishingDispatchAddressAndItem addressAndItem, bool unitSimple, int operatorId)
        {
            storageUnit storage = new storageUnit();

            string insertQuery = "";
            if (unitSimple)
            {
                insertQuery = $"insert into storage_unit(courier_type, create_time_stamp, unit_simple, operator_id, dispatch_date_CM) values ('{addressAndItem.courier_type}', NOW(), 1, {operatorId.ToString()}, '{storage.Dispatch_date_CM.ToString("yyy-MM-dd")}')";
            }
            else
            {
                insertQuery = $"insert into storage_unit(courier_type, dispatch_company, dispatch_name, dispatch_surname, street, city, post_code, create_time_stamp, unit_simple, operator_id, dispatch_date_CM) values ('{addressAndItem.courier_type}', '{addressAndItem.address.dispatch_company}', '{addressAndItem.address.dispatch_name}', '{addressAndItem.address.dispatch_surname}','{addressAndItem.dispachAdressStreet}', '{addressAndItem.dispachAdressCity}', '{addressAndItem.dispachAdressPost}', NOW(), 0, {operatorId.ToString()}, '{storage.Dispatch_date_CM.ToString("yyy-MM-dd")}')";
            }

            this.SqlConnection.InsertData(insertQuery);

            string selectQuery = "SELECT LAST_INSERT_ID() as id";
            DataTable dTabel = this.SqlConnection.GetData(selectQuery);
            if (dTabel.Rows.Count > 0)
            {
                int storageId = int.Parse(dTabel.Rows[0]["id"].ToString());
                storage = getStorageUnitById(storageId);
            }

            return storage;
        }

        public storageUnit insertStorageUnit(storageUnit storage)
        {
            storageUnit storageRetVal = new storageUnit();

            string insertQuery = "";
            if (storage.Unit_simple)
            {
                insertQuery = $"insert into storage_unit(courier_type, create_time_stamp, unit_simple, operator_id, reference_id, ref_from, ref_to, dispatch_date_CM) values ('{storage.Courier_type}', NOW(), 1, {storage.Operator_id.ToString()}, '{storage.ReferenceID}', {storage.Ref_from.ToString()}, {storage.Ref_to.ToString()}, '{storage.Dispatch_date_CM.ToString("yyy-MM-dd")}')";
            }
            else
            {
                insertQuery = $"insert into storage_unit(courier_type, dispatch_company, dispatch_name, dispatch_surname, street, city, post_code, create_time_stamp, unit_simple, operator_id, reference_id, ref_from, ref_to, dispatch_date_CM) values ('{storage.Courier_type}', '{storage.Dispatch_company}', '{storage.Dispatch_name}', '{storage.Dispatch_surname}','{storage.Street}', '{storage.City}', '{storage.Post_code}', NOW(), 0, {storage.Operator_id.ToString()}, '{storage.ReferenceID}', {storage.Ref_from.ToString()}, {storage.Ref_to.ToString()}, '{storage.Dispatch_date_CM.ToString("yyy-MM-dd")}')";
            }

            this.SqlConnection.InsertData(insertQuery);

            string selectQuery = "SELECT LAST_INSERT_ID() as id";
            DataTable dTabel = this.SqlConnection.GetData(selectQuery);
            if (dTabel.Rows.Count > 0)
            {
                int storageId = int.Parse(dTabel.Rows[0]["id"].ToString());
                storage = getStorageUnitById(storageId);
            }

            return storage;
        }

        public storageUnit getStorageUnitById(int storageId)
        {
            storageUnit storage = new storageUnit();
            storage.Id = 0;

            string selectQuery = $"select * from storage_unit where id={storageId.ToString()}";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                storage = DataRowToStorageUnit(dTabel.Rows[0]);
            }
            return storage;
        }

        public List<storageUnit> getStorageUnitAllActive()
        {
            List<storageUnit> storageList = new List<storageUnit>();
            
            string selectQuery = $"select * from storage_unit where unit_close = false order by create_time_stamp desc";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            foreach (DataRow dRow in dTabel.Rows)
            {
                storageUnit storage = new storageUnit();
                storage = DataRowToStorageUnit(dRow);
                storageList.Add(storage);
            }
            return storageList;
        }

        //public storageUnit getStorageUnitById(int storageId)
        //{
        //    storageUnit storage = new storageUnit();
        //    storage.Id = 0;

        //    string selectQuery = $"select * from storage_unit where id={storageId.ToString()}";

        //    DataTable dTabel = this.SqlConnection.GetData(selectQuery);

        //    if (dTabel.Rows.Count > 0)
        //    {
        //        storage = DataRowToStorageUnit(dTabel.Rows[0]);
        //    }
        //    return storage;
        //}

        public storageUnit DataRowToStorageUnit(DataRow dataRow)
        {
            storageUnit retVal = new storageUnit();
            DateTime rez;

            retVal.Id = int.Parse(dataRow["id"].ToString()); 
            retVal.Courier_type = dataRow["courier_type"].ToString();
            retVal.Dispatch_company = dataRow["Dispatch_company"].ToString();
            retVal.Dispatch_name = dataRow["dispatch_name"].ToString();
            retVal.Dispatch_surname = dataRow["dispatch_surname"].ToString();
            retVal.Street = dataRow["street"].ToString();
            retVal.City = dataRow["city"].ToString();
            retVal.Post_code = dataRow["post_code"].ToString();
            retVal.Create_time_stamp = DateTime.Parse(dataRow["create_time_stamp"].ToString());

            if (DateTime.TryParse(dataRow["dispatch_date_CM"].ToString(), out rez))
            {
                if (rez != null) retVal.Dispatch_date_CM = rez;
                else retVal.Dispatch_date_CM = new DateTime(2018, 1, 1, 1, 1, 0);
            }

            retVal.Unit_simple = SqlConnection.dbBoolNullToBool(dataRow["unit_simple"].ToString());
            retVal.Operator_id = int.Parse(dataRow["operator_id"].ToString());
            //retVal.Package_barcode = dataRow["package_barcode"].ToString(); 
            retVal.ReferenceID = dataRow["reference_id"].ToString();
            retVal.Ref_from = int.Parse(dataRow["ref_from"].ToString());
            retVal.Ref_to = int.Parse(dataRow["ref_to"].ToString());
            return retVal;
        }

        public List<dispatchDeliveryShow> getAllDispatchDeliveryShowForUnitId(int unitId, bool onlyReleased = false)
        {
            //Dopisane ŁPA
            //Zmieniłem funkcję żeby patrzyła też w archiwum jak nie znajdzie wyników
            List<dispatchDeliveryShow> dispatchDeliveries = new List<dispatchDeliveryShow>();

            //string selectQuery = $"select * from dispatch_delivery where storage_unit_id={unitId.ToString()} and allocation_obsolete = 0";
            
            string selectQuery = $"SELECT";
            selectQuery += $" CONCAT(di.jdnr,'.',di.jdline) AS 'KP_LKP',";
            selectQuery += $" CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END AS 'Odbiorca',";
            selectQuery += $" di.qty_packed AS 'Spak',";
            selectQuery += $" da.pack_type AS 'Typ',";
            selectQuery += $" IFNULL(DATE_FORMAT(di.pack_date,'%Y-%m-%d'),'-') AS 'D_Pak',";
            selectQuery += $" IFNULL(DATE_FORMAT(dd.allocation_time_stamp,'%Y-%m-%d'),'-') AS 'D_Alok',";
            selectQuery += $" courier_type AS 'Kurier',";
            selectQuery += $" di.tilename AS 'Bryty',";
            selectQuery += $" IFNULL(DATE_FORMAT(da.dispatch_date,'%Y-%m-%d'),'-') AS 'D_Wys'";
            selectQuery += $" FROM dispatch_delivery dd";
            selectQuery += $" LEFT JOIN dispatch_address_table da on dd.reference_id=da.reference_id and dd.ref_from=da.ref_from and dd.ref_to=da.ref_to";
            selectQuery += $" LEFT JOIN dispatch_item_table di on da.reference_id=di.reference_id and da.ref_from=di.ref_from and da.ref_to=di.ref_to";
            selectQuery += $" LEFT JOIN Jobsdata jd on di.jdnr=jd.jdnr and di.jdline=jd.jdline";
            selectQuery += $" WHERE dd.allocation_obsolete=0 ";
            selectQuery += $" and dd.dispatch_released=0 AND da.reference_id IS NOT NULL";
            //To jest bez sensu bo 2 warunki sie wykluczaja, lepiej zbudowac liste przed updatenm
            //if (onlyReleased)
            //    selectQuery += " AND dd.dispatch_released=1";
            selectQuery += $" AND dd.storage_unit_id={unitId.ToString()}";
            selectQuery += $" ORDER BY CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END,CONCAT(di.jdnr,'.',di.jdline); ";

            string selectArchiveQuery = $"SELECT";
            selectArchiveQuery += $" CONCAT(di.jdnr,'.',di.jdline) AS 'KP_LKP',";
            selectArchiveQuery += $" CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END AS 'Odbiorca',";
            selectArchiveQuery += $" di.qty_packed AS 'Spak',";
            selectArchiveQuery += $" da.pack_type AS 'Typ',";
            selectArchiveQuery += $" IFNULL(DATE_FORMAT(di.pack_date,'%Y-%m-%d'),'-') AS 'D_Pak',";
            selectArchiveQuery += $" IFNULL(DATE_FORMAT(dd.allocation_time_stamp,'%Y-%m-%d'),'-') AS 'D_Alok',";
            selectArchiveQuery += $" courier_type AS 'Kurier',";
            selectArchiveQuery += $" di.tilename AS 'Bryty',";
            selectArchiveQuery += $" IFNULL(DATE_FORMAT(da.dispatch_date,'%Y-%m-%d'),'-') AS 'D_Wys'";
            selectArchiveQuery += $" FROM dispatch_delivery dd";
            selectArchiveQuery += $" LEFT JOIN dispatch_address_table_archive da on dd.reference_id=da.reference_id and dd.ref_from=da.ref_from and dd.ref_to=da.ref_to";
            selectArchiveQuery += $" LEFT JOIN dispatch_item_table_archive di on da.reference_id=di.reference_id and da.ref_from=di.ref_from and da.ref_to=di.ref_to";
            selectArchiveQuery += $" LEFT JOIN Jobsdata jd on di.jdnr=jd.jdnr and di.jdline=jd.jdline";
            selectArchiveQuery += $" WHERE dd.allocation_obsolete=0 ";
            selectArchiveQuery += $" and dd.dispatch_released=0 AND da.reference_id IS NOT NULL";
            //To jest bez sensu bo 2 warunki sie wykluczaja, lepiej zbudowac liste przed updatenm
            //if (onlyReleased)
            //    selectArchiveQuery += " AND dd.dispatch_released=1 AND da.reference_id IS NOT NULL";
            selectArchiveQuery += $" AND dd.storage_unit_id={unitId.ToString()}";
            selectArchiveQuery += $" ORDER BY CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END,CONCAT(di.jdnr,'.',di.jdline); ";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);
            DataTable dTabelArchive = this.SqlConnection.GetData(selectArchiveQuery);

            if (dTabel.Rows.Count > 0)
            {
                foreach (DataRow dRow in dTabel.Rows)
                {
                    dispatchDeliveryShow dispatchDeliveryShow = DataRowToDispatchDeliveryShow(dRow);

                    dispatchDeliveries.Add(dispatchDeliveryShow);
                }
            }

            if (dTabelArchive.Rows.Count > 0)
            {
                foreach (DataRow dRow in dTabelArchive.Rows)
                {
                   dispatchDeliveryShow dispatchDeliveryShow = DataRowToDispatchDeliveryShow(dRow);
                   dispatchDeliveries.Add(dispatchDeliveryShow);
                
                }
            }

            return dispatchDeliveries;
        }

        public int countPackagesForUnitId(int unitId, bool onlyReleased = false)
        {
            //Dopisane ŁPA
            //Zliczanie liczby paczek i podpaczek
  
            string selectQuery = $"SELECT DISTINCT";
            selectQuery += $" dd.reference_id,dd.ref_from,dd.ref_to ";
            selectQuery += $" FROM dispatch_delivery dd";
            selectQuery += $" WHERE dd.allocation_obsolete=0 ";
            selectQuery += $" and dd.dispatch_released=0";
            if (onlyReleased)
                selectQuery += " AND dd.dispatch_released=1";
            selectQuery += $" AND dd.storage_unit_id={unitId.ToString()};";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            return dTabel.Rows.Count;
        }

        public int countPackagesForZPL(global.zplShortDataType zplShort)
        {
            //Dopisane ŁPA
            //Zliczanie liczby paczek i podpaczek

            string selectQuery = $"SELECT DISTINCT";
            selectQuery += $" dd.reference_id,dd.ref_from,dd.ref_to ";
            selectQuery += $" FROM dispatch_delivery dd";
            selectQuery += $" WHERE dd.allocation_obsolete=0 ";
            selectQuery += $" and dd.dispatch_released=0";
            selectQuery += $" AND dd.reference_id='{zplShort.referenceID}'";
            selectQuery += $" AND dd.ref_from={zplShort.ref_from}";
            selectQuery += $" AND dd.ref_to={zplShort.ref_to}";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            return dTabel.Rows.Count;
        }

        public dispatchDeliveryShow getDispatchDeliveryShowForZpl(global.zplShortDataType zplShort)
        {
            dispatchDeliveryShow dispatchDeliveries = new dispatchDeliveryShow();

            //string selectQuery = $"select * from dispatch_delivery where storage_unit_id={unitId.ToString()} and allocation_obsolete = 0";

            string selectQuery = $"SELECT";
            selectQuery += $" CONCAT(di.jdnr,'.',di.jdline) AS 'KP_LKP',";
            selectQuery += $" CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END AS 'Odbiorca',";
            selectQuery += $" di.qty_packed AS 'Spak',";
            selectQuery += $" da.pack_type AS 'Typ',";
            selectQuery += $" IFNULL(DATE_FORMAT(di.pack_date,'%Y-%m-%d'),'-') AS 'D_Pak',";
            selectQuery += $" IFNULL(DATE_FORMAT(dd.allocation_time_stamp,'%Y-%m-%d'),'-') AS 'D_Alok',";
            selectQuery += $" courier_type AS 'Kurier',";
            selectQuery += $" di.tilename AS 'Bryty',";
            selectQuery += $" IFNULL(DATE_FORMAT(da.dispatch_date,'%Y-%m-%d'),'-') AS 'D_Wys'";
            selectQuery += $" FROM dispatch_delivery dd";
            selectQuery += $" LEFT JOIN dispatch_address_table da on dd.reference_id=da.reference_id and dd.ref_from=da.ref_from and dd.ref_to=da.ref_to";
            selectQuery += $" LEFT JOIN dispatch_item_table di on da.reference_id=di.reference_id and da.ref_from=di.ref_from and da.ref_to=di.ref_to";
            selectQuery += $" LEFT JOIN Jobsdata jd on di.jdnr=jd.jdnr and di.jdline=jd.jdline";
            selectQuery += $" WHERE dd.allocation_obsolete=0  AND da.reference_id IS NOT NULL";
            selectQuery += $" AND dd.dispatch_released=0";
            selectQuery += $" AND dd.reference_id='{zplShort.referenceID}'";
            selectQuery += $" AND dd.ref_from={zplShort.ref_from}";
            selectQuery += $" AND dd.ref_to={zplShort.ref_to}";
            selectQuery += $" ORDER BY CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END,CONCAT(di.jdnr,'.',di.jdline); ";


            string selectArchiveQuery = $"SELECT";
            selectArchiveQuery += $" CONCAT(di.jdnr,'.',di.jdline) AS 'KP_LKP',";
            selectArchiveQuery += $" CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END AS 'Odbiorca',";
            selectArchiveQuery += $" di.qty_packed AS 'Spak',";
            selectArchiveQuery += $" da.pack_type AS 'Typ',";
            selectArchiveQuery += $" IFNULL(DATE_FORMAT(di.pack_date,'%Y-%m-%d'),'-') AS 'D_Pak',";
            selectArchiveQuery += $" IFNULL(DATE_FORMAT(dd.allocation_time_stamp,'%Y-%m-%d'),'-') AS 'D_Alok',";
            selectArchiveQuery += $" courier_type AS 'Kurier',";
            selectArchiveQuery += $" di.tilename AS 'Bryty',";
            selectArchiveQuery += $" IFNULL(DATE_FORMAT(da.dispatch_date,'%Y-%m-%d'),'-') AS 'D_Wys'";
            selectArchiveQuery += $" FROM dispatch_delivery dd";
            selectArchiveQuery += $" LEFT JOIN dispatch_address_table_archive da on dd.reference_id=da.reference_id and dd.ref_from=da.ref_from and dd.ref_to=da.ref_to";
            selectArchiveQuery += $" LEFT JOIN dispatch_item_table_archive di on da.reference_id=di.reference_id and da.ref_from=di.ref_from and da.ref_to=di.ref_to";
            selectArchiveQuery += $" LEFT JOIN Jobsdata jd on di.jdnr=jd.jdnr and di.jdline=jd.jdline";
            selectArchiveQuery += $" WHERE dd.allocation_obsolete=0  AND da.reference_id IS NOT NULL";
            selectArchiveQuery += $" AND dd.dispatch_released=0";
            selectArchiveQuery += $" AND dd.reference_id='{zplShort.referenceID}'";
            selectArchiveQuery += $" AND dd.ref_from={zplShort.ref_from}";
            selectArchiveQuery += $" AND dd.ref_to={zplShort.ref_to}";
            selectArchiveQuery += $" ORDER BY CASE WHEN da.dispatch_company<>'' THEN da.dispatch_company ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) END,CONCAT(di.jdnr,'.',di.jdline); ";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                dispatchDeliveries = DataRowToDispatchDeliveryShow(dTabel.Rows[0]);
            }
            else
            {
                DataTable dTabelArchive = this.SqlConnection.GetData(selectArchiveQuery);

                if (dTabelArchive.Rows.Count > 0)
                {
                    dispatchDeliveries = DataRowToDispatchDeliveryShow(dTabelArchive.Rows[0]);
                }
            }

            return dispatchDeliveries;
        }

        public int countAllDispatchDeliveryForUnitId(int unitId)
        {
            int retVal = 0;

            string selectQuery = $"SELECT COUNT(*) as count FROM dispatch_delivery WHERE dispatch_released = 0 and allocation_obsolete = 0 AND storage_unit_id =[{unitId}]; ";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                retVal = int.Parse(dTabel.Rows[0]["count"].ToString());
            }

            return retVal;
        }

        public dispatchDelivery getDispatchDeliveryById(int dispatchDeliveryId)
        {
            dispatchDelivery dispatchDelivery = new dispatchDelivery();

            string selectQuery = $"select * from dispatch_delivery where id={dispatchDeliveryId.ToString()} and allocation_obsolete = 0";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                dispatchDelivery = DataRowToDispatchDelivery(dTabel.Rows[0]);
            }

            return dispatchDelivery;
        }

        public dispatchDelivery DataRowToDispatchDelivery(DataRow dataRow)
        {
            dispatchDelivery retVal = new dispatchDelivery();

            retVal.Id = int.Parse(dataRow["id"].ToString());
            retVal.Storage_unit_id = int.Parse(dataRow["storage_unit_id"].ToString());
            retVal.Reference_id = dataRow["reference_id"].ToString();
            retVal.Ref_from = int.Parse(dataRow["ref_from"].ToString());
            retVal.Ref_to = int.Parse(dataRow["ref_to"].ToString());
            retVal.Allocation_time_stamp = DateTime.Parse(dataRow["allocation_time_stamp"].ToString());
            retVal.Allocation_operator_id = int.Parse(dataRow["allocation_operator_id"].ToString());
            retVal.Allocation_obsolete = SqlConnection.dbBoolNullToBool(dataRow["allocation_obsolete"].ToString());
            retVal.Dispatch_waybill = dataRow["dispatch_waybill"].ToString();
            retVal.Dispatch_released = SqlConnection.dbBoolNullToBool(dataRow["dispatch_released"].ToString());
            if (!String.IsNullOrEmpty(dataRow["release_operator_id"].ToString()))
                retVal.Release_time_stamp = DateTime.Parse(dataRow["release_time_stamp"].ToString());
            else
                retVal.Release_time_stamp = null;
            retVal.Release_operator_id = (!String.IsNullOrEmpty(dataRow["release_operator_id"].ToString())) ? int.Parse(dataRow["release_operator_id"].ToString()) : 0;

            return retVal;
        }

        public dispatchDeliveryShow DataRowToDispatchDeliveryShow(DataRow dataRow)
        {
            dispatchDeliveryShow retVal = new dispatchDeliveryShow();

            retVal.KP_LKP = dataRow["KP_LKP"].ToString();
            retVal.Odbiorca = dataRow["Odbiorca"].ToString();
            retVal.Spak = dataRow["Spak"].ToString();
            retVal.Typ = dataRow["Typ"].ToString();
            retVal.D_Pak = dataRow["D_Pak"].ToString();
            retVal.D_Alok = dataRow["D_Alok"].ToString();
            retVal.Kurier = dataRow["Kurier"].ToString();
            retVal.Bryty = dataRow["Bryty"].ToString();
            retVal.D_Wys = dataRow["D_Wys"].ToString();
            
            return retVal;
        }
        public dispatchDelivery insertDispatchDelivery(dispatchDelivery dispatch)
        {
            dispatchDelivery retVal = new dispatchDelivery();

            string insertQuery = $"INSERT INTO `tjdata`.`dispatch_delivery`(`storage_unit_id`, `reference_id`, `ref_from`, `ref_to`, `allocation_operator_id`, `allocation_time_stamp`, `dispatch_waybill`, `allocation_obsolete`, `dispatch_released`) ";
            insertQuery += $"VALUES ({dispatch.Storage_unit_id}, '{dispatch.Reference_id}', {dispatch.Ref_from}, {dispatch.Ref_to}, {dispatch.Allocation_operator_id}, NOW(), '{dispatch.Dispatch_waybill}', 0, 0)";

            this.SqlConnection.InsertData(insertQuery);

            string selectQuery = "SELECT LAST_INSERT_ID() as id";
            DataTable dTabel = this.SqlConnection.GetData(selectQuery);
            if (dTabel.Rows.Count > 0)
            {
                int dispatchDeliveryId = int.Parse(dTabel.Rows[0]["id"].ToString());
                retVal = getDispatchDeliveryById(dispatchDeliveryId);
            }

            if (dispatch.Dispatch_waybill != null)
            {
                string updateQuery = $"UPDATE `tjdata`.`dispatch_address_table` ";
                updateQuery += $" SET deliv_list_no='{dispatch.Dispatch_waybill}' WHERE reference_id='{dispatch.Reference_id}' AND ref_from='{dispatch.Ref_from}' and ref_to='{dispatch.Ref_to}' ";

                this.SqlConnection.UpdateData(updateQuery);
            }

            return retVal;
        }

        public void updateDispatchDeliveryObsolet(global.zplShortDataType zplShort)
        {
            string updateQuery = $"update dispatch_delivery set allocation_obsolete = 1 where reference_id = '{zplShort.referenceID}' and ref_from = {zplShort.ref_from.ToString()} and ref_to = {zplShort.ref_to.ToString()}";
            this.SqlConnection.UpdateData(updateQuery);
        }

        public void updateReason(string query1,string query2)
        {
            this.SqlConnection.UpdateData(query1);
            this.SqlConnection.UpdateData(query2);
        }

        public void updateDeliveryListNoForFedex(global.zplShortDataType zplShort, string fedex)
        {
            string updateQuery = $"update dispatch_address_table set deliv_list_no = '{fedex}' where reference_id = '{zplShort.referenceID}' and ref_from = {zplShort.ref_from.ToString()} and ref_to = {zplShort.ref_to.ToString()}";
            this.SqlConnection.UpdateData(updateQuery);
        }

        public bool checkDeliveryAllocated(global.zplShortDataType zplShort)
        {
            bool outVal = false;

            string selectQuery = $"SELECT * FROM tjdata.dispatch_delivery where reference_id = '{zplShort.referenceID}' and ref_from = {zplShort.ref_from.ToString()} and ref_to = {zplShort.ref_to.ToString()} and allocation_obsolete = 0";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                return true;
            }

            return outVal;
        }


        public (List<dispachReportClass>, string) dispachReport(string where)
        {
            List<dispachReportClass> retVal = new List<dispachReportClass>();
            string selectQuery = "";

            selectQuery += " SELECT ";
            selectQuery += " IFNULL(da.courier_type,'-') As 'Kurier',";
            selectQuery += " CASE WHEN di.jdnr IS NULL THEN CONCAT(jd.jdnr,'.',jd.jdline) ELSE CONCAT(di.jdnr,'.',di.jdline) END AS 'KP_LKP',";
            selectQuery += " CASE ";
            selectQuery += " WHEN di.jdnr IS NULL THEN jd.customer ";
            selectQuery += " WHEN da.dispatch_company<>'' THEN da.dispatch_company ";
            selectQuery += " ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) ";
            selectQuery += " END AS 'Odbiorca',";
            selectQuery += " IFNULL(di.qty_to_pack,jd.qtyordered) AS 'Ile',";
            selectQuery += " IFNULL(di.qty_packed,0)  AS 'Spak',";
            selectQuery += " IFNULL(da.pack_type,'-') AS 'Typ',";
            selectQuery += " IFNULL(DATE_FORMAT(da.dispatch_date,'%Y-%m-%d'),'-') 'D_Wys_CM',";
            selectQuery += " IFNULL(DATE_FORMAT(jd.dispatch_date,'%Y-%m-%d'),'-') 'D_Wys_PM',";
            selectQuery += " IFNULL(DATE_FORMAT(dd.release_time_stamp,'%Y-%m-%d'),'-') 'D_Wys_R',";
            selectQuery += " IFNULL(DATE_FORMAT(di.pack_date,'%Y-%m-%d'),'-') AS 'D_Pak',";
            selectQuery += " IFNULL(DATE_FORMAT(dd.allocation_time_stamp,'%Y-%m-%d'),'-') AS 'D_Alok',";
            selectQuery += " IFNULL(dd.storage_unit_id,'-') AS NRMO,";
            selectQuery += " IFNULL(actual_status,'-') AS 'Kurier Info',";
            selectQuery += " IFNULL(di.tilename,'-') AS 'Bryt',";
            selectQuery += " IFNULL(jd.linedesc,'-') AS 'Opis_linii',";
            selectQuery += " CASE WHEN IFNULL(di.booked_station_id,0)=0 THEN IFNULL(di.stationId,0) ELSE IFNULL(di.booked_station_id,0) END AS 'Stacja',";
            selectQuery += " IFNULL(di.reason_not_done,'') AS 'Powod_niewyk',";
            selectQuery += " di.reference_id as referenceid,di.ref_from as reffrom,di.ref_to as refto";
            selectQuery += " FROM dispatch_address_table da";
            selectQuery += " LEFT JOIN dispatch_item_table di on da.reference_id=di.reference_id and da.ref_from=di.ref_from and da.ref_to=di.ref_to";
            selectQuery += " LEFT JOIN dispatch_statuses ds on da.deliv_list_no=ds.deliv_list_no";
            selectQuery += " LEFT JOIN stroer_companies sc on da.dispatch_company=sc.dispatch_company";
            selectQuery += " LEFT JOIN dispatch_delivery dd on da.reference_id=dd.reference_id and da.ref_from=dd.ref_from and da.ref_to=dd.ref_to";
            selectQuery += " JOIN Jobsdata jd on di.jdnr=jd.jdnr and di.jdline=jd.jdline";
            selectQuery += $" {where}";
            selectQuery += " ORDER BY dd.release_time_stamp,";
            selectQuery += " IFNULL(da.courier_type,'-'),";
            selectQuery += " CASE ";
            selectQuery += " WHEN di.jdnr IS NULL THEN jd.customer ";
            selectQuery += " WHEN da.dispatch_company<>'' THEN da.dispatch_company ";
            selectQuery += " ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) ";
            selectQuery += " END,";
            selectQuery += " CASE WHEN di.jdnr IS NULL THEN CONCAT(jd.jdnr,'.',jd.jdline) ELSE CONCAT(di.jdnr,'.',di.jdline) END";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            foreach(DataRow dRow in dTabel.Rows)
            {
                dispachReportClass dispachReport = DataRowTodispachReportClass(dRow);
                retVal.Add(dispachReport);
            }

            return (retVal, selectQuery);
        }



        public raportDataSet datasetReport(string selectQuery)
        {

            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();

            builder.Server = this.SqlConnection.Server;
            builder.UserID = this.SqlConnection.UserID;
            builder.Password = this.SqlConnection.Password;
            builder.Database = this.SqlConnection.Database;
            builder.CharacterSet = "utf8";

            string connString = builder.ToString(); //"server=127.0.0.1;user id=root;password=root;database=tjdata;charset=utf8";//builder.ToString();

            MySqlConnection con = new MySqlConnection();
            con.ConnectionString = connString;

            MySqlDataAdapter adapter = new MySqlDataAdapter(selectQuery, con);

            raportDataSet dataS = new raportDataSet();
            adapter.Fill(dataS, "reportTable");

            return dataS;
        }

        public dispachReportClass DataRowTodispachReportClass(DataRow dRow)
        {
            dispachReportClass retVal = new dispachReportClass();
            
            retVal.Kurier = dRow["Kurier"].ToString();
            retVal.KP_LKP = dRow["KP_LKP"].ToString();
            retVal.Odbiorca = dRow["Odbiorca"].ToString();
            retVal.Ile = dRow["Ile"].ToString();
            retVal.Spak = dRow["Spak"].ToString();
            retVal.Typ = dRow["Typ"].ToString();
            retVal.D_Wys_CM = dRow["D_Wys_CM"].ToString();
            retVal.D_Wys_PM = dRow["D_Wys_PM"].ToString();
            retVal.D_Wys_R = dRow["D_Wys_R"].ToString();
            retVal.D_Pak = dRow["D_Pak"].ToString();
            retVal.D_Alok = dRow["D_Alok"].ToString();
            retVal.NRMO = dRow["NRMO"].ToString();
            retVal.KurierInfo = dRow["Kurier Info"].ToString();
            retVal.Bryt = dRow["Bryt"].ToString();
            retVal.OpisLinii = dRow["Opis_linii"].ToString();
            retVal.Stacja = dRow["Stacja"].ToString();
            retVal.PowodNiewyk = dRow["Powod_niewyk"].ToString();
            retVal.Referenceid = dRow["referenceid"].ToString();
            retVal.Reffrom = dRow["reffrom"].ToString();
            retVal.Refto = dRow["refto"].ToString();
        

            return retVal;
        }

        public string getKPFromId(string id)
        {
            string retVal = "";
            //SELECT jdnr from Printtemp WHERE id =[KOD ID Z KONTROLKI]
            //UNION ALL
            //SELECT jdnr from Archive WHERE id =[KOD ID Z KONTROLKI]

            string selectQuery = $" SELECT jdnr from Printtemp WHERE id = {id}";
            selectQuery += $" UNION ALL";
            selectQuery += $" SELECT jdnr from Archive WHERE id = {id}";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);
            if ((dTabel != null) && (dTabel.Rows.Count > 0))
            {
                retVal = dTabel.Rows[0][0].ToString();
            }

            return retVal;
        }

        public bool checkStorageClosed(storageUnit storage)
        {
            string selectQuery = $"SELECT * FROM tjdata.storage_unit where reference_id = '{storage.ReferenceID}' and ref_from = {storage.Ref_from.ToString()} and ref_to = {storage.Ref_to.ToString()} and unit_close <> 1";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                return true;
            }
            else 
                return false;
        }

        public bool checkStorageEmpty(storageUnit storage)
        {
            //string selectQuery = $"SELECT * FROM tjdata.dispatch_delivery where reference_id = '{storage.ReferenceID}' and ref_from = {storage.Ref_from.ToString()} and ref_to = {storage.Ref_to.ToString()} and allocation_obsolete <> 1 and dispatch_released <> 1";
            string selectQuery = $"SELECT * FROM tjdata.dispatch_delivery where storage_unit_id = {storage.Id.ToString()} and allocation_obsolete <> 1 and dispatch_released <> 1";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                return false;
            }
            else
                return true;
        }

        public (bool, int) checkDispachAllocated(global.zplShortDataType zplShort)
        {
            string selectQuery = $"SELECT * FROM tjdata.dispatch_delivery where reference_id = '{zplShort.referenceID}' and ref_from = {zplShort.ref_from.ToString()} and ref_to = {zplShort.ref_to.ToString()} and allocation_obsolete <> 1 and dispatch_released <> 1";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                return (true, int.Parse(dTabel.Rows[0]["storage_unit_id"].ToString()));
            }
            else
                return (false, 0);
        }

        public (bool, int) checkDispachAllocatedOrReleased(global.zplShortDataType zplShort)
        {
            string selectQuery = $"SELECT * FROM tjdata.dispatch_delivery where reference_id = '{zplShort.referenceID}' and ref_from = {zplShort.ref_from.ToString()} and ref_to = {zplShort.ref_to.ToString()} and allocation_obsolete <> 1";

            DataTable dTabel = this.SqlConnection.GetData(selectQuery);

            if (dTabel.Rows.Count > 0)
            {
                return (true, int.Parse(dTabel.Rows[0]["storage_unit_id"].ToString()));
            }
            else
                return (false, 0);
        }

        public void releaseDispachForUnitId(int unitId)
        {
            string updateQuery = $"update `tjdata`.`dispatch_delivery` set dispatch_released = 1, release_time_stamp = now(), release_operator_id = 1 where storage_unit_id = {unitId} and dispatch_released = 0 and allocation_obsolete = 0";
            this.SqlConnection.UpdateData(updateQuery);
        }

        public void releaseDispachForZpl(global.zplShortDataType zplShort)
        {
            string updateQuery = $"update `tjdata`.`dispatch_delivery` set dispatch_released = 1, release_time_stamp = now(), release_operator_id = 1 where reference_id = '{zplShort.referenceID}' and ref_from = {zplShort.ref_from.ToString()} and ref_to = {zplShort.ref_to.ToString()} and dispatch_released = 0 and allocation_obsolete = 0";
            this.SqlConnection.UpdateData(updateQuery);
        }
    }
}
