﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace DispachWarehouse
{
   public class FileTree
   {
      public enum ID
      {
         FT_INVOICE_XSL,
         FT_INVOICE_TMP,
         FT_WEBVIEW_XSL,
         FT_DEBUG_DMP,
            FT_DEBUG_DMP_1,
            FT_DEBUG_DMP_2,
            FT_MAIN_DB,
         FT_DB_DIR,
         FT_BACKUP_DIR,
         FT_ORDER_SCHEMA,
         FT_CANCEL_SCHEMA,
         FT_REPORT_TMP,
         FT_STYLESHEET_DIR,
         FT_XML_DUMP,
         FT_DEFAULT_SETTINGS,
         FT_FINAL_SETTINGS,
         FT_XML_TEMP,
      }

      public FileTree()
      {
         string appPath = Path.GetDirectoryName(Application.ExecutablePath);
         string userDataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Autojet", "DispachWarehouse");

         // Debug - related files.
            paths[ID.FT_DEBUG_DMP] = Path.Combine(userDataDir, "debug.log");
            paths[ID.FT_DEBUG_DMP_1] = Path.Combine(userDataDir, $"debug1.log");
            paths[ID.FT_DEBUG_DMP_2] = Path.Combine(userDataDir, $"debug2.log");

            // Temporary.
            //paths[ID.FT_REPORT_TMP] = Path.Combine(appPath, "..", "temp", "report.html");
            //paths[ID.FT_XML_DUMP] = Path.Combine(appPath, "..", "temp", "dump.xml");
            //paths[ID.FT_XML_TEMP] = Path.Combine(appPath, "..", "temp", "temp.xml");


            paths[ID.FT_REPORT_TMP] = Path.Combine(userDataDir, "temp", "report.html");
         paths[ID.FT_XML_DUMP] = Path.Combine(userDataDir, "temp", "dump.xml");
         paths[ID.FT_XML_TEMP] = Path.Combine(userDataDir, "temp", "temp.xml");

         existingPaths[ID.FT_STYLESHEET_DIR] = Path.Combine(appPath, "..", "stylesheets");
         existingPaths[ID.FT_DEFAULT_SETTINGS] = Path.Combine(userDataDir, "settings", "settings.xml");
      }

      public string Get(ID id)
      {
         if (existingPaths.ContainsKey(id))
         {
            return (Path.GetFullPath(existingPaths[id]));
         }
         else if (paths.ContainsKey(id))
         {
            return (Path.GetFullPath(paths[id]));
         }
         else
         {
            try
            {
                Program.dispachWarehouseMain.station[1].Hm.Error(string.Format("Cannot determine path for identifier '{0}'", id));
            }
            catch { }
            return (null);
         }
      }

      public bool Validate()
      {
         bool valid = true;

         // Perform sanity check and report.
         foreach (KeyValuePair<ID, string> entry in this.existingPaths)
         {
            if (File.Exists(entry.Value) || Directory.Exists(entry.Value))
            {
               continue;
            }
            else
            {
                //Program.proPlatePrintMainForm.Hm.Warning("The path '{0}' doesn't exist", entry.Value);
               valid = false;
            }
         }

         return (valid);
      }

      private Dictionary<ID, string> existingPaths = new Dictionary<ID, string>();
      private Dictionary<ID, string> paths = new Dictionary<ID, string>();
   }
}
