﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispachWarehouse
{
    public class storageUnit
    {
        /*
        CREATE TABLE `storage_unit` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `courier_type` varchar(255) CHARACTER SET utf8 NOT NULL,
	
	    `dispatch_company` varchar(255) CHARACTER SET utf8 NULL,
	    `dispatch_name` varchar(255) CHARACTER SET utf8 NULL,
	    `dispatch_surname` varchar(255) CHARACTER SET utf8 NULL,
        `street` varchar(255) CHARACTER SET utf8 NULL,
        `city` varchar(255) CHARACTER SET utf8 NULL,
        `post_code` varchar(255) CHARACTER SET utf8 NULL,
  
	    `create_time_stamp` DATETIME NOT NULL,
	    `unit_close` BIT NULL DEFAULT false COMMENT 'true =  unit is closed',
        `unit_simple` BIT NULL DEFAULT false COMMENT 'true =  unit for fedex or dpd, no adress data aviable ',
        `operator_id` INT NOT NULL,
	    PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
        */

        private int id;
        private string courier_type;
        private string dispatch_name;
        private string street;
        private string city;
        private string post_code;
        private DateTime create_time_stamp;
        private bool unit_close;
        private bool unit_simple;
        private int operator_id;
        private string dispatch_company;
        private string dispatch_surname;
        private string referenceID;      //always 22 characterscharacters
        private int ref_from;
        private int ref_to;
        private DateTime dispatch_date_cm;

        public int Id { get => id; set => id = value; }
        public string Courier_type { get => courier_type; set => courier_type = value; }
        public string Dispatch_name { get => dispatch_name; set => dispatch_name = value; }
        public string Street { get => street; set => street = value; }
        public string City { get => city; set => city = value; }
        public string Post_code { get => post_code; set => post_code = value; }
        public DateTime Create_time_stamp { get => create_time_stamp; set => create_time_stamp = value; }
        public bool Unit_close { get => unit_close; set => unit_close = value; }
        public bool Unit_simple { get => unit_simple; set => unit_simple = value; }
        public int Operator_id { get => operator_id; set => operator_id = value; }
        public string Dispatch_company { get => dispatch_company; set => dispatch_company = value; }
        public string Dispatch_surname { get => dispatch_surname; set => dispatch_surname = value; }
        public DateTime Dispatch_date_CM { get => dispatch_date_cm; set => dispatch_date_cm = value; }

        public string dispachShowName
        {
            get
            {
                if ((this.dispatch_company!=null) && (this.dispatch_company.Length > 0))
                {
                    return this.dispatch_company;
                }
                else
                {
                    return this.dispatch_name + " " + this.dispatch_surname;
                }
            }
        }

        public string ReferenceID { get => referenceID; set => referenceID = value; }
        public int Ref_from { get => ref_from; set => ref_from = value; }
        public int Ref_to { get => ref_to; set => ref_to = value; }
    }
}
