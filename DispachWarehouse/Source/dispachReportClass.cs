﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispachWarehouse
{
    public class dispachReportClass
    {

        public string Kurier { get; set; }
        public string KP_LKP { get; set; }
        public string Odbiorca { get; set; }
        public string Ile { get; set; }
        public string Spak { get; set; }
        public string Typ { get; set; }
        public string D_Wys_CM { get; set; }
        public string D_Wys_PM { get; set; }
        public string D_Wys_R { get; set; }
        public string D_Pak { get; set; }
        public string D_Alok { get; set; }
        public string NRMO { get; set; }
        public string KurierInfo { get; set; }
        public string Bryt { get; set; }
        public string OpisLinii { get; set; }
        public string Stacja { get; set; }
        public string PowodNiewyk { get; set; }
        public string Referenceid { get; set; }
        public string Reffrom { get; set; }
        public string Refto { get; set; }


        /*
SELECT 
IFNULL(da.courier_type,'-') As 'Kurier',
CASE WHEN di.jdnr IS NULL THEN CONCAT(jd.jdnr,'.',jd.jdline) ELSE CONCAT(di.jdnr,'.',di.jdline) END AS 'KP_LKP',
CASE 
	WHEN di.jdnr IS NULL THEN jd.customer 
    WHEN da.dispatch_company<>'' THEN da.dispatch_company 
    ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) 
END AS 'Odbiorca',
IFNULL(di.qty_to_pack,jd.qtyordered) AS 'Ile',
IFNULL(di.qty_packed,0)  AS 'Spak',
IFNULL(da.pack_type,'-') AS 'Typ',
IFNULL(DATE_FORMAT(da.dispatch_date,'%Y-%m-%d'),'-') 'D_Wys_CM',
IFNULL(DATE_FORMAT(jd.dispatch_date,'%Y-%m-%d'),'-') 'D_Wys_PM',
IFNULL(DATE_FORMAT(dd.release_time_stamp,'%Y-%m-%d'),'-') 'D_Wys_R',
IFNULL(DATE_FORMAT(di.pack_date,'%Y-%m-%d'),'-') AS 'D_Pak',
IFNULL(DATE_FORMAT(dd.allocation_time_stamp,'%Y-%m-%d'),'-') AS 'D_Alok',
IFNULL(dd.storage_unit_id,'-') AS NRMO,
IFNULL(actual_status,'-') AS 'Kurier Info',
IFNULL(di.tilename,'-') AS 'Bryt'
FROM dispatch_address_table da
LEFT JOIN dispatch_item_table di on da.reference_id=di.reference_id and da.ref_from=di.ref_from and da.ref_to=di.ref_to
LEFT JOIN dispatch_statuses ds on da.deliv_list_no=ds.deliv_list_no
LEFT JOIN stroer_companies sc on da.dispatch_company=sc.dispatch_company
LEFT JOIN dispatch_delivery dd on da.reference_id=dd.reference_id and da.ref_from=dd.ref_from and da.ref_to=dd.ref_to
JOIN Jobsdata jd on di.jdnr=jd.jdnr and di.jdline=jd.jdline

ORDER BY dd.release_time_stamp,
	IFNULL(da.courier_type,'-'),
	CASE 
		WHEN di.jdnr IS NULL THEN jd.customer 
		WHEN da.dispatch_company<>'' THEN da.dispatch_company 
		ELSE CONCAT(da.dispatch_name,' ',da.dispatch_surname) 
	END,
    CASE WHEN di.jdnr IS NULL THEN CONCAT(jd.jdnr,'.',jd.jdline) ELSE CONCAT(di.jdnr,'.',di.jdline) END
;
         */
    }
}
