﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoreScanner;

using System.Xml;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Collections.Concurrent;
using FastMember;

namespace DispachWarehouse
{
    public partial class DispachWarehouseMain : Form
    {
        private bool modeSimulation = false;    //if == true no user login form displayed, only for debug!

        private HealthMonitor[] Hm = new HealthMonitor[3];
        public SQLConnection[] SQLConnection = new SQLConnection[3];
        public stationClass[] station = new stationClass[3];
    
        public zebraScannerClass zebraScanner;

        public DispachWarehouseMain()
        {
            InitializeComponent();
        }

        private void DispachWarehouseMain_Load(object sender, EventArgs e)
        {
            tableLayoutPanel1.RowStyles[3].Height = 2;
            zebraScanner = new zebraScannerClass(this);

            initForm();

            zebraScannerConnect();

            tConnection.Enabled = true;
        }

        public void initForm()
        {
            this.Text = utilities.setWindowTitle();
            utilities.logAppStart();

            station[1] = new stationClass();
            station[2] = new stationClass();

            //dla testow (MC)
            station[1].CradleId = 1;
            station[2].CradleId = 2;

            station[1].Id = 1;
            station[1].Debug = rtbDebugG1;
            station[1].Hm = this.Hm[1];
            station[1].SqlConnection = SQLConnection[1];
            station[1].TsslDatabaseStatus = tsslDatabaseStatus1;
            station[1].TsslScanerStatus = tsslScanerStatus1;
            station[1].TsslUser = tsslUser1;

            station[2].Id = 2;
            station[2].Debug = rtbDebugG2;
            station[2].Hm = this.Hm[2];
            station[2].SqlConnection = SQLConnection[2];
            station[2].TsslDatabaseStatus = tsslDatabaseStatus2;
            station[2].TsslScanerStatus = tsslScanerStatus2;
            station[2].TsslUser = tsslUser2;

            station[1].Hm = new HealthMonitor(this.station[1].Debug, Program.Ft.Get(FileTree.ID.FT_DEBUG_DMP_1), true);
            station[2].Hm = new HealthMonitor(this.station[2].Debug, Program.Ft.Get(FileTree.ID.FT_DEBUG_DMP_2), true);

            station[1].SqlConnection = new SQLConnection(1, Program.Settings.Data.DbServer, Program.Settings.Data.DbUserID, Program.Settings.Data.DbPassword, Program.Settings.Data.DbDatabase);
            station[2].SqlConnection = new SQLConnection(2, Program.Settings.Data.DbServer, Program.Settings.Data.DbUserID, Program.Settings.Data.DbPassword, Program.Settings.Data.DbDatabase);

            //---------------------------------------------------------------------------------------------------------
            station[1].TcMain = tcMainG1;
            station[1].TpLogin = tpLogin1;
            station[1].TpMain = tpMain1;
            station[1].TpStorageUnit = tpStorageUnit1;
            station[1].CbLoginUsers = cbLoginUsers1;
            station[1].LInfo = lInfo1;
            station[1].TLogInfoOff = new System.Timers.Timer(10000);
            station[1].TLogInfoOff.Elapsed += new System.Timers.ElapsedEventHandler(TLogInfoOff1_Tick);
            station[1].TLogInfoOff.SynchronizingObject = this;
            station[1].TpIssuing = tpIssuing1;
            station[1].TpAllocation = tpAllocation1;
            station[1].TpAllocationChange = tpAllocationChange1;
            station[1].TbLoginPass = tbLoginPass1;
            station[1].TpReport = tpReport1;
            station[1].TpStoragePrintLabel = tpStoragePrintLabel1;
            station[1].TcRaportParam = tcReport1;
            station[1].TpParamDate = tpParamDate1;
            station[1].TpParamKp = tpParamKp1;
            station[1].TpParamKpLkp = tpParamKpLkp1;
            station[1].TpParamZpl = tpParamZpl1;
            station[1].TpParamUnit = tpParamUnit1;
            station[1].TpReportInfo = tpReportInfo1;

            station[1].Data = new dataAcces(1, station[1].SqlConnection);
            station[1].ZebraPrinter = new zebraPrinter(1);
            station[1].LStorageUnitNewCourierType = lStorageUnitNewCourierType1;
            station[1].LStorageUnitNewUnit = lStorageUnitNewUnit1;
            station[1].LStorageUnitNewTimeStamp = lStorageUnitNewTimeStamp1;
            station[1].LStorageUnitNewAdress = lStorageUnitNewAdress1;
            station[1].DgvStorageList = dgvStorageList1;
            station[1].LSatationName = lSatationName1;
            station[1].LAllocationName = lAllocationName1;
            station[1].LAllocationCourierType = lAllocationCourierType1;
            station[1].LAllocationUnit = lAllocationUnit1;
            station[1].LAllocationTimeStamp = lAllocationTimeStamp1;
            station[1].LAllocationCount = lAllocationCount1;
            station[1].LAllocationAdress = lAllocationAdress1;
            station[1].DgvAllocationList = dgvAllocationList1;

            station[1].LReAllocationSourceCourierType = lReAllocationSourceCourierType1;
            station[1].LReAllocationSourceUnit = lReAllocationSourceUnit1;
            station[1].LReAllocationSourceTimeStamp = lReAllocationSourceTimeStamp1;
            station[1].LReAllocationSourceCount = lReAllocationSourceCount1;
            station[1].LReAllocationSourceAdress = lReAllocationSourceAdress1;
            station[1].LReAllocationDestinationCourierType = lReAllocationDestinationCourierType1;
            station[1].LReAllocationDestinationUnit = lReAllocationDestinationUnit1;
            station[1].LReAllocationDestinationTimeStamp = lReAllocationDestinationTimeStamp1;
            station[1].LReAllocationDestinationCount = lReAllocationDestinationCount1;
            station[1].LReAllocationDestinationAdress = lReAllocationDestinationAdress1;
            station[1].DgvReAllocationSourceList = dgvReAllocationSourceList1;

            station[1].LIssuingCourierType = lIssuingCourierType1;
            station[1].LIssuingUnit = lIssuingUnit1;
            station[1].LIssuingTimeStamp = lIssuingTimeStamp1;
            station[1].LIssuingCount = lIssuingCount1;
            station[1].LIssuingAdress = lIssuingAdress1;
            station[1].DgvIssuingList = dgvIssuingList1;

            station[1].BModeAct = bModeAct1;
            station[1].LOperatorName = lOperatorName1;
            station[1].LAllocationInfo = lAllocationInfo1;
            station[1].TErrorShow = new System.Timers.Timer(1000);
            station[1].TErrorShow.Elapsed += new System.Timers.ElapsedEventHandler(TErrorShow1_Tick);
            station[1].TErrorShow.SynchronizingObject = this;
            station[1].DgvReport = dgvReport1;

            station[1].DtpReportParam = dtpReportParam1;
            station[1].TbReportKp = tbReportKp1;
            station[1].TbReportKpLkp = tbReportKpLkp1;
            station[1].TbReportZpl = tbReportZpl1;
            station[1].DtpReportUnit = dtpReportUnit1;
            station[1].TbReportUnit = tbReportUnit1;

            station[1].DebugMsg = new BlockingCollection<string>();
            station[1].UpdateDebugMsq = Task.Run(() => updateDebugMsg1(station[1].DebugMsg));   //
            station[1].BarcodeAnalizeOff = new AutoResetEvent(true);

            station[1].LReportKpError = lReportKpError1;
            station[1].LReportKpLkpError = lReportKpLkpError1;
            station[1].LReportZplError = lReportZplError1;
            station[1].LReportUnitError = lReportUnitError1;

            station[1].bReportPrint1 = bReportPrint11;
            station[1].bReportPrint2 = bReportPrint21;
            station[1].bReportPrint3 = bReportPrint31;
            station[1].bReportPrint4 = bReportPrint41;

            station[1].NotPacked1 = dtpReportNP1.Checked ? 1 : 0;
            station[1].NotSent1 = dtpReportNS1.Checked ? 1 : 0;

            Courier1.Items.Add("WSZYSCY");
            Courier1.Items.Add("CHESTER");
            Courier1.Items.Add("DPD");
            Courier1.Items.Add("DROZDZ");
            Courier1.Items.Add("KURIERKLIENTA");
            Courier1.Items.Add("PDPL");
            Courier1.Items.Add("SNAMAGAZYN");
            Courier1.Items.Add("SHELL");
            Courier1.Items.Add("SPLOGIS");
            Courier1.Items.Add("VIVA");
            Courier1.Items.Add("UPS");
            Courier1.Items.Add("MURAWSKI");
            Courier1.Items.Add("WŁASNY");
            Courier1.SelectedIndex = Courier1.FindStringExact("WSZYSCY");

            station[1].Courier1 = Courier1;
            /*
            station[2].TcMain = tcMainG2;
            station[2].TpLogin = tpLogin2;
            station[2].TpMain = tpMain2;
            station[2].TpStorageUnit = tpStorageUnit2;
            station[2].CbLoginUsers = cbLoginUsers2;
            station[2].LInfo = lInfo2;
            station[2].TLogInfoOff = new System.Timers.Timer(10000);
            station[2].TLogInfoOff.Elapsed += new System.Timers.ElapsedEventHandler(TLogInfoOff2_Tick);
            station[2].TLogInfoOff.SynchronizingObject = this;
            station[2].TpIssuing = tpIssuing2;
            station[2].TpAllocation = tpAllocation2;
            station[2].TbLoginPass = tbLoginPass2;
            station[2].TpReport = tpReport2;
            station[2].TpStoragePrintLabel = tpStoragePrintLabel2;
            station[2].Data = new dataAcces(2, station[2].SqlConnection);
            station[2].ZebraPrinter = new zebraPrinter(2);
            //station[2].lStorageUnitNewCourierType 
            //station[2].lStorageUnitNewUnit 
            //station[2].lStorageUnitNewTimeStamp
            //station[2].lStorageUnitNewAdress 
            //station[2].DgvStorageList = DgvStorageList2
            station[2].LSatationName = lSatationName2;
            //station[2].LAllocationName = lAllocationName2
            //station[2].LAllocationCourierType = lAllocationCourierType2;
            //station[2].LAllocationUnit = lAllocationUnit2;
            //station[2].LAllocationTimeStamp = lAllocationTimeStamp2;
            //station[2].LAllocationCount = lAllocationCount2;
            //station[2].LAllocationAdress = lAllocationAdress2;
            //station[2].DgvAllocationList = dgvAllocationList2;
            //station[2].BModeAct = bModeAct2;
            //station[2].LOperatorName = lOperatorName2;

            station[2].DebugMsg = new BlockingCollection<string>();
            station[2].UpdateDebugMsq = Task.Run(() => updateDebugMsg2(station[2].DebugMsg));
            station[2].BarcodeAnalizeOff = new AutoResetEvent(true);
            */

            station[2].TcMain = tcMainG2;
            station[2].TpLogin = tpLogin2;
            station[2].TpMain = tpMain2;
            station[2].TpStorageUnit = tpStorageUnit2;
            station[2].CbLoginUsers = cbLoginUsers2;
            station[2].LInfo = lInfo2;
            station[2].TLogInfoOff = new System.Timers.Timer(10000);
            station[2].TLogInfoOff.Elapsed += new System.Timers.ElapsedEventHandler(TLogInfoOff2_Tick);
            station[2].TLogInfoOff.SynchronizingObject = this;
            station[2].TpIssuing = tpIssuing2;
            station[2].TpAllocation = tpAllocation2;
            station[2].TpAllocationChange = tpAllocationChange2;
            station[2].TbLoginPass = tbLoginPass2;
            station[2].TpReport = tpReport2;
            station[2].TpStoragePrintLabel = tpStoragePrintLabel2;
            station[2].TcRaportParam = tcReport2;
            station[2].TpParamDate = tpParamDate2;
            station[2].TpParamKp = tpParamKp2;
            station[2].TpParamKpLkp = tpParamKpLkp2;
            station[2].TpParamZpl = tpParamZpl2;
            station[2].TpParamUnit = tpParamUnit2;
            station[2].TpReportInfo = tpReportInfo2;

            station[2].Data = new dataAcces(2, station[2].SqlConnection);
            station[2].ZebraPrinter = new zebraPrinter(2);
            station[2].LStorageUnitNewCourierType = lStorageUnitNewCourierType2;
            station[2].LStorageUnitNewUnit = lStorageUnitNewUnit2;
            station[2].LStorageUnitNewTimeStamp = lStorageUnitNewTimeStamp2;
            station[2].LStorageUnitNewAdress = lStorageUnitNewAdress2;
            station[2].DgvStorageList = dgvStorageList2;
            station[2].LSatationName = lSatationName2;
            station[2].LAllocationName = lAllocationName2;
            station[2].LAllocationCourierType = lAllocationCourierType2;
            station[2].LAllocationUnit = lAllocationUnit2;
            station[2].LAllocationTimeStamp = lAllocationTimeStamp2;
            station[2].LAllocationCount = lAllocationCount2;
            station[2].LAllocationAdress = lAllocationAdress2;
            station[2].DgvAllocationList = dgvAllocationList2;

            station[2].LReAllocationSourceCourierType = lReAllocationSourceCourierType2;
            station[2].LReAllocationSourceUnit = lReAllocationSourceUnit2;
            station[2].LReAllocationSourceTimeStamp = lReAllocationSourceTimeStamp2;
            station[2].LReAllocationSourceCount = lReAllocationSourceCount2;
            station[2].LReAllocationSourceAdress = lReAllocationSourceAdress2;
            station[2].LReAllocationDestinationCourierType = lReAllocationDestinationCourierType2;
            station[2].LReAllocationDestinationUnit = lReAllocationDestinationUnit2;
            station[2].LReAllocationDestinationTimeStamp = lReAllocationDestinationTimeStamp2;
            station[2].LReAllocationDestinationCount = lReAllocationDestinationCount2;
            station[2].LReAllocationDestinationAdress = lReAllocationDestinationAdress2;
            station[2].DgvReAllocationSourceList = dgvReAllocationSourceList2;

            station[2].LIssuingCourierType = lIssuingCourierType2;
            station[2].LIssuingUnit = lIssuingUnit2;
            station[2].LIssuingTimeStamp = lIssuingTimeStamp2;
            station[2].LIssuingCount = lIssuingCount2;
            station[2].LIssuingAdress = lIssuingAdress2;
            station[2].DgvIssuingList = dgvIssuingList2;

            station[2].BModeAct = bModeAct2;
            station[2].LOperatorName = lOperatorName2;
            station[2].LAllocationInfo = lAllocationInfo2;
            station[2].TErrorShow = new System.Timers.Timer(1000);
            station[2].TErrorShow.Elapsed += new System.Timers.ElapsedEventHandler(TErrorShow1_Tick);
            station[2].TErrorShow.SynchronizingObject = this;
            station[2].DgvReport = dgvReport2;

            station[2].DtpReportParam = dtpReportParam2;
            station[2].TbReportKp = tbReportKp2;
            station[2].TbReportKpLkp = tbReportKpLkp2;
            station[2].TbReportZpl = tbReportZpl2;
            station[2].DtpReportUnit = dtpReportUnit2;
            station[2].TbReportUnit = tbReportUnit2;

            station[2].DebugMsg = new BlockingCollection<string>();
            station[2].UpdateDebugMsq = Task.Run(() => updateDebugMsg2(station[2].DebugMsg));   //
            station[2].BarcodeAnalizeOff = new AutoResetEvent(true);

            station[2].LReportKpError = lReportKpError2;
            station[2].LReportKpLkpError = lReportKpLkpError2;
            station[2].LReportZplError = lReportZplError2;
            station[2].LReportUnitError = lReportUnitError2;

            station[2].bReportPrint1 = bReportPrint12;
            station[2].bReportPrint2 = bReportPrint22;
            station[2].bReportPrint3 = bReportPrint32;
            station[2].bReportPrint4 = bReportPrint42;

            station[2].NotPacked2 = dtpReportNP2.Checked ? 1 : 0;
            station[2].NotSent2 = dtpReportNS2.Checked ? 1 : 0;

            Courier2.Items.Add("WSZYSCY");
            Courier2.Items.Add("CHESTER");
            Courier2.Items.Add("DPD");
            Courier2.Items.Add("DROZDZ");
            Courier2.Items.Add("KURIERKLIENTA");
            Courier2.Items.Add("PDPL");
            Courier2.Items.Add("SNAMAGAZYN");
            Courier2.Items.Add("SHELL");
            Courier2.Items.Add("SPLOGIS");
            Courier2.Items.Add("VIVA");
            Courier2.Items.Add("UPS");
            Courier2.Items.Add("MURAWSKI");
            Courier2.Items.Add("WŁASNY");
            Courier2.SelectedIndex = Courier2.FindStringExact("WSZYSCY");

            station[2].Courier2 = Courier2;
            //==============================================================================================================================================================

            station[1].DebugMsg.Add($"Started! DispachWarehouse Version: {utilities.GetProgramVersion()}. ");
            station[2].DebugMsg.Add($"Started! DispachWarehouse Version: {utilities.GetProgramVersion()}. ");

            dtpReportParam1.Format = DateTimePickerFormat.Custom;
            dtpReportUnit1.Format = DateTimePickerFormat.Custom;
            // Display the date as "Mon 27 Feb 2012".  
            dtpReportParam1.CustomFormat = "yyyy - MM - dd";
            dtpReportUnit1.CustomFormat = "yyyy - MM - dd";

            dtpReportParam2.Format = DateTimePickerFormat.Custom;
            dtpReportUnit2.Format = DateTimePickerFormat.Custom;
            // Display the date as "Mon 27 Feb 2012".  
            dtpReportParam2.CustomFormat = "yyyy - MM - dd";
            dtpReportUnit2.CustomFormat = "yyyy - MM - dd";
        }

        /// <summary>
        /// dodaje do Hm logi z blockingCollection dla stacji 1
        /// </summary>
        /// <param name="logs"></param>
        public void updateDebugMsg1(BlockingCollection<String> logs)
        {
            while (true)
            {
                string debugMsg;
                if (logs.TryTake(out debugMsg, 0))
                    station[1].Hm.Info(debugMsg);
            }
        }

        /// <summary>
        /// dodaje do Hm logi z blockingCollection dla stacji 2
        /// </summary>
        /// <param name="logs"></param>
        public void updateDebugMsg2(BlockingCollection<String> logs)
        {
            while (true)
            {
                string debugMsg;
                if (logs.TryTake(out debugMsg, 0))
                    station[2].Hm.Info(debugMsg);
            }
        }

        #region Zebra Scanner methods
        /// <summary>
        /// Połączenie ze skanerami 
        /// </summary>
        public void zebraScannerConnect()
        {
            List<zebraScannerClass.scanner> scannerList = zebraScanner.Connect();
            foreach (zebraScannerClass.scanner scanner in scannerList)
            {
                scanner.connected = true;
            }
            connectScanners(scannerList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scannerList"></param>
        private void connectScanners(List<zebraScannerClass.scanner> scannerList)
        {
            foreach (zebraScannerClass.scanner scanner in scannerList)
            {
                if (scanner.connected)
                {
                    if (scanner.serialnumber.Trim().Equals(Program.Settings.Data.barcodeScannerSerial1))
                    {
                        station[1].ScannerID = int.Parse(scanner.scannerID);
                    }
                    else if (scanner.serialnumber.Trim().Equals(Program.Settings.Data.barcodeScannerSerial2))
                    {
                        station[2].ScannerID = int.Parse(scanner.scannerID);
                    }
                    else if (scanner.serialnumber.Trim().Equals(Program.Settings.Data.barcodeCradleSerial1))
                    {
                        station[1].CradleId = int.Parse(scanner.scannerID);
                    }
                    else if (scanner.serialnumber.Trim().Equals(Program.Settings.Data.barcodeCradleSerial2))
                    {
                        station[2].CradleId = int.Parse(scanner.scannerID);
                    }
                }
                else
                {
                    if (scanner.serialnumber.Trim().Equals(Program.Settings.Data.barcodeScannerSerial1))
                    {
                        station[1].ScannerID = 0;
                    }
                    else if (scanner.serialnumber.Trim().Equals(Program.Settings.Data.barcodeScannerSerial2))
                    {
                        station[2].ScannerID = 0;
                    }
                    else if (scanner.serialnumber.Trim().Equals(Program.Settings.Data.barcodeCradleSerial1))
                    {
                        station[1].CradleId = 0;
                    }
                    else if (scanner.serialnumber.Trim().Equals(Program.Settings.Data.barcodeCradleSerial2))
                    {
                        station[2].CradleId = 0;
                    }
                }
            }


            if ((station[1].CradleId > 0) && (station[1].ScannerID > 0))
            {
                station[1].TsslScanerStatus.Text = "Skaner: połączony";
                station[1].TsslScanerStatus.Image = Properties.Resources.yes;
            }
            else
            {
                station[1].TsslScanerStatus.Text = "Skaner: nie połączony";
                station[1].TsslScanerStatus.Image = Properties.Resources.no;
            }

            if ((station[2].CradleId > 0) && (station[2].ScannerID > 0))
            {
                station[2].TsslScanerStatus.Text = "Skaner: połączony";
                station[2].TsslScanerStatus.Image = Properties.Resources.yes;
            }
            else
            {
                station[2].TsslScanerStatus.Text = "Skaner: nie połączony";
                station[2].TsslScanerStatus.Image = Properties.Resources.no;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scanners"></param>
        public void onZebraScannerPNP(List<zebraScannerClass.scanner> scanners)
        {
            connectScanners(scanners);
        }

        /// <summary>
        /// Analiza kodu kreskowego ze skanera ZEBRA
        /// </summary>
        /// <param name="barcode"></param>
        /// <param name="scannerId"></param>
        public void scannerBarcodeRevieved(string barcode, string scannerId)
        {
            try
            {
                int id = int.Parse(scannerId.Trim());
                int stationId = 0;
                if (station[1].CradleId == id) stationId = 1;
                else if (station[2].CradleId == id) stationId = 2;

                if (stationId > 0)
                {
                    station[stationId].DebugMsg.Add($"barcodeTxt = {barcode}");

                    barcodeAnalize barcodeAction = new barcodeAnalize(this, station[stationId], stationId);
                    Thread barcodeAction_thread = new Thread(() => barcodeAction.Run(barcode));
                    barcodeAction_thread.IsBackground = true;
                    barcodeAction_thread.Start();
                }
                else
                {
                    station[1].DebugMsg.Add($"Otrzymano kod kreskowy '{barcode}' ze skanera który nie jest połączony z żadną stacją");
                }
            }
            catch (Exception e)
            {
                station[1].DebugMsg.Add($"scannerBarcodeRevieved: Otrzymano kod kreskowy '{barcode}' ale nie udło się go zanalizować: {e.Message}");
            }
        }

        /// <summary>
        /// public message from zebra scanner class
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="id"></param>
        public void scannerPublicMsg(string msg, int stationId)
        {
            station[stationId].DebugMsg.Add($"scannerMsg: {msg}");
        }
        #endregion


        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AutojetAboutBox aboutBox = new AutojetAboutBox();
            aboutBox.Show();
        }

        private void oProgramieToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        #region database connection
        private void OnDatabaseConnectionStateChange1(object sender, StateChangeEventArgs args)
        {
            //MySqlConnection con = (MySqlConnection)sender;
            switch (args.CurrentState)
            {
                case ConnectionState.Open:
                    {
                        station[1].TsslDatabaseStatus.Text = "Baza danych: połączono";
                        startLoginUser(1);
                        station[1].TsslDatabaseStatus.Image = Properties.Resources.yes;
                        break;
                    }
                default:
                    {
                        station[1].TsslDatabaseStatus.Text = "Baza danych: nie połączono";
                        station[1].TsslDatabaseStatus.Image = Properties.Resources.no;
                        break;
                    }
            }
        }

        private void OnDatabaseConnectionStateChange2(object sender, StateChangeEventArgs args)
        {
            switch (args.CurrentState)
            {
                case ConnectionState.Open:
                    {
                        station[2].TsslDatabaseStatus.Text = "Baza danych: połączono";
                        startLoginUser(2);
                        station[2].TsslDatabaseStatus.Image = Properties.Resources.yes;
                        break;
                    }
                default:
                    {
                        station[2].TsslDatabaseStatus.Text = "Baza danych: nie połączono";
                        station[2].TsslDatabaseStatus.Image = Properties.Resources.no;
                        break;
                    }
            }
        }

        private void tConnection_Tick(object sender, EventArgs e)
        {
            if (!station[1].SqlConnection.IsActive())
                station[1].SqlConnection.Connect(OnDatabaseConnectionStateChange1);

            if (!station[2].SqlConnection.IsActive())
                station[2].SqlConnection.Connect(OnDatabaseConnectionStateChange2);
        }
        #endregion


        private void bBeepScanner1_Click(object sender, EventArgs e)
        {
            zebraScanner.scannerBeep(station[1].ScannerID);
        }

        private void bBeepScanner2_Click(object sender, EventArgs e)
        {
            zebraScanner.scannerBeep(station[2].ScannerID);
        }

        private void DispachWarehouseMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.Settings.Save();
        }

        private void bLEDRedScanner1_Click(object sender, EventArgs e)
        {
            if (station[1].ScanerLEDRed)
            {
                zebraScanner.scannerLEDRedOff(station[1].ScannerID);
            }
            else
            {
                zebraScanner.scannerLEDRedOn(station[1].ScannerID);
            }
            station[1].ScanerLEDRed = !station[1].ScanerLEDRed;

        }

        private void bLEDRedScanner2_Click(object sender, EventArgs e)
        {
            if (station[2].ScanerLEDRed)
            {
                zebraScanner.scannerLEDRedOff(station[2].ScannerID);
            }
            else
            {
                zebraScanner.scannerLEDRedOn(station[2].ScannerID);
            }
            station[2].ScanerLEDRed = !station[2].ScanerLEDRed;
        }

       
        private void bLoginLog1_Click(object sender, EventArgs e)
        {
            logByPass(1);
        }

        private void bLoginLog2_Click(object sender, EventArgs e)
        {
            logByPass(2);
        }

        public void TpStorageUnitActive(int stationId)
        {
            clearStationLabel(stationId);

            station[stationId].TcMain.SelectedTab = station[stationId].TpStorageUnit;
            station[stationId].BModeAct.Visible = true;
            station[stationId].BModeAct.Text = "Miejsca odkładcze";
        }

        public void TpStorageLabelPrintActive(int stationId)
        {
            clearStationLabel(stationId);

            station[stationId].TcMain.SelectedTab = station[stationId].TpStoragePrintLabel;
            station[stationId].BModeAct.Visible = true;
            station[stationId].BModeAct.Text = "Dodruk etykiety";
        }

        public void TpAllocationActive(int stationId)
        {
            clearStationLabel(stationId);

            station[stationId].BModeAct.Visible = true;

            station[stationId].TcMain.SelectedTab = station[stationId].TpAllocation;

            station[stationId].LAllocationName.Text = "Alokacja paczek";
            station[stationId].BModeAct.Text = "Alokacja paczek";
            station[stationId].LAllocationInfo.Text = "Zeskanuj kod z miejsca odkładczego";
        }

        public void TpAllocationChangeActive(int stationId)
        {
            clearStationLabel(stationId);

            station[stationId].BModeAct.Visible = true;

            station[stationId].TcMain.SelectedTab = station[stationId].TpAllocationChange;
            station[stationId].BModeAct.Text = "Zmiana alokacji";

        }

        public void TpIssuingActive(int stationId)
        {
            clearStationLabel(stationId);

            station[stationId].TcMain.SelectedTab = station[stationId].TpIssuing;
            station[stationId].BModeAct.Visible = true;
            station[stationId].BModeAct.Text = "Wydawanie paczek";
        }

        public void TpReportActive(int stationId)
        {
            clearStationLabel(stationId);

            station[stationId].TcMain.SelectedTab = station[stationId].TpReport;
            station[stationId].BModeAct.Visible = true;
            station[stationId].BModeAct.Text = "Raport";

            showReportTp(stationId);
        }

        public void clearStationLabel(int stationId)
        {
            //storage unit
            station[stationId].LStorageUnitNewCourierType.Text = "";
            station[stationId].LStorageUnitNewUnit.Text = "";
            station[stationId].LStorageUnitNewTimeStamp.Text = "";
            station[stationId].LStorageUnitNewAdress.Text = "";
            station[stationId].DgvStorageList.DataSource = null;

            //allocation
            station[stationId].LAllocationCourierType.Text = "";
            station[stationId].LAllocationUnit.Text = "";
            station[stationId].LAllocationAdress.Text = "";
            station[stationId].LAllocationTimeStamp.Text = "";
            station[stationId].LAllocationCount.Text = "";
            station[stationId].DgvAllocationList.DataSource = null;

            //allaocatin change
            station[1].LReAllocationSourceCourierType.Text = "";
            station[1].LReAllocationSourceUnit.Text = "";
            station[1].LReAllocationSourceTimeStamp.Text = "";
            station[1].LReAllocationSourceCount.Text = "";
            station[1].LReAllocationSourceAdress.Text = "";
            station[1].LReAllocationDestinationCourierType.Text = "";
            station[1].LReAllocationDestinationUnit.Text = "";
            station[1].LReAllocationDestinationTimeStamp.Text = "";
            station[1].LReAllocationDestinationCount.Text = "";
            station[1].LReAllocationDestinationAdress.Text = "";
            station[1].DgvReAllocationSourceList.DataSource = null;

            //issuing
            station[1].LIssuingCourierType.Text = "";
            station[1].LIssuingUnit.Text = "";
            station[1].LIssuingTimeStamp.Text = "";
            station[1].LIssuingCount.Text = "";
            station[1].LIssuingAdress.Text = "";
            station[1].DgvIssuingList.DataSource = null;

            //raport
            station[stationId].TbReportKp.Text = "";
            station[stationId].TbReportKpLkp.Text = "";
            station[stationId].TbReportZpl.Text = "";
            station[stationId].TbReportUnit.Text = "";
            station[stationId].LReportKpError.Visible = false;
            station[stationId].LReportKpLkpError.Visible = false;
            station[stationId].LReportZplError.Visible = false;
            station[stationId].LReportUnitError.Visible = false;

            station[stationId].bReportPrint1.Visible = false;
            station[stationId].bReportPrint2.Visible = false;
            station[stationId].bReportPrint3.Visible = false;
            station[stationId].bReportPrint4.Visible = false;

            station[stationId].DgvReport.DataSource = null;
        }

        public void storageUnitPageUpdate(int stationId)
        {
            try
            {
                station[stationId].LStorageUnitNewCourierType.Text = station[stationId].StorageUnitNew.Courier_type;
                station[stationId].LStorageUnitNewUnit.Text = $"U{station[stationId].StorageUnitNew.Id.ToString("D10")}";
                station[stationId].LStorageUnitNewTimeStamp.Text = station[stationId].StorageUnitNew.Create_time_stamp.ToString();
                station[stationId].LStorageUnitNewAdress.Text = station[stationId].StorageUnitNew.dispachShowName;

                if (station[stationId].StorageUnits != null)
                {
                    station[stationId].DgvStorageList.DataSource = station[stationId].StorageUnits;

                    DgvStorageListInit(stationId);
                }
            }
            catch
            { }
        }

        public void DgvStorageListInit(int stationId)
        {
            station[stationId].DgvStorageList.Columns["Id"].HeaderText = "Nr miejsca";
            station[stationId].DgvStorageList.Columns["Create_time_stamp"].HeaderText = "Utworzono";
            station[stationId].DgvStorageList.Columns["Post_code"].HeaderText = "Kod pocztowy";
            station[stationId].DgvStorageList.Columns["City"].HeaderText = "Miasto";
            station[stationId].DgvStorageList.Columns["Street"].HeaderText = "Ulica";
            station[stationId].DgvStorageList.Columns["dispachShowName"].HeaderText = "Odbiorca";
            station[stationId].DgvStorageList.Columns["Courier_type"].HeaderText = "Kurier";

            station[stationId].DgvStorageList.Columns["Id"].DisplayIndex = 0;
            station[stationId].DgvStorageList.Columns["Create_time_stamp"].DisplayIndex = 5;
            station[stationId].DgvStorageList.Columns["Post_code"].DisplayIndex = 3;
            station[stationId].DgvStorageList.Columns["City"].DisplayIndex = 4;
            station[stationId].DgvStorageList.Columns["Street"].DisplayIndex = 2;
            station[stationId].DgvStorageList.Columns["dispachShowName"].DisplayIndex = 1;


            //station[stationId].DgvStorageList.Columns["Package_barcode"].Visible = false;
            station[stationId].DgvStorageList.Columns["ReferenceID"].Visible = false;
            station[stationId].DgvStorageList.Columns["Ref_from"].Visible = false;
            station[stationId].DgvStorageList.Columns["Ref_to"].Visible = false;
            station[stationId].DgvStorageList.Columns["Dispatch_surname"].Visible = false;
            station[stationId].DgvStorageList.Columns["Dispatch_company"].Visible = false;
            station[stationId].DgvStorageList.Columns["Operator_id"].Visible = false;
            station[stationId].DgvStorageList.Columns["Unit_simple"].Visible = false;
            station[stationId].DgvStorageList.Columns["Unit_close"].Visible = false;
            station[stationId].DgvStorageList.Columns["Dispatch_name"].Visible = false;
            //.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            //.Visible = false;
        }


        public void allocationPageUpdate(int stationId)
        {
            station[stationId].BModeAct.Visible = true;
            //Zmienione przez ŁPA - przecież to jest bez sensu bo jak skanuję meijsce odkładcze w alokacji to mi u góry tryb zmienia
            //station[stationId].BModeAct.Text = "Miejsca odkładcze";

            if (station[stationId].Allocation.Storage.ReferenceID != null)
            {
                station[stationId].LAllocationCourierType.Text = station[stationId].Allocation.Storage.Courier_type;
                station[stationId].LAllocationUnit.Text = $"U{station[stationId].Allocation.Storage.Id.ToString("D10")}";
                station[stationId].LAllocationAdress.Text = station[stationId].Allocation.Storage.dispachShowName;
                station[stationId].LAllocationTimeStamp.Text = station[stationId].Allocation.Storage.Create_time_stamp.ToString();

                //Dopisane przez ŁPA jak >0 to przy nowej etykiecie w trakcie alokacji nei czyścił listy
                if (station[stationId].Allocation.DeliveryDispachShows != null && station[stationId].Allocation.DeliveryDispachShows.Count > 0)
                {
                    station[stationId].LAllocationCount.Text = station[stationId].Allocation.PackCounter.ToString() + " / " + station[stationId].Allocation.DeliveryDispachShows.Count.ToString();
                    station[stationId].DgvAllocationList.DataSource = station[stationId].Allocation.DeliveryDispachShows;
                    //DgvListReportInit(station[stationId].DgvAllocationList);
                    DgvListInit(station[stationId].DgvAllocationList);
                }
                else if (station[stationId].Allocation.DeliveryDispachShows != null)
                {
                    station[stationId].DgvAllocationList.DataSource = null;
                    station[stationId].LAllocationCount.Text = "0 / 0";
                }
                else
                {
                    station[stationId].LAllocationCount.Text = "0 / 0";
                }

                //
                if (!station[stationId].Allocation.StorageScanDone)
                {
                    station[stationId].LAllocationInfo.Text = "Zeskanuj kod z miejsca odkładczego";
                }
                else if (station[stationId].Allocation.ZplScanDone)
                {
                    station[stationId].LAllocationInfo.Text = "Zeskanuj numer listu przewozowego FEDEX (górny kod)";
                }
                else
                {
                    //Dopisane ŁPA
                    //Czyli to pokazuje kolejny krok do zrobienia
                    if (station[stationId].Allocation.Storage.Courier_type.Equals("DPD"))
                    {
                        station[stationId].LAllocationInfo.Text = "Zeskanuj kod paczki z listu DPD";
                    }
                    else if (station[stationId].Allocation.Storage.Courier_type.Equals("FEDEX") || station[stationId].Allocation.Storage.Courier_type.Equals("FEDEX-S"))
                    {
                        station[stationId].LAllocationInfo.Text = "Zeskanuj kod etykiety końcowej";
                    }
                    else
                    {
                        station[stationId].LAllocationInfo.Text = "Zeskanuj kod etykiety końcowej";
                    }
                }
            }
        }

        public void allocationChangePageUpdate(int stationId)
        {
            station[stationId].BModeAct.Visible = true;
            station[stationId].BModeAct.Text = "Zmiana alokacji";

            if (station[stationId].ReAllocationSource.Storage.ReferenceID != null)
            {
                station[stationId].LReAllocationSourceCourierType.Text = station[stationId].ReAllocationSource.Storage.Courier_type;
                station[stationId].LReAllocationSourceUnit.Text = $"U{station[stationId].ReAllocationSource.Storage.Id.ToString("D10")}";
                station[stationId].LReAllocationSourceAdress.Text = station[stationId].ReAllocationSource.Storage.dispachShowName;
                station[stationId].LReAllocationSourceTimeStamp.Text = station[stationId].ReAllocationSource.Storage.Create_time_stamp.ToString();

                if (station[stationId].ReAllocationSource.DeliveryDispachShows != null && station[stationId].ReAllocationSource.DeliveryDispachShows.Count > 0)
                {
                    station[stationId].LReAllocationSourceCount.Text = station[stationId].ReAllocationSource.PackCounter.ToString() + " / " + station[stationId].ReAllocationSource.DeliveryDispachShows.Count.ToString();
                    station[stationId].DgvReAllocationSourceList.DataSource = station[stationId].ReAllocationSource.DeliveryDispachShows;
                    //DgvListReportInit(station[stationId].DgvReAllocationSourceList);
                    DgvListInit(station[stationId].DgvReAllocationSourceList);
                }
                else
                {
                    station[stationId].LReAllocationSourceCount.Text = "0 / 0";
                }

                if (station[stationId].ReAllocationDestination.Storage != null && station[stationId].ReAllocationDestination.Storage.ReferenceID != null)
                {
                    station[stationId].LReAllocationDestinationCourierType.Text = station[stationId].ReAllocationDestination.Storage.Courier_type;
                    station[stationId].LReAllocationDestinationUnit.Text = $"U{station[stationId].ReAllocationDestination.Storage.Id.ToString("D10")}";
                    station[stationId].LReAllocationDestinationAdress.Text = station[stationId].ReAllocationDestination.Storage.dispachShowName;
                    station[stationId].LReAllocationDestinationTimeStamp.Text = station[stationId].ReAllocationDestination.Storage.Create_time_stamp.ToString();

                    station[stationId].LReAllocationDestinationCount.Text = station[stationId].ReAllocationDestination.PackCounter.ToString() + " / " + station[stationId].ReAllocationDestination.DeliveryDispachShows.Count.ToString();
                }
                ////
                //if (!station[stationId].Allocation.StorageScanDone)
                //{
                //    station[stationId].LAllocationInfo.Text = "Zeskanuj kod z miejsca odkładczego";
                //}
                //else if (station[stationId].Allocation.ZplScanDone)
                //{
                //    station[stationId].LAllocationInfo.Text = "Zeskanuj numer listu przewozowego FEDEX (górny kod)";
                //}
                //else
                //{
                //    station[stationId].LAllocationInfo.Text = "Zeskanuj kod etykiety końcowej";
                //}
            }
            else
            {
                station[stationId].LReAllocationSourceCourierType.Text = "";
                station[stationId].LReAllocationSourceUnit.Text = "";
                station[stationId].LReAllocationSourceAdress.Text = "";
                station[stationId].LReAllocationSourceTimeStamp.Text = "";

                station[stationId].LReAllocationSourceCount.Text = "0 / 0";
                station[stationId].DgvReAllocationSourceList.DataSource = null;

                station[stationId].LReAllocationDestinationCourierType.Text = ""; 
                station[stationId].LReAllocationDestinationUnit.Text = "";
                station[stationId].LReAllocationDestinationAdress.Text = "";
                station[stationId].LReAllocationDestinationTimeStamp.Text = "";

                station[stationId].LReAllocationDestinationCount.Text = "0 / 0";

            }
        }

        public void issuingPageUpdate(int stationId)
        {

            station[stationId].BModeAct.Visible = true;

            if (station[stationId].Issuing.Storage.ReferenceID != null)
            {
                station[stationId].LIssuingCourierType.Text = station[stationId].Issuing.Storage.Courier_type;
                station[stationId].LIssuingUnit.Text = $"U{station[stationId].Issuing.Storage.Id.ToString("D10")}";
                station[stationId].LIssuingAdress.Text = station[stationId].Issuing.Storage.dispachShowName;
                station[stationId].LIssuingTimeStamp.Text = station[stationId].Issuing.Storage.Create_time_stamp.ToString();

                if (station[stationId].Issuing.DeliveryDispachShows != null && station[stationId].Issuing.DeliveryDispachShows.Count > 0)
                {
                    station[stationId].LIssuingCount.Text = station[stationId].Issuing.PackCounter.ToString() + " / " +station[stationId].Issuing.DeliveryDispachShows.Count.ToString();
                    station[stationId].DgvIssuingList.DataSource = station[stationId].Issuing.DeliveryDispachShows;
                    //DgvListReportInit(station[stationId].DgvIssuingList);
                    DgvListInit(station[stationId].DgvIssuingList);
                }
                else
                {
                    station[stationId].LIssuingCount.Text = "0 / 0";
                }
            }
            else
            {
                station[stationId].LIssuingCourierType.Text = "";
                station[stationId].LIssuingUnit.Text = "";
                station[stationId].LIssuingAdress.Text = "";
                station[stationId].LIssuingTimeStamp.Text = "";
                station[stationId].LIssuingCount.Text = "0 / 0";
                station[stationId].DgvIssuingList.DataSource = null;
            }


        }

        public void reportPageUpdate(int stationId)
        {
            showReportTp(stationId);
            if (station[stationId].DispachReports != null && station[stationId].DispachReports.Count > 0)
            {
                station[stationId].DgvReport.DataSource = station[stationId].DispachReports;

                //station[stationId].ReportData = new DataTable();
                //using (var reader = ObjectReader.Create(station[stationId].DispachReports))
                //{
                //    station[stationId].ReportData.Load(reader);
                //}

                DgvListReportInit(station[stationId].DgvReport, true);
            }
        }


        public void DgvListInit(DataGridView dgv)
        {
            dgv.Columns["KP_LKP"].HeaderText = "KP.LKP";
            dgv.Columns["D_Pak"].HeaderText = "D.Pak";
            dgv.Columns["D_Alok"].HeaderText = "D.Alok";
            dgv.Columns["D_Wys"].HeaderText = "D.Wys";

            dgv.Columns["KP_LKP"].Width = 60;
            dgv.Columns["Odbiorca"].Width = 90;
            dgv.Columns["Spak"].Width = 20;
            dgv.Columns["Typ"].Width = 55;
            dgv.Columns["D_Pak"].Width = 65;
            dgv.Columns["D_Alok"].Width = 65;
            dgv.Columns["Bryty"].Width = 80;
            dgv.Columns["D_Wys"].Width = 65;

            //dgv.Columns["Odbiorca"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.RowHeadersVisible = false;

            /*
        public string KP_LKP { get; set; }
        public string Odbiorca { get; set; }
        public string Spak { get; set; }
        public string Typ { get; set; }
        public string D_Pak { get; set; }
        public string D_Alok { get; set; }
        public string Kurier { get; set; }
        public string Bryty { get; set; }
        public string D_Wys { get; set; }
             */
        }

        public void DgvListReportInit(DataGridView dgv, bool colour = false)
        {
            //dgv.Columns["KP_LKP"].HeaderText = "KP.LKP";
            //dgv.Columns["D_Pak"].HeaderText = "D.Pak";
            //dgv.Columns["D_Alok"].HeaderText = "D.Alok";
            //dgv.Columns["D_Wys"].HeaderText = "D.Wys";

            dgv.Columns["Kurier"].Width = 60;
            dgv.Columns["Kurier"].ReadOnly = true;
            dgv.Columns["KP_LKP"].Width = 60;
            dgv.Columns["KP_LKP"].ReadOnly = true;
            dgv.Columns["Odbiorca"].Width = 90;
            dgv.Columns["Odbiorca"].ReadOnly = true;
            dgv.Columns["Ile"].Width = 20;
            dgv.Columns["Ile"].ReadOnly = true;
            dgv.Columns["Spak"].Width = 20;
            dgv.Columns["Spak"].ReadOnly = true;
            dgv.Columns["Typ"].Width = 55;
            dgv.Columns["Typ"].ReadOnly = true;
            dgv.Columns["D_Wys_CM"].Width = 65;
            dgv.Columns["D_Wys_CM"].ReadOnly = true;
            dgv.Columns["D_Wys_PM"].Width = 65;
            dgv.Columns["D_Wys_PM"].ReadOnly = true;
            dgv.Columns["D_Wys_R"].Width = 65;
            dgv.Columns["D_Wys_R"].ReadOnly = true;
            dgv.Columns["D_Pak"].Width = 65;
            dgv.Columns["D_Pak"].ReadOnly = true;
            dgv.Columns["D_Alok"].Width = 65;
            dgv.Columns["D_Alok"].ReadOnly = true;
            dgv.Columns["NRMO"].Width = 80;
            dgv.Columns["NRMO"].ReadOnly = true;
            dgv.Columns["KurierInfo"].Width = 80;
            dgv.Columns["KurierInfo"].ReadOnly = true;
            dgv.Columns["Bryt"].Width = 80;
            dgv.Columns["Bryt"].ReadOnly = true;
            dgv.Columns["OpisLinii"].Width = 250;
            dgv.Columns["OpisLinii"].ReadOnly = true;
            dgv.Columns["Stacja"].Width = 60;
            dgv.Columns["Stacja"].ReadOnly = true;
            dgv.Columns["PowodNiewyk"].Width = 250;
            dgv.Columns["Referenceid"].Visible = false;
            dgv.Columns["Reffrom"].Visible = false;
            dgv.Columns["Refto"].Visible = false;

            //dgv.Columns["Odbiorca"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv.RowHeadersVisible = true;

            if (colour)
            {
                foreach (DataGridViewRow dgvr in dgv.Rows)
                {
                    if (dgvr.Cells["D_Pak"].Value.ToString().Equals("-"))
                        dgvr.Cells["D_Pak"].Style.BackColor = Color.Red;

                    if (dgvr.Cells["D_Wys_R"].Value.ToString().Equals("-"))
                        dgvr.Cells["D_Wys_R"].Style.BackColor = Color.Red;

                    //Dopsiane ŁPA Pomarańczowy jak jest powód niewydania
                    if (!dgvr.Cells["PowodNiewyk"].Value.ToString().Equals("") && (dgvr.Cells["D_Pak"].Value.ToString().Equals("-") || dgvr.Cells["D_Wys_R"].Value.ToString().Equals("-")))
                        dgvr.DefaultCellStyle.BackColor = Color.Green;
                }
            }
            //station[stationId].DgvAllocationList.Columns["Package_barcode"].Visible = false;

            /*
            
        public string Kurier { get; set; }
        public string KP_LKP { get; set; }
        public string Odbiorca { get; set; }
        public string Ile { get; set; }
        public string Spak { get; set; }
        public string Typ { get; set; }
        public string D_Wys_CM { get; set; }
        public string D_Wys_PM { get; set; }
        public string D_Wys_R { get; set; }
        public string D_Pak { get; set; }
        public string D_Alok { get; set; }
        public string NRMO { get; set; }
        public string KurierInfo { get; set; }
        public string Bryt { get; set; }
             */
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        //==========================================================================================================================================================

        public const int REPORT_STROER = 1;
        public const int REPORT_STROER_WARSZAWA = 2;
        public const int REPORT_DATE = 3;
        public const int REPORT_KP = 4;
        public const int REPORT_KPLKP = 5;
        public const int REPORT_ZPL = 6;
        public const int REPORT_UNIT = 7;
        public const int REPORT_INFO = 8;


        private void bReportStroer1_Click(object sender, EventArgs e)
        {
            station[1].ModeAct = MODE_REPORT;
            station[1].ReportType = REPORT_STROER;
            showReportTp(1);
        }

        private void bReportStroerWaw1_Click(object sender, EventArgs e)
        {
            station[1].ModeAct = MODE_REPORT;
            station[1].ReportType = REPORT_STROER_WARSZAWA;
            showReportTp(1);
        }

        private void bReportDate1_Click(object sender, EventArgs e)
        {
            station[1].ModeAct = MODE_REPORT;
            station[1].ReportType = REPORT_DATE;
            showReportTp(1);
        }

        private void bReportKp1_Click(object sender, EventArgs e)
        {
            station[1].ModeAct = MODE_REPORT;
            station[1].ReportType = REPORT_KP;
            showReportTp(1);
        }

        private void bReportKpLkp1_Click(object sender, EventArgs e)
        {
            station[1].ModeAct = MODE_REPORT;
            station[1].ReportType = REPORT_KPLKP;
            showReportTp(1);
        }

        private void bReportZpl1_Click(object sender, EventArgs e)
        {
            station[1].ReportType = REPORT_ZPL;
            showReportTp(1);
        }

        private void bReportUnit1_Click(object sender, EventArgs e)
        {
            station[1].ModeAct = MODE_REPORT;
            station[1].ReportType = REPORT_UNIT;
            showReportTp(1);
        }

        private void bReportMake1_Click(object sender, EventArgs e)
        {

            int stationId = 1;

            //station[stationId].ReportType = REPORT_DATE;
            //station[stationId].ModeAct = MODE_REPORT;

            if (station[stationId].ReportType == REPORT_STROER)
            {
                if (stationId == 1)
                {
                    station[stationId].NotPacked1 = dtpReportNP1.Checked ? 1 : 0;
                    station[stationId].NotSent1 = dtpReportNS1.Checked ? 1 : 0;
                    station[stationId].Courier1 = Courier1;
                }
                else
                {
                    station[stationId].NotPacked2 = dtpReportNP2.Checked ? 1 : 0;
                    station[stationId].NotSent2 = dtpReportNS2.Checked ? 1 : 0;
                    station[stationId].Courier2 = Courier2;
                }
            }
            else if (station[stationId].ReportType == REPORT_STROER_WARSZAWA)
            {
                if (stationId == 1)
                {
                    station[stationId].NotPacked1 = dtpReportNP1.Checked ? 1 : 0;
                    station[stationId].NotSent1 = dtpReportNS1.Checked ? 1 : 0;
                    station[stationId].Courier1 = Courier1;
                }
                else
                {
                    station[stationId].NotPacked2 = dtpReportNP2.Checked ? 1 : 0;
                    station[stationId].NotSent2 = dtpReportNS2.Checked ? 1 : 0;
                    station[stationId].Courier2 = Courier2;
                }
            }
            else if (station[stationId].ReportType == REPORT_DATE)
            {
                if (stationId==1)
                {
                    station[stationId].NotPacked1= dtpReportNP1.Checked ? 1 : 0;
                    station[stationId].NotSent1 = dtpReportNS1.Checked ? 1 : 0;
                    station[stationId].Courier1 = Courier1;
                }
                else
                {
                    station[stationId].NotPacked2 = dtpReportNP2.Checked ? 1 : 0;
                    station[stationId].NotSent2 = dtpReportNS2.Checked ? 1 : 0;
                    station[stationId].Courier2 = Courier2;
                }
            }
            else if (station[stationId].ReportType == REPORT_KP)
            {
                if (!checkKp(station[stationId].TbReportKp.Text))
                {
                    station[stationId].LReportKpError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_KPLKP)
            {
                if (!checkKpLkp(station[stationId].TbReportKpLkp.Text))
                {
                    station[stationId].LReportKpLkpError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_ZPL)
            {
                if (!checkZpl(station[stationId].TbReportZpl.Text))
                {
                    station[stationId].LReportZplError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_UNIT)
            {
                if (!checkUnit(station[stationId].TbReportUnit.Text))
                {
                    station[stationId].LReportUnitError.Visible = true;
                    return;
                }
            }
            reportMade(1);
        }

        public bool checkKp(string inStr)
        {
            return int.TryParse(inStr, out _);
        }

        public bool checkKpLkp(string inStr)
        {
            string[] strArray = inStr.Split('.');
            if (strArray.Count() != 2)
                return false;
            else
            {
                bool p1 = int.TryParse(strArray[0], out _);
                bool p2 = int.TryParse(strArray[1], out _);
                return p1 && p2;
            }
        }

        public bool checkZpl(string inStr)
        {
            return int.TryParse(inStr, out _);
        }

        public bool checkUnit(string inStr)
        {
            return int.TryParse(inStr, out _);
        }

        private void updateReason(int stationId, int rowIndex)
        {
            string reason = null;
            string referenceid = null;
            string reffrom = null;
            string refto = null;
            string query = "";
            string queryArchive = "";

            if (station[stationId].DgvReport.Rows[rowIndex].Cells[16].Value != null)
            {
                reason = station[stationId].DgvReport.Rows[rowIndex].Cells[16].Value.ToString();
            }
            else
            {
                reason = "";
            }

            if (station[stationId].DgvReport.Rows[rowIndex].Cells[17].Value != null)
            {
                referenceid = station[stationId].DgvReport.Rows[rowIndex].Cells[17].Value.ToString();
                reffrom = station[stationId].DgvReport.Rows[rowIndex].Cells[18].Value.ToString();
                refto = station[stationId].DgvReport.Rows[rowIndex].Cells[19].Value.ToString();
                query = $"UPDATE dispatch_item_table SET reason_not_done='{reason}' WHERE reference_id='{referenceid}' AND ref_from='{reffrom}' and ref_to='{refto}'";
                queryArchive = $"UPDATE dispatch_item_table SET reason_not_done='{reason}' WHERE reference_id='{referenceid}' AND ref_from='{reffrom}' and ref_to='{refto}'";
            }


            if (referenceid != null && reason != null)
            {
                if (!referenceid.Equals(""))
                {
                    station[stationId].Data.updateReason(query, queryArchive);
                }
            }
        }

        private void reportMade(int stationId)
        {
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamDate);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamKp);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamKpLkp);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamZpl);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamUnit);

            station[stationId].TcRaportParam.TabPages.Add(station[stationId].TpReportInfo);

            if (station[stationId].ReportType == REPORT_STROER)
            {

                /*   
                v1.0 - zmienione przez p. Łukasza 2018-08-28
                WHERE (da.dispatch_date='[DATA Z KONTROLKI])
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type='FEDEX-S' OR sc.dispatch_company IS NOT NULL)
                */
                /*
                string where = $"WHERE (da.dispatch_date like '{station[stationId].DtpReportParam.Value.Year.ToString("D4")}-{station[stationId].DtpReportParam.Value.Month.ToString("D2")}-{station[stationId].DtpReportParam.Value.Day.ToString("D2")}%')";
                where += " AND(dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)";
                where += " AND(da.courier_type='FEDEX-S' OR sc.dispatch_company IS NOT NULL)";
                */

                /*
                v2.0 - od p. Łukasza z dnia 2018-08-28
                WHERE (da.dispatch_date=’[DATA Z KONTROLKI]’)
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type='FEDEX-S')
                */
                string date = $"{station[stationId].DtpReportParam.Value.Year.ToString("D4")}-{station[stationId].DtpReportParam.Value.Month.ToString("D2")}-{station[stationId].DtpReportParam.Value.Day.ToString("D2")}";

                //xxxx
                int notpacked = 0;
                int notsent = 0;
                string courier = "";

                if (stationId==1)
                {
                    notpacked = station[stationId].NotPacked1;
                    notsent = station[stationId].NotSent1;
                    courier = station[stationId].Courier1.SelectedItem.ToString();
                }
                else
                {
                    notpacked = station[stationId].NotPacked2;
                    notsent = station[stationId].NotSent2;
                    courier = station[stationId].Courier2.SelectedItem.ToString();
                }
 
                string where = $"WHERE (da.dispatch_date like '{date}%' OR (jd.dispatch_date like '{date}%' AND da.dispatch_date IS NULL) OR dd.release_time_stamp like '{date}%')";
                where += " AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)";
                where += " AND (da.courier_type='FEDEX-S') AND jd.jobstatus<>'ANULOWANA'";

                if (notpacked==1)
                {
                    where += " AND di.pack_date IS NULL";
                }

                if (notsent==1)
                {
                    where += " AND dd.release_time_stamp IS NULL";
                }

                if (!courier.Equals("") && !courier.Equals("WSZYSCY"))
                {
                    where += " AND da.courier_type='{courier}'";
                }

                station[stationId].ReportWhere =  where;
                scannerBarcodeRevieved("RS00000001", station[stationId].CradleId.ToString());

                station[stationId].ReportName = "Raport po dacie wysyłki STROER";
                station[stationId].ReportParam1 = $"Data: {date}";
                station[stationId].ReportParam2 = $"";
            }
            else if (station[stationId].ReportType == REPORT_STROER_WARSZAWA)
            {
                /*
                WHERE (da.dispatch_date=’[DATA Z KONTROLKI]’)
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (sc.dispatch_company IS NOT NULL)
                */
                int notpacked = 0;
                int notsent = 0;
                string courier = "";

                if (stationId == 1)
                {
                    notpacked = station[stationId].NotPacked1;
                    notsent = station[stationId].NotSent1;
                    courier = station[stationId].Courier1.SelectedItem.ToString();
                }
                else
                {
                    notpacked = station[stationId].NotPacked2;
                    notsent = station[stationId].NotSent2;
                    courier = station[stationId].Courier2.SelectedItem.ToString();
                }

                string date = $"{station[stationId].DtpReportParam.Value.Year.ToString("D4")}-{station[stationId].DtpReportParam.Value.Month.ToString("D2")}-{station[stationId].DtpReportParam.Value.Day.ToString("D2")}";
                string where = $"WHERE (da.dispatch_date like '{date}%' OR (jd.dispatch_date like '{date}%' AND da.dispatch_date IS NULL) OR dd.release_time_stamp like '{date}%') ";
                where += " AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)";
                where += " AND (sc.dispatch_company IS NOT NULL) AND jd.jobstatus<>'ANULOWANA'";

                if (notpacked == 1)
                {
                    where += " AND di.pack_date IS NULL";
                }

                if (notsent == 1)
                {
                    where += " AND dd.release_time_stamp IS NULL";
                }

                if (!courier.Equals("") && !courier.Equals("WSZYSCY"))
                {
                    where += " AND da.courier_type='{courier}'";
                }

                station[stationId].ReportWhere = where;
                scannerBarcodeRevieved("RS00000001", station[stationId].CradleId.ToString());

                station[stationId].ReportName = "Raport po dacie wysyłki STROER WARSZAWA";
                station[stationId].ReportParam1 = $"Data: {date}";
                station[stationId].ReportParam2 = $"";
            }
            else if (station[stationId].ReportType == REPORT_DATE)
            {
                /*
                WHERE (da.dispatch_date=['DATA Z KONTROLKI]' OR jd.dispatch_date=' [DATA Z KONTROLK] ')
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL)
                */
                int notpacked = 0;
                int notsent = 0;
                string courier = "";

                if (stationId == 1)
                {
                    notpacked = station[stationId].NotPacked1;
                    notsent = station[stationId].NotSent1;
                    courier = station[stationId].Courier1.SelectedItem.ToString();
                }
                else
                {
                    notpacked = station[stationId].NotPacked2;
                    notsent = station[stationId].NotSent2;
                    courier = station[stationId].Courier2.SelectedItem.ToString();
                }

                string date = $"{station[stationId].DtpReportParam.Value.Year.ToString("D4")}-{station[stationId].DtpReportParam.Value.Month.ToString("D2")}-{station[stationId].DtpReportParam.Value.Day.ToString("D2")}";
                string where = $"WHERE (da.dispatch_date like '{date}%' OR (jd.dispatch_date like '{date}%' AND da.dispatch_date IS NULL) OR dd.release_time_stamp like '{date}%') ";
                where += " AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)";
                where += " AND (da.courier_type <> 'FEDEX-S' AND sc.dispatch_company IS NULL) AND jd.jobstatus<>'ANULOWANA'";

                if (notpacked == 1)
                {
                    where += " AND di.pack_date IS NULL";
                }

                if (notsent == 1)
                {
                    where += " AND dd.release_time_stamp IS NULL";
                }

                if (!courier.Equals("") && !courier.Equals("WSZYSCY"))
                {
                    where += $" AND (da.courier_type='{courier}' or da.courier_type IS NULL)";
                }

                station[stationId].ReportWhere = where;
                scannerBarcodeRevieved("RS00000001", station[stationId].CradleId.ToString());

                station[stationId].ReportName = "Raport po dacie wysyłki";
                station[stationId].ReportParam1 = $"Data: {date}";
                station[stationId].ReportParam2 = $"";
            }
            else if (station[stationId].ReportType == REPORT_KP)
            {
                /*
                WHERE (di.jdnr=[KP Z KONTROLKI] OR jd.jdnr=[KP Z KONTROLKI])
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL)
                */
                string kp = $"{station[stationId].TbReportKp.Text}";
                string where = $"WHERE (di.jdnr={kp} OR jd.jdnr={kp})";
                where += " AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL) AND jd.jobstatus<>'ANULOWANA'";
                //where += " AND (da.courier_type <> 'FEDEX-S' AND sc.dispatch_company IS NULL)";

                station[stationId].ReportWhere = where;
                scannerBarcodeRevieved("RS00000001", station[stationId].CradleId.ToString());

                station[stationId].ReportName = "Raport po numerze KP";
                station[stationId].ReportParam1 = $"KP: {kp}";
                station[stationId].ReportParam2 = $"";
            }
            else if (station[stationId].ReportType == REPORT_KPLKP)
            {
                /*
                WHERE (CONCAT(di.jdnr,'.',di.jdline)='[TESKT Z KONTROLKI]' OR CONCAT(jd.jdnr,'.',jd.jdline)='[TEKST Z KONTROLKI]')
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL)*/
                string kplkp = $"{station[stationId].TbReportKpLkp.Text}";
                string where = $"WHERE (CONCAT(di.jdnr,'.',di.jdline)='{kplkp}' OR CONCAT(jd.jdnr,'.',jd.jdline)='{kplkp}')";
                where += " AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL) AND jd.jobstatus<>'ANULOWANA'";
                //where += " AND (da.courier_type <> 'FEDEX-S' AND sc.dispatch_company IS NULL)";

                station[stationId].ReportWhere = where;
                scannerBarcodeRevieved("RS00000001", station[stationId].CradleId.ToString());

                station[stationId].ReportName = "Raport po numerze KP i LKP";
                station[stationId].ReportParam1 = $"KP.LKP: {kplkp}";
                station[stationId].ReportParam2 = $"";
            }
            else if (station[stationId].ReportType == REPORT_ZPL)
            {
                /*
                SELECT jdnr from Printtemp WHERE id=[KOD ID Z KONTROLKI]
                UNION ALL
                SELECT jdnr from Archive WHERE id=[KOD ID Z KONTROLKI]

                WHERE (di.jdnr=[JDNR Z ZAPYTANIA POWYŻEJ] OR jd.jdnr=[JDNR Z ZAPYTANIA POWYŻEJ])
                AND (dd.allocation_obsolete=0 OR dd.allocation_obsolete IS NULL)
                AND (da.courier_type <> 'FEDEX-S' OR sc.dispatch_company IS NULL) 
                */
                string zpl = $"{station[stationId].TbReportZpl.Text}";
                string where = $"{zpl}";

                station[stationId].ReportWhere = where;
                scannerBarcodeRevieved("RS00000001", station[stationId].CradleId.ToString());

                station[stationId].ReportName = "Raport po kodzie z plakatu (CAŁA KP)";
                station[stationId].ReportParam1 = $"KP: {zpl}";
                station[stationId].ReportParam2 = $"";
            }
            else if (station[stationId].ReportType == REPORT_UNIT)
            {
                /*
                WHERE da.dispatch_date='[DATA Z KONTROLKI]'
                AND dd.allocation_obsolete=0 AND dd.storage_unit_id=[NUMER Z KODU MIEJSCA]
                */


                string unit = $"{int.Parse(station[stationId].TbReportUnit.Text).ToString()}";
                string date = $"{station[stationId].DtpReportUnit.Value.Year.ToString("D4")}-{station[stationId].DtpReportUnit.Value.Month.ToString("D2")}-{station[stationId].DtpReportUnit.Value.Day.ToString("D2")}";
                string where = $" WHERE (da.dispatch_date like '{date}%' OR (jd.dispatch_date like '{date}%' AND da.dispatch_date IS NULL) OR dd.release_time_stamp like '{date}%') ";
                where += $" AND dd.allocation_obsolete=0 AND dd.storage_unit_id={unit} AND jd.jobstatus<>'ANULOWANA'";
                //ŁPA złe castowanie tu ma być int a nie string

                station[stationId].ReportWhere = where;
                scannerBarcodeRevieved("RS00000001", station[stationId].CradleId.ToString());

                station[stationId].ReportName = "Raport po dacie wysyłki i miejscu";
                station[stationId].ReportParam1 = $"Miejsce odkładcze: {unit}";
                station[stationId].ReportParam2 = $"Data: {date}";
            }
        }

        private void showReportTp(int stationId)
        {
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamDate);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamKp);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamKpLkp);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamZpl);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpParamUnit);
            station[stationId].TcRaportParam.TabPages.Remove(station[stationId].TpReportInfo);

            clearStationLabel(stationId);

            if (station[stationId].ReportType == REPORT_STROER)
            {
                station[stationId].TcRaportParam.TabPages.Add(station[stationId].TpParamDate);
                station[stationId].bReportPrint1.Visible = true;
            }
            else if (station[stationId].ReportType == REPORT_STROER_WARSZAWA)
            {
                station[stationId].TcRaportParam.TabPages.Add(station[stationId].TpParamDate);
                station[stationId].bReportPrint1.Visible = true;
            }
            else if (station[stationId].ReportType == REPORT_DATE)
            {
                station[stationId].TcRaportParam.TabPages.Add(station[stationId].TpParamDate);
                station[stationId].bReportPrint1.Visible = true;
            }
            else if (station[stationId].ReportType == REPORT_KP)
            {
                station[stationId].TcRaportParam.TabPages.Add(station[stationId].TpParamKp);
                station[stationId].bReportPrint2.Visible = true;
            }
            else if (station[stationId].ReportType == REPORT_KPLKP)
            {
                station[stationId].TcRaportParam.TabPages.Add(station[stationId].TpParamKpLkp);
                station[stationId].bReportPrint3.Visible = true;
            }
            else if (station[stationId].ReportType == REPORT_ZPL)
            {
                station[stationId].TcRaportParam.TabPages.Add(station[stationId].TpParamZpl);
                station[stationId].bReportPrint4.Visible = true;
            }
            else if (station[stationId].ReportType == REPORT_UNIT)
            {
                station[stationId].TcRaportParam.TabPages.Add(station[stationId].TpParamUnit);
            }
        }

        public void updateTbReportKp(string text, int stationId)
        {
            station[stationId].TbReportKp.Text = text;
        }

        public void updateTbReportKpLkp(string text, int stationId)
        {
            station[stationId].TbReportKpLkp.Text = text;
        }

        public void updateTbReportZpl(string text, int stationId)
        {
            station[stationId].TbReportZpl.Text = text;
        }

        public void updateTbReportUnit(string text, int stationId)
        {
            station[stationId].TbReportUnit.Text = text;
        }

        public void reportShow(int stationId)
        {
            crReport rep = new crReport();
            rep.SetDataSource(station[stationId].raportSet);
            rep.SetParameterValue("reportName", $"{station[stationId].ReportName}");
            rep.SetParameterValue("reportParam1", $"{station[stationId].ReportParam1}");
            rep.SetParameterValue("reportParam2", $"{station[stationId].ReportParam2}");

            rep.PrintOptions.PrinterName = Program.Settings.Data.reportPrinterName;
            rep.PrintToPrinter(1, false, 0, 0);

            //reportForm form = new reportForm();
            //form.crystalReportViewer1.ReportSource = rep;
            //form.ShowDialog();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            scannerBarcodeRevieved("XM10000080", station[1].CradleId.ToString());

            //reportShow(1);
            //printReport(1);
            /*
            DataTable dt = new DataTable();
            dt.Columns.Add("KP_LKP", typeof(string));
            dt.Columns.Add("Odbiorca", typeof(string));
            dt.Columns.Add("Spak", typeof(string));
            dt.Columns.Add("Typ", typeof(string));
            dt.Columns.Add("D_Pak", typeof(string));
            dt.Columns.Add("D_Alok", typeof(string));
            dt.Columns.Add("Kurier", typeof(string));
            dt.Columns.Add("Bryty", typeof(string));
            dt.Columns.Add("D_Wys", typeof(string));
            foreach (DataGridViewRow dgv in dgvReport1.Rows)
            {
                dt.Rows.Add(dgv.Cells[0].Value, dgv.Cells[1].Value, dgv.Cells[2], dgv.Cells[3], dgv.Cells[4], dgv.Cells[5], dgv.Cells[6], dgv.Cells[7], dgv.Cells[8]);
            }


            crReport rep = new crReport();

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            ds.WriteXmlSchema("reportSchema.xml");

            rep.SetDataSource(ds);

            reportForm form = new reportForm();
            form.crystalReportViewer1.ReportSource = rep;
            form.ShowDialog();
            */
        }

        private void label116_Click(object sender, EventArgs e)
        {

        }

        private void printReport(int stationId)
        {

        }

        private void bReportMake2_Click(object sender, EventArgs e)
        {
            int stationId = 2;

            if (station[stationId].ReportType == REPORT_STROER)
            {

            }
            else if (station[stationId].ReportType == REPORT_DATE)
            {

            }
            else if (station[stationId].ReportType == REPORT_KP)
            {
                if (!checkKp(station[stationId].TbReportKp.Text))
                {
                    station[stationId].LReportKpError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_KPLKP)
            {
                if (!checkKpLkp(station[stationId].TbReportKpLkp.Text))
                {
                    station[stationId].LReportKpLkpError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_ZPL)
            {
                if (!checkZpl(station[stationId].TbReportZpl.Text))
                {
                    station[stationId].LReportZplError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_UNIT)
            {
                if (!checkUnit(station[stationId].TbReportUnit.Text))
                {
                    station[stationId].LReportUnitError.Visible = true;
                    return;
                }
            }
            reportMade(stationId);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            reportPageUpdate(1);
        }

        private void bReportPrint12_Click(object sender, EventArgs e)
        {
            scannerBarcodeRevieved("XM10000080", station[2].CradleId.ToString());

            //reportShow(2);
            //printReport(2);
        }

        private void label111_Click(object sender, EventArgs e)
        {

        }

        private void bReportDate2_Click(object sender, EventArgs e)
        {
            station[2].ModeAct = MODE_REPORT;
            station[2].ReportType = REPORT_DATE;
            showReportTp(2);
        }

        private void bReportStroer2_Click(object sender, EventArgs e)
        {
            station[2].ModeAct = MODE_REPORT;
            station[2].ReportType = REPORT_STROER;
            showReportTp(2);
        }

        private void bReportStroerWaw2_Click(object sender, EventArgs e)
        {
            station[2].ModeAct = MODE_REPORT;
            station[2].ReportType = REPORT_STROER_WARSZAWA;
            showReportTp(2);
        }

        private void bReportKp2_Click(object sender, EventArgs e)
        {
            station[2].ModeAct = MODE_REPORT;
            station[2].ReportType = REPORT_KP;
            showReportTp(2);
        }

        private void bReportKpLkp2_Click(object sender, EventArgs e)
        {
            station[2].ModeAct = MODE_REPORT;
            station[2].ReportType = REPORT_KPLKP;
            showReportTp(2);
        }

        private void bReportZpl2_Click(object sender, EventArgs e)
        {
            station[2].ModeAct = MODE_REPORT;
            station[2].ReportType = REPORT_ZPL;
            showReportTp(2);
        }

        private void bReportUnit2_Click(object sender, EventArgs e)
        {
            station[2].ModeAct = MODE_REPORT;
            station[2].ReportType = REPORT_UNIT;
            showReportTp(2);
        }

        private void bReportMake12_Click(object sender, EventArgs e)
        {
            int stationId = 2;

            //Uruchamiam tlyko dt testow raportu bez skanera ale na drugiej stacji nei dziala bo nei sutawia zmienne stationid w warehouseoperations
            //station[stationId].ReportType = REPORT_DATE;
            //station[stationId].ModeAct = MODE_REPORT;

            if (station[stationId].ReportType == REPORT_STROER)
            {
                if (stationId == 1)
                {
                    station[stationId].NotPacked1 = dtpReportNP1.Checked ? 1 : 0;
                    station[stationId].NotSent1 = dtpReportNS1.Checked ? 1 : 0;
                    station[stationId].Courier1 = Courier1;
                }
                else
                {
                    station[stationId].NotPacked2 = dtpReportNP2.Checked ? 1 : 0;
                    station[stationId].NotSent2 = dtpReportNS2.Checked ? 1 : 0;
                    station[stationId].Courier2 = Courier2;
                }
            }
            else if (station[stationId].ReportType == REPORT_DATE)
            {
                if (stationId == 1)
                {
                    station[stationId].NotPacked1 = dtpReportNP1.Checked ? 1 : 0;
                    station[stationId].NotSent1 = dtpReportNS1.Checked ? 1 : 0;
                    station[stationId].Courier1 = Courier1;
                }
                else
                {
                    station[stationId].NotPacked2 = dtpReportNP2.Checked ? 1 : 0;
                    station[stationId].NotSent2 = dtpReportNS2.Checked ? 1 : 0;
                    station[stationId].Courier2 = Courier2;
                }
            }
            else if (station[stationId].ReportType == REPORT_STROER_WARSZAWA)
            {
                if (stationId == 1)
                {
                    station[stationId].NotPacked1 = dtpReportNP1.Checked ? 1 : 0;
                    station[stationId].NotSent1 = dtpReportNS1.Checked ? 1 : 0;
                    station[stationId].Courier1 = Courier1;
                }
                else
                {
                    station[stationId].NotPacked2 = dtpReportNP2.Checked ? 1 : 0;
                    station[stationId].NotSent2 = dtpReportNS2.Checked ? 1 : 0;
                    station[stationId].Courier2 = Courier2;
                }
            }
            else if (station[stationId].ReportType == REPORT_KP)
            {
                if (!checkKp(station[stationId].TbReportKp.Text))
                {
                    station[stationId].LReportKpError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_KPLKP)
            {
                if (!checkKpLkp(station[stationId].TbReportKpLkp.Text))
                {
                    station[stationId].LReportKpLkpError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_ZPL)
            {
                if (!checkZpl(station[stationId].TbReportZpl.Text))
                {
                    station[stationId].LReportZplError.Visible = true;
                    return;
                }
            }
            else if (station[stationId].ReportType == REPORT_UNIT)
            {
                if (!checkUnit(station[stationId].TbReportUnit.Text))
                {
                    station[stationId].LReportUnitError.Visible = true;
                    return;
                }
            }
            reportMade(2);
        }

        private void tbReportUnit1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvReport1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvReport1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            // Update the balance column whenever te value of any cell changes.
            if (e.RowIndex != -1 && (e.ColumnIndex == 16))
            {
                updateReason(1,e.RowIndex);
            }
        }

        private void dgvReport2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            // Update the balance column whenever te value of any cell changes.
            if (e.RowIndex != -1 && (e.ColumnIndex == 16))
            {
                updateReason(2,e.RowIndex);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }
    }
}
