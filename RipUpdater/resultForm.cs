﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RipUpdater
{
    public partial class resultForm : Form
    {
        public bgwParameters parameters = new bgwParameters();

        public resultForm()
        {
            InitializeComponent();
        }

        private void resultForm_Load(object sender, EventArgs e)
        {
            bgwCopy.RunWorkerAsync(parameters);
        }

        //disable form from resize and move
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void bgwCopy_DoWork(object sender, DoWorkEventArgs e)
        {
            bgwParameters par = e.Argument as bgwParameters;

            string destPath = par.sourcePath + "\\" + par.newDirectory;
            System.IO.Directory.CreateDirectory(destPath);

            int i = 0;
            foreach (StringValue s in par.fileList)
            {
                string fileName = Path.GetFileName(s.Value);
                string filePath = Path.GetDirectoryName(s.Value);

                string newFileName = par.newDirectory + fileName;
                string newFilePath = filePath + "\\" + par.newDirectory;
                File.Copy(s.Value, newFilePath + "\\" + newFileName);

                i++;
                bgwCopy.ReportProgress(i);
                //Thread.Sleep(100);

                if (bgwCopy.CancellationPending)
                {
                    // Set the e.Cancel flag so that the WorkerCompleted event
                    // knows that the process was cancelled.
                    e.Cancel = true;
                    bgwCopy.ReportProgress(0);
                    return;
                }
            }

        }

        private void bgwCopy_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lCopyProgess.Text = $"Skopiowano {e.ProgressPercentage} z {parameters.fileList.Count}";
        }

        private void bgwCopy_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bDone.Text = "OK";
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            if (bgwCopy.IsBusy)
            {
                bgwCopy.CancelAsync();
            }
            else
            {
                this.Close();
            }
        }
    }
}
