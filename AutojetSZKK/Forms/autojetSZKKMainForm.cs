﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using PcSoftUtilities;
using System.Timers;
using System.Collections.Concurrent;

//Intrex Libraries
using System.IO;
using System.Net.Sockets;

namespace AutojetSZKK
{
    public partial class autojetSZKKMainForm : Form
    {
        static int rack = 0;
        static int slot = 1;
        static bool plcConnected = false;
        public static int firstTimeConnected = 0;
        private int pCounter = 0;
        private int labelCount = 3;

        public HealthMonitor Hm;
        public PcSoftUtilities.dominoPrinter domino;

        static libnodave.daveOSserialType fds;
        static libnodave.daveInterface di;
        static libnodave.daveConnection dc;

        //printer2
        //public List<Byte[]> dominoBlackOutBuffor2 = new List<Byte[]>();
        public BlockingCollection<Byte[]> printerQueue = new BlockingCollection<Byte[]>(); //BlockingCollection
        Thread dominoBufforSendThread2;
        Thread pingPlcThread;

        public System.Timers.Timer tPrinterResponse;
        //public System.Timers.Timer tPrinterRenewConnection2;     //send 

        //printers tcp server, 2017-07-18
        //public TcpStock dominoPrinterTcp2 = new TcpStock();

        public bool plcPingOk = false;  //
        public bool plcPrintOn = false; //true=trwa druk
        public int plcErrorCode = 0;    //kod bledu plc

        public bool workAutomat = false;    //true = praca w automace
        public int actPrintNum = 0;         //
        public bool settingsVisible = false;

        public bool dominoPrinter2LastLabelEmpty = false;

        public class labelData : IEquatable<labelData>
        {
            public int kpracy { get; set; }
            public int lkpracy { get; set; }
            public string bryt { get; set; }
            public int serial { get; set; }
            public int brytStartPos { get; set; }
            public int brytEndPos { get; set; }
            public int brytPrintPos { get; set; }
            public int printerNum { get; set; }

            public bool isUpdated { get; set; }

            //update
            public int id { get; set; }

            public labelData()
            {
                this.kpracy = 0;
                this.lkpracy = 0;
                this.bryt = "";
                this.serial = 0;
                this.brytStartPos = 0;
                this.brytEndPos = 0;
                this.brytPrintPos = 0;
                this.printerNum = 0;

                this.id = 0;
                this.isUpdated = false;
            }

            public labelData(int _kpracy, int _lkpracy, string _bryt, int _serial, int _brytStartPos, int _brytEndPos, int _brytPrintPos, int _printerNum, int _id)
            {
                this.kpracy = _kpracy;
                this.lkpracy = _lkpracy;
                this.bryt = _bryt;
                this.serial = _serial;
                this.brytStartPos = _brytStartPos;
                this.brytEndPos = _brytEndPos;
                this.brytPrintPos = _brytPrintPos;
                this.printerNum = _printerNum;
                this.isUpdated = false;
                this.id = _id;
            }

            public bool Equals(labelData other)
            {
                if (other == null) return false;

                if ((this.kpracy == other.kpracy) && (this.lkpracy == other.lkpracy) && (String.Equals(this.bryt, other.bryt)) && (this.serial == other.serial) && (this.brytStartPos == other.brytStartPos) && (this.brytEndPos == other.brytEndPos) && (this.brytPrintPos == this.brytPrintPos) && (this.printerNum == other.printerNum) && (this.id == other.id))
                    return true;
                else
                    return false;
            }
        }

        List<labelData> labelDataList = new List<labelData>();
        DataTable dtlabelData;

        int kpracy = 1;
        int lkpracy = 1;
        int bryt = 1;
        int serial = 1;
        int printerNum = 1;

        //Printer Variables like in Intrex
        //public int labelsSent=0;
        //public int labelsConfirmed=0;
        //public int labelsPrinted=0;
        //public int labelsPrintingSent = 0;

        public ASeriesCodenet printerCodenet;
        Boolean isPrinterRespone;
        delegate void PrinterDataReceivedCallback(object oSender, EventArgs oEventArgs);
        delegate void UpdatePrintedCodeCallback();
        Boolean czyszczenieKolejkiDanych; //w momecnie przejścia do następnego kafla i czyszczenia kolejki danych ustawiany na True, po odpowiedzi z drukarki #06 na False. Potrzebne aby przy czyszczeniu nie brać potwierdzenia za przyjęcie danych
        public Boolean printerReceivedData = true; // flaga sprawdzająca czy drukarka odebrała kod
        public string lastSentCode; //ostatni wysłany kod do drukarki


        public autojetSZKKMainForm()
        {
            InitializeComponent();
        }

        private void autojetSZKKMain_Load(object sender, EventArgs e)
        {
            initForm();

            pingPlcThread = new Thread(new ThreadStart(utilities.pingPlcMain));
            pingPlcThread.Start();

            //timer init
            //tPrinterAnswerRecError2 = new System.Timers.Timer(3000);
            //tPrinterAnswerRecError2.Elapsed += new System.Timers.ElapsedEventHandler(tPrinterAnswerRecError2_Tick);
            //tPrinterAnswerRecError2.SynchronizingObject = this; // this to formatka

            //tPrinterRenewConnection1.Start();     //2018-07-16

            //tPrinterRenewConnection2 = new System.Timers.Timer(30000);
            //tPrinterRenewConnection2.Elapsed += new System.Timers.ElapsedEventHandler(tPrinterRenewConnection2_Tick);
            //tPrinterRenewConnection2.SynchronizingObject = this; // this to formatka
            //tPrinterRenewConnection2.Start();     //2018-07-16

            //tConnection_Tick(sender, e);
            printerCodenet = new ASeriesCodenet(Program.Settings.Data.dominoPrinterIP2);
            Hm.Info("Codenet Printer Object Created {0}", Program.Settings.Data.dominoPrinterIP2);
            printerCodenet.DataReceived += CodenetPrinterReceived;
        }

        public void showError()
        {
            if (labelDataList.Count > 0)
            {
                lError1.Text = String.Format("Wystąpił błąd. Praca jest kontynuowana. jdnr={0}, jdline={1}, tilecounter={2}", labelDataList[0].kpracy, labelDataList[0].lkpracy.ToString(), labelDataList[0].lkpracy.ToString());
                Program.autojetSZKKMain.Hm.Info("ERROR: 0x01. Praca jest kontynuowana. jdnr={0}, jdline={1}, tilecounter={2}", labelDataList[0].kpracy, labelDataList[0].lkpracy.ToString(), labelDataList[0].lkpracy.ToString());
                lError1.Visible = true;
                lError2.Visible = true;
                bErrorAck.Visible = true;
            }
            {
                lError1.Text = String.Format("Wystąpił błąd. labelDataList.Count=0");
                Program.autojetSZKKMain.Hm.Info("ERROR: 0x02. labelDataList.Count=0");
                lError1.Visible = true;
                lError2.Visible = true;
                bErrorAck.Visible = true;
            }
        }

        /*public void dominoDataRecieved2(string recStr)
        {
            //stop timer, recived message
            //tPrinterAnswerRecError2.Stop();
            Program.autojetSZKKMain.Hm.Warning($"dominoDataRecieved2, tPrinterAnswerRecError2.Stop()");

            if (labelDataList.Count > actPrintNum)
            {
                Program.autojetSZKKMain.Hm.Info("Odpowiedz od drukarki 2, actPrintNum={0}, labelDataList.Count={1}, plcPrintOn={2}, labelDataList[actPrintNum].printerNum={3}", actPrintNum.ToString(), labelDataList.Count.ToString(), plcPrintOn.ToString(), labelDataList[actPrintNum].printerNum.ToString());
            }
            else
            {
                Program.autojetSZKKMain.Hm.Info("ERROR: 0x12 ");
                Program.autojetSZKKMain.Hm.Info("Odpowiedz od drukarki 2, actPrintNum={0}, labelDataList.Count={1}, plcPrintOn={2}", actPrintNum.ToString(), labelDataList.Count.ToString(), plcPrintOn.ToString());
            
                actPrintNum = 0;
                updateVisu();
                //dominoBlackWaitForAnswer2.Set(); //= false;

                return;
            }

            tbDominoOut.Text += recStr;

            //allow to send new data to printer
            //dominoBlackWaitForAnswer2.Set();  // = false;

            if (workAutomat)
            {
                if (recStr == "A")
                {
                    if (labelDataList.Count > 0)
                    {
                        if (labelDataList[actPrintNum].printerNum == 2)
                        {
                            if (labelDataList.Count > actPrintNum + 1)
                            {
                                actPrintNum++;
                                updateVisu();
                                printerSendLabel("dominoSerial2Recieved");
                            }
                            else
                            {
                                actPrintNum = 0;
                                Program.autojetSZKKMain.Hm.Info("Odpowiedz od drukarki 2, actPrintNum = 0");
                            }
                        }
                    }
                }
            }
        }*/

        public void initForm()
        {
            this.Text = utilities.setWindowTitle();
            utilities.logAppStart();

            this.Hm = new HealthMonitor(this.debugTestBox, Program.Ft.Get(FileTree.ID.FT_DEBUG_DMP));
            Hm.Info("Started! autojetASZZ Version: {0}. ", utilities.GetProgramVersion());

            domino = new PcSoftUtilities.dominoPrinter();

            tbLabelLenghtMm.Text = Program.Settings.Data.labelLenghtMm.ToString();
            tbLabelRepeatPitch.Text = Program.Settings.Data.labelRepeatPitch.ToString();
            cbDbAutoUpade.Checked = Program.Settings.Data.dbAutoUpade;

            statusStripePLC.Text = "PLC: nie połączono";
            statusStripePLC.ForeColor = Color.Red;
            statusStripePLC.Image = Properties.Resources.no;

            tabControlMain.TabPages.Remove(tabPageMain);
            tabControlMain.TabPages.Remove(tabPageData);
            tabControlMain.TabPages.Remove(tabPageSetup);
            tabControlMain.TabPages.Remove(tabPageDebug);

            settingsVisible = false;
            settingsVis(settingsVisible);
        }

        public void settingsVis(bool vis)
        {
            llabelLenghtMm.Visible = vis;
            tbLabelLenghtMm.Visible = vis;
            tbLabelLenghtUnit.Visible = vis;
            tbLabel1RepeatPitch.Visible = vis;
            tbLabel2RepeatPitch.Visible = vis;
            tbLabelRepeatPitch.Visible = vis;
            tbLabelRepeatPitchUnit.Visible = vis;
            cbDbAutoUpade.Visible = vis;
            bControlSettingsSave.Visible = vis;
        }


        private void bDominoSwGo_Click(object sender, EventArgs e)
        {
            byte[] toSend = Program.autojetSZKKMain.domino.printerSwGo();
            //byte[] toSend = Program.autojatSZKKMain.domino.printerIdentity();
            //Program.autojatSZKKMain.dominoBlackOutBuffor.Add(toSend);

            //dominoPrinterSerial.send(toSend);
            this.dominoPrinterSerialPort1.Write(toSend, 0, toSend.Length);
        }

        /*private void button1_Click(object sender, EventArgs e)
        {
            SQLConnection.Connect(OnConnectionStateChange);
        }*/

        private void OnConnectionStateChange(object sender, StateChangeEventArgs args)
        {
            switch (args.CurrentState)
            {
                case ConnectionState.Open:
                    {
                        this.statusStripeDb.Text = "Baza danych: połączono";
                        this.statusStripeDb.ForeColor = Color.Green;
                        this.statusStripeDb.Image = Properties.Resources.yes;
                        break;
                    }
                default:
                    {
                        this.statusStripeDb.Text = "Baza danych: nie połączono";
                        this.statusStripeDb.ForeColor = Color.Red;
                        this.statusStripeDb.Image = Properties.Resources.no;
                        break;
                    }
            }
        }

        /*private void bSqlSelectAll_Click(object sender, EventArgs e)
        {
            if (SQLConnection.IsActive())        
            {
                DateTime start = DateTime.Now;
                string queryBox = string.Format("SELECT * FROM tjdata.Printtemp");
                DataTable pdtBoxTable = SQLConnection.GetData(queryBox);
                dgvMainData.DataSource = pdtBoxTable;
                TimeSpan lap = DateTime.Now - start;
                lTimeSpan.Text = lap.ToString();
            }
            
        }*/

        private void bSiemensConnect_Click(object sender, EventArgs e)
        {
            connectPLC();
        }

        private void bPLCRead_Click(object sender, EventArgs e)
        {
            int res, a = 0, b = 0, c = 0;
            int d = 0;

            //res = dc.

            res = dc.readBytes(libnodave.daveFlags, 1, 10, 2, null);
            if (res == 0)
            {
                a = dc.getS8();
                b = dc.getS8();
                lVal0.Text = a.ToString();
                lVal1.Text = b.ToString();
                lVal2.Text = c.ToString();
                lVal3.Text = d.ToString();
            }
            else
            {
                Console.WriteLine("error " + res + " " + libnodave.daveStrerror(res));
                Program.autojetSZKKMain.Hm.Info("error " + res + " " + libnodave.daveStrerror(res));
            }
        }

        private void bPLCWrite_Click(object sender, EventArgs e)
        {
            int a = int.Parse(tbVal0.Text.ToString());
            a = libnodave.daveSwapIed_16(a);
            dc.writeBytes(libnodave.daveFlags, 1, 0, 2, BitConverter.GetBytes(a));

            int b = int.Parse(tbVal1.Text.ToString());
            b = libnodave.daveSwapIed_16(b);
            dc.writeBytes(libnodave.daveFlags, 1, 2, 2, BitConverter.GetBytes(b));

            int c = int.Parse(tbVal2.Text.ToString());
            c = libnodave.daveSwapIed_16(c);
            dc.writeBytes(libnodave.daveFlags, 1, 4, 2, BitConverter.GetBytes(c));

            int d = int.Parse(tbVal3.Text.ToString());
            d = libnodave.daveSwapIed_16(d);
            dc.writeBytes(libnodave.daveFlags, 1, 6, 2, BitConverter.GetBytes(d));

            int d2 = int.Parse(tbVal4.Text.ToString());
            d2 = libnodave.daveSwapIed_16(d2);
            dc.writeBytes(libnodave.daveFlags, 1, 8, 2, BitConverter.GetBytes(d2));

            int f = int.Parse(tbVal5.Text.ToString());
            f = libnodave.daveSwapIed_16(f);
            dc.writeBytes(libnodave.daveFlags, 1, 10, 2, BitConverter.GetBytes(f));

            int g = int.Parse(tbVal6.Text.ToString());
            g = libnodave.daveSwapIed_16(g);
            dc.writeBytes(libnodave.daveFlags, 1, 12, 2, BitConverter.GetBytes(g));

            int h = int.Parse(tbVal7.Text.ToString());
            h = libnodave.daveSwapIed_16(h);
            dc.writeBytes(libnodave.daveFlags, 1, 14, 2, BitConverter.GetBytes(h));

            int h2 = int.Parse(tbVal8.Text.ToString());
            h2 = libnodave.daveSwapIed_16(h2);
            dc.writeBytes(libnodave.daveFlags, 1, 16, 2, BitConverter.GetBytes(h2));

            int h3 = int.Parse(tbVal9.Text.ToString());
            h3 = libnodave.daveSwapIed_16(h3);
            dc.writeBytes(libnodave.daveFlags, 1, 18, 2, BitConverter.GetBytes(h3));

            int h4 = int.Parse(tbVal10.Text.ToString());
            h4 = libnodave.daveSwapIed_16(h4);
            dc.writeBytes(libnodave.daveFlags, 1, 20, 2, BitConverter.GetBytes(h4));

            int h5 = int.Parse(tbVal11.Text.ToString());
            h5 = libnodave.daveSwapIed_16(h5);
            dc.writeBytes(libnodave.daveFlags, 1, 22, 2, BitConverter.GetBytes(h5));
        }

        //private void bSelActData_Click(object sender, EventArgs e)
        //{
        /*
        jdnr - numer karty pracy
        jdline - numer linii pracy
        tiles - numer  brytu w formacie RxxCyy, przed drukiem przekonwertować do xxyy
        machine - drukarka, TJ1 - 1, TJ2 - 2, TJ3 - 3
        tilecounter - indywidualny numer
         * 
        SELECT * FROM Printtemp
        WHERE isprinted=0 and machine='TJ3'
        ORDER BY sheetcounter, sheetpos DESC
        */

        /*    if (SQLConnection.IsActive())
            {

                string queryBox = string.Format("SELECT * FROM tjdata.Printtemp WHERE isprinted=0 ORDER BY sheetcounter, sheetpos DESC");
                DataTable pdtActDataTable = SQLConnection.GetData(queryBox);

                kpracy = int.Parse(pdtActDataTable.Rows[0]["jdnr"].ToString());
                lkpracy = int.Parse(pdtActDataTable.Rows[0]["jdline"].ToString());
                bryt = int.Parse(pdtActDataTable.Rows[0]["tileno"].ToString());
                serial = int.Parse(pdtActDataTable.Rows[0]["tilecounter"].ToString());
                printerNum = 1;

                kpracyLabel.Text = kpracy.ToString();
                lkpracyLabel.Text = lkpracy.ToString();
                brytLabel.Text = bryt.ToString();
                serialLabel.Text = serial.ToString();

            }

        }*/

        private void bDominoRepeat_Click(object sender, EventArgs e)
        {
            int labelCount = Int32.Parse(tbLabelCount.Text.ToString());

            //byte[] toSend = Program.autojetSZKKMain.domino.setRepeat(labelCount);

            //this.dominoPrinterSerialPort.Write(toSend, 0, toSend.Length);
        }

        public bool dominoPrinterTcp2Old = false;

        private void tConnection_Tick(object sender, EventArgs e)
        {
            if (!SQLConnection.IsActive())
            {
                if (SQLConnection.Connect(OnConnectionStateChange))
                {
                    Hm.Info("DB Connected");
                }
                else
                {
                    Hm.Info("DB Cannot Connect");
                };
            }

            if ((plcPingOk) && (!plcConnected))
            {
                connectPLC();
                Hm.Info("PLC Connected");
            }

            if (firstTimeConnected == 0)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Program.Settings.Data.dominoPrinterIP2))
                    {
                        dominoBufforSendThread2 = new Thread(new ThreadStart(dominoQueueThread.main2));
                        dominoBufforSendThread2.Start();

                        //printerCodenet.DataReceived += CodenetPrinterReceived; - nic nie wysyłaj narazie
                        TcpClient intercomm = new TcpClient();
                        intercomm.Connect(Program.Settings.Data.dominoPrinterIP2, 703);
                        String str = "\x07" + "\x01" + "\x01" + "\x2C";
                        Stream stm = intercomm.GetStream();
                        ASCIIEncoding asen = new ASCIIEncoding();
                        byte[] ba = asen.GetBytes(str);
                        stm.Write(ba, 0, ba.Length);
                        intercomm.Close();

                        Program.autojetSZKKMain.Hm.Info("Connection Reset Done od port 703");
                        Thread.Sleep(1000);

                        printerCodenet.Connect();

                        Thread.Sleep(1000);

                        //Start main Thread
                        if (printerCodenet.connected)
                        {
                            Hm.Info("Printer First time Connected");
                            printerConnected();

                            //ustawienie wysyłania P jako potwierdzenia wydruku
                            Program.autojetSZKKMain.printerCodenet.SendData("\x1B" + "I1P" + "\x04");

                            Thread.Sleep(500);

                            //Krotność
                            byte[] tileRepeatCountToSend = Program.autojetSZKKMain.domino.setRepeat(2, Program.Settings.Data.labelRepeatPitch);
                            Program.autojetSZKKMain.printerCodenet.SendData(tileRepeatCountToSend);

                            Thread.Sleep(500);

                            firstTimeConnected = 1;
                        }
                        else
                        {
                            Hm.Info("Cannot Connect to Printer - No IP on first Start IP {0}", printerCodenet.IpAddress);
                            printerDisconnected();
                        }
                    }
                    else
                    {
                        Hm.Info("Cannot Connect to Printer - No IP");
                        printerDisconnected();
                    }
                }
                catch (Exception ex)
                {
                    Hm.Info("Cannot reset connections on port 703 {0}", ex.ToString());
                    printerDisconnected();
                }
            }
        }

        public void printerConnected()
        {
            try
            {
                statusStripePrinter2.Text = "Drukarka 2 TCP: połączona";
                statusStripePrinter2.ForeColor = Color.Green;
                statusStripePrinter2.Image = Properties.Resources.yes;
            }
            catch (Exception ex)
            {

            }
        }

        public void printerDisconnected()
        {
            try
            {
                statusStripePrinter2.Text = "Drukarka 2 TCP: nie połączona";
                statusStripePrinter2.ForeColor = Color.Red;
                statusStripePrinter2.Image = Properties.Resources.no;
            }
            catch (Exception ex)
            {

            }
        }

        private void bControlSettingsSave_Click(object sender, EventArgs e)
        {
            try
            {
                int labelLenghtMm = Int32.Parse(tbLabelLenghtMm.Text);
                int labelRepeatPitch = Int32.Parse(tbLabelRepeatPitch.Text);

                Program.Settings.Data.labelLenghtMm = labelLenghtMm;
                Program.Settings.Data.labelRepeatPitch = labelRepeatPitch;
                Program.Settings.Data.dbAutoUpade = cbDbAutoUpade.Checked;
            }
            catch (Exception ex)
            {
                HealthMonitor.ErrorDlg("Wprowadzono niepoprawne dane", "BŁĄD");

                tbLabelLenghtMm.Text = Program.Settings.Data.labelLenghtMm.ToString();
                tbLabelRepeatPitch.Text = Program.Settings.Data.labelRepeatPitch.ToString();
                cbDbAutoUpade.Checked = Program.Settings.Data.dbAutoUpade;

                return;
            }
        }

        private void autojetSZKKMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.Settings.Save();

            tDbRefresh.Enabled = false;
            tConnection.Enabled = false;

            //czyszczenie kolejki
            //printerCodenet.SendData("\x1B" + "OE00000" + "\x04");

            Thread.Sleep(2000);

            if (dominoBufforSendThread2 != null)
            {
                dominoQueueThread.isEnabled = false;
                Program.autojetSZKKMain.Hm.Info("Domino Thread Disabled");
            }

            if (pingPlcThread != null)
            {
                utilities.isEnabled = false;
                Program.autojetSZKKMain.Hm.Info("PingPLC Thread Disabled");
            }

            Thread.Sleep(1500);

            try
            {
                if (printerCodenet.connected)
                    printerCodenet.Disconnect();
            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info("ERROR: BŁĄD PRZY ZAMYKANIU APLIKACJI {0}", ex.ToString());
            }

            Thread.Sleep(1500);

            TcpClient intercomm = new TcpClient();
            intercomm.Connect(Program.Settings.Data.dominoPrinterIP2, 703);
            String str = "\x07" + "\x01" + "\x01" + "\x2C";
            Stream stm = intercomm.GetStream();
            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] ba = asen.GetBytes(str);
            stm.Write(ba, 0, ba.Length);
            intercomm.Close();

            Program.autojetSZKKMain.Hm.Info("Connection Reset Done od port 703");
            Thread.Sleep(1500);
        }


        private void bDominoInit_Click(object sender, EventArgs e)
        {
            byte[] toSend = Program.autojetSZKKMain.domino.printerInit();
            //Program.autojatSZKKMain.dominoBlackOutBuffor.Add(toSend);

            //dominoPrinterSerial.send(toSend);
            this.dominoPrinterSerialPort1.Write(toSend, 0, toSend.Length);
        }

        private void bPingTest_Click(object sender, EventArgs e)
        {
            lPingRes.Text = utilities.PingHost("192.168.0.1").ToString();
        }

        public void ubdateUIFromThreat()
        {
            Program.autojetSZKKMain.lPingRes.Text = plcPingOk.ToString();
        }

        private void bStartAutomat_Click(object sender, EventArgs e)
        {
            workAutomatOnOff();
        }

        private void tDbRefresh_Tick(object sender, EventArgs e)
        {
            workAutomatDbRefresch();
        }

        private void tPlcRead_Tick(object sender, EventArgs e)
        {
            plcRead();
        }

        private void tUbdateVisu_Tick(object sender, EventArgs e)
        {
            updateVisu();
        }

        private void ustawieniaDrukuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settingsVisible = !settingsVisible;
            settingsVis(settingsVisible);
            ustawieniaDrukuToolStripMenuItem.Checked = settingsVisible;
        }

        private void tUpdateCpntrolVisu_Tick(object sender, EventArgs e)
        {
            updateControlVisu();
        }

        private void bControlDataUpdate_Click(object sender, EventArgs e)
        {
            workAutomatDbRefresch();
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AutojetAboutBox aboutBox = new AutojetAboutBox();
            aboutBox.Show();
        }

        private void bErrorAck_Click(object sender, EventArgs e)
        {
            lError1.Visible = false;
            lError2.Visible = false;
            bErrorAck.Visible = false;
        }

        bool debugMode = false;

        private void debugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!debugMode)
            {
                tabControlMain.TabPages.Add(tabPageDebug);
                debugMode = !debugMode;
            }
            else
            {
                tabControlMain.TabPages.Remove(tabPageDebug);
                debugMode = !debugMode;
            }
        }


        private void bCheckConnection_Click(object sender, EventArgs e)
        {
            // Program.autojetSZKKMain.dominoBlackWaitForAnswer2.Set();    // = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Program.autojetSZKKMain.dominoBlackOutBuffor2 = new List<byte[]>();
            //Program.autojetSZKKMain.dominoBlackOutBuffor2 = new BlockingCollection<byte[]>();
        }

        private void bTestF1_Click(object sender, EventArgs e)
        {
            //if (dominoPrinterTcp2.Connected() || dominoPrinterTcp2.Connecting())
            //{
            //    dominoPrinterTcp2.Disconnect();
            //}
        }

        //System.Timers.Timer timer;
        private void bTestF2_Click(object sender, EventArgs e)
        {
            //Program.autojetSZKKMain.tPrinterAnswerRecError2.Enabled = true;
            //Program.autojetSZKKMain.timer1.Enabled = true;
            //timer = new System.Timers.Timer(1000);
            //timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            //timer.SynchronizingObject = this; // this to formatka
            //timer.Start();
            addConnectionInfoMessage("test...");
            //rtbConnectionInfo.Text.Insert(0, DateTime.UtcNow + ": " + "\n");
            //rtbConnectionInfo.AppendText(Environment.NewLine + DateTime.Today + " Hello");
        }

        public void timer_Elapsed(Object source, ElapsedEventArgs e)
        {
            //Program.autojetSZKKMain.Hm.Warning($"timer_Elapsed");
            //timer.Stop();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Program.autojetSZKKMain.tPrinterAnswerRecError2.Interval = int.Parse(tbIntervalTest.Text);
            //addConnectionInfoMessage($"tPrinterAnswerRecError2.Interval = {tPrinterAnswerRecError2.Interval.ToString()}");
        }

        private void bTcpConnected_Click(object sender, EventArgs e)
        {
            //tcpConnected.Checked = dominoPrinterTcp2.Connected();
        }

        private void tUpdateMachineStatus_Tick(object sender, EventArgs e)
        {
            //Program.autojetSZKKMain.Hm.Info("tUpdateMachineStatus_Tick(): Started...");
            if (SQLConnection.IsActive())
            {
                string queryUpdate = string.Format($"update machine_status set bcodeprinter={printerCodenet.connected}, plc={plcConnected}, date=NOW() WHERE machine='{Program.Settings.Data.machineNo}'");
                SQLConnection.UpdateData(queryUpdate);
            }
        }

        /// <summary>
        /// Obsługa danych otrzymanych z drukarki
        /// </summary>
        /// <param name="oSender"></param>
        /// <param name="oEventArgs"></param>
        public void CodenetPrinterReceived(object oSender, EventArgs oEventArgs)
        {
            if (this.InvokeRequired)
            {
                PrinterDataReceivedCallback d = new PrinterDataReceivedCallback(CodenetPrinterReceived);
                this.Invoke(d, new object[] { oSender, oEventArgs });
            }
            else
            {
                isPrinterRespone = true;
                ReceivedArgs oReceivedArgs = oEventArgs as ReceivedArgs;
                string tempReceivedText = oReceivedArgs.receivedText;
                // Informacja z drukarki o przyjęciu danych
                if (tempReceivedText.Contains("\x06"))
                {
                    Hm.Info("Printer Accepted Sent Data waitResponse=false {0}", tempReceivedText);
                    dominoQueueThread.waitPrinterResponse = false;
                    dominoQueueThread.printerACK.Enabled = false;
                    //Program.autojetSZKKMain.labelsConfirmed++;
                }

                // Informacja print-done z drukarki
                if (tempReceivedText.Contains("P"))
                {
                    Hm.Info("Printer Confirmed printing {0}", tempReceivedText);
                    //Program.autojetSZKKMain.labelsPrinted++;

                    pCounter++;
                    Program.autojetSZKKMain.Hm.Info("pCounter = {0}", pCounter.ToString());

                    if (pCounter % labelCount == 0)
                    {
                        //Try to send a label for next tile
                        //wysylanie nowego kodu do kolejki (zawsze z actPrintNum==0) i powinienem tylko jeden a dopiero jak P dostanę z drukarki to kolejny
                        actPrintNum++;
                        Program.autojetSZKKMain.Hm.Info("Next Label sent to Queue: actPrintNum={0}, labelDataList.Count={1} ", actPrintNum, labelDataList.Count);
                        Thread.Sleep(4000);
                        printerSendLabel("workAutomatDbRefresch");
                    }
                }

                //Hm.Info("L. Sent: {0} L. Confirmed: {1} L. Printing Sent {2} L. Printed {3}", labelsSent.ToString(), labelsConfirmed.ToString(), labelsPrintingSent.ToString(), labelsPrinted.ToString());
            }
        }
    }
}
