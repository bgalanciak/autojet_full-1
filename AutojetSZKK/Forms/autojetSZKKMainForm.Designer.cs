﻿namespace AutojetSZKK
{
    partial class autojetSZKKMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(autojetSZKKMainForm));
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.ustawieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ustawieniaDrukuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.statusStripePrinter2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStripeDb = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStripePLC = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStripeTimer = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageMain = new System.Windows.Forms.TabPage();
            this.lPingRes = new System.Windows.Forms.Label();
            this.bPingTest = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.tbVal11 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tbVal10 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbVal9 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbVal8 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbVal7 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbVal6 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbVal5 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbVal4 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbVal3 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbVal2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbVal1 = new System.Windows.Forms.TextBox();
            this.tbVal0 = new System.Windows.Forms.TextBox();
            this.bPLCWrite = new System.Windows.Forms.Button();
            this.lVal3 = new System.Windows.Forms.Label();
            this.lVal2 = new System.Windows.Forms.Label();
            this.lVal1 = new System.Windows.Forms.Label();
            this.lVal0 = new System.Windows.Forms.Label();
            this.bPLCRead = new System.Windows.Forms.Button();
            this.lPLCStatus = new System.Windows.Forms.Label();
            this.bSiemensConnect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bDominoRepeat = new System.Windows.Forms.Button();
            this.tbLabelCount = new System.Windows.Forms.TextBox();
            this.bDominoSwGo = new System.Windows.Forms.Button();
            this.tbDominoOut = new System.Windows.Forms.TextBox();
            this.bDominoInit = new System.Windows.Forms.Button();
            this.bDominoSendLabel = new System.Windows.Forms.Button();
            this.tabPageData = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvMainData = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.serialLabel = new System.Windows.Forms.Label();
            this.brytLabel = new System.Windows.Forms.Label();
            this.lkpracyLabel = new System.Windows.Forms.Label();
            this.kpracyLabel = new System.Windows.Forms.Label();
            this.bSelActData = new System.Windows.Forms.Button();
            this.lTimeSpan = new System.Windows.Forms.Label();
            this.bSqlSelectAll = new System.Windows.Forms.Button();
            this.tabPageSetup = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPageDebug = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelDebug = new System.Windows.Forms.TableLayoutPanel();
            this.debugTestBox = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.tcpConnected = new System.Windows.Forms.CheckBox();
            this.bTcpConnected = new System.Windows.Forms.Button();
            this.bTcpClose = new System.Windows.Forms.Button();
            this.bOpenTcpTst = new System.Windows.Forms.Button();
            this.tbIntervalTest = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.bTestF2 = new System.Windows.Forms.Button();
            this.bTestF1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.bCheckConnection = new System.Windows.Forms.Button();
            this.rtbConnectionInfo = new System.Windows.Forms.RichTextBox();
            this.tabPageControl = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cbDbAutoUpade = new System.Windows.Forms.CheckBox();
            this.bControlSettingsSave = new System.Windows.Forms.Button();
            this.tbLabel2RepeatPitch = new System.Windows.Forms.Label();
            this.tbLabelRepeatPitch = new System.Windows.Forms.TextBox();
            this.tbLabelRepeatPitchUnit = new System.Windows.Forms.Label();
            this.tbLabel1RepeatPitch = new System.Windows.Forms.Label();
            this.tbLabelLenghtMm = new System.Windows.Forms.TextBox();
            this.tbLabelLenghtUnit = new System.Windows.Forms.Label();
            this.llabelLenghtMm = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lError2 = new System.Windows.Forms.Label();
            this.lError1 = new System.Windows.Forms.Label();
            this.bErrorAck = new System.Windows.Forms.Button();
            this.lWorkAutomat = new System.Windows.Forms.Label();
            this.bSendNLabels = new System.Windows.Forms.Button();
            this.bControlDataUpdate = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.bStartAutomat = new System.Windows.Forms.Button();
            this.tbTileYmm = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dgvControl = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.lActPrintNum = new System.Windows.Forms.Label();
            this.lInfoVal4 = new System.Windows.Forms.Label();
            this.lInfoVal3 = new System.Windows.Forms.Label();
            this.lInfoVal2 = new System.Windows.Forms.Label();
            this.lInfoVal1 = new System.Windows.Forms.Label();
            this.lInfo4 = new System.Windows.Forms.Label();
            this.lInfo3 = new System.Windows.Forms.Label();
            this.lInfo2 = new System.Windows.Forms.Label();
            this.lInfo1 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.cbPrintOn = new System.Windows.Forms.CheckBox();
            this.cbDbReadEnable = new System.Windows.Forms.CheckBox();
            this.dominoPrinterSerialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.tConnection = new System.Windows.Forms.Timer(this.components);
            this.tDbRefresh = new System.Windows.Forms.Timer(this.components);
            //this.tWaitPrintEnd = new System.Windows.Forms.Timer(this.components);
            this.tPlcRead = new System.Windows.Forms.Timer(this.components);
            this.tUbdateVisu = new System.Windows.Forms.Timer(this.components);
            this.dominoPrinterSerialPort2 = new System.IO.Ports.SerialPort(this.components);
            this.tUpdateControlVisu = new System.Windows.Forms.Timer(this.components);
            this.tUpdateMachineStatus = new System.Windows.Forms.Timer(this.components);
            this.tPrinterEthReconnect = new System.Windows.Forms.Timer(this.components);
            this.menuStripMain.SuspendLayout();
            this.statusStripMain.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabPageMain.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageData.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainData)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabPageSetup.SuspendLayout();
            this.tabPageDebug.SuspendLayout();
            this.tableLayoutPanelDebug.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPageControl.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvControl)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ustawieniaToolStripMenuItem,
            this.pomocToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStripMain.Size = new System.Drawing.Size(1535, 28);
            this.menuStripMain.TabIndex = 0;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // ustawieniaToolStripMenuItem
            // 
            this.ustawieniaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ustawieniaDrukuToolStripMenuItem});
            this.ustawieniaToolStripMenuItem.Name = "ustawieniaToolStripMenuItem";
            this.ustawieniaToolStripMenuItem.Size = new System.Drawing.Size(93, 24);
            this.ustawieniaToolStripMenuItem.Text = "Ustawienia";
            // 
            // ustawieniaDrukuToolStripMenuItem
            // 
            this.ustawieniaDrukuToolStripMenuItem.Name = "ustawieniaDrukuToolStripMenuItem";
            this.ustawieniaDrukuToolStripMenuItem.Size = new System.Drawing.Size(197, 26);
            this.ustawieniaDrukuToolStripMenuItem.Text = "Ustawienia druku";
            this.ustawieniaDrukuToolStripMenuItem.Click += new System.EventHandler(this.ustawieniaDrukuToolStripMenuItem_Click);
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oProgramieToolStripMenuItem,
            this.debugToolStripMenuItem});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(66, 24);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.oProgramieToolStripMenuItem.Text = "O programie...";
            this.oProgramieToolStripMenuItem.Click += new System.EventHandler(this.oProgramieToolStripMenuItem_Click);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.debugToolStripMenuItem.Text = "Debug";
            this.debugToolStripMenuItem.Click += new System.EventHandler(this.debugToolStripMenuItem_Click);
            // 
            // statusStripMain
            // 
            this.statusStripMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusStripePrinter2,
            this.statusStripeDb,
            this.statusStripePLC,
            this.statusStripeTimer});
            this.statusStripMain.Location = new System.Drawing.Point(0, 525);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStripMain.Size = new System.Drawing.Size(1535, 24);
            this.statusStripMain.TabIndex = 1;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // statusStripePrinter2
            // 
            this.statusStripePrinter2.AutoSize = false;
            this.statusStripePrinter2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.statusStripePrinter2.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.statusStripePrinter2.Name = "statusStripePrinter2";
            this.statusStripePrinter2.Size = new System.Drawing.Size(200, 19);
            this.statusStripePrinter2.Text = "PrinterStatus";
            this.statusStripePrinter2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusStripeDb
            // 
            this.statusStripeDb.AutoSize = false;
            this.statusStripeDb.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.statusStripeDb.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.statusStripeDb.Name = "statusStripeDb";
            this.statusStripeDb.Size = new System.Drawing.Size(200, 19);
            this.statusStripeDb.Text = "DbStatus";
            this.statusStripeDb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusStripePLC
            // 
            this.statusStripePLC.AutoSize = false;
            this.statusStripePLC.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.statusStripePLC.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.statusStripePLC.Name = "statusStripePLC";
            this.statusStripePLC.Size = new System.Drawing.Size(200, 19);
            this.statusStripePLC.Text = "PLCStatus";
            this.statusStripePLC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusStripeTimer
            // 
            this.statusStripeTimer.AutoSize = false;
            this.statusStripeTimer.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.statusStripeTimer.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.statusStripeTimer.Name = "statusStripeTimer";
            this.statusStripeTimer.Size = new System.Drawing.Size(200, 19);
            this.statusStripeTimer.Text = ".";
            this.statusStripeTimer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageMain);
            this.tabControlMain.Controls.Add(this.tabPageData);
            this.tabControlMain.Controls.Add(this.tabPageSetup);
            this.tabControlMain.Controls.Add(this.tabPageDebug);
            this.tabControlMain.Controls.Add(this.tabPageControl);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 28);
            this.tabControlMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1535, 497);
            this.tabControlMain.TabIndex = 2;
            // 
            // tabPageMain
            // 
            this.tabPageMain.Controls.Add(this.lPingRes);
            this.tabPageMain.Controls.Add(this.bPingTest);
            this.tabPageMain.Controls.Add(this.groupBox2);
            this.tabPageMain.Controls.Add(this.groupBox1);
            this.tabPageMain.Location = new System.Drawing.Point(4, 25);
            this.tabPageMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageMain.Name = "tabPageMain";
            this.tabPageMain.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageMain.Size = new System.Drawing.Size(1527, 468);
            this.tabPageMain.TabIndex = 0;
            this.tabPageMain.Tag = "main";
            this.tabPageMain.Text = "Main";
            this.tabPageMain.UseVisualStyleBackColor = true;
            // 
            // lPingRes
            // 
            this.lPingRes.AutoSize = true;
            this.lPingRes.Location = new System.Drawing.Point(923, 297);
            this.lPingRes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lPingRes.Name = "lPingRes";
            this.lPingRes.Size = new System.Drawing.Size(54, 17);
            this.lPingRes.TabIndex = 7;
            this.lPingRes.Text = "label37";
            // 
            // bPingTest
            // 
            this.bPingTest.Location = new System.Drawing.Point(815, 290);
            this.bPingTest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bPingTest.Name = "bPingTest";
            this.bPingTest.Size = new System.Drawing.Size(100, 28);
            this.bPingTest.TabIndex = 6;
            this.bPingTest.Text = "Ping";
            this.bPingTest.UseVisualStyleBackColor = true;
            this.bPingTest.Click += new System.EventHandler(this.bPingTest_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.tbVal11);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.tbVal10);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.tbVal9);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.tbVal8);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.tbVal7);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.tbVal6);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.tbVal5);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.tbVal4);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.tbVal3);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tbVal2);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tbVal1);
            this.groupBox2.Controls.Add(this.tbVal0);
            this.groupBox2.Controls.Add(this.bPLCWrite);
            this.groupBox2.Controls.Add(this.lVal3);
            this.groupBox2.Controls.Add(this.lVal2);
            this.groupBox2.Controls.Add(this.lVal1);
            this.groupBox2.Controls.Add(this.lVal0);
            this.groupBox2.Controls.Add(this.bPLCRead);
            this.groupBox2.Controls.Add(this.lPLCStatus);
            this.groupBox2.Controls.Add(this.bSiemensConnect);
            this.groupBox2.Location = new System.Drawing.Point(11, 7);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(796, 351);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Siemens";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(657, 123);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 17);
            this.label30.TabIndex = 45;
            this.label30.Text = "Kod błędu PC";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(500, 123);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 17);
            this.label31.TabIndex = 44;
            this.label31.Text = "22-24";
            // 
            // tbVal11
            // 
            this.tbVal11.Location = new System.Drawing.Point(549, 119);
            this.tbVal11.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal11.Name = "tbVal11";
            this.tbVal11.Size = new System.Drawing.Size(99, 22);
            this.tbVal11.TabIndex = 43;
            this.tbVal11.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(657, 94);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(106, 17);
            this.label29.TabIndex = 42;
            this.label29.Text = "Pozycja głowicy";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(657, 62);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 17);
            this.label28.TabIndex = 41;
            this.label28.Text = "pos Y bryt 1";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(391, 316);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 17);
            this.label27.TabIndex = 40;
            this.label27.Text = "pos Y bryt 2";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(391, 284);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(84, 17);
            this.label26.TabIndex = 39;
            this.label26.Text = "pos Y bryt 2";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(391, 220);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(84, 17);
            this.label25.TabIndex = 38;
            this.label25.Text = "pos Y bryt 2";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(391, 188);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(84, 17);
            this.label24.TabIndex = 37;
            this.label24.Text = "pos Y bryt 2";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(391, 252);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(84, 17);
            this.label23.TabIndex = 37;
            this.label23.Text = "pos Y bryt 2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(391, 156);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 17);
            this.label22.TabIndex = 36;
            this.label22.Text = "pos Y bryt 4";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(391, 124);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 17);
            this.label21.TabIndex = 35;
            this.label21.Text = "pos Y bryt 3";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(500, 94);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 17);
            this.label20.TabIndex = 34;
            this.label20.Text = "20-21";
            // 
            // tbVal10
            // 
            this.tbVal10.Location = new System.Drawing.Point(549, 90);
            this.tbVal10.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal10.Name = "tbVal10";
            this.tbVal10.Size = new System.Drawing.Size(99, 22);
            this.tbVal10.TabIndex = 33;
            this.tbVal10.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(500, 62);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 17);
            this.label19.TabIndex = 32;
            this.label19.Text = "18-19";
            // 
            // tbVal9
            // 
            this.tbVal9.Location = new System.Drawing.Point(549, 58);
            this.tbVal9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal9.Name = "tbVal9";
            this.tbVal9.Size = new System.Drawing.Size(99, 22);
            this.tbVal9.TabIndex = 31;
            this.tbVal9.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(233, 316);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 17);
            this.label18.TabIndex = 30;
            this.label18.Text = "16-17";
            // 
            // tbVal8
            // 
            this.tbVal8.Location = new System.Drawing.Point(283, 313);
            this.tbVal8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal8.Name = "tbVal8";
            this.tbVal8.Size = new System.Drawing.Size(99, 22);
            this.tbVal8.TabIndex = 29;
            this.tbVal8.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(233, 284);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 17);
            this.label17.TabIndex = 28;
            this.label17.Text = "14-15";
            // 
            // tbVal7
            // 
            this.tbVal7.Location = new System.Drawing.Point(283, 281);
            this.tbVal7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal7.Name = "tbVal7";
            this.tbVal7.Size = new System.Drawing.Size(99, 22);
            this.tbVal7.TabIndex = 27;
            this.tbVal7.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(233, 252);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 17);
            this.label16.TabIndex = 26;
            this.label16.Text = "12-13";
            // 
            // tbVal6
            // 
            this.tbVal6.Location = new System.Drawing.Point(283, 249);
            this.tbVal6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal6.Name = "tbVal6";
            this.tbVal6.Size = new System.Drawing.Size(99, 22);
            this.tbVal6.TabIndex = 25;
            this.tbVal6.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(233, 220);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 17);
            this.label15.TabIndex = 24;
            this.label15.Text = "10-11";
            // 
            // tbVal5
            // 
            this.tbVal5.Location = new System.Drawing.Point(283, 217);
            this.tbVal5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal5.Name = "tbVal5";
            this.tbVal5.Size = new System.Drawing.Size(99, 22);
            this.tbVal5.TabIndex = 23;
            this.tbVal5.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(233, 188);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 17);
            this.label14.TabIndex = 22;
            this.label14.Text = "8-9";
            // 
            // tbVal4
            // 
            this.tbVal4.Location = new System.Drawing.Point(283, 185);
            this.tbVal4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal4.Name = "tbVal4";
            this.tbVal4.Size = new System.Drawing.Size(99, 22);
            this.tbVal4.TabIndex = 21;
            this.tbVal4.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(233, 156);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 17);
            this.label13.TabIndex = 20;
            this.label13.Text = "6-7";
            // 
            // tbVal3
            // 
            this.tbVal3.Location = new System.Drawing.Point(283, 153);
            this.tbVal3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal3.Name = "tbVal3";
            this.tbVal3.Size = new System.Drawing.Size(99, 22);
            this.tbVal3.TabIndex = 19;
            this.tbVal3.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(233, 124);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "4-5";
            // 
            // tbVal2
            // 
            this.tbVal2.Location = new System.Drawing.Point(283, 121);
            this.tbVal2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal2.Name = "tbVal2";
            this.tbVal2.Size = new System.Drawing.Size(99, 22);
            this.tbVal2.TabIndex = 17;
            this.tbVal2.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(233, 92);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 17);
            this.label11.TabIndex = 16;
            this.label11.Text = "2-3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(233, 62);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 17);
            this.label10.TabIndex = 15;
            this.label10.Text = "0-1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 218);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 17);
            this.label9.TabIndex = 14;
            this.label9.Text = "32";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 186);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 17);
            this.label8.TabIndex = 13;
            this.label8.Text = "31";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 155);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "30";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(391, 92);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "pos Y bryt 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(391, 62);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "pos Y bryt 1";
            // 
            // tbVal1
            // 
            this.tbVal1.Location = new System.Drawing.Point(283, 89);
            this.tbVal1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal1.Name = "tbVal1";
            this.tbVal1.Size = new System.Drawing.Size(99, 22);
            this.tbVal1.TabIndex = 9;
            this.tbVal1.Text = "0";
            // 
            // tbVal0
            // 
            this.tbVal0.Location = new System.Drawing.Point(283, 58);
            this.tbVal0.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbVal0.Name = "tbVal0";
            this.tbVal0.Size = new System.Drawing.Size(99, 22);
            this.tbVal0.TabIndex = 8;
            this.tbVal0.Text = "0";
            // 
            // bPLCWrite
            // 
            this.bPLCWrite.Location = new System.Drawing.Point(283, 23);
            this.bPLCWrite.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bPLCWrite.Name = "bPLCWrite";
            this.bPLCWrite.Size = new System.Drawing.Size(100, 28);
            this.bPLCWrite.TabIndex = 7;
            this.bPLCWrite.Text = "write PLC";
            this.bPLCWrite.UseVisualStyleBackColor = true;
            this.bPLCWrite.Click += new System.EventHandler(this.bPLCWrite_Click);
            // 
            // lVal3
            // 
            this.lVal3.AutoSize = true;
            this.lVal3.Location = new System.Drawing.Point(47, 246);
            this.lVal3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lVal3.Name = "lVal3";
            this.lVal3.Size = new System.Drawing.Size(46, 17);
            this.lVal3.TabIndex = 6;
            this.lVal3.Text = "label1";
            // 
            // lVal2
            // 
            this.lVal2.AutoSize = true;
            this.lVal2.Location = new System.Drawing.Point(47, 218);
            this.lVal2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lVal2.Name = "lVal2";
            this.lVal2.Size = new System.Drawing.Size(46, 17);
            this.lVal2.TabIndex = 5;
            this.lVal2.Text = "label1";
            // 
            // lVal1
            // 
            this.lVal1.AutoSize = true;
            this.lVal1.Location = new System.Drawing.Point(47, 186);
            this.lVal1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lVal1.Name = "lVal1";
            this.lVal1.Size = new System.Drawing.Size(46, 17);
            this.lVal1.TabIndex = 4;
            this.lVal1.Text = "label1";
            // 
            // lVal0
            // 
            this.lVal0.AutoSize = true;
            this.lVal0.Location = new System.Drawing.Point(47, 155);
            this.lVal0.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lVal0.Name = "lVal0";
            this.lVal0.Size = new System.Drawing.Size(46, 17);
            this.lVal0.TabIndex = 3;
            this.lVal0.Text = "label1";
            // 
            // bPLCRead
            // 
            this.bPLCRead.Location = new System.Drawing.Point(12, 117);
            this.bPLCRead.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bPLCRead.Name = "bPLCRead";
            this.bPLCRead.Size = new System.Drawing.Size(100, 28);
            this.bPLCRead.TabIndex = 2;
            this.bPLCRead.Text = "read PLC";
            this.bPLCRead.UseVisualStyleBackColor = true;
            this.bPLCRead.Click += new System.EventHandler(this.bPLCRead_Click);
            // 
            // lPLCStatus
            // 
            this.lPLCStatus.AutoSize = true;
            this.lPLCStatus.Location = new System.Drawing.Point(8, 62);
            this.lPLCStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lPLCStatus.Name = "lPLCStatus";
            this.lPLCStatus.Size = new System.Drawing.Size(46, 17);
            this.lPLCStatus.TabIndex = 1;
            this.lPLCStatus.Text = "label1";
            // 
            // bSiemensConnect
            // 
            this.bSiemensConnect.Location = new System.Drawing.Point(12, 23);
            this.bSiemensConnect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSiemensConnect.Name = "bSiemensConnect";
            this.bSiemensConnect.Size = new System.Drawing.Size(100, 28);
            this.bSiemensConnect.TabIndex = 0;
            this.bSiemensConnect.Text = "connect";
            this.bSiemensConnect.UseVisualStyleBackColor = true;
            this.bSiemensConnect.Click += new System.EventHandler(this.bSiemensConnect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bDominoRepeat);
            this.groupBox1.Controls.Add(this.tbLabelCount);
            this.groupBox1.Controls.Add(this.bDominoSwGo);
            this.groupBox1.Controls.Add(this.tbDominoOut);
            this.groupBox1.Controls.Add(this.bDominoInit);
            this.groupBox1.Controls.Add(this.bDominoSendLabel);
            this.groupBox1.Location = new System.Drawing.Point(815, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(267, 276);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Printer";
            // 
            // bDominoRepeat
            // 
            this.bDominoRepeat.Location = new System.Drawing.Point(116, 32);
            this.bDominoRepeat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDominoRepeat.Name = "bDominoRepeat";
            this.bDominoRepeat.Size = new System.Drawing.Size(100, 28);
            this.bDominoRepeat.TabIndex = 5;
            this.bDominoRepeat.Text = "set repeat";
            this.bDominoRepeat.UseVisualStyleBackColor = true;
            this.bDominoRepeat.Click += new System.EventHandler(this.bDominoRepeat_Click);
            // 
            // tbLabelCount
            // 
            this.tbLabelCount.Location = new System.Drawing.Point(116, 81);
            this.tbLabelCount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbLabelCount.Name = "tbLabelCount";
            this.tbLabelCount.Size = new System.Drawing.Size(132, 22);
            this.tbLabelCount.TabIndex = 4;
            this.tbLabelCount.Text = "1";
            // 
            // bDominoSwGo
            // 
            this.bDominoSwGo.Location = new System.Drawing.Point(8, 123);
            this.bDominoSwGo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDominoSwGo.Name = "bDominoSwGo";
            this.bDominoSwGo.Size = new System.Drawing.Size(100, 28);
            this.bDominoSwGo.TabIndex = 3;
            this.bDominoSwGo.Text = "SwGo";
            this.bDominoSwGo.UseVisualStyleBackColor = true;
            this.bDominoSwGo.Click += new System.EventHandler(this.bDominoSwGo_Click);
            // 
            // tbDominoOut
            // 
            this.tbDominoOut.Location = new System.Drawing.Point(8, 188);
            this.tbDominoOut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbDominoOut.Name = "tbDominoOut";
            this.tbDominoOut.Size = new System.Drawing.Size(132, 22);
            this.tbDominoOut.TabIndex = 2;
            // 
            // bDominoInit
            // 
            this.bDominoInit.Location = new System.Drawing.Point(8, 32);
            this.bDominoInit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDominoInit.Name = "bDominoInit";
            this.bDominoInit.Size = new System.Drawing.Size(100, 28);
            this.bDominoInit.TabIndex = 0;
            this.bDominoInit.Text = "init";
            this.bDominoInit.UseVisualStyleBackColor = true;
            this.bDominoInit.Click += new System.EventHandler(this.bDominoInit_Click);
            // 
            // bDominoSendLabel
            // 
            this.bDominoSendLabel.Location = new System.Drawing.Point(8, 79);
            this.bDominoSendLabel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDominoSendLabel.Name = "bDominoSendLabel";
            this.bDominoSendLabel.Size = new System.Drawing.Size(100, 28);
            this.bDominoSendLabel.TabIndex = 1;
            this.bDominoSendLabel.Text = "send label";
            this.bDominoSendLabel.UseVisualStyleBackColor = true;
            // 
            // tabPageData
            // 
            this.tabPageData.Controls.Add(this.tableLayoutPanel1);
            this.tabPageData.Location = new System.Drawing.Point(4, 25);
            this.tabPageData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageData.Name = "tabPageData";
            this.tabPageData.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageData.Size = new System.Drawing.Size(1527, 463);
            this.tabPageData.TabIndex = 3;
            this.tabPageData.Tag = "data";
            this.tabPageData.Text = "Data";
            this.tabPageData.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1519, 455);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvMainData);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 66);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1511, 385);
            this.panel1.TabIndex = 0;
            // 
            // dgvMainData
            // 
            this.dgvMainData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMainData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMainData.Location = new System.Drawing.Point(0, 0);
            this.dgvMainData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvMainData.Name = "dgvMainData";
            this.dgvMainData.Size = new System.Drawing.Size(1511, 385);
            this.dgvMainData.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.serialLabel);
            this.panel3.Controls.Add(this.brytLabel);
            this.panel3.Controls.Add(this.lkpracyLabel);
            this.panel3.Controls.Add(this.kpracyLabel);
            this.panel3.Controls.Add(this.bSelActData);
            this.panel3.Controls.Add(this.lTimeSpan);
            this.panel3.Controls.Add(this.bSqlSelectAll);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 4);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1511, 54);
            this.panel3.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(624, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Nr seryjny:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(624, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Bryt:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(371, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Linia karty pracy:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(371, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Karta pracy:";
            // 
            // serialLabel
            // 
            this.serialLabel.AutoSize = true;
            this.serialLabel.Location = new System.Drawing.Point(707, 32);
            this.serialLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.serialLabel.Name = "serialLabel";
            this.serialLabel.Size = new System.Drawing.Size(14, 17);
            this.serialLabel.TabIndex = 6;
            this.serialLabel.Text = "x";
            // 
            // brytLabel
            // 
            this.brytLabel.AutoSize = true;
            this.brytLabel.Location = new System.Drawing.Point(707, 9);
            this.brytLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.brytLabel.Name = "brytLabel";
            this.brytLabel.Size = new System.Drawing.Size(14, 17);
            this.brytLabel.TabIndex = 5;
            this.brytLabel.Text = "x";
            // 
            // lkpracyLabel
            // 
            this.lkpracyLabel.AutoSize = true;
            this.lkpracyLabel.Location = new System.Drawing.Point(495, 32);
            this.lkpracyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lkpracyLabel.Name = "lkpracyLabel";
            this.lkpracyLabel.Size = new System.Drawing.Size(14, 17);
            this.lkpracyLabel.TabIndex = 4;
            this.lkpracyLabel.Text = "x";
            // 
            // kpracyLabel
            // 
            this.kpracyLabel.AutoSize = true;
            this.kpracyLabel.Location = new System.Drawing.Point(495, 9);
            this.kpracyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.kpracyLabel.Name = "kpracyLabel";
            this.kpracyLabel.Size = new System.Drawing.Size(14, 17);
            this.kpracyLabel.TabIndex = 3;
            this.kpracyLabel.Text = "x";
            // 
            // bSelActData
            // 
            this.bSelActData.Location = new System.Drawing.Point(241, 4);
            this.bSelActData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSelActData.Name = "bSelActData";
            this.bSelActData.Size = new System.Drawing.Size(115, 47);
            this.bSelActData.TabIndex = 2;
            this.bSelActData.Text = "Select data";
            this.bSelActData.UseVisualStyleBackColor = true;
            //this.bSelActData.Click += new System.EventHandler(this.bSelActData_Click);
            // 
            // lTimeSpan
            // 
            this.lTimeSpan.AutoSize = true;
            this.lTimeSpan.Location = new System.Drawing.Point(127, 20);
            this.lTimeSpan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lTimeSpan.Name = "lTimeSpan";
            this.lTimeSpan.Size = new System.Drawing.Size(75, 17);
            this.lTimeSpan.TabIndex = 1;
            this.lTimeSpan.Text = "lTimeSpan";
            // 
            // bSqlSelectAll
            // 
            this.bSqlSelectAll.Location = new System.Drawing.Point(4, 4);
            this.bSqlSelectAll.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSqlSelectAll.Name = "bSqlSelectAll";
            this.bSqlSelectAll.Size = new System.Drawing.Size(115, 47);
            this.bSqlSelectAll.TabIndex = 0;
            this.bSqlSelectAll.Text = "Select All";
            this.bSqlSelectAll.UseVisualStyleBackColor = true;
            //this.bSqlSelectAll.Click += new System.EventHandler(this.bSqlSelectAll_Click);
            // 
            // tabPageSetup
            // 
            this.tabPageSetup.Controls.Add(this.button1);
            this.tabPageSetup.Location = new System.Drawing.Point(4, 25);
            this.tabPageSetup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageSetup.Name = "tabPageSetup";
            this.tabPageSetup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageSetup.Size = new System.Drawing.Size(1527, 463);
            this.tabPageSetup.TabIndex = 1;
            this.tabPageSetup.Tag = "setup";
            this.tabPageSetup.Text = "Setup";
            this.tabPageSetup.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(11, 7);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(215, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "mySqlConnect";
            this.button1.UseVisualStyleBackColor = true;
            //this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPageDebug
            // 
            this.tabPageDebug.Controls.Add(this.tableLayoutPanelDebug);
            this.tabPageDebug.Location = new System.Drawing.Point(4, 25);
            this.tabPageDebug.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageDebug.Name = "tabPageDebug";
            this.tabPageDebug.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageDebug.Size = new System.Drawing.Size(1527, 463);
            this.tabPageDebug.TabIndex = 2;
            this.tabPageDebug.Tag = "debug";
            this.tabPageDebug.Text = "Debug";
            this.tabPageDebug.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelDebug
            // 
            this.tableLayoutPanelDebug.ColumnCount = 1;
            this.tableLayoutPanelDebug.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDebug.Controls.Add(this.debugTestBox, 0, 1);
            this.tableLayoutPanelDebug.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanelDebug.Controls.Add(this.rtbConnectionInfo, 0, 2);
            this.tableLayoutPanelDebug.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDebug.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanelDebug.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelDebug.Name = "tableLayoutPanelDebug";
            this.tableLayoutPanelDebug.RowCount = 3;
            this.tableLayoutPanelDebug.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanelDebug.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDebug.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanelDebug.Size = new System.Drawing.Size(1519, 455);
            this.tableLayoutPanelDebug.TabIndex = 2;
            // 
            // debugTestBox
            // 
            this.debugTestBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.debugTestBox.Location = new System.Drawing.Point(4, 53);
            this.debugTestBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.debugTestBox.Name = "debugTestBox";
            this.debugTestBox.Size = new System.Drawing.Size(1511, 213);
            this.debugTestBox.TabIndex = 0;
            this.debugTestBox.Text = "";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.tcpConnected);
            this.panel2.Controls.Add(this.bTcpConnected);
            this.panel2.Controls.Add(this.bTcpClose);
            this.panel2.Controls.Add(this.bOpenTcpTst);
            this.panel2.Controls.Add(this.tbIntervalTest);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.bTestF2);
            this.panel2.Controls.Add(this.bTestF1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.bCheckConnection);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1511, 41);
            this.panel2.TabIndex = 1;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(440, 7);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 28);
            this.button4.TabIndex = 10;
            this.button4.Text = "Auto";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            // 
            // tcpConnected
            // 
            this.tcpConnected.AutoSize = true;
            this.tcpConnected.Location = new System.Drawing.Point(328, 12);
            this.tcpConnected.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tcpConnected.Name = "tcpConnected";
            this.tcpConnected.Size = new System.Drawing.Size(98, 21);
            this.tcpConnected.TabIndex = 9;
            this.tcpConnected.Text = "Connected";
            this.tcpConnected.UseVisualStyleBackColor = true;
            this.tcpConnected.Visible = false;
            // 
            // bTcpConnected
            // 
            this.bTcpConnected.Location = new System.Drawing.Point(220, 7);
            this.bTcpConnected.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bTcpConnected.Name = "bTcpConnected";
            this.bTcpConnected.Size = new System.Drawing.Size(100, 28);
            this.bTcpConnected.TabIndex = 8;
            this.bTcpConnected.Text = "Connected";
            this.bTcpConnected.UseVisualStyleBackColor = true;
            this.bTcpConnected.Visible = false;
            this.bTcpConnected.Click += new System.EventHandler(this.bTcpConnected_Click);
            // 
            // bTcpClose
            // 
            this.bTcpClose.Location = new System.Drawing.Point(112, 7);
            this.bTcpClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bTcpClose.Name = "bTcpClose";
            this.bTcpClose.Size = new System.Drawing.Size(100, 28);
            this.bTcpClose.TabIndex = 7;
            this.bTcpClose.Text = "Close";
            this.bTcpClose.UseVisualStyleBackColor = true;
            this.bTcpClose.Visible = false;
            // 
            // bOpenTcpTst
            // 
            this.bOpenTcpTst.Location = new System.Drawing.Point(4, 7);
            this.bOpenTcpTst.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bOpenTcpTst.Name = "bOpenTcpTst";
            this.bOpenTcpTst.Size = new System.Drawing.Size(100, 28);
            this.bOpenTcpTst.TabIndex = 6;
            this.bOpenTcpTst.Text = "Open";
            this.bOpenTcpTst.UseVisualStyleBackColor = true;
            this.bOpenTcpTst.Visible = false;
            // 
            // tbIntervalTest
            // 
            this.tbIntervalTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbIntervalTest.Location = new System.Drawing.Point(648, 7);
            this.tbIntervalTest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbIntervalTest.Name = "tbIntervalTest";
            this.tbIntervalTest.Size = new System.Drawing.Size(132, 22);
            this.tbIntervalTest.TabIndex = 5;
            this.tbIntervalTest.Text = "100";
            this.tbIntervalTest.Visible = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(790, 7);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 28);
            this.button3.TabIndex = 4;
            this.button3.Text = "Interval Update";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // bTestF2
            // 
            this.bTestF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bTestF2.Location = new System.Drawing.Point(898, 7);
            this.bTestF2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bTestF2.Name = "bTestF2";
            this.bTestF2.Size = new System.Drawing.Size(100, 28);
            this.bTestF2.TabIndex = 3;
            this.bTestF2.Text = "Połącz";
            this.bTestF2.UseVisualStyleBackColor = true;
            this.bTestF2.Visible = false;
            this.bTestF2.Click += new System.EventHandler(this.bTestF2_Click);
            // 
            // bTestF1
            // 
            this.bTestF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bTestF1.Location = new System.Drawing.Point(1006, 7);
            this.bTestF1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bTestF1.Name = "bTestF1";
            this.bTestF1.Size = new System.Drawing.Size(100, 28);
            this.bTestF1.TabIndex = 2;
            this.bTestF1.Text = "Rozłącz";
            this.bTestF1.UseVisualStyleBackColor = true;
            this.bTestF1.Visible = false;
            this.bTestF1.Click += new System.EventHandler(this.bTestF1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(1114, 7);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(191, 28);
            this.button2.TabIndex = 1;
            this.button2.Text = "Wyczyść kolejkę";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bCheckConnection
            // 
            this.bCheckConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bCheckConnection.Location = new System.Drawing.Point(1336, 7);
            this.bCheckConnection.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bCheckConnection.Name = "bCheckConnection";
            this.bCheckConnection.Size = new System.Drawing.Size(167, 28);
            this.bCheckConnection.TabIndex = 0;
            this.bCheckConnection.Text = "Wyczyść oczekiwanie";
            this.bCheckConnection.UseVisualStyleBackColor = true;
            this.bCheckConnection.Visible = false;
            this.bCheckConnection.Click += new System.EventHandler(this.bCheckConnection_Click);
            // 
            // rtbConnectionInfo
            // 
            this.rtbConnectionInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbConnectionInfo.Location = new System.Drawing.Point(4, 274);
            this.rtbConnectionInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rtbConnectionInfo.Name = "rtbConnectionInfo";
            this.rtbConnectionInfo.Size = new System.Drawing.Size(1511, 177);
            this.rtbConnectionInfo.TabIndex = 2;
            this.rtbConnectionInfo.Text = "";
            // 
            // tabPageControl
            // 
            this.tabPageControl.Controls.Add(this.tableLayoutPanel2);
            this.tabPageControl.Location = new System.Drawing.Point(4, 25);
            this.tabPageControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageControl.Name = "tabPageControl";
            this.tabPageControl.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageControl.Size = new System.Drawing.Size(1527, 463);
            this.tabPageControl.TabIndex = 4;
            this.tabPageControl.Tag = "control";
            this.tabPageControl.Text = "Control";
            this.tabPageControl.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.1845F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.8155F));
            this.tableLayoutPanel2.Controls.Add(this.panel7, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel6, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel8, 0, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1519, 455);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.cbDbAutoUpade);
            this.panel7.Controls.Add(this.bControlSettingsSave);
            this.panel7.Controls.Add(this.tbLabel2RepeatPitch);
            this.panel7.Controls.Add(this.tbLabelRepeatPitch);
            this.panel7.Controls.Add(this.tbLabelRepeatPitchUnit);
            this.panel7.Controls.Add(this.tbLabel1RepeatPitch);
            this.panel7.Controls.Add(this.tbLabelLenghtMm);
            this.panel7.Controls.Add(this.tbLabelLenghtUnit);
            this.panel7.Controls.Add(this.llabelLenghtMm);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(766, 212);
            this.panel7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(749, 177);
            this.panel7.TabIndex = 3;
            // 
            // cbDbAutoUpade
            // 
            this.cbDbAutoUpade.AutoSize = true;
            this.cbDbAutoUpade.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbDbAutoUpade.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbDbAutoUpade.Location = new System.Drawing.Point(16, 92);
            this.cbDbAutoUpade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbDbAutoUpade.Name = "cbDbAutoUpade";
            this.cbDbAutoUpade.Size = new System.Drawing.Size(175, 21);
            this.cbDbAutoUpade.TabIndex = 8;
            this.cbDbAutoUpade.Text = "Aktualizuj bazę danych";
            this.cbDbAutoUpade.UseVisualStyleBackColor = true;
            // 
            // bControlSettingsSave
            // 
            this.bControlSettingsSave.Location = new System.Drawing.Point(431, 4);
            this.bControlSettingsSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bControlSettingsSave.Name = "bControlSettingsSave";
            this.bControlSettingsSave.Size = new System.Drawing.Size(100, 127);
            this.bControlSettingsSave.TabIndex = 7;
            this.bControlSettingsSave.Text = "Aktualizuj";
            this.bControlSettingsSave.UseVisualStyleBackColor = true;
            this.bControlSettingsSave.Click += new System.EventHandler(this.bControlSettingsSave_Click);
            // 
            // tbLabel2RepeatPitch
            // 
            this.tbLabel2RepeatPitch.AutoSize = true;
            this.tbLabel2RepeatPitch.Location = new System.Drawing.Point(17, 60);
            this.tbLabel2RepeatPitch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tbLabel2RepeatPitch.Name = "tbLabel2RepeatPitch";
            this.tbLabel2RepeatPitch.Size = new System.Drawing.Size(148, 17);
            this.tbLabel2RepeatPitch.TabIndex = 6;
            this.tbLabel2RepeatPitch.Text = "początkami nadruków:";
            // 
            // tbLabelRepeatPitch
            // 
            this.tbLabelRepeatPitch.Location = new System.Drawing.Point(177, 49);
            this.tbLabelRepeatPitch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbLabelRepeatPitch.Name = "tbLabelRepeatPitch";
            this.tbLabelRepeatPitch.Size = new System.Drawing.Size(132, 22);
            this.tbLabelRepeatPitch.TabIndex = 5;
            // 
            // tbLabelRepeatPitchUnit
            // 
            this.tbLabelRepeatPitchUnit.AutoSize = true;
            this.tbLabelRepeatPitchUnit.Location = new System.Drawing.Point(319, 53);
            this.tbLabelRepeatPitchUnit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tbLabelRepeatPitchUnit.Name = "tbLabelRepeatPitchUnit";
            this.tbLabelRepeatPitchUnit.Size = new System.Drawing.Size(55, 17);
            this.tbLabelRepeatPitchUnit.TabIndex = 4;
            this.tbLabelRepeatPitchUnit.Text = "[stroke]";
            // 
            // tbLabel1RepeatPitch
            // 
            this.tbLabel1RepeatPitch.AutoSize = true;
            this.tbLabel1RepeatPitch.Location = new System.Drawing.Point(17, 42);
            this.tbLabel1RepeatPitch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tbLabel1RepeatPitch.Name = "tbLabel1RepeatPitch";
            this.tbLabel1RepeatPitch.Size = new System.Drawing.Size(107, 17);
            this.tbLabel1RepeatPitch.TabIndex = 3;
            this.tbLabel1RepeatPitch.Text = "Przerwa między";
            // 
            // tbLabelLenghtMm
            // 
            this.tbLabelLenghtMm.Location = new System.Drawing.Point(177, 12);
            this.tbLabelLenghtMm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbLabelLenghtMm.Name = "tbLabelLenghtMm";
            this.tbLabelLenghtMm.Size = new System.Drawing.Size(132, 22);
            this.tbLabelLenghtMm.TabIndex = 2;
            // 
            // tbLabelLenghtUnit
            // 
            this.tbLabelLenghtUnit.AutoSize = true;
            this.tbLabelLenghtUnit.Location = new System.Drawing.Point(319, 16);
            this.tbLabelLenghtUnit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tbLabelLenghtUnit.Name = "tbLabelLenghtUnit";
            this.tbLabelLenghtUnit.Size = new System.Drawing.Size(38, 17);
            this.tbLabelLenghtUnit.TabIndex = 1;
            this.tbLabelLenghtUnit.Text = "[mm]";
            // 
            // llabelLenghtMm
            // 
            this.llabelLenghtMm.AutoSize = true;
            this.llabelLenghtMm.Location = new System.Drawing.Point(17, 16);
            this.llabelLenghtMm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.llabelLenghtMm.Name = "llabelLenghtMm";
            this.llabelLenghtMm.Size = new System.Drawing.Size(119, 17);
            this.llabelLenghtMm.TabIndex = 0;
            this.llabelLenghtMm.Text = "Długość nadruku:";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel2.SetColumnSpan(this.panel4, 2);
            this.panel4.Controls.Add(this.lError2);
            this.panel4.Controls.Add(this.lError1);
            this.panel4.Controls.Add(this.bErrorAck);
            this.panel4.Controls.Add(this.lWorkAutomat);
            this.panel4.Controls.Add(this.bSendNLabels);
            this.panel4.Controls.Add(this.bControlDataUpdate);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.bStartAutomat);
            this.panel4.Controls.Add(this.tbTileYmm);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(4, 4);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1511, 41);
            this.panel4.TabIndex = 0;
            // 
            // lError2
            // 
            this.lError2.AutoSize = true;
            this.lError2.ForeColor = System.Drawing.Color.Red;
            this.lError2.Location = new System.Drawing.Point(441, 21);
            this.lError2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lError2.Name = "lError2";
            this.lError2.Size = new System.Drawing.Size(264, 17);
            this.lError2.TabIndex = 12;
            this.lError2.Text = "Kliknij \"Potwierdź\" aby usunąć komunikat";
            this.lError2.Visible = false;
            // 
            // lError1
            // 
            this.lError1.AutoSize = true;
            this.lError1.ForeColor = System.Drawing.Color.Red;
            this.lError1.Location = new System.Drawing.Point(441, 2);
            this.lError1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lError1.Name = "lError1";
            this.lError1.Size = new System.Drawing.Size(258, 17);
            this.lError1.TabIndex = 11;
            this.lError1.Text = "Wystąpił błąd. Praca jest kontynuowana";
            this.lError1.Visible = false;
            // 
            // bErrorAck
            // 
            this.bErrorAck.Location = new System.Drawing.Point(275, 4);
            this.bErrorAck.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bErrorAck.Name = "bErrorAck";
            this.bErrorAck.Size = new System.Drawing.Size(159, 32);
            this.bErrorAck.TabIndex = 10;
            this.bErrorAck.Text = "Potwierdź";
            this.bErrorAck.UseVisualStyleBackColor = true;
            this.bErrorAck.Visible = false;
            this.bErrorAck.Click += new System.EventHandler(this.bErrorAck_Click);
            // 
            // lWorkAutomat
            // 
            this.lWorkAutomat.AutoSize = true;
            this.lWorkAutomat.Location = new System.Drawing.Point(112, 14);
            this.lWorkAutomat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lWorkAutomat.Name = "lWorkAutomat";
            this.lWorkAutomat.Size = new System.Drawing.Size(126, 17);
            this.lWorkAutomat.TabIndex = 9;
            this.lWorkAutomat.Text = "Praca: zatrzymana";
            // 
            // bSendNLabels
            // 
            this.bSendNLabels.Location = new System.Drawing.Point(1001, 6);
            this.bSendNLabels.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSendNLabels.Name = "bSendNLabels";
            this.bSendNLabels.Size = new System.Drawing.Size(67, 31);
            this.bSendNLabels.TabIndex = 8;
            this.bSendNLabels.Text = "->";
            this.bSendNLabels.UseVisualStyleBackColor = true;
            this.bSendNLabels.Visible = false;
            // 
            // bControlDataUpdate
            // 
            this.bControlDataUpdate.Location = new System.Drawing.Point(676, 4);
            this.bControlDataUpdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bControlDataUpdate.Name = "bControlDataUpdate";
            this.bControlDataUpdate.Size = new System.Drawing.Size(123, 34);
            this.bControlDataUpdate.TabIndex = 1;
            this.bControlDataUpdate.Text = "Pobierz dane";
            this.bControlDataUpdate.UseVisualStyleBackColor = true;
            this.bControlDataUpdate.Visible = false;
            this.bControlDataUpdate.Click += new System.EventHandler(this.bControlDataUpdate_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(805, 14);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 17);
            this.label36.TabIndex = 7;
            this.label36.Text = "tileymm ";
            this.label36.Visible = false;
            // 
            // bStartAutomat
            // 
            this.bStartAutomat.Enabled = false;
            this.bStartAutomat.Location = new System.Drawing.Point(4, 4);
            this.bStartAutomat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bStartAutomat.Name = "bStartAutomat";
            this.bStartAutomat.Size = new System.Drawing.Size(100, 34);
            this.bStartAutomat.TabIndex = 0;
            this.bStartAutomat.Text = "Start";
            this.bStartAutomat.UseVisualStyleBackColor = true;
            this.bStartAutomat.Visible = false;
            this.bStartAutomat.Click += new System.EventHandler(this.bStartAutomat_Click);
            // 
            // tbTileYmm
            // 
            this.tbTileYmm.Location = new System.Drawing.Point(872, 10);
            this.tbTileYmm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbTileYmm.Name = "tbTileYmm";
            this.tbTileYmm.Size = new System.Drawing.Size(121, 22);
            this.tbTileYmm.TabIndex = 6;
            this.tbTileYmm.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel2.SetColumnSpan(this.panel5, 2);
            this.panel5.Controls.Add(this.dgvControl);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(4, 53);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1511, 151);
            this.panel5.TabIndex = 1;
            // 
            // dgvControl
            // 
            this.dgvControl.AllowUserToAddRows = false;
            this.dgvControl.AllowUserToDeleteRows = false;
            this.dgvControl.AllowUserToResizeRows = false;
            this.dgvControl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvControl.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvControl.Location = new System.Drawing.Point(0, 0);
            this.dgvControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvControl.MultiSelect = false;
            this.dgvControl.Name = "dgvControl";
            this.dgvControl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvControl.Size = new System.Drawing.Size(1509, 149);
            this.dgvControl.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label37);
            this.panel6.Controls.Add(this.lActPrintNum);
            this.panel6.Controls.Add(this.lInfoVal4);
            this.panel6.Controls.Add(this.lInfoVal3);
            this.panel6.Controls.Add(this.lInfoVal2);
            this.panel6.Controls.Add(this.lInfoVal1);
            this.panel6.Controls.Add(this.lInfo4);
            this.panel6.Controls.Add(this.lInfo3);
            this.panel6.Controls.Add(this.lInfo2);
            this.panel6.Controls.Add(this.lInfo1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(4, 212);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(754, 177);
            this.panel6.TabIndex = 2;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(23, 143);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(94, 17);
            this.label37.TabIndex = 3;
            this.label37.Text = "Aktualny bryt:";
            // 
            // lActPrintNum
            // 
            this.lActPrintNum.AutoSize = true;
            this.lActPrintNum.Location = new System.Drawing.Point(181, 143);
            this.lActPrintNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lActPrintNum.Name = "lActPrintNum";
            this.lActPrintNum.Size = new System.Drawing.Size(16, 17);
            this.lActPrintNum.TabIndex = 2;
            this.lActPrintNum.Text = "0";
            // 
            // lInfoVal4
            // 
            this.lInfoVal4.AutoSize = true;
            this.lInfoVal4.Location = new System.Drawing.Point(181, 113);
            this.lInfoVal4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lInfoVal4.Name = "lInfoVal4";
            this.lInfoVal4.Size = new System.Drawing.Size(0, 17);
            this.lInfoVal4.TabIndex = 7;
            // 
            // lInfoVal3
            // 
            this.lInfoVal3.AutoSize = true;
            this.lInfoVal3.Location = new System.Drawing.Point(181, 84);
            this.lInfoVal3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lInfoVal3.Name = "lInfoVal3";
            this.lInfoVal3.Size = new System.Drawing.Size(0, 17);
            this.lInfoVal3.TabIndex = 6;
            // 
            // lInfoVal2
            // 
            this.lInfoVal2.AutoSize = true;
            this.lInfoVal2.Location = new System.Drawing.Point(181, 54);
            this.lInfoVal2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lInfoVal2.Name = "lInfoVal2";
            this.lInfoVal2.Size = new System.Drawing.Size(0, 17);
            this.lInfoVal2.TabIndex = 5;
            // 
            // lInfoVal1
            // 
            this.lInfoVal1.AutoSize = true;
            this.lInfoVal1.Location = new System.Drawing.Point(181, 25);
            this.lInfoVal1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lInfoVal1.Name = "lInfoVal1";
            this.lInfoVal1.Size = new System.Drawing.Size(0, 17);
            this.lInfoVal1.TabIndex = 4;
            // 
            // lInfo4
            // 
            this.lInfo4.AutoSize = true;
            this.lInfo4.Location = new System.Drawing.Point(23, 113);
            this.lInfo4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lInfo4.Name = "lInfo4";
            this.lInfo4.Size = new System.Drawing.Size(138, 17);
            this.lInfo4.TabIndex = 3;
            this.lInfo4.Text = "Numer indywidualny:";
            // 
            // lInfo3
            // 
            this.lInfo3.AutoSize = true;
            this.lInfo3.Location = new System.Drawing.Point(23, 84);
            this.lInfo3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lInfo3.Name = "lInfo3";
            this.lInfo3.Size = new System.Drawing.Size(37, 17);
            this.lInfo3.TabIndex = 2;
            this.lInfo3.Text = "Bryt:";
            // 
            // lInfo2
            // 
            this.lInfo2.AutoSize = true;
            this.lInfo2.Location = new System.Drawing.Point(23, 54);
            this.lInfo2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lInfo2.Name = "lInfo2";
            this.lInfo2.Size = new System.Drawing.Size(116, 17);
            this.lInfo2.TabIndex = 1;
            this.lInfo2.Text = "Linia karty pracy:";
            // 
            // lInfo1
            // 
            this.lInfo1.AutoSize = true;
            this.lInfo1.Location = new System.Drawing.Point(23, 25);
            this.lInfo1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lInfo1.Name = "lInfo1";
            this.lInfo1.Size = new System.Drawing.Size(81, 17);
            this.lInfo1.TabIndex = 0;
            this.lInfo1.Text = "Karta pracy";
            // 
            // panel8
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.panel8, 2);
            this.panel8.Controls.Add(this.cbPrintOn);
            this.panel8.Controls.Add(this.cbDbReadEnable);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(4, 397);
            this.panel8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1511, 54);
            this.panel8.TabIndex = 4;
            // 
            // cbPrintOn
            // 
            this.cbPrintOn.AutoSize = true;
            this.cbPrintOn.Location = new System.Drawing.Point(172, 17);
            this.cbPrintOn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPrintOn.Name = "cbPrintOn";
            this.cbPrintOn.Size = new System.Drawing.Size(81, 21);
            this.cbPrintOn.TabIndex = 1;
            this.cbPrintOn.Text = "print On";
            this.cbPrintOn.UseVisualStyleBackColor = true;
            // 
            // cbDbReadEnable
            // 
            this.cbDbReadEnable.AutoSize = true;
            this.cbDbReadEnable.Location = new System.Drawing.Point(16, 17);
            this.cbDbReadEnable.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbDbReadEnable.Name = "cbDbReadEnable";
            this.cbDbReadEnable.Size = new System.Drawing.Size(126, 21);
            this.cbDbReadEnable.TabIndex = 0;
            this.cbDbReadEnable.Text = "db read enable";
            this.cbDbReadEnable.UseVisualStyleBackColor = true;

            // 
            // tConnection
            // 
            this.tConnection.Enabled = true;
            this.tConnection.Interval = 3000;
            this.tConnection.Tick += new System.EventHandler(this.tConnection_Tick);
            // 
            // tDbRefresh
            // 
            this.tDbRefresh.Interval = 3000;
            this.tDbRefresh.Tick += new System.EventHandler(this.tDbRefresh_Tick);
            // 
            // tWaitPrintEnd
            // 
            //this.tWaitPrintEnd.Tick += new System.EventHandler(this.tWaitPrintEnd_Tick);
            // 
            // tPlcRead
            // 
            this.tPlcRead.Enabled = true;
            this.tPlcRead.Interval = 500;
            this.tPlcRead.Tick += new System.EventHandler(this.tPlcRead_Tick);

            // 
            // tUbdateVisu
            // 
            this.tUbdateVisu.Enabled = true;
            this.tUbdateVisu.Interval = 3000;
            this.tUbdateVisu.Tick += new System.EventHandler(this.tUbdateVisu_Tick);

            // 
            // tUpdateControlVisu
            // 
            this.tUpdateControlVisu.Enabled = true;
            this.tUpdateControlVisu.Tick += new System.EventHandler(this.tUpdateCpntrolVisu_Tick);
            // 
            // tUpdateMachineStatus
            // 
            this.tUpdateMachineStatus.Enabled = true;
            this.tUpdateMachineStatus.Interval = 30000;
            this.tUpdateMachineStatus.Tick += new System.EventHandler(this.tUpdateMachineStatus_Tick);
            // 
            // tPrinterEthReconnect
            // 
            //this.tPrinterEthReconnect.Interval = 600000;
            //this.tPrinterEthReconnect.Tick += new System.EventHandler(this.tPrinterEthReconnect_Tick);
            // 
            // autojetSZKKMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1535, 549);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.menuStripMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "autojetSZKKMainForm";
            this.Text = "AutoJet SZKK";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.autojetSZKKMainForm_FormClosing);
            this.Load += new System.EventHandler(this.autojetSZKKMain_Load);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.tabControlMain.ResumeLayout(false);
            this.tabPageMain.ResumeLayout(false);
            this.tabPageMain.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageData.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainData)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPageSetup.ResumeLayout(false);
            this.tabPageDebug.ResumeLayout(false);
            this.tableLayoutPanelDebug.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPageControl.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvControl)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageMain;
        private System.Windows.Forms.TabPage tabPageSetup;
        private System.Windows.Forms.TabPage tabPageDebug;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDebug;
        private System.Windows.Forms.RichTextBox debugTestBox;
        public System.IO.Ports.SerialPort dominoPrinterSerialPort1;
        private System.Windows.Forms.Button bDominoSendLabel;
        private System.Windows.Forms.Button bDominoInit;
        private System.Windows.Forms.TextBox tbDominoOut;
        private System.Windows.Forms.Button bDominoSwGo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPageData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button bSqlSelectAll;
        private System.Windows.Forms.DataGridView dgvMainData;
        private System.Windows.Forms.Label lTimeSpan;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lPLCStatus;
        private System.Windows.Forms.Button bSiemensConnect;
        private System.Windows.Forms.Button bPLCRead;
        private System.Windows.Forms.Label lVal3;
        private System.Windows.Forms.Label lVal2;
        private System.Windows.Forms.Label lVal1;
        private System.Windows.Forms.Label lVal0;
        private System.Windows.Forms.TextBox tbVal0;
        private System.Windows.Forms.Button bPLCWrite;
        private System.Windows.Forms.TextBox tbVal1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label serialLabel;
        private System.Windows.Forms.Label brytLabel;
        private System.Windows.Forms.Label lkpracyLabel;
        private System.Windows.Forms.Label kpracyLabel;
        private System.Windows.Forms.Button bSelActData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbVal10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbVal9;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbVal8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbVal7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbVal6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbVal5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbVal4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbVal3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbVal2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbVal11;
        private System.Windows.Forms.TextBox tbLabelCount;
        private System.Windows.Forms.Button bDominoRepeat;
        private System.Windows.Forms.TabPage tabPageControl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button bStartAutomat;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dgvControl;
        private System.Windows.Forms.ToolStripStatusLabel statusStripeDb;
        private System.Windows.Forms.ToolStripStatusLabel statusStripePLC;
        private System.Windows.Forms.ToolStripStatusLabel statusStripePrinter2;
        private System.Windows.Forms.Timer tConnection;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox tbLabelRepeatPitch;
        private System.Windows.Forms.Label tbLabelRepeatPitchUnit;
        private System.Windows.Forms.Label tbLabel1RepeatPitch;
        private System.Windows.Forms.TextBox tbLabelLenghtMm;
        private System.Windows.Forms.Label tbLabelLenghtUnit;
        private System.Windows.Forms.Label llabelLenghtMm;
        private System.Windows.Forms.Button bControlDataUpdate;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label tbLabel2RepeatPitch;
        private System.Windows.Forms.Button bControlSettingsSave;
        private System.Windows.Forms.CheckBox cbDbAutoUpade;
        private System.Windows.Forms.Button bSendNLabels;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbTileYmm;
        private System.Windows.Forms.Button bPingTest;
        public System.Windows.Forms.Label lPingRes;
        private System.Windows.Forms.Timer tDbRefresh;
        private System.Windows.Forms.Timer tWaitPrintEnd;
        private System.Windows.Forms.Label lWorkAutomat;
        private System.Windows.Forms.Timer tPlcRead;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.CheckBox cbPrintOn;
        private System.Windows.Forms.CheckBox cbDbReadEnable;
        private System.Windows.Forms.Timer tUbdateVisu;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lActPrintNum;
        public System.IO.Ports.SerialPort dominoPrinterSerialPort2;
        private System.Windows.Forms.ToolStripMenuItem ustawieniaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ustawieniaDrukuToolStripMenuItem;
        private System.Windows.Forms.Label lInfo3;
        private System.Windows.Forms.Label lInfo2;
        private System.Windows.Forms.Label lInfo1;
        private System.Windows.Forms.Label lInfo4;
        private System.Windows.Forms.Label lInfoVal4;
        private System.Windows.Forms.Label lInfoVal3;
        private System.Windows.Forms.Label lInfoVal2;
        private System.Windows.Forms.Label lInfoVal1;
        private System.Windows.Forms.Timer tUpdateControlVisu;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
        private System.Windows.Forms.Label lError2;
        private System.Windows.Forms.Label lError1;
        private System.Windows.Forms.Button bErrorAck;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bCheckConnection;
        private System.Windows.Forms.Button bTestF1;
        private System.Windows.Forms.Button bTestF2;
        private System.Windows.Forms.RichTextBox rtbConnectionInfo;
        private System.Windows.Forms.TextBox tbIntervalTest;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button bTcpClose;
        private System.Windows.Forms.Button bOpenTcpTst;
        private System.Windows.Forms.Button bTcpConnected;
        private System.Windows.Forms.CheckBox tcpConnected;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Timer tUpdateMachineStatus;
        private System.Windows.Forms.Timer tPrinterEthReconnect;
        private System.Windows.Forms.ToolStripStatusLabel statusStripeTimer;
    }
}

