﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using FormUtils;

namespace AutojetSZKK
{
   /// <summary>
   /// 
   /// </summary>
   /// <remarks>Before application starts you may need to specify the parent window explicitly to prevent 
   ///          the message boxes from showing in taskbar with an empty icon.</remarks>
   public class HealthMonitor
   {
      private enum hmType
      {
         Info,
         Warning,
         Error,
         Fatal,
      };

      public bool SaveToFile;

      public static bool OnUiThread()
      {
          return (Program.autojetSZKKMain.InvokeRequired == false);
      }

      public HealthMonitor(RichTextBox destination)
      {
         this.logger = new SimpleTextLogger(destination, null);
      }

      public HealthMonitor(RichTextBox destination, string fileName, bool saveToFile=false)
      {
          this.SaveToFile = saveToFile;

          if (destination != null && fileName != null)
          {
              this.logger = new SimpleTextLogger(destination, fileName);
          }
          else
          {
              this.logger = null;
          }
      }

      public HealthMonitor(IWin32Window parentWindow, RichTextBox destination, string fileName)
      {
         if (destination != null && fileName != null)
         {
            this.logger = new SimpleTextLogger(destination, fileName);
         }
         else
         {
            this.logger = null;
         }

         this.parentWindow = parentWindow;
      }

      private string log(hmType type, string text)
      {
         if (this.logger == null)
         {
            return (null);
         }

         StackTrace stackTrace = new StackTrace();
         string methodName = stackTrace.GetFrame(2).GetMethod().ReflectedType.Name + "." + stackTrace.GetFrame(2).GetMethod().Name;
         Color color;

         switch (type)
         {
            case hmType.Info:
               color = Color.DarkGreen;
            break;
            case hmType.Warning:
               color = Color.Orange;
            break;
            case hmType.Error:
               color = Color.Red;
            break;
            case hmType.Fatal:
               color = Color.Red;
            break;
            default:
               color = Color.Black;
            break;
         }

         string buf = string.Empty;
		/*
         if (includeThreadId == true)
         {
            buf += string.Format("0x{0:X4}: ", Thread.CurrentThread.ManagedThreadId);
         }

         if (includeMethodName == true)
         {
            buf += string.Format("{0}: ", methodName);
         }
		  */
         buf += text;
         this.logger.LogLine(buf, color);

         return (buf);
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="text"></param>
      public void Info(string text)
      {
         log(hmType.Info, text);
      }

      public void Info(string format, params object[] args)
      {
         string buf = string.Format(format, args);
         log(hmType.Info, buf);
      }

      public void Warning(string text)
      {
         log(hmType.Warning, text);
      }

      public void Warning(string format, params object[] args)
      {
         string buf = string.Format(format, args);
         log(hmType.Warning, buf);
      }

      public void Error(string text)
      {
         log(hmType.Error, text);
      }

      public void Error(string format, params object[] args)
      {
         string buf = string.Format(format, args);
         log(hmType.Error, buf);
      }

      /// <summary>
      /// Raise fatal error event.
      /// </summary>
      /// <param name="text">The error message.</param>
      /// <remarks>This method terminates the program with an error code of -1.</remarks>
      public void Fatal(string text)
      {
         string completeMsg = log(hmType.Fatal, text);
         MessageBox.Show(
            string.Format(
               "Fatal error occurred: '{0}'\n\n" +
               "Program will be closed.\n" +
               "Please contact to aplication producer to solve it.", 
               text
               ),
            "Health monitor message",
            MessageBoxButtons.OK, 
            MessageBoxIcon.Error
            );
		  /*
         //MAPI mapi = new MAPI();
         System.OperatingSystem osInfo = System.Environment.OSVersion;

         mapi.AddRecipientTo("biuro@viresco.pl");
         mapi.SendMailPopup("ProPlate fatal error", 
            "Please describe the problem here:\n\n\n" +
            "[Do not modify]\n" +
			"Program version: " + autojatSZKKMainForm.GetProgramVersion() + "\n" +
            "System version:  " + osInfo.ToString() + "\n" +
            "64-bit system:   " + System.Environment.Is64BitOperatingSystem.ToString() + "\n" +
            "CLR version:     " + System.Environment.Version.ToString() + "\n" +
            "Error message:   " + completeMsg);
		  */
         Environment.Exit(-1);
      }

      /// <summary>
      /// Show info dialog.
      /// </summary>
      /// <param name="message">The message to show. Could be translation identifier.</param>
      /// <param name="title">The title. Could be a translation identifier.</param>
      /// <remarks>Does nothing if executed from non-UI thread.</remarks>
      public static void InfoDlg(string message, string title = "dlg_information", params object[] args)
      {
         if (OnUiThread())
         {
			string tranTitle = "Info";			//Program.Translation.Get(title);
			string tranMessage = message;		//Program.Translation.Get(message);
			string formatted = string.Format(tranMessage, args);

			MessageBox.Show(
				formatted,
				tranTitle,
				MessageBoxButtons.OK,
				MessageBoxIcon.Information
				);
         }
      }

      /// <summary>
      /// Show info dialog.
      /// </summary>
      /// <param name="parent">The parent window.</param>
      /// <param name="message">The message to show. Could be translation identifier.</param>
      /// <param name="title">The title. Could be a translation identifier.</param>
      /// <remarks>Does nothing if executed from non-UI thread.</remarks>
      public static void InfoDlg(IWin32Window parent, string message, string title = "dlg_information", params object[] args)
      {
         if (OnUiThread())
         {
			string tranTitle = "Info";			//Program.Translation.Get(title);
			string tranMessage = message;		//Program.Translation.Get(message);
			string formatted = string.Format(tranMessage, args);

            MessageBox.Show(
               parent,
               formatted,
               tranTitle,
               MessageBoxButtons.OK,
               MessageBoxIcon.Information
            );
         }
      }

      /// <summary>
      /// Show warning dialog.
      /// </summary>
      /// <param name="message">The message to show. Could be translation identifier.</param>
      /// <param name="title">The title. Could be a translation identifier.</param>
      /// <remarks>Does nothing if executed from non-UI thread.</remarks>
      public static void WargningDlg(string message, string title = "dlg_warning")
      {
         if (OnUiThread())
         {
			string tranTitle = "";			//Program.Translation.Get(title);
			string tranMessage = message;	//Program.Translation.Get(message);

            MessageBox.Show(
               tranMessage,
               tranTitle,
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning
            );
         }
      }

      /// <summary>
      /// Show warning dialog.
      /// </summary>
      /// <param name="parent">The parent window.</param>
      /// <param name="message">The message to show. Could be translation identifier.</param>
      /// <param name="title">The title. Could be a translation identifier.</param>
      /// <remarks>Does nothing if executed from non-UI thread.</remarks>
      public static void WargningDlg(IWin32Window parent, string message, string title = "dlg_warning")
      {
         if (OnUiThread())
         {
			string tranTitle = "Warning";			//Program.Translation.Get(title);
			string tranMessage = message;	//Program.Translation.Get(message);

            MessageBox.Show(
               parent,
               tranMessage,
               tranTitle,
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning
            );
         }
      }

      /// <summary>
      /// Show error dialog.
      /// </summary>
      /// <param name="message">The message to show. Could be translation identifier.</param>
      /// <param name="title">The title. Could be a translation identifier.</param>
      /// <remarks>Does nothing if executed from non-UI thread.</remarks>
      public static void ErrorDlg(string message, string title = "dlg_error")
      {
         if (OnUiThread())
         {
			string tranTitle = "Error"; //Program.Translation.Get(title);
			string tranMessage = message; //Program.Translation.Get(message);

            MessageBox.Show(
               tranMessage,
               tranTitle,
               MessageBoxButtons.OK,
               MessageBoxIcon.Error
            );
         }
      }


      private string newline = System.Environment.NewLine;
      private SimpleTextLogger logger;
      private bool includeMethodName = true;
      private bool includeThreadId = true;
      private IWin32Window parentWindow = null;
   }
}
