﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutojetSZKK
{
    static class Program
    {
        public static autojetSZKKMainForm autojetSZKKMain;
        public static FileTree Ft;
        public static Settings Settings;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            autojetSZKKMain = new autojetSZKKMainForm();

            //
            Program.Ft = new FileTree();
            Program.Settings = new AutojetSZKK.Settings(Program.Ft.Get(FileTree.ID.FT_DEFAULT_SETTINGS));
            //

            Application.Run(autojetSZKKMain);
        }
    }
}
