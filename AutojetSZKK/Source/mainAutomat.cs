﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.Text;

namespace AutojetSZKK
{
    public partial class autojetSZKKMainForm
    {
        public bool sheetPrinted = false;
        private bool emptyLabelSent = false;

        public void workAutomatOnOff()
        {
            //if (!workAutomat)
            ///{
            //załączam automat
            workAutomat = true;
            lWorkAutomat.Text = "Praca: automat";
            bStartAutomat.Text = "Stop";

            //
            tDbRefresh.Interval = Program.Settings.Data.dbScanTime;
            tDbRefresh.Enabled = true;

            //
            //byte[] toSend = Program.autojetSZKKMain.domino.printerInit();
            //Program.autojetSZKKMain.dominoBlackOutBuffor2.Add(toSend);

            //actPrintNum = 0;
            //}
            /*else
            {
                //wyłączam automat
                workAutomat = false;
                lWorkAutomat.Text = "Praca: zatrzymana";
                bStartAutomat.Text = "Start";

                //
                tDbRefresh.Enabled = false;

                //
                labelDataList.Clear();
                dtlabelData = null;
                dgvControl.DataSource = null;
            }*/
        }

        List<labelData> labelDataListNew = new List<labelData>();

        /// <summary>
        /// 
        /// </summary>
        public void workAutomatDbRefresch()
        {
            /*
            jdnr - numer karty pracy
            jdline - numer linii pracy
            tiles - numer  brytu w formacie RxxCyy, przed drukiem przekonwertować do xxyy
            machine - drukarka, TJ1 - 1, TJ2 - 2, TJ3 - 3
            tilecounter - indywidualny numer
            
            SELECT * FROM Printtemp
            WHERE isprinted=0 and machine='TJ1'
            ORDER BY sheetcounter, sheetpos DESC
            */
            //tRestartPrinter.Enabled = false;
            //tRestartPrinter.enable;

            //jezeli nie 

            if (plcPrintOn)
            {
                Program.autojetSZKKMain.Hm.Info("DB Refresh plcPrintON");
                //druk sie rozpoczął - nie sprawdzam bazy
                //tDbRefresh.Enabled = false;
                sheetPrinted = true;
                updateDbData();

                return;
            }

            if (sheetPrinted == true)
            {
                Program.autojetSZKKMain.Hm.Info("DB Refresh Ask for Restart");
                sheetPrinted = false;
                Program.autojetSZKKMain.pCounter = 0;
                //tDbRefresh.Enabled= false;
                //Restart Printer

                dominoQueueThread.hardRestartAllowed = true;//było false; 2018-12-04 
            }


            if (SQLConnection.IsActive() && !plcPrintOn)
            {
                //comperin two lists
                //Enumerable.SequenceEqual(list1.OrderBy(t => t), list2.OrderBy(t => t))
                //http://stackoverflow.com/questions/3669970/compare-two-listt-objects-for-equality-ignoring-order

                //                      Id, filename,   quename,    tiles,  jdnr,       jdline, tilecounter,    sheetpos,   format, fotobasign, printer
                //Polskie tłumaczenia   Id, Plik,       Kolejka,    Bryt,   Nr pracy,   Linia,  TC,             SP,         Format, Fotoba,     Drukarka
                //string queryBox = string.Format("SELECT id, filename, quename, tiles, jdnr, jdline, tilecounter, realymmstart, realymmend, realxmm, sheetpos, format, fotobasign, printer FROM Printtemp WHERE isprinted=0 and machine='{0}' ORDER BY sheetcounter, sheetpos ASC", Program.Settings.Data.machineNo);
                string queryBox = string.Format("SELECT id, filename, quename, tiles, jdnr, jdline, tilecounter, realymmstart, realymmend, realxmm, sheetpos, format, fotobasign, printer FROM Printtemp WHERE bcodeprinted is null and isprinted=0 and machine='{0}' ORDER BY sheetcounter, sheetpos ASC", Program.Settings.Data.machineNo);
                DataTable dtActData = SQLConnection.GetData(queryBox);

                Program.autojetSZKKMain.Hm.Info("DB Refresh Query Executed");
                if (dtActData.Rows.Count > 0)
                {
                    emptyLabelSent = false;

                    labelDataListNew.Clear();

                    foreach (DataRow dRow in dtActData.Rows)
                    {
                        int kpracy = int.Parse(dRow["jdnr"].ToString());
                        int lkpracy = int.Parse(dRow["jdline"].ToString());
                        string brytStr = dRow["tiles"].ToString();
                        int serial = int.Parse(dRow["tilecounter"].ToString());
                        int brytStartPos = (int)Convert.ToDouble(dRow["realymmstart"].ToString());
                        int brytEndPos = (int)Convert.ToDouble(dRow["realymmend"].ToString());
                        int brytPrintPos = (int)Convert.ToDouble(dRow["realxmm"].ToString());
                        int printerNum = int.Parse(dRow["printer"].ToString());

                        int id = int.Parse(dRow["id"].ToString());

                        //zmiana od versji 1.0.1.0 
                        //string brytStr2 = brytStr.Substring(1, 2) + brytStr.Substring(4, 2);
                        string brytStr2 = brytStr.Substring(0, 4);

                        labelData actLab = new labelData(kpracy, lkpracy, brytStr2, serial, brytStartPos, brytEndPos, brytPrintPos, printerNum, id);
                        labelDataListNew.Add(actLab);
                    }

                    bool equal = true;

                    try
                    {
                        if (labelDataListNew.Count != labelDataList.Count)
                            equal = false;
                        else if ((labelDataListNew.Count > 0) && (labelDataList.Count > 0))
                        {
                            for (int i = 0; i < labelDataListNew.Count; i++)
                            {
                                if (!labelDataListNew[i].Equals(labelDataList[i]))
                                    equal = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.autojetSZKKMain.Hm.Info($"Error:3 {0}", ex.ToString());
                    }

                    if (!equal) //Dane sie zmienily wiec wyczysc kolejke i wyslij nowa etykiete
                    {
                        dominoQueueThread.printerRestartTimer.Enabled = false;
                        dominoQueueThread.printerRestartTimer.Enabled = true;

                        Program.autojetSZKKMain.Hm.Info("DB Data has changed");
                        actPrintNum = 0;

                        labelDataList = new List<labelData>(labelDataListNew);
                        dtlabelData = dtActData;
                        dgvControl.DataSource = dtActData;

                        updateDatagridViev();

                        try
                        {
                            // czyszczenie kolejki w aplikacji

                            Program.autojetSZKKMain.Hm.Info("DATA HAS CHANGED");
                            byte[] temp;
                            while (Program.autojetSZKKMain.printerQueue.Count > 0)
                            {
                                Program.autojetSZKKMain.printerQueue.TryTake(out temp);
                            }

                            Program.autojetSZKKMain.Hm.Info("Add to Queue clear code");
                            Program.autojetSZKKMain.printerQueue.Add(Encoding.ASCII.GetBytes("\x1B" + "OE00000" + "\x04"));

                            //czyszczenie kolejki w drukarce, nie trzeba bo przeciez nowa etykieta ja zastapi
                            //Program.autojetSZKKMain.Hm.Info("CLEAR CODE SENT TO QUEUE");
                            //Program.autojetSZKKMain.printerQueue.Add(Encoding.ASCII.GetBytes("\x1B" + "OE00000" + "\x04"));

                            if (labelDataList.Count > 0)
                            {
                                //wysylanie nowego kodu do kolejki (zawsze z actPrintNum==0) i powinienem tylko jeden a dopiero jak P dostanę z drukarki to kolejny
                                Program.autojetSZKKMain.Hm.Info("Db Refresh: actPrintNum={0}, labelDataList.Count={1}, Label sent to Queue...", actPrintNum, labelDataList.Count);
                                printerSendLabel("workAutomatDbRefresch");
                                plcSend();
                            }
                        }
                        catch (Exception ex)
                        {
                            Program.autojetSZKKMain.Hm.Info("Error: in Queue Change Clear {0}", ex.ToString());
                        }
                        //restart timer
                        //tPrinterEthReconnect.Stop();
                        //tPrinterEthReconnect.Start();
                        //addConnectionInfoMessage("tPrinterEthReconnect reset");
                    }
                    else //Te same dane wiec nic nie rob
                    {
                        Program.autojetSZKKMain.Hm.Info("DATA is not CHANGED");

                        /*   try
                           {
                               if (labelDataList.Count > 0)
                               {
                                   //wysylanie kodu do kolejki (zawsze z actPrintNum==0) i powinienem tylko jeden a dopiero jak P dostanę z drukarki to kolejny
                                   Program.autojetSZKKMain.Hm.Info("workAutomatDbRefresch(): actPrintNum={0}, labelDataList.Count={1}, Repeat Label sent to Queue...", actPrintNum, labelDataList.Count);
                                   printerSendLabel("workAutomatDbRefresch");
                               }
                           }
                           catch (Exception ex)
                           {
                               Program.autojetSZKKMain.Hm.Info("workAutomatDbRefresch(): actPrintNum={0}, labelDataList.Count={1}, Error: in Repeat Label sent to Queue... {2}", actPrintNum, labelDataList.Count,ex.ToString());
                           }*/
                    }
                }
                else if (emptyLabelSent == false) //No data to send so send Empty Label Oncexxxx
                {
                    emptyLabelSent = true;

                    Program.autojetSZKKMain.Hm.Info("No DATA to send, EMPTY LABEL");
                    try
                    {
                        labelDataList.Clear();
                        dtlabelData = null;
                        dgvControl.DataSource = null;

                        Program.autojetSZKKMain.Hm.Info("Add to Queue clear code");
                        Program.autojetSZKKMain.printerQueue.Add(Encoding.ASCII.GetBytes("\x1B" + "OE00000" + "\x04"));

                        //byte[] tileRepeatCountToSend = Program.autojetSZKKMain.domino.setRepeat(1, Program.Settings.Data.labelRepeatPitch);
                        byte[] labelEmpty = Program.autojetSZKKMain.domino.printerEmptyLabel();

                        //Program.autojetSZKKMain.printerQueue.Add(tileRepeatCountToSend);
                        Program.autojetSZKKMain.printerQueue.Add(labelEmpty);
                        // Program.autojetSZKKMain.Hm.Info("workAutomatDbRefresch(): actPrintNum={0}, labelDataList.Count={1}, Printer2: Repeat Empty Label Sent to QUEUE", actPrintNum, labelDataList.Count);
                        plcSend();
                    }
                    catch (Exception ex)
                    {
                        Program.autojetSZKKMain.Hm.Info("Error: workAutomatDbRefresch(): actPrintNum={0}, labelDataList.Count={1}, Printer2: Error: in Repeat Empty Label Sent to QUEUE {2}", actPrintNum, labelDataList.Count, ex.ToString());
                    }
                }
            }


        }

        public void updateDatagridViev()
        {
            //Id,filename,quename,tiles,jdnr,jdline,tilecounter,sheetpos,format,fotobasign,printer
            //Polskie tłumaczenia Id,Plik,Kolejka,Bryt,Nr pracy,Linia,TC,SP,Format,Fotoba,Drukarka

            dgvControl.Columns["id"].HeaderText = "Id";
            dgvControl.Columns["filename"].HeaderText = "Plik";
            dgvControl.Columns["quename"].HeaderText = "Kolejka";
            dgvControl.Columns["tiles"].HeaderText = "Bryt";
            dgvControl.Columns["jdnr"].HeaderText = "Nr pracy";
            dgvControl.Columns["jdline"].HeaderText = "Linia";
            dgvControl.Columns["tilecounter"].HeaderText = "TC";
            dgvControl.Columns["sheetpos"].HeaderText = "SP";
            dgvControl.Columns["format"].HeaderText = "Format";
            dgvControl.Columns["fotobasign"].HeaderText = "Fotoba";
            dgvControl.Columns["printer"].HeaderText = "Drukarka";

            dgvControl.Columns["id"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["filename"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dgvControl.Columns["quename"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["tiles"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["jdnr"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["jdline"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["tilecounter"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["sheetpos"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["format"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["fotobasign"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["printer"].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;

            dgvControl.Columns["quename"].Width = 200;

            dgvControl.Columns["id"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["filename"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dgvControl.Columns["quename"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["tiles"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["jdnr"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["jdline"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["tilecounter"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["sheetpos"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["format"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["fotobasign"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dgvControl.Columns["printer"].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;

            dgvControl.Columns["filename"].AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;

            //            realymmstart, realymmend, realxmm
            dgvControl.Columns["realymmstart"].Visible = false;
            dgvControl.Columns["realymmend"].Visible = false;
            dgvControl.Columns["realxmm"].Visible = false;
        }

        public void printerSendLabel(string sendLabelMetodName)
        {
            //sprawdzenie czy dane są poprawne czy actPrintNum nie przekracza wartości listy bo już zakończyłem kafle robić
            if (actPrintNum >= labelDataList.Count)
            {
                Program.autojetSZKKMain.Hm.Info("No more labels to send: actPrintNum={0}, labelDataList.Count={1}", actPrintNum, labelDataList.Count);
                return;
            }

            Program.autojetSZKKMain.Hm.Info("printerSendLabel() SENDING TO QUEUE START: actPrintNum={0}, labelDataList.Count={1}", actPrintNum, labelDataList.Count);
            //byte[] tileRepeatCountToSend;
            byte[] labelToSend;

            //Program.autojetSZKKMain.Hm.Info("printerSendLabel(): actPrintNum OK");

            //wyliczenie ilości powtórzeń kodu
            int tileYmm = 100; ;

            try
            {
                tileYmm = labelDataList[actPrintNum].brytEndPos - labelDataList[actPrintNum].brytStartPos;

            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info($"Error:1 {0}", ex.ToString());
            }

            //int tileRepeatCount = (tileYmm / Program.Settings.Data.labelLenghtMm);
            //tileRepeatCountToSend = Program.autojetSZKKMain.domino.setRepeat(tileRepeatCount, Program.Settings.Data.labelRepeatPitch);
            //Program.autojetSZKKMain.Hm.Info($"printerSendLabel(): tileRepeatCount = {tileRepeatCount.ToString()}");

            //generowanie etykiety do druku
            labelToSend = null;

            try
            {
                if (Program.Settings.Data.barcodeOnlyId)
                {
                    labelToSend = Program.autojetSZKKMain.domino.printerAutojetPDDoubleLineleLabel2of5Id(labelDataList[actPrintNum].kpracy.ToString("D6"), labelDataList[actPrintNum].lkpracy.ToString("D3"), labelDataList[actPrintNum].bryt, Program.Settings.Data.machinePrintNo.Substring(0, 1), labelDataList[actPrintNum].serial.ToString("D3"), labelDataList[actPrintNum].id, Program.Settings.Data.barcodeOnlyIdMode);
                }
                else if (!Program.Settings.Data.barcode128)
                {
                    if (Program.Settings.Data.barcodeShort)
                    {
                        //etykieta krótka
                        //labelToSend = Program.autojetSZKKMain.domino.printerAutojetPDDoubleLineleLabelShort(labelDataList[actPrintNum].kpracy.ToString("D6"), labelDataList[actPrintNum].lkpracy.ToString("D3"), labelDataList[actPrintNum].bryt, Program.Settings.Data.machinePrintNo.Substring(0, 1), labelDataList[actPrintNum].serial.ToString("D3"));
                        labelToSend = Program.autojetSZKKMain.domino.printerAutojetPDDoubleLineleLabelShortRand(labelDataList[actPrintNum].kpracy.ToString("D6"), labelDataList[actPrintNum].lkpracy.ToString("D3"), labelDataList[actPrintNum].bryt, Program.Settings.Data.machinePrintNo.Substring(0, 1), labelDataList[actPrintNum].serial.ToString("D3"));
                    }
                    else
                    {
                        //etykieta długa
                        labelToSend = Program.autojetSZKKMain.domino.printerAutojetPDDoubleLineleLabel2(labelDataList[actPrintNum].kpracy.ToString("D6"), labelDataList[actPrintNum].lkpracy.ToString("D3"), labelDataList[actPrintNum].bryt, Program.Settings.Data.machinePrintNo.Substring(0, 1), labelDataList[actPrintNum].serial.ToString("D3"));
                    }
                }
                else
                {
                    //wersja z kodem kreskowym typu Code 128
                    if (Program.Settings.Data.barcodeShort)
                    {
                        //etykieta krótka
                        labelToSend = Program.autojetSZKKMain.domino.printerAutojetPDDoubleLineleLabel128Short(labelDataList[actPrintNum].kpracy.ToString("D6"), labelDataList[actPrintNum].lkpracy.ToString("D3"), labelDataList[actPrintNum].bryt, Program.Settings.Data.machinePrintNo.Substring(0, 1), labelDataList[actPrintNum].serial.ToString("D3"));
                    }
                    else
                    {
                        //etykieta długa
                        labelToSend = Program.autojetSZKKMain.domino.printerAutojetPDDoubleLineleLabel128(labelDataList[actPrintNum].kpracy.ToString("D6"), labelDataList[actPrintNum].lkpracy.ToString("D3"), labelDataList[actPrintNum].bryt, Program.Settings.Data.machinePrintNo.Substring(0, 1), labelDataList[actPrintNum].serial.ToString("D3"));
                    }
                }
            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info($"Error:2 {0}", ex.ToString());
            }

            Program.autojetSZKKMain.Hm.Info("printerSendLabel(): LABEL READY");

            //wysylanie do druku jak drukarka 0 w parametrze to wyslij pusty label (kropke)
            try
            {

                if (labelDataList[actPrintNum].printerNum == 0)
                {
                    byte[] labelEmpty = Program.autojetSZKKMain.domino.printerEmptyLabel();

                    Program.autojetSZKKMain.printerQueue.Add(labelEmpty);
                    Program.autojetSZKKMain.Hm.Info("printerSendLabel(): actPrintNum={0}, labelDataList.Count={1}, Printer2: Send empty label", actPrintNum, labelDataList.Count);
                }
                else
                {
                    //dominoPrinter2LastLabelEmpty = false;
                    //Program.autojetSZKKMain.printerQueue.Add(tileRepeatCountToSend);

                    Program.autojetSZKKMain.printerQueue.Add(labelToSend);
                    Program.autojetSZKKMain.Hm.Info("printerSendLabel(): Label sent to QUEUE");
                }
            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info($"Error:4 {0}", ex.ToString());
            }
        }

        public void plcRead()
        {
            if (plcConnected)
            {
                int res, a = 0, b = 0, c = 0;
                int d = 0;

                //res = dc.

                res = dc.readBytes(libnodave.daveFlags, 1, 44, 2, null);
                if (res == 0)
                {
                    a = dc.getS8();
                    b = dc.getS8();
                    plcErrorCode = a;
                    plcPrintOn = Convert.ToBoolean(b);
                }
                else
                {
                    Program.autojetSZKKMain.Hm.Info("plcRead() error: " + libnodave.daveStrerror(res));
                    updateVisuPlcNotConnected();
                }

                //Program.autojetSZKKMain.Hm.Info("plcRead(): plcPrintOn={0}", plcPrintOn);

                plcInAnalize();
            }
        }

        public void plcInAnalize()
        {
            if (/*(workAutomat)&&*/(!plcPrintOn))
            {
                tDbRefresh.Interval = Program.Settings.Data.dbScanTime;
                tDbRefresh.Enabled = true;
            }
        }

        public void updateDbData()
        {
            //update printtemp set isprinted=1 WHERE isprinted=0 and machine='TJ2'

            String listIds = "";

            foreach (labelData ld in labelDataList)
            {
                if (ld.isUpdated.Equals(false))
                {
                    listIds += "," + ld.id.ToString();
                }
            }

            if (!listIds.Equals(""))
            {
                listIds = listIds.Substring(1);
            }

            if (Program.Settings.Data.dbAutoUpade && !listIds.Equals(""))
            {
                string queryBox = string.Format("update Printtemp set bcodeprinted=NOW(),timeprinted=NOW(),isprinted=1,whoconfirmed=2 WHERE isprinted IN(0,1) and machine='{0}' and id IN ({1})", Program.Settings.Data.machineNo, listIds);
                SQLConnection.UpdateData(queryBox);

                Hm.Info("DB UPDATED FOR Printtemp ids '{0}'", listIds);

                foreach (labelData ld in labelDataList)
                {
                    ld.isUpdated = true;
                }
            }
        }

        public void plcSend()
        {
            if (plcConnected)
            {
                try
                {


                    //bryt1
                    if (labelDataList.Count > 0)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[0].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 0, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[0].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 20, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 0, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 20, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt2
                    if (labelDataList.Count > 1)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[1].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 2, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[1].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 22, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 2, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 22, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt3
                    if (labelDataList.Count > 2)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[2].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 4, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[2].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 24, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 4, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 24, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt4
                    if (labelDataList.Count > 3)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[3].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 6, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[3].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 26, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 6, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 26, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt5
                    if (labelDataList.Count > 4)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[4].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 8, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[4].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 28, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 8, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 28, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt6
                    if (labelDataList.Count > 5)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[5].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 10, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[5].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 30, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 10, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 30, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt7
                    if (labelDataList.Count > 6)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[6].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 12, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[6].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 32, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 12, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 32, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt8
                    if (labelDataList.Count > 7)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[7].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 14, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[7].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 34, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 14, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 34, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt9
                    if (labelDataList.Count > 8)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[8].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 16, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[8].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 36, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 16, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 36, 2, BitConverter.GetBytes(out1));
                    }

                    //bryt10
                    if (labelDataList.Count > 9)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[9].brytStartPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 18, 2, BitConverter.GetBytes(out1));

                        int out2 = libnodave.daveSwapIed_16(labelDataList[9].brytEndPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 38, 2, BitConverter.GetBytes(out2));
                    }
                    else
                    {
                        int out1 = 0;
                        dc.writeBytes(libnodave.daveFlags, 1, 18, 2, BitConverter.GetBytes(out1));
                        dc.writeBytes(libnodave.daveFlags, 1, 38, 2, BitConverter.GetBytes(out1));
                    }

                    //pozycja glowicy w osi x
                    if (labelDataList.Count > 0)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[0].brytPrintPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 40, 2, BitConverter.GetBytes(out1));
                    }
                    else
                    {
                        int out1 = libnodave.daveSwapIed_16(0);
                        dc.writeBytes(libnodave.daveFlags, 1, 40, 2, BitConverter.GetBytes(out1));
                    }

                    //2017-05-15 Zmiana: dodanie wysyłania 4 pozycji druku w osi x
                    if (labelDataList.Count > 1)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[1].brytPrintPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 42, 2, BitConverter.GetBytes(out1));
                    }
                    else
                    {
                        int out1 = libnodave.daveSwapIed_16(0);
                        dc.writeBytes(libnodave.daveFlags, 1, 42, 2, BitConverter.GetBytes(out1));
                    }

                    if (labelDataList.Count > 2)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[2].brytPrintPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 46, 2, BitConverter.GetBytes(out1));
                    }
                    else
                    {
                        int out1 = libnodave.daveSwapIed_16(0);
                        dc.writeBytes(libnodave.daveFlags, 1, 46, 2, BitConverter.GetBytes(out1));
                    }

                    if (labelDataList.Count > 3)
                    {
                        int out1 = libnodave.daveSwapIed_16(labelDataList[3].brytPrintPos);
                        dc.writeBytes(libnodave.daveFlags, 1, 48, 2, BitConverter.GetBytes(out1));
                    }
                    else
                    {
                        int out1 = libnodave.daveSwapIed_16(0);
                        dc.writeBytes(libnodave.daveFlags, 1, 48, 2, BitConverter.GetBytes(out1));
                    }
                    //2017-05-15 Zmiana: dodanie wysyłania 4 pozycji druku w osi x

                    //2017-05-15 Zmiana: dodanie wysyłania 4 pozycji druku w osi x, zakomentowano następujące linijki
                    //int out11 = libnodave.daveSwapIed_16(0);
                    //dc.writeBytes(libnodave.daveFlags, 1, 42, 2, BitConverter.GetBytes(out11));
                }
                catch (Exception ex)
                {
                    Program.autojetSZKKMain.Hm.Info($"Error:5 {0}", ex.ToString());
                }
            }
        }


        public void updateControlVisu()
        {
            try
            {

                if ((labelDataList.Count > 0) && (actPrintNum <= labelDataList.Count - 1))
                {
                    lInfoVal1.Text = labelDataList[actPrintNum].kpracy.ToString("D6");
                    lInfoVal2.Text = labelDataList[actPrintNum].lkpracy.ToString("D3");
                    lInfoVal3.Text = labelDataList[actPrintNum].bryt;
                    lInfoVal4.Text = labelDataList[actPrintNum].serial.ToString("D3");
                }
            }
            catch (Exception ex)
            {
                Program.autojetSZKKMain.Hm.Info($"Error:6 {0}", ex.ToString());
            }
        }

        public void updateVisu()
        {
            cbDbReadEnable.Checked = tDbRefresh.Enabled;
            cbPrintOn.Checked = plcPrintOn;
            lActPrintNum.Text = actPrintNum.ToString();

            //statusStripeTimer.Text = tPrinterEthReconnect.t

            //if ((SQLConnection.IsActive()) && (this.dominoPrinterTcp1.Connected()) && (this.dominoPrinterTcp2.Connected()) && (plcConnected) && (!plcPrintOn))
            //if ((SQLConnection.IsActive())  && (plcConnected) && (!plcPrintOn))
            //{
            //    bStartAutomat.Enabled = true;
            //}
            //else
            //{
            //    bStartAutomat.Enabled = false;
            //}
            try
            {
                bool connectionEnable = ((SQLConnection.IsActive()) && (plcConnected));

                workAutomatOnOff();

                if ((connectionEnable) && (!plcPrintOn) && (!workAutomat))
                {
                    //gdy możliwe jest połączenie, nie ma elementu na drukarce a połączenie jest wyłączone
                    workAutomatOnOff(); //załącz komunikację
                }
                else if ((!connectionEnable) && (!plcPrintOn) && (workAutomat))
                {
                    //gdy nie możliwe jest połączenie, nie ma elementu na drukarce a połączenie jest załączone
                    workAutomatOnOff(); //wyłącz komunikację
                }

                //if (connectionEnable != workAutomat)
                //{
                //    Program.autojetSZKKMain.Hm.Info($"updateVisu(), connectionEnable ={connectionEnable.ToString()},workAutomat={workAutomat.ToString()}");
                //    workAutomatOnOff();
                //}

            }
            catch (Exception e)
            {
                tUbdateVisu.Enabled = false;
                MessageBox.Show(e.Message);
            }

        }

        public void addConnectionInfoMessage(string msg)
        {
            Program.autojetSZKKMain.Hm.Info($"addConnectionInfoMessage: {msg}");
            rtbConnectionInfo.Text = rtbConnectionInfo.Text.Insert(0, DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss.ffffff") + ": " + msg + "\n");
        }
    }
}
